{$mode objfpc}
unit vulkan_autobasecls;


interface
uses vulkan_h,vulkan_class,vulkan_typeinit,vulkan_auto;


type
T_vulkan_API_commands_class_plus = class(T_vulkan_auto)
  function version2string(v : dword) : string;
  function version_patch(v : dword) : dword;
  function version_minor(v : dword) : dword;
  function version_major(v : dword) : dword;
  function version(maj,mi,p : dword) : dword;

  const
  vulkan_header_version_major = 1;
  vulkan_header_version_minor = 2;
end;

TPchararr = array of Pchar;

T_vulkan_base_instance = class(T_vulkan_API_commands_class_plus)
  constructor create();
  destructor destroy(); override;

  procedure log(s : string); virtual;

  { create a vulkan instance, making inherited load all the API commands.}
  function init_vk(wantedlayers,wantedext : array of pchar) : boolean;

  protected
  function get_supported_layers(var wanted,missing : TPchararr) : boolean;
  function get_supported_extensions(var layers,wanted,missing : TPchararr) : boolean;

  function really_choose_layer(lp : VkLayerProperties) : boolean; virtual;
  function missing_layer_fatal(p : Pchar) : boolean; virtual;
  function really_choose_ext(lp : VkExtensionProperties) : boolean; virtual;
  function missing_ext_fatal(p : Pchar) : boolean; virtual;


  procedure fetchInstanceVersion();
  
  function onDebugCallback( _mSeverity : VkDebugUtilsMessageSeverityFlagBitsEXT; _mTypes : VkDebugUtilsMessageTypeFlagsEXT; _pcb : PVkDebugUtilsMessengerCallbackDataEXT) : VkBool32; virtual;

  public
  instance : pVkInstance;
  instancevsersion : dword;
  appinfo : pVkApplicationInfo;

  pacb : pVkAllocationCallbacks;

  use_dbg,                   // if set and have_dbg, init_vk calls vkCreateDebugUtilsMessengerEXT 
  use_dbg_initial : boolean; // if set and have_dbg, init_vk links a message-cb-def to VkInstanceCreateInfo.pnext 
  messageSeverity : VkDebugUtilsMessageSeverityFlagBitsEXT;
  messageType : VkDebugUtilsMessageTypeFlagsEXT;


  protected
  maindbgobj : pVkDebugUtilsMessengerEXT;
  have_dbg : boolean; // set if VK_EXT_debug_utils is both, available and requested
  use_devgrp : boolean;

end;


function inttostring(a : int64) : string;
function ptrtostring(a : pointer) : string;



implementation
uses strings;

{$include vulkan_calling.inc}

FUNCTION vulkan_debug_callback(
            _messageSeverity : VkDebugUtilsMessageSeverityFlagBitsEXT; 
            _messageTypes : VkDebugUtilsMessageTypeFlagsEXT; 
            _pCallbackData : PVkDebugUtilsMessengerCallbackDataEXT; 
            _pUserData : pointer 
          ) : VkBool32; vulkan_cb_callingconv;
begin
  vulkan_debug_callback := T_vulkan_base_instance(_pUserData).onDebugCallback(_messageSeverity, _messageTypes, _pCallbackData);
end;




function inttostring(a : int64) : string;
var s : string;
begin
  str(a,s);
  inttostring := s;
end;

function ptrtostring(a : pointer) : string;
begin
  ptrtostring := hexstr(ptrint(a),2*sizeof(pointer));
end; 

procedure init_TPchararr(out a : TPchararr; x : array of pchar);
var i : longint;
begin
  a := default(TPchararr);
  setlength(a,high(x)+1);
  for i := high(x) downto 0 do a[i] := x[i];
end;

procedure remove_nil(var a : TPchararr);
var i,j : longint;
begin
  j := 0;
  for i := 0 to high(a) do begin
    if (a[i]<>nil) then begin
      a[j] := a[i];
      j += 1;
    end;
  end;
  setlength(a,j);
end;




function T_vulkan_API_commands_class_plus.version(maj,mi,p : dword) : dword;
begin
  version := (maj shl 22) or (mi shl 12) or p;
end;

function T_vulkan_API_commands_class_plus.version_major(v : dword) : dword;
begin
  version_major := v shr 22;
end;

function T_vulkan_API_commands_class_plus.version_minor(v : dword) : dword;
begin
  version_minor := (v shr 12) and $3ff;
end;

function T_vulkan_API_commands_class_plus.version_patch(v : dword) : dword;
begin
  version_patch := (v shr 12) and $fff;
end;

function T_vulkan_API_commands_class_plus.version2string(v : dword) : string;
begin
  version2string :=  inttostring(version_major(v))+'.'
                    +inttostring(version_minor(v))+'.'
                    +inttostring(version_patch(v));
end;









constructor T_vulkan_base_instance.create();
begin
  inherited create();
  vknew(instance);
  vknew(appinfo);
  vknew(maindbgobj);
  pacb := nil;
  use_dbg := false;
  use_dbg_initial := false;
  have_dbg := false;
  with appinfo^ do begin
    pApplicationName := 'T_vulkan_base_instance';
    applicationVersion := 0;
    pEngineName := 'fpc';
    {$macro on}
    engineVersion := FPC_FULLVERSION;
    apiVersion := version(vulkan_header_version_major,vulkan_header_version_minor,0);
  end;
  messageSeverity := VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT or
                     VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT    or
                     VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT or
                     VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
  messageType := VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT    or
                 VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT or
                 VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
end;


destructor T_vulkan_base_instance.destroy();
begin
  if (instance^<>VK_NiL_HANDLE) then begin
    if (VK_NULL_HANDLE<>maindbgobj^) then begin
      vkDestroyDebugUtilsMessengerEXT(instance^,maindbgobj^,pacb);
    end;
    vkDestroyInstance(instance^,pacb);
  end;
  vkdispose(maindbgobj);
  vkdispose(instance);
  vkdispose(appinfo);
end;


procedure T_vulkan_base_instance.log(s : string);
begin
  writeln(s);
end;




procedure T_vulkan_base_instance.fetchInstanceVersion();
var iv : pdword;
begin
  new(iv);
  if (nil<>pointer(vkEnumerateInstanceVersion)) then begin
    if VK_SUCCESS=vkEnumerateInstanceVersion(iv) then begin
      instancevsersion := iv^;
      log('vkEnumerateInstanceVersion reported version '+version2string(instancevsersion));
    end else begin
      log('vkEnumerateInstanceVersion failed(!), assume 1.0.x');
      instancevsersion := version(1,0,0);
    end;
  end else begin
    log('vkEnumerateInstanceVersion isn''t available, assume 1.0.x');
    instancevsersion := version(1,0,0);
  end;
  dispose(iv);
end;

function T_vulkan_base_instance.init_vk(wantedlayers,wantedext : array of pchar) : boolean;
var layerswanted,layersmissing,extwanted,extmissing : TPchararr; vkic : pVkInstanceCreateInfo;
dbginfo : pVkDebugUtilsMessengerCreateInfoEXT;
begin
  layerswanted := default(TPchararr);
  layersmissing := default(TPchararr);
  extwanted := default(TPchararr);
  extmissing := default(TPchararr);
  init_vk := false;
  fetchInstanceVersion();
  if instancevsersion<version(1,1,0) then appinfo^.apiVersion := instancevsersion;
  if (vulkan_available) then begin
    init_TPchararr(layerswanted,wantedlayers);
    if get_supported_layers(layerswanted,layersmissing) then begin
      init_TPchararr(extwanted,wantedext);
      if get_supported_extensions(layerswanted,extwanted,extmissing) then begin
        vknew(vkic);
        vknew(dbginfo);
        with vkic^ do begin
          pApplicationInfo := appinfo;
          ppEnabledLayerNames := @layerswanted[0];
          enabledLayerCount := high(layerswanted)+1;
          ppEnabledExtensionNames := @extwanted[0];
          enabledExtensionCount := high(extwanted)+1;
        end;
        if (use_dbg_initial and have_dbg) then begin
          with dbginfo^ do begin
            pointer(pfnUserCallback) := pointer(@vulkan_debug_callback);
            pUserData := self;
          end;
          dbginfo^.messageSeverity := messageSeverity;
          dbginfo^.messageType := messageType;
          vkic^.pNext := dbginfo;
        end;
        if VK_SUCCESS=vkCreateInstance(vkic,pacb,instance) then begin
          load_all_vk(instance^,VK_NiL_HANDLE);
          if (use_dbg and have_dbg) then begin
            dbginfo^ := default_VkDebugUtilsMessengerCreateInfoEXT;
            with dbginfo^ do begin
              pointer(pfnUserCallback) := pointer(@vulkan_debug_callback);
              pUserData := self;
            end;
            dbginfo^.messageSeverity := messageSeverity;
            dbginfo^.messageType := messageType;
            if VK_SUCCESS=vkCreateDebugUtilsMessengerEXT(instance^,dbginfo,pacb,maindbgobj) then begin
              log('ok vkCreateDebugUtilsMessengerEXT');
            end else begin
              log('fail vkCreateDebugUtilsMessengerEXT');
            end;          
          end;
          init_vk := true;
        end;
        vkdispose(vkic);
        vkdispose(dbginfo);
      end;
    end;    
  end;
end;


function T_vulkan_base_instance.really_choose_layer(lp : VkLayerProperties) : boolean;
begin
  really_choose_layer := true;
end;

function T_vulkan_base_instance.missing_layer_fatal(p : Pchar) : boolean;
begin
  missing_layer_fatal := false;
end;

type
Tlayerarr = array of VkLayerProperties;

function T_vulkan_base_instance.get_supported_layers(var wanted,missing : TPchararr) : boolean;
var cnt : pdword; i,j : dword; ap : Tlayerarr; found,canrettrue : boolean;
begin
  ap := default(Tlayerarr);
  vknew(cnt);
  canrettrue := true;
  cnt^ := 0;  
  if VK_SUCCESS=vkEnumerateInstanceLayerProperties(cnt,nil) then begin
    log('vulkan has '+inttostring(cnt^)+' layers here:');
    setlength(ap,cnt^); 
    if VK_SUCCESS=vkEnumerateInstanceLayerProperties( cnt,@ap[0]) then begin
      if (cnt^>0) then begin
        for i := 0 to cnt^-1 do begin
	  log('avail layer : '+pchar(@ap[i].layerName));
        end;
      end;
      setlength(missing,high(wanted)+1);
      if (high(wanted)>=0) then begin
	for j := 0 to high(wanted) do begin
	  missing[j] := nil;
	  found := false;
	  if (cnt^>0) then begin
	    for i := 0 to cnt^-1 do begin
	      if 0=strcomp(wanted[j],pchar(@ap[i].layerName)) then begin
	        found := really_choose_layer(ap[i]);
	      end;
            end;
	  end;
	  if not(found) then begin
	    missing[j] := wanted[j];
	    wanted[j] := nil;
	    canrettrue := canrettrue and not(missing_layer_fatal(missing[j]));
	    log('missed layer: '+missing[j]); 
	  end else log('chosen layer: '+wanted[j]); 
	end;
	remove_nil(wanted);
        remove_nil(missing);
      end;
    end else begin
      canrettrue := false;
      log('failed with 2nd vkEnumerateInstanceLayerProperties');
    end;
  end else begin
    log('failed with vkEnumerateInstanceLayerProperties');
    canrettrue := false;
  end;
  vkdispose(cnt);
  get_supported_layers := canrettrue;
end;


function T_vulkan_base_instance.really_choose_ext(lp : VkExtensionProperties) : boolean;
begin
  if 0=strcomp(lp.extensionName,'VK_EXT_debug_utils') then have_dbg := true;
  if 0=strcomp(lp.extensionName,'VK_KHR_device_group_creation') then use_devgrp := true;
  really_choose_ext := true;
end;

function T_vulkan_base_instance.missing_ext_fatal(p : Pchar) : boolean; 
begin
  missing_ext_fatal := false;
end;

type Tvkextarr = array of VkExtensionProperties;
function T_vulkan_base_instance.get_supported_extensions(var layers,wanted,missing : TPchararr) : boolean;
var i,j : dword; cnt,cnt2 : Pdword; ap : Tvkextarr; found,canrettrue : boolean;
begin
  ap := default(Tvkextarr);
  canrettrue := true;
  vknew(cnt);
  vknew(cnt2);
  cnt^ := 0;  
  if VK_SUCCESS=vkEnumerateInstanceExtensionProperties(nil,cnt,nil) then begin
    log('vulkan instance has '+inttostring(cnt^)+' extensions here.');
    setlength(ap,cnt^); 
    if VK_SUCCESS=vkEnumerateInstanceExtensionProperties(nil,cnt,@ap[0]) then begin
      i := 0;
      while (i<=high(layers)) do begin
        if VK_SUCCESS=vkEnumerateInstanceExtensionProperties(layers[i], cnt2,nil) then begin
          setlength(ap,cnt^+cnt2^);
          if VK_SUCCESS=vkEnumerateInstanceExtensionProperties(layers[i], cnt2,@ap[cnt^]) then begin
            log('layer '+layers[i]+' adds '+inttostring(cnt2^)+' extensions.');
            setlength(ap,cnt^+cnt2^);
            cnt^ += cnt2^;
          end;
        end;
        i+=1;
      end;
      if (cnt^>0) then begin
        for i := 0 to cnt^-1 do begin
	  log('avail ext : '+pchar(@ap[i].extensionName));
        end;
      end;
      setlength(missing,high(wanted)+1);
      if (high(wanted)>=0) then begin
	for j := 0 to high(wanted) do begin
	  missing[j] := nil;
	  found := false;
	  if (cnt^>0) then begin
	    for i := 0 to cnt^-1 do begin
	      if 0=strcomp(wanted[j],pchar(@ap[i].extensionName)) then begin
	        found := really_choose_ext(ap[i]);
	      end;
            end;
	  end;
	  if not(found) then begin
	    missing[j] := wanted[j];
	    wanted[j] := nil;
	    canrettrue := canrettrue and not(missing_ext_fatal(missing[j]));
	    log('missed ext: '+missing[j]); 
	  end else log('chosen ext: '+wanted[j]); 
	end;
	remove_nil(wanted);
        remove_nil(missing);
      end;
    end else begin
      canrettrue := false;
      log('failed with 2nd vkEnumerateInstanceExtensionProperties');
    end;
  end else begin
    log('failed with vkEnumerateInstanceExtensionProperties');
    canrettrue := false;
  end;
  vkdispose(cnt);
  vkdispose(cnt2);
  get_supported_extensions := canrettrue;
end;

function T_vulkan_base_instance.onDebugCallback( _mSeverity : VkDebugUtilsMessageSeverityFlagBitsEXT; _mTypes : VkDebugUtilsMessageTypeFlagsEXT; _pcb : PVkDebugUtilsMessengerCallbackDataEXT) : VkBool32; 
begin
  log('dbgcb: '+inttostring(_pcb^.messageIdNumber)+':'+_pcb^.pMessageIdName+' : '+_pcb^.pMessage);
  onDebugCallback := VK_FALSE;
end;



begin
end.

