{$MODE delphi}

uses vulkan_class,vulkan_h,vulkan_basecls,vulkan_typeinit,sysutils;



{ 
  A brainless translation from: https://github.com/Erkaman/vulkan_minimal_compute

  NOT a vulkan-tutorial, just a demo about how to use the supplied vulkan units.

  The SPIR-V file comp.spv was generated from testshader.comp, which is
  https://github.com/Erkaman/vulkan_minimal_compute/blob/master/shaders/shader.comp

  using the open source cmd-line glsl-compiler "glslangValidator" from Khronos. 

}




type
Tcomputeappcls=class(T_vulkan_base_instance)
  constructor create();

  procedure choose_physicalDevive();
  procedure choose_QueueFamily(reqflags : dword);
  function createDevice() : boolean;
  function findMemoryType(mb:dword; props:VkMemoryPropertyFlags) : longint;
  procedure create_buffer(s : longint);
  procedure createDescriptorSetLayout();
  procedure connectBufferWithDescriptor();
  procedure alloc_descriptorSet();
  procedure createDescriptorPool();
  function read_shader(fn : string) : VkShaderModule;
  procedure make_compute_pipeline();
  procedure create_cmd_pool();
  procedure make_cmd_buff();
  procedure record_cmd_buff();
  procedure runCommandBuffer();
  procedure writeresult(fn :string);

  procedure initapp();

  public
  dev : VkDevice;
  pdev : VkPhysicalDevice; // chosen pdev
  queue : VkQueue;
  famidx : dword;
  buffer : VkBuffer;
  bufferMemory : VkDeviceMemory;
  descriptorSetLayout : VkDescriptorSetLayout;
  descriptorSet : VkDescriptorSet;
  descriptorPool : VkDescriptorPool;
  pipelineLayout : VkPipelineLayout;
  pipeline : VkPipeline;
  computeShaderModule : VkShaderModule;
  commandPool : VkCommandPool;
  commandBuffer : VkCommandBuffer;
  width,height,WORKGROUP_SIZE : longint;
end;


constructor Tcomputeappcls.create();
begin
  inherited create();
  dev  := VK_NiL_HANDLE;
  pdev  := VK_NiL_HANDLE;
  queue  := VK_NiL_HANDLE;
  buffer  := VK_NULL_HANDLE;
  bufferMemory  := VK_NULL_HANDLE;
  descriptorSetLayout  := VK_NULL_HANDLE;
  descriptorSet  := VK_NULL_HANDLE;
  descriptorPool  := VK_NULL_HANDLE;
  pipelineLayout  := VK_NULL_HANDLE;
  pipeline  := VK_NULL_HANDLE;
  computeShaderModule  := VK_NULL_HANDLE;
  commandPool  := VK_NULL_HANDLE;
  commandBuffer := VK_NiL_HANDLE;
end;

procedure Tcomputeappcls.choose_physicalDevive();
var devarr : array of VkPhysicalDevice; devcnt : dword;
begin
  if VK_SUCCESS=vkEnumeratePhysicalDevices(instance,@devcnt,nil) then begin
    setlength(devarr,devcnt);
    if VK_SUCCESS=vkEnumeratePhysicalDevices(instance,@devcnt,@devarr[0]) then begin
      log('have '+inttostring(devcnt)+' devices... choosing first one.');
      pdev := devarr[0];
    end;
  end;
end;

procedure Tcomputeappcls.choose_QueueFamily(reqflags : dword);
var  i : longint; nqf : dword; qf : array of VkQueueFamilyProperties;
begin
  famidx := 0;
  vkGetPhysicalDeviceQueueFamilyProperties(pdev,@nqf,nil);
  setlength(qf,nqf);
  vkGetPhysicalDeviceQueueFamilyProperties(pdev,@nqf,@qf[0]);
  log('chosen pdev has '+inttostring(nqf)+' queue families.');
  for i := 0 to nqf-1 do begin
    with qf[i] do begin
      log('family('+inttostring(i)+'): flgs='+hexstr(queueFlags,8)+' cnt='+inttostring(queueCount)+' tsbits='+inttostring(timestampValidBits));
      if (queueFlags and reqflags)=reqflags then famidx := i;
    end;
  end;
  writeln('chosen famidx='+inttostring(famidx));
end;

function Tcomputeappcls.createDevice() : boolean;
var qci : VkDeviceQueueCreateInfo;
dci : VkDeviceCreateInfo;
prio : single;
begin 
  qci := default_VkDeviceQueueCreateInfo;
  dci := default_VkDeviceCreateInfo;
  createDevice := false;
  qci.stype := VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
  qci.pnext := nil;
  qci.flags := 0;
  qci.queueFamilyIndex := famidx;
  qci.queueCount := 1;
  qci.pQueuePriorities := @prio;
  prio := 1.0;
  dci.stype := VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
  dci.pnext := nil;
  dci.flags := 0;
  dci.enabledLayerCount := 0;
  dci.ppEnabledLayerNames := nil;
  dci.pQueueCreateInfos := @qci;
  dci.queueCreateInfoCount := 1;
  dci.pEnabledFeatures  := nil;
  dci.enabledExtensionCount := 0;
  dci.ppEnabledExtensionNames := nil;
  if VK_SUCCESS=vkCreateDevice(pdev,@dci,nil,@dev) then begin
    log('VK_SUCCESS with create dev');
    createDevice := true;
  end else begin
    log('create dev failed.');
  end;
end;



function Tcomputeappcls.findMemoryType(mb:dword; props:VkMemoryPropertyFlags) : longint;
var i : longint; mp : VkPhysicalDeviceMemoryProperties;
begin
  findMemoryType := -1;
  vkGetPhysicalDeviceMemoryProperties(pdev,@mp);
  for i := 0 to mp.memoryTypeCount-1 do begin
    if ((mb and (1 shl i))>0) and ((props and mp.memoryTypes[i].propertyFlags)>0)
    then findMemoryType := i;
  end;
end;

procedure Tcomputeappcls.create_buffer(s : longint);
var bci : VkBufferCreateInfo;  mreq : VkMemoryRequirements; vma : VkMemoryAllocateInfo;
begin
  bci := default_VkBufferCreateInfo;
  vma := default_VkMemoryAllocateInfo;
  buffer := VK_NULL_HANDLE;
  with bci do begin
    size  := s;
    usage := VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    sharingMode := VK_SHARING_MODE_EXCLUSIVE;
  end;
  if VK_SUCCESS=vkCreateBuffer(dev, @bci, nil, @buffer) then begin
    vkGetBufferMemoryRequirements(dev,buffer,@mreq);
    with vma do begin
      allocationSize := mreq.size;
      memoryTypeIndex := findMemoryType(mreq.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT or VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
    end;
    if VK_SUCCESS=vkAllocateMemory(dev, @vma, nil, @bufferMemory) then begin
      writeln('bindbuff...',vkBindBufferMemory(dev, buffer, bufferMemory, 0));
    end; 
  end else log('Probmlems creating buffer');
end;

procedure Tcomputeappcls.createDescriptorSetLayout();
var ddesc : VkDescriptorSetLayoutCreateInfo; lb :VkDescriptorSetLayoutBinding;
begin
  ddesc := default_VkDescriptorSetLayoutCreateInfo;
  with lb do begin
    binding := 0;
    descriptorType := VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorCount := 1;
    stageFlags := VK_SHADER_STAGE_COMPUTE_BIT;
  end;
  with ddesc do begin
    bindingCount := 1;
    pBindings := @lb;
  end;
  vkCreateDescriptorSetLayout(dev, @ddesc, nil, @descriptorSetLayout);
end;


procedure Tcomputeappcls.createDescriptorPool();
var ps : VkDescriptorPoolSize; dpci : VkDescriptorPoolCreateInfo;
begin
  dpci := default_VkDescriptorPoolCreateInfo;
  ps._type := VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
  ps.descriptorCount := 1;
  dpci.maxSets := 1; 
  dpci.poolSizeCount := 1;
  dpci.pPoolSizes := @ps;
  if VK_SUCCESS=vkCreateDescriptorPool(dev, @dpci, nil, @descriptorPool) then begin
    log('descriptor pool created');
  end else log('probs with descriptor pool');
end;


procedure Tcomputeappcls.alloc_descriptorSet();
var dai : VkDescriptorSetAllocateInfo;
begin
  dai := default_VkDescriptorSetAllocateInfo;
  dai.descriptorPool := descriptorPool;
  dai.descriptorSetCount := 1;
  dai.pSetLayouts := @descriptorSetLayout;
  if VK_SUCCESS=vkAllocateDescriptorSets(dev, @dai, @descriptorSet) 
  then begin
    log('ok vkAllocateDescriptorSets');
  end else log('prob vkAllocateDescriptorSets');
end;

procedure Tcomputeappcls.connectBufferWithDescriptor();
var wds : VkWriteDescriptorSet; vdb : VkDescriptorBufferInfo;
begin
  log('vkUpdateDescriptorSets...');
  wds := default_VkWriteDescriptorSet;
  vdb := default_VkDescriptorBufferInfo;
  vdb.buffer := buffer;
  vdb.offset := 0;
  vdb.range := VK_WHOLE_SIZE;
  wds.dstSet := descriptorSet;
  wds.dstBinding := 0;
  wds.descriptorCount := 1;
  wds.descriptorType := VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
  wds.pBufferInfo := @vdb;
  vkUpdateDescriptorSets(dev, 1, @wds, 0, nil);
  log('vkUpdateDescriptorSets... done');
end;

function read_file(fn : string) : ansistring;
var f : file; n : longint; a : ansistring;
begin
  a := '';
  filemode := 0; 
  assign(f,fn);
  reset(f,1);
  n := filesize(f);
  setlength(a,n);
  blockread(f,a[1],filesize(f));
  close(f);
  while (length(a) and 3)>0 do a+= #0;
  read_file := a;
end;

function Tcomputeappcls.read_shader(fn : string) : VkShaderModule;
var smci : VkShaderModuleCreateInfo; sm : VkShaderModule; a : ansistring;
begin
  smci := default_VkShaderModuleCreateInfo;
  log('read shader file: '+fn);
  a := read_file(fn);
  smci.codeSize := length(a);
  smci.pCode := @a[1];
  if VK_SUCCESS=vkCreateShaderModule(dev,@smci, nil, @sm) then begin
    log('sucess reading shader file '+fn);
    read_shader := sm;
  end else begin
    log('probs reading shader file '+fn);
    read_shader := VK_NULL_HANDLE;
  end;
end;





procedure Tcomputeappcls.make_compute_pipeline();
var pssci : VkPipelineShaderStageCreateInfo; plci : VkPipelineLayoutCreateInfo; cpci : VkComputePipelineCreateInfo; 
begin
  plci := default_VkPipelineLayoutCreateInfo;
  plci.flags := 0;
  plci.setLayoutCount := 1;
  plci.pushConstantRangeCount := 0;
  plci.pPushConstantRanges := nil;
  plci.pSetLayouts := @descriptorSetLayout; 
  if VK_SUCCESS=vkCreatePipelineLayout(dev, @plci,nil, @pipelineLayout) then begin
    log('ok vkCreatePipelineLayout');
    pssci := default_VkPipelineShaderStageCreateInfo;
    pssci.flags := 0;
    pssci.stage := VK_SHADER_STAGE_COMPUTE_BIT;
    pssci.module := computeShaderModule;
    pssci.pName  := 'main';
    pssci.pSpecializationInfo := nil;
    cpci := default_VkComputePipelineCreateInfo;
    cpci.flags := 0;
    cpci.stage := pssci;
    cpci.layout := pipelineLayout;
    if VK_SUCCESS=vkCreateComputePipelines(dev, VK_NULL_HANDLE, 1, @cpci,nil, @pipeline) then begin
      log('ok vkCreateComputePipelines');
    end else begin
      log('prob vkCreateComputePipelines');
    end;
  end else begin
    log('prob vkCreateComputePipelines');
  end;
end;

procedure Tcomputeappcls.record_cmd_buff();
var cbbi : VkCommandBufferBeginInfo;
begin
  cbbi.sType := VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  cbbi.pnext := nil;
  cbbi.flags := VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT or VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT; // the buffer is only submitted and used once in this application.
  cbbi.pInheritanceInfo := nil;
  if VK_SUCCESS=vkBeginCommandBuffer(commandBuffer, @cbbi) then begin; // start recording commands.
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
    vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, @descriptorSet, 0, nil);
    vkCmdDispatch(commandBuffer, 
      (width +WORKGROUP_SIZE-1) DIV WORKGROUP_SIZE, 
      (height+WORKGROUP_SIZE-1) DIV WORKGROUP_SIZE, 1);
    if VK_SUCCESS=vkEndCommandBuffer(commandBuffer) then log('done rec cmd buf') else log('prob during rec cmd buf');
  end else log('prob recording to cmd buf');
end;


procedure Tcomputeappcls.create_cmd_pool();
var cpci : VkCommandPoolCreateInfo;
begin
  cpci.sType := VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
  cpci.pnext := nil;
  cpci.flags := VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  cpci.queueFamilyIndex := famidx;
  if VK_SUCCESS=vkCreateCommandPool(dev, @cpci, nil, @commandPool) then begin
    log('ok vkCreateCommandPool');
  end else begin
    log('probs vkCreateCommandPool');
  end;
end;



procedure Tcomputeappcls.make_cmd_buff();
var cbai : VkCommandBufferAllocateInfo;
begin
  cbai.sType := VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  cbai.pnext := nil;
  cbai.commandPool := commandPool; // specify the command pool to allocate from.
  cbai.level := VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  cbai.commandBufferCount := 1; // allocate a single command buffer. 
  if VK_SUCCESS=vkAllocateCommandBuffers(dev, @cbai, @commandBuffer) then begin
    log('created command buffer');
  end else begin
    log('probs creating command buffer');
  end;
end;



procedure Tcomputeappcls.runCommandBuffer();
var sinf : VkSubmitInfo; fence : VkFence; fci : VkFenceCreateInfo; i : longint; tim : double;
begin
  sinf.sType := VK_STRUCTURE_TYPE_SUBMIT_INFO;
  sinf.pnext := nil;
  sinf.waitSemaphoreCount := 0;
  sinf.pWaitSemaphores := nil;
  sinf.pWaitDstStageMask := nil;
  sinf.commandBufferCount := 1; // submit a single command buffer
  sinf.pCommandBuffers := @commandBuffer; // the command buffer to submit.
  sinf.signalSemaphoreCount := 0;
  sinf.pSignalSemaphores := nil;
  fci.sType := VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fci.pnext := nil; 
  fci.flags := 0;
  tim := now();
  vkCreateFence(dev,@fci, nil, @fence);
  for i := 1 to 1 do (vkQueueSubmit(queue, 1, @sinf, VK_NULL_HANDLE));
  vkQueueSubmit(queue, 1, @sinf, fence);
  vkWaitForFences(dev, 1, @fence, VK_TRUE, 100000000000);
writeln;
writeln('comupte-time: ',(now()-tim)*3600*24);
  vkDestroyFence(dev, fence, nil);
end;

procedure Tcomputeappcls.writeresult(fn :string);
var r : Psingle; i,j,m : longint; dat : array of byte; f : file; a : string;
begin
  r := nil;
  vkMapMemory(dev, bufferMemory, 0, width*height*4*4, 0, @r);
  if (r=nil) then begin
    log('prob with vkMapMemory');
  end else begin
    a := 'P6'+#10+inttostring(width)+' '+inttostring(height)+#10+'255'+#10;
    m := width*height;
    setlength(dat,m*4);
    m := m*4-1; j := 0;
    for i := 0 to m do begin
      if (i and 3)<3 then begin
        dat[j] := round(255*r[i]);
        j += 1;
      end;
    end;
    assign(f,fn);
    rewrite(f,1);
    blockwrite(f,a[1],length(a));
    blockwrite(f,dat[0],width*height*3);
    close(f);
  end;
end;

procedure Tcomputeappcls.initapp();
begin

  WORKGROUP_SIZE := 32;
  width := 3200;
  height := 2400;

  choose_physicalDevive();
  choose_QueueFamily(VK_QUEUE_COMPUTE_BIT);
  if createDevice() then begin
    vkGetDeviceQueue(dev,famidx,0,@queue);
    log('ok vkGetDeviceQueue');
    create_buffer(width*height*4*4);
    createDescriptorSetLayout();
    createDescriptorPool();
    alloc_descriptorSet();
    connectBufferWithDescriptor();
    computeShaderModule := read_shader('./comp.spv');
    make_compute_pipeline();
    create_cmd_pool();
    make_cmd_buff();
    record_cmd_buff();
    runCommandBuffer();
    writeresult('res.ppm');
  end;
end;




var
vk :  Tcomputeappcls;


begin
  vk := Tcomputeappcls.create();
  vk.use_dbg := true;
  vk.use_dbg_initial := true;

  if vk.init_vk(
    (['some_nonexisting_layername1',
      'VK_LAYER_LUNARG_standard_validation',
      'some_nonexisting_layername2'
     ]),
    (['VK_KHR_device_group_creation',
      '__nonexisting__EXT__1',
      'VK_EXT_debug_utils',
      'VK_KHR_display',
      'VK_EXT_display_surface_counter',
      'VK_EXT_direct_mode_display',
      'VK_KHR_get_display_properties2',
      'VK_KHR_surface',
      'VK_KHR_get_surface_capabilities2',
      'VK_KHR_xlib_surface',
      '__nonexisting__EXT__2',
      'VK_EXT_acquire_xlib_display',
      'VK_KHR_external_fence_capabilities',
      'VK_KHR_external_memory_capabilities',
      'VK_KHR_external_semaphore_capabilities',
      'VK_KHR_get_physical_device_properties2',
      '__nonexisting__EXT__3'
     ])
   ) then 
  begin 
    vk.log('ready to work with vulkan.');
    vk.initapp();
  end else begin
    vk.log('cannot use vulkan.');
  end;
  vk.destroy();
end.