unit vulkan_lib;

interface


function vulkan_getGIPA() : pointer;


implementation
{$IFDEF LINUX}
uses dynlibs;

var lh : TLibHandle;

function vulkan_getGIPA() : pointer;
begin
  if (lh=NilHandle) then begin
    lh := LoadLibrary('libvulkan.so.1');
  end;
  if (not(lh=NilHandle)) then begin
    vulkan_getGIPA:=GetProcedureAddress(lh,'vkGetInstanceProcAddr');
  end else vulkan_getGIPA:=nil;
end;
{$ELSE}

{$IFDEF WIN32}
uses dynlibs;

var lh : TLibHandle;

function vulkan_getGIPA() : pointer;
begin
  if (lh=NilHandle) then begin
    lh := LoadLibrary('vulkan-1.dll'); // in windows\system32, according to "loader/LoaderAndLayerInterface.md"
  end;
  if (not(lh=NilHandle)) then begin
    vulkan_getGIPA:=GetProcedureAddress(lh,'vkGetInstanceProcAddr');
  end else vulkan_getGIPA:=nil;
end;
{$ELSE}

{$IFDEF WIN64}
uses dynlibs;

var lh : TLibHandle;

function vulkan_getGIPA() : pointer;
begin
  if (lh=NilHandle) then begin
    lh := LoadLibrary('vulkan-1.dll'); // in windows\sysWOW64, according to "loader/LoaderAndLayerInterface.md"
  end;
  if (not(lh=NilHandle)) then begin
    vulkan_getGIPA:=GetProcedureAddress(lh,'vkGetInstanceProcAddr');
  end else vulkan_getGIPA:=nil;
end;
{$ELSE}


{$ENDIF win64}
{$ENDIF win32}
{$ENDIF linux}

begin
  lh := NilHandle;
end.
