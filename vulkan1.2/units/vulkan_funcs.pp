{$mode objfpc}
unit vulkan_funcs;

interface
uses vulkan_h;



{all known command procvars}


var

{ from VK_VERSION_1_0 }
vkCreateInstance : T_vkCreateInstance;
vkDestroyInstance : T_vkDestroyInstance;
vkEnumeratePhysicalDevices : T_vkEnumeratePhysicalDevices;
vkGetDeviceProcAddr : T_vkGetDeviceProcAddr;
vkGetInstanceProcAddr : T_vkGetInstanceProcAddr;
vkGetPhysicalDeviceProperties : T_vkGetPhysicalDeviceProperties;
vkGetPhysicalDeviceQueueFamilyProperties : T_vkGetPhysicalDeviceQueueFamilyProperties;
vkGetPhysicalDeviceMemoryProperties : T_vkGetPhysicalDeviceMemoryProperties;
vkGetPhysicalDeviceFeatures : T_vkGetPhysicalDeviceFeatures;
vkGetPhysicalDeviceFormatProperties : T_vkGetPhysicalDeviceFormatProperties;
vkGetPhysicalDeviceImageFormatProperties : T_vkGetPhysicalDeviceImageFormatProperties;
vkCreateDevice : T_vkCreateDevice;
vkDestroyDevice : T_vkDestroyDevice;
vkEnumerateInstanceLayerProperties : T_vkEnumerateInstanceLayerProperties;
vkEnumerateInstanceExtensionProperties : T_vkEnumerateInstanceExtensionProperties;
vkEnumerateDeviceLayerProperties : T_vkEnumerateDeviceLayerProperties;
vkEnumerateDeviceExtensionProperties : T_vkEnumerateDeviceExtensionProperties;
vkGetDeviceQueue : T_vkGetDeviceQueue;
vkQueueSubmit : T_vkQueueSubmit;
vkQueueWaitIdle : T_vkQueueWaitIdle;
vkDeviceWaitIdle : T_vkDeviceWaitIdle;
vkAllocateMemory : T_vkAllocateMemory;
vkFreeMemory : T_vkFreeMemory;
vkMapMemory : T_vkMapMemory;
vkUnmapMemory : T_vkUnmapMemory;
vkFlushMappedMemoryRanges : T_vkFlushMappedMemoryRanges;
vkInvalidateMappedMemoryRanges : T_vkInvalidateMappedMemoryRanges;
vkGetDeviceMemoryCommitment : T_vkGetDeviceMemoryCommitment;
vkGetBufferMemoryRequirements : T_vkGetBufferMemoryRequirements;
vkBindBufferMemory : T_vkBindBufferMemory;
vkGetImageMemoryRequirements : T_vkGetImageMemoryRequirements;
vkBindImageMemory : T_vkBindImageMemory;
vkGetImageSparseMemoryRequirements : T_vkGetImageSparseMemoryRequirements;
vkGetPhysicalDeviceSparseImageFormatProperties : T_vkGetPhysicalDeviceSparseImageFormatProperties;
vkQueueBindSparse : T_vkQueueBindSparse;
vkCreateFence : T_vkCreateFence;
vkDestroyFence : T_vkDestroyFence;
vkResetFences : T_vkResetFences;
vkGetFenceStatus : T_vkGetFenceStatus;
vkWaitForFences : T_vkWaitForFences;
vkCreateSemaphore : T_vkCreateSemaphore;
vkDestroySemaphore : T_vkDestroySemaphore;
vkCreateEvent : T_vkCreateEvent;
vkDestroyEvent : T_vkDestroyEvent;
vkGetEventStatus : T_vkGetEventStatus;
vkSetEvent : T_vkSetEvent;
vkResetEvent : T_vkResetEvent;
vkCreateQueryPool : T_vkCreateQueryPool;
vkDestroyQueryPool : T_vkDestroyQueryPool;
vkGetQueryPoolResults : T_vkGetQueryPoolResults;
vkCreateBuffer : T_vkCreateBuffer;
vkDestroyBuffer : T_vkDestroyBuffer;
vkCreateBufferView : T_vkCreateBufferView;
vkDestroyBufferView : T_vkDestroyBufferView;
vkCreateImage : T_vkCreateImage;
vkDestroyImage : T_vkDestroyImage;
vkGetImageSubresourceLayout : T_vkGetImageSubresourceLayout;
vkCreateImageView : T_vkCreateImageView;
vkDestroyImageView : T_vkDestroyImageView;
vkCreateShaderModule : T_vkCreateShaderModule;
vkDestroyShaderModule : T_vkDestroyShaderModule;
vkCreatePipelineCache : T_vkCreatePipelineCache;
vkDestroyPipelineCache : T_vkDestroyPipelineCache;
vkGetPipelineCacheData : T_vkGetPipelineCacheData;
vkMergePipelineCaches : T_vkMergePipelineCaches;
vkCreateGraphicsPipelines : T_vkCreateGraphicsPipelines;
vkCreateComputePipelines : T_vkCreateComputePipelines;
vkDestroyPipeline : T_vkDestroyPipeline;
vkCreatePipelineLayout : T_vkCreatePipelineLayout;
vkDestroyPipelineLayout : T_vkDestroyPipelineLayout;
vkCreateSampler : T_vkCreateSampler;
vkDestroySampler : T_vkDestroySampler;
vkCreateDescriptorSetLayout : T_vkCreateDescriptorSetLayout;
vkDestroyDescriptorSetLayout : T_vkDestroyDescriptorSetLayout;
vkCreateDescriptorPool : T_vkCreateDescriptorPool;
vkDestroyDescriptorPool : T_vkDestroyDescriptorPool;
vkResetDescriptorPool : T_vkResetDescriptorPool;
vkAllocateDescriptorSets : T_vkAllocateDescriptorSets;
vkFreeDescriptorSets : T_vkFreeDescriptorSets;
vkUpdateDescriptorSets : T_vkUpdateDescriptorSets;
vkCreateFramebuffer : T_vkCreateFramebuffer;
vkDestroyFramebuffer : T_vkDestroyFramebuffer;
vkCreateRenderPass : T_vkCreateRenderPass;
vkDestroyRenderPass : T_vkDestroyRenderPass;
vkGetRenderAreaGranularity : T_vkGetRenderAreaGranularity;
vkCreateCommandPool : T_vkCreateCommandPool;
vkDestroyCommandPool : T_vkDestroyCommandPool;
vkResetCommandPool : T_vkResetCommandPool;
vkAllocateCommandBuffers : T_vkAllocateCommandBuffers;
vkFreeCommandBuffers : T_vkFreeCommandBuffers;
vkBeginCommandBuffer : T_vkBeginCommandBuffer;
vkEndCommandBuffer : T_vkEndCommandBuffer;
vkResetCommandBuffer : T_vkResetCommandBuffer;
vkCmdBindPipeline : T_vkCmdBindPipeline;
vkCmdSetViewport : T_vkCmdSetViewport;
vkCmdSetScissor : T_vkCmdSetScissor;
vkCmdSetLineWidth : T_vkCmdSetLineWidth;
vkCmdSetDepthBias : T_vkCmdSetDepthBias;
vkCmdSetBlendConstants : T_vkCmdSetBlendConstants;
vkCmdSetDepthBounds : T_vkCmdSetDepthBounds;
vkCmdSetStencilCompareMask : T_vkCmdSetStencilCompareMask;
vkCmdSetStencilWriteMask : T_vkCmdSetStencilWriteMask;
vkCmdSetStencilReference : T_vkCmdSetStencilReference;
vkCmdBindDescriptorSets : T_vkCmdBindDescriptorSets;
vkCmdBindIndexBuffer : T_vkCmdBindIndexBuffer;
vkCmdBindVertexBuffers : T_vkCmdBindVertexBuffers;
vkCmdDraw : T_vkCmdDraw;
vkCmdDrawIndexed : T_vkCmdDrawIndexed;
vkCmdDrawIndirect : T_vkCmdDrawIndirect;
vkCmdDrawIndexedIndirect : T_vkCmdDrawIndexedIndirect;
vkCmdDispatch : T_vkCmdDispatch;
vkCmdDispatchIndirect : T_vkCmdDispatchIndirect;
vkCmdCopyBuffer : T_vkCmdCopyBuffer;
vkCmdCopyImage : T_vkCmdCopyImage;
vkCmdBlitImage : T_vkCmdBlitImage;
vkCmdCopyBufferToImage : T_vkCmdCopyBufferToImage;
vkCmdCopyImageToBuffer : T_vkCmdCopyImageToBuffer;
vkCmdUpdateBuffer : T_vkCmdUpdateBuffer;
vkCmdFillBuffer : T_vkCmdFillBuffer;
vkCmdClearColorImage : T_vkCmdClearColorImage;
vkCmdClearDepthStencilImage : T_vkCmdClearDepthStencilImage;
vkCmdClearAttachments : T_vkCmdClearAttachments;
vkCmdResolveImage : T_vkCmdResolveImage;
vkCmdSetEvent : T_vkCmdSetEvent;
vkCmdResetEvent : T_vkCmdResetEvent;
vkCmdWaitEvents : T_vkCmdWaitEvents;
vkCmdPipelineBarrier : T_vkCmdPipelineBarrier;
vkCmdBeginQuery : T_vkCmdBeginQuery;
vkCmdEndQuery : T_vkCmdEndQuery;
vkCmdResetQueryPool : T_vkCmdResetQueryPool;
vkCmdWriteTimestamp : T_vkCmdWriteTimestamp;
vkCmdCopyQueryPoolResults : T_vkCmdCopyQueryPoolResults;
vkCmdPushConstants : T_vkCmdPushConstants;
vkCmdBeginRenderPass : T_vkCmdBeginRenderPass;
vkCmdNextSubpass : T_vkCmdNextSubpass;
vkCmdEndRenderPass : T_vkCmdEndRenderPass;
vkCmdExecuteCommands : T_vkCmdExecuteCommands;

{ from VK_VERSION_1_1 }
vkEnumerateInstanceVersion : T_vkEnumerateInstanceVersion;
vkGetPhysicalDeviceFeatures2 : T_vkGetPhysicalDeviceFeatures2;
vkGetPhysicalDeviceProperties2 : T_vkGetPhysicalDeviceProperties2;
vkGetPhysicalDeviceFormatProperties2 : T_vkGetPhysicalDeviceFormatProperties2;
vkGetPhysicalDeviceImageFormatProperties2 : T_vkGetPhysicalDeviceImageFormatProperties2;
vkGetPhysicalDeviceQueueFamilyProperties2 : T_vkGetPhysicalDeviceQueueFamilyProperties2;
vkGetPhysicalDeviceMemoryProperties2 : T_vkGetPhysicalDeviceMemoryProperties2;
vkGetPhysicalDeviceSparseImageFormatProperties2 : T_vkGetPhysicalDeviceSparseImageFormatProperties2;
vkTrimCommandPool : T_vkTrimCommandPool;
vkGetPhysicalDeviceExternalBufferProperties : T_vkGetPhysicalDeviceExternalBufferProperties;
vkGetPhysicalDeviceExternalSemaphoreProperties : T_vkGetPhysicalDeviceExternalSemaphoreProperties;
vkGetPhysicalDeviceExternalFenceProperties : T_vkGetPhysicalDeviceExternalFenceProperties;
vkEnumeratePhysicalDeviceGroups : T_vkEnumeratePhysicalDeviceGroups;
vkGetDeviceGroupPeerMemoryFeatures : T_vkGetDeviceGroupPeerMemoryFeatures;
vkBindBufferMemory2 : T_vkBindBufferMemory2;
vkBindImageMemory2 : T_vkBindImageMemory2;
vkCmdSetDeviceMask : T_vkCmdSetDeviceMask;
vkCmdDispatchBase : T_vkCmdDispatchBase;
vkCreateDescriptorUpdateTemplate : T_vkCreateDescriptorUpdateTemplate;
vkDestroyDescriptorUpdateTemplate : T_vkDestroyDescriptorUpdateTemplate;
vkUpdateDescriptorSetWithTemplate : T_vkUpdateDescriptorSetWithTemplate;
vkGetBufferMemoryRequirements2 : T_vkGetBufferMemoryRequirements2;
vkGetImageMemoryRequirements2 : T_vkGetImageMemoryRequirements2;
vkGetImageSparseMemoryRequirements2 : T_vkGetImageSparseMemoryRequirements2;
vkCreateSamplerYcbcrConversion : T_vkCreateSamplerYcbcrConversion;
vkDestroySamplerYcbcrConversion : T_vkDestroySamplerYcbcrConversion;
vkGetDeviceQueue2 : T_vkGetDeviceQueue2;
vkGetDescriptorSetLayoutSupport : T_vkGetDescriptorSetLayoutSupport;

{ from VK_VERSION_1_2 }
vkResetQueryPool : T_vkResetQueryPool;
vkCreateRenderPass2 : T_vkCreateRenderPass2;
vkCmdBeginRenderPass2 : T_vkCmdBeginRenderPass2;
vkCmdNextSubpass2 : T_vkCmdNextSubpass2;
vkCmdEndRenderPass2 : T_vkCmdEndRenderPass2;
vkGetSemaphoreCounterValue : T_vkGetSemaphoreCounterValue;
vkWaitSemaphores : T_vkWaitSemaphores;
vkSignalSemaphore : T_vkSignalSemaphore;
vkCmdDrawIndirectCount : T_vkCmdDrawIndirectCount;
vkCmdDrawIndexedIndirectCount : T_vkCmdDrawIndexedIndirectCount;
vkGetBufferOpaqueCaptureAddress : T_vkGetBufferOpaqueCaptureAddress;
vkGetBufferDeviceAddress : T_vkGetBufferDeviceAddress;
vkGetDeviceMemoryOpaqueCaptureAddress : T_vkGetDeviceMemoryOpaqueCaptureAddress;

{ from VK_EXT_host_query_reset }
vkResetQueryPoolEXT : T_vkResetQueryPool;

{ from VK_EXT_conditional_rendering }
vkCmdBeginConditionalRenderingEXT : T_vkCmdBeginConditionalRenderingEXT;
vkCmdEndConditionalRenderingEXT : T_vkCmdEndConditionalRenderingEXT;

{ from VK_KHR_android_surface }
vkCreateAndroidSurfaceKHR : T_vkCreateAndroidSurfaceKHR;

{ from VK_KHR_display }
vkGetPhysicalDeviceDisplayPropertiesKHR : T_vkGetPhysicalDeviceDisplayPropertiesKHR;
vkGetPhysicalDeviceDisplayPlanePropertiesKHR : T_vkGetPhysicalDeviceDisplayPlanePropertiesKHR;
vkGetDisplayPlaneSupportedDisplaysKHR : T_vkGetDisplayPlaneSupportedDisplaysKHR;
vkGetDisplayModePropertiesKHR : T_vkGetDisplayModePropertiesKHR;
vkCreateDisplayModeKHR : T_vkCreateDisplayModeKHR;
vkGetDisplayPlaneCapabilitiesKHR : T_vkGetDisplayPlaneCapabilitiesKHR;
vkCreateDisplayPlaneSurfaceKHR : T_vkCreateDisplayPlaneSurfaceKHR;

{ from VK_KHR_display_swapchain }
vkCreateSharedSwapchainsKHR : T_vkCreateSharedSwapchainsKHR;

{ from VK_KHR_surface }
vkDestroySurfaceKHR : T_vkDestroySurfaceKHR;
vkGetPhysicalDeviceSurfaceSupportKHR : T_vkGetPhysicalDeviceSurfaceSupportKHR;
vkGetPhysicalDeviceSurfaceCapabilitiesKHR : T_vkGetPhysicalDeviceSurfaceCapabilitiesKHR;
vkGetPhysicalDeviceSurfaceFormatsKHR : T_vkGetPhysicalDeviceSurfaceFormatsKHR;
vkGetPhysicalDeviceSurfacePresentModesKHR : T_vkGetPhysicalDeviceSurfacePresentModesKHR;

{ from VK_KHR_swapchain }
vkCreateSwapchainKHR : T_vkCreateSwapchainKHR;
vkDestroySwapchainKHR : T_vkDestroySwapchainKHR;
vkGetSwapchainImagesKHR : T_vkGetSwapchainImagesKHR;
vkAcquireNextImageKHR : T_vkAcquireNextImageKHR;
vkQueuePresentKHR : T_vkQueuePresentKHR;
vkGetDeviceGroupPresentCapabilitiesKHR : T_vkGetDeviceGroupPresentCapabilitiesKHR;
vkGetDeviceGroupSurfacePresentModesKHR : T_vkGetDeviceGroupSurfacePresentModesKHR;
vkAcquireNextImage2KHR : T_vkAcquireNextImage2KHR;
vkGetPhysicalDevicePresentRectanglesKHR : T_vkGetPhysicalDevicePresentRectanglesKHR;

{ from VK_NN_vi_surface }
vkCreateViSurfaceNN : T_vkCreateViSurfaceNN;

{ from VK_KHR_wayland_surface }
vkCreateWaylandSurfaceKHR : T_vkCreateWaylandSurfaceKHR;
vkGetPhysicalDeviceWaylandPresentationSupportKHR : T_vkGetPhysicalDeviceWaylandPresentationSupportKHR;

{ from VK_KHR_win32_surface }
vkCreateWin32SurfaceKHR : T_vkCreateWin32SurfaceKHR;
vkGetPhysicalDeviceWin32PresentationSupportKHR : T_vkGetPhysicalDeviceWin32PresentationSupportKHR;

{ from VK_KHR_xlib_surface }
vkCreateXlibSurfaceKHR : T_vkCreateXlibSurfaceKHR;
vkGetPhysicalDeviceXlibPresentationSupportKHR : T_vkGetPhysicalDeviceXlibPresentationSupportKHR;

{ from VK_KHR_xcb_surface }
vkCreateXcbSurfaceKHR : T_vkCreateXcbSurfaceKHR;
vkGetPhysicalDeviceXcbPresentationSupportKHR : T_vkGetPhysicalDeviceXcbPresentationSupportKHR;

{ from VK_FUCHSIA_imagepipe_surface }
vkCreateImagePipeSurfaceFUCHSIA : T_vkCreateImagePipeSurfaceFUCHSIA;

{ from VK_GGP_stream_descriptor_surface }
vkCreateStreamDescriptorSurfaceGGP : T_vkCreateStreamDescriptorSurfaceGGP;

{ from VK_EXT_debug_report }
vkCreateDebugReportCallbackEXT : T_vkCreateDebugReportCallbackEXT;
vkDestroyDebugReportCallbackEXT : T_vkDestroyDebugReportCallbackEXT;
vkDebugReportMessageEXT : T_vkDebugReportMessageEXT;

{ from VK_EXT_debug_marker }
vkDebugMarkerSetObjectNameEXT : T_vkDebugMarkerSetObjectNameEXT;
vkDebugMarkerSetObjectTagEXT : T_vkDebugMarkerSetObjectTagEXT;
vkCmdDebugMarkerBeginEXT : T_vkCmdDebugMarkerBeginEXT;
vkCmdDebugMarkerEndEXT : T_vkCmdDebugMarkerEndEXT;
vkCmdDebugMarkerInsertEXT : T_vkCmdDebugMarkerInsertEXT;

{ from VK_NV_external_memory_capabilities }
vkGetPhysicalDeviceExternalImageFormatPropertiesNV : T_vkGetPhysicalDeviceExternalImageFormatPropertiesNV;

{ from VK_NV_external_memory_win32 }
vkGetMemoryWin32HandleNV : T_vkGetMemoryWin32HandleNV;

{ from VK_NVX_device_generated_commands }
vkCmdProcessCommandsNVX : T_vkCmdProcessCommandsNVX;
vkCmdReserveSpaceForCommandsNVX : T_vkCmdReserveSpaceForCommandsNVX;
vkCreateIndirectCommandsLayoutNVX : T_vkCreateIndirectCommandsLayoutNVX;
vkDestroyIndirectCommandsLayoutNVX : T_vkDestroyIndirectCommandsLayoutNVX;
vkCreateObjectTableNVX : T_vkCreateObjectTableNVX;
vkDestroyObjectTableNVX : T_vkDestroyObjectTableNVX;
vkRegisterObjectsNVX : T_vkRegisterObjectsNVX;
vkUnregisterObjectsNVX : T_vkUnregisterObjectsNVX;
vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX : T_vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX;

{ from VK_KHR_get_physical_device_properties2 }
vkGetPhysicalDeviceFeatures2KHR : T_vkGetPhysicalDeviceFeatures2;
vkGetPhysicalDeviceProperties2KHR : T_vkGetPhysicalDeviceProperties2;
vkGetPhysicalDeviceFormatProperties2KHR : T_vkGetPhysicalDeviceFormatProperties2;
vkGetPhysicalDeviceImageFormatProperties2KHR : T_vkGetPhysicalDeviceImageFormatProperties2;
vkGetPhysicalDeviceQueueFamilyProperties2KHR : T_vkGetPhysicalDeviceQueueFamilyProperties2;
vkGetPhysicalDeviceMemoryProperties2KHR : T_vkGetPhysicalDeviceMemoryProperties2;
vkGetPhysicalDeviceSparseImageFormatProperties2KHR : T_vkGetPhysicalDeviceSparseImageFormatProperties2;

{ from VK_KHR_push_descriptor }
vkCmdPushDescriptorSetKHR : T_vkCmdPushDescriptorSetKHR;
vkCmdPushDescriptorSetWithTemplateKHR : T_vkCmdPushDescriptorSetWithTemplateKHR;

{ from VK_KHR_maintenance1 }
vkTrimCommandPoolKHR : T_vkTrimCommandPool;

{ from VK_KHR_external_memory_capabilities }
vkGetPhysicalDeviceExternalBufferPropertiesKHR : T_vkGetPhysicalDeviceExternalBufferProperties;

{ from VK_KHR_external_memory_win32 }
vkGetMemoryWin32HandleKHR : T_vkGetMemoryWin32HandleKHR;
vkGetMemoryWin32HandlePropertiesKHR : T_vkGetMemoryWin32HandlePropertiesKHR;

{ from VK_KHR_external_memory_fd }
vkGetMemoryFdKHR : T_vkGetMemoryFdKHR;
vkGetMemoryFdPropertiesKHR : T_vkGetMemoryFdPropertiesKHR;

{ from VK_KHR_external_semaphore_capabilities }
vkGetPhysicalDeviceExternalSemaphorePropertiesKHR : T_vkGetPhysicalDeviceExternalSemaphoreProperties;

{ from VK_KHR_external_semaphore_win32 }
vkGetSemaphoreWin32HandleKHR : T_vkGetSemaphoreWin32HandleKHR;
vkImportSemaphoreWin32HandleKHR : T_vkImportSemaphoreWin32HandleKHR;

{ from VK_KHR_external_semaphore_fd }
vkGetSemaphoreFdKHR : T_vkGetSemaphoreFdKHR;
vkImportSemaphoreFdKHR : T_vkImportSemaphoreFdKHR;

{ from VK_KHR_external_fence_capabilities }
vkGetPhysicalDeviceExternalFencePropertiesKHR : T_vkGetPhysicalDeviceExternalFenceProperties;

{ from VK_KHR_external_fence_win32 }
vkGetFenceWin32HandleKHR : T_vkGetFenceWin32HandleKHR;
vkImportFenceWin32HandleKHR : T_vkImportFenceWin32HandleKHR;

{ from VK_KHR_external_fence_fd }
vkGetFenceFdKHR : T_vkGetFenceFdKHR;
vkImportFenceFdKHR : T_vkImportFenceFdKHR;

{ from VK_EXT_direct_mode_display }
vkReleaseDisplayEXT : T_vkReleaseDisplayEXT;

{ from VK_EXT_acquire_xlib_display }
vkAcquireXlibDisplayEXT : T_vkAcquireXlibDisplayEXT;
vkGetRandROutputDisplayEXT : T_vkGetRandROutputDisplayEXT;

{ from VK_EXT_display_control }
vkDisplayPowerControlEXT : T_vkDisplayPowerControlEXT;
vkRegisterDeviceEventEXT : T_vkRegisterDeviceEventEXT;
vkRegisterDisplayEventEXT : T_vkRegisterDisplayEventEXT;
vkGetSwapchainCounterEXT : T_vkGetSwapchainCounterEXT;

{ from VK_EXT_display_surface_counter }
vkGetPhysicalDeviceSurfaceCapabilities2EXT : T_vkGetPhysicalDeviceSurfaceCapabilities2EXT;

{ from VK_KHR_device_group_creation }
vkEnumeratePhysicalDeviceGroupsKHR : T_vkEnumeratePhysicalDeviceGroups;

{ from VK_KHR_device_group }
vkGetDeviceGroupPeerMemoryFeaturesKHR : T_vkGetDeviceGroupPeerMemoryFeatures;
vkCmdSetDeviceMaskKHR : T_vkCmdSetDeviceMask;
vkCmdDispatchBaseKHR : T_vkCmdDispatchBase;

{ from VK_KHR_bind_memory2 }
vkBindBufferMemory2KHR : T_vkBindBufferMemory2;
vkBindImageMemory2KHR : T_vkBindImageMemory2;

{ from VK_KHR_descriptor_update_template }
vkCreateDescriptorUpdateTemplateKHR : T_vkCreateDescriptorUpdateTemplate;
vkDestroyDescriptorUpdateTemplateKHR : T_vkDestroyDescriptorUpdateTemplate;
vkUpdateDescriptorSetWithTemplateKHR : T_vkUpdateDescriptorSetWithTemplate;

{ from VK_EXT_hdr_metadata }
vkSetHdrMetadataEXT : T_vkSetHdrMetadataEXT;

{ from VK_KHR_shared_presentable_image }
vkGetSwapchainStatusKHR : T_vkGetSwapchainStatusKHR;

{ from VK_GOOGLE_display_timing }
vkGetRefreshCycleDurationGOOGLE : T_vkGetRefreshCycleDurationGOOGLE;
vkGetPastPresentationTimingGOOGLE : T_vkGetPastPresentationTimingGOOGLE;

{ from VK_MVK_ios_surface }
vkCreateIOSSurfaceMVK : T_vkCreateIOSSurfaceMVK;

{ from VK_MVK_macos_surface }
vkCreateMacOSSurfaceMVK : T_vkCreateMacOSSurfaceMVK;

{ from VK_EXT_metal_surface }
vkCreateMetalSurfaceEXT : T_vkCreateMetalSurfaceEXT;

{ from VK_NV_clip_space_w_scaling }
vkCmdSetViewportWScalingNV : T_vkCmdSetViewportWScalingNV;

{ from VK_EXT_discard_rectangles }
vkCmdSetDiscardRectangleEXT : T_vkCmdSetDiscardRectangleEXT;

{ from VK_EXT_sample_locations }
vkCmdSetSampleLocationsEXT : T_vkCmdSetSampleLocationsEXT;
vkGetPhysicalDeviceMultisamplePropertiesEXT : T_vkGetPhysicalDeviceMultisamplePropertiesEXT;

{ from VK_KHR_get_surface_capabilities2 }
vkGetPhysicalDeviceSurfaceCapabilities2KHR : T_vkGetPhysicalDeviceSurfaceCapabilities2KHR;
vkGetPhysicalDeviceSurfaceFormats2KHR : T_vkGetPhysicalDeviceSurfaceFormats2KHR;

{ from VK_KHR_get_display_properties2 }
vkGetPhysicalDeviceDisplayProperties2KHR : T_vkGetPhysicalDeviceDisplayProperties2KHR;
vkGetPhysicalDeviceDisplayPlaneProperties2KHR : T_vkGetPhysicalDeviceDisplayPlaneProperties2KHR;
vkGetDisplayModeProperties2KHR : T_vkGetDisplayModeProperties2KHR;
vkGetDisplayPlaneCapabilities2KHR : T_vkGetDisplayPlaneCapabilities2KHR;

{ from VK_KHR_get_memory_requirements2 }
vkGetBufferMemoryRequirements2KHR : T_vkGetBufferMemoryRequirements2;
vkGetImageMemoryRequirements2KHR : T_vkGetImageMemoryRequirements2;
vkGetImageSparseMemoryRequirements2KHR : T_vkGetImageSparseMemoryRequirements2;

{ from VK_KHR_sampler_ycbcr_conversion }
vkCreateSamplerYcbcrConversionKHR : T_vkCreateSamplerYcbcrConversion;
vkDestroySamplerYcbcrConversionKHR : T_vkDestroySamplerYcbcrConversion;

{ from VK_EXT_validation_cache }
vkCreateValidationCacheEXT : T_vkCreateValidationCacheEXT;
vkDestroyValidationCacheEXT : T_vkDestroyValidationCacheEXT;
vkGetValidationCacheDataEXT : T_vkGetValidationCacheDataEXT;
vkMergeValidationCachesEXT : T_vkMergeValidationCachesEXT;

{ from VK_KHR_maintenance3 }
vkGetDescriptorSetLayoutSupportKHR : T_vkGetDescriptorSetLayoutSupport;

{ from VK_ANDROID_native_buffer }
vkGetSwapchainGrallocUsageANDROID : T_vkGetSwapchainGrallocUsageANDROID;
vkGetSwapchainGrallocUsage2ANDROID : T_vkGetSwapchainGrallocUsage2ANDROID;
vkAcquireImageANDROID : T_vkAcquireImageANDROID;
vkQueueSignalReleaseImageANDROID : T_vkQueueSignalReleaseImageANDROID;

{ from VK_AMD_shader_info }
vkGetShaderInfoAMD : T_vkGetShaderInfoAMD;

{ from VK_AMD_display_native_hdr }
vkSetLocalDimmingAMD : T_vkSetLocalDimmingAMD;

{ from VK_EXT_calibrated_timestamps }
vkGetPhysicalDeviceCalibrateableTimeDomainsEXT : T_vkGetPhysicalDeviceCalibrateableTimeDomainsEXT;
vkGetCalibratedTimestampsEXT : T_vkGetCalibratedTimestampsEXT;

{ from VK_EXT_debug_utils }
vkSetDebugUtilsObjectNameEXT : T_vkSetDebugUtilsObjectNameEXT;
vkSetDebugUtilsObjectTagEXT : T_vkSetDebugUtilsObjectTagEXT;
vkQueueBeginDebugUtilsLabelEXT : T_vkQueueBeginDebugUtilsLabelEXT;
vkQueueEndDebugUtilsLabelEXT : T_vkQueueEndDebugUtilsLabelEXT;
vkQueueInsertDebugUtilsLabelEXT : T_vkQueueInsertDebugUtilsLabelEXT;
vkCmdBeginDebugUtilsLabelEXT : T_vkCmdBeginDebugUtilsLabelEXT;
vkCmdEndDebugUtilsLabelEXT : T_vkCmdEndDebugUtilsLabelEXT;
vkCmdInsertDebugUtilsLabelEXT : T_vkCmdInsertDebugUtilsLabelEXT;
vkCreateDebugUtilsMessengerEXT : T_vkCreateDebugUtilsMessengerEXT;
vkDestroyDebugUtilsMessengerEXT : T_vkDestroyDebugUtilsMessengerEXT;
vkSubmitDebugUtilsMessageEXT : T_vkSubmitDebugUtilsMessageEXT;

{ from VK_EXT_external_memory_host }
vkGetMemoryHostPointerPropertiesEXT : T_vkGetMemoryHostPointerPropertiesEXT;

{ from VK_AMD_buffer_marker }
vkCmdWriteBufferMarkerAMD : T_vkCmdWriteBufferMarkerAMD;

{ from VK_KHR_create_renderpass2 }
vkCreateRenderPass2KHR : T_vkCreateRenderPass2;
vkCmdBeginRenderPass2KHR : T_vkCmdBeginRenderPass2;
vkCmdNextSubpass2KHR : T_vkCmdNextSubpass2;
vkCmdEndRenderPass2KHR : T_vkCmdEndRenderPass2;

{ from VK_KHR_timeline_semaphore }
vkGetSemaphoreCounterValueKHR : T_vkGetSemaphoreCounterValue;
vkWaitSemaphoresKHR : T_vkWaitSemaphores;
vkSignalSemaphoreKHR : T_vkSignalSemaphore;

{ from VK_ANDROID_external_memory_android_hardware_buffer }
vkGetAndroidHardwareBufferPropertiesANDROID : T_vkGetAndroidHardwareBufferPropertiesANDROID;
vkGetMemoryAndroidHardwareBufferANDROID : T_vkGetMemoryAndroidHardwareBufferANDROID;

{ from VK_KHR_draw_indirect_count }
vkCmdDrawIndirectCountKHR : T_vkCmdDrawIndirectCount;
vkCmdDrawIndexedIndirectCountKHR : T_vkCmdDrawIndexedIndirectCount;

{ from VK_AMD_draw_indirect_count }
vkCmdDrawIndirectCountAMD : T_vkCmdDrawIndirectCount;
vkCmdDrawIndexedIndirectCountAMD : T_vkCmdDrawIndexedIndirectCount;

{ from VK_NV_device_diagnostic_checkpoints }
vkCmdSetCheckpointNV : T_vkCmdSetCheckpointNV;
vkGetQueueCheckpointDataNV : T_vkGetQueueCheckpointDataNV;

{ from VK_EXT_transform_feedback }
vkCmdBindTransformFeedbackBuffersEXT : T_vkCmdBindTransformFeedbackBuffersEXT;
vkCmdBeginTransformFeedbackEXT : T_vkCmdBeginTransformFeedbackEXT;
vkCmdEndTransformFeedbackEXT : T_vkCmdEndTransformFeedbackEXT;
vkCmdBeginQueryIndexedEXT : T_vkCmdBeginQueryIndexedEXT;
vkCmdEndQueryIndexedEXT : T_vkCmdEndQueryIndexedEXT;
vkCmdDrawIndirectByteCountEXT : T_vkCmdDrawIndirectByteCountEXT;

{ from VK_NV_scissor_exclusive }
vkCmdSetExclusiveScissorNV : T_vkCmdSetExclusiveScissorNV;

{ from VK_NV_shading_rate_image }
vkCmdBindShadingRateImageNV : T_vkCmdBindShadingRateImageNV;
vkCmdSetViewportShadingRatePaletteNV : T_vkCmdSetViewportShadingRatePaletteNV;
vkCmdSetCoarseSampleOrderNV : T_vkCmdSetCoarseSampleOrderNV;

{ from VK_NV_mesh_shader }
vkCmdDrawMeshTasksNV : T_vkCmdDrawMeshTasksNV;
vkCmdDrawMeshTasksIndirectNV : T_vkCmdDrawMeshTasksIndirectNV;
vkCmdDrawMeshTasksIndirectCountNV : T_vkCmdDrawMeshTasksIndirectCountNV;

{ from VK_NV_ray_tracing }
vkCompileDeferredNV : T_vkCompileDeferredNV;
vkCreateAccelerationStructureNV : T_vkCreateAccelerationStructureNV;
vkDestroyAccelerationStructureNV : T_vkDestroyAccelerationStructureNV;
vkGetAccelerationStructureMemoryRequirementsNV : T_vkGetAccelerationStructureMemoryRequirementsNV;
vkBindAccelerationStructureMemoryNV : T_vkBindAccelerationStructureMemoryNV;
vkCmdCopyAccelerationStructureNV : T_vkCmdCopyAccelerationStructureNV;
vkCmdWriteAccelerationStructuresPropertiesNV : T_vkCmdWriteAccelerationStructuresPropertiesNV;
vkCmdBuildAccelerationStructureNV : T_vkCmdBuildAccelerationStructureNV;
vkCmdTraceRaysNV : T_vkCmdTraceRaysNV;
vkGetRayTracingShaderGroupHandlesNV : T_vkGetRayTracingShaderGroupHandlesNV;
vkGetAccelerationStructureHandleNV : T_vkGetAccelerationStructureHandleNV;
vkCreateRayTracingPipelinesNV : T_vkCreateRayTracingPipelinesNV;

{ from VK_NV_cooperative_matrix }
vkGetPhysicalDeviceCooperativeMatrixPropertiesNV : T_vkGetPhysicalDeviceCooperativeMatrixPropertiesNV;

{ from VK_NVX_image_view_handle }
vkGetImageViewHandleNVX : T_vkGetImageViewHandleNVX;

{ from VK_EXT_full_screen_exclusive }
vkGetPhysicalDeviceSurfacePresentModes2EXT : T_vkGetPhysicalDeviceSurfacePresentModes2EXT;
vkGetDeviceGroupSurfacePresentModes2EXT : T_vkGetDeviceGroupSurfacePresentModes2EXT;
vkAcquireFullScreenExclusiveModeEXT : T_vkAcquireFullScreenExclusiveModeEXT;
vkReleaseFullScreenExclusiveModeEXT : T_vkReleaseFullScreenExclusiveModeEXT;

{ from VK_KHR_performance_query }
vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR : T_vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR;
vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR : T_vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR;
vkAcquireProfilingLockKHR : T_vkAcquireProfilingLockKHR;
vkReleaseProfilingLockKHR : T_vkReleaseProfilingLockKHR;

{ from VK_EXT_image_drm_format_modifier }
vkGetImageDrmFormatModifierPropertiesEXT : T_vkGetImageDrmFormatModifierPropertiesEXT;

{ from VK_KHR_buffer_device_address }
vkGetBufferOpaqueCaptureAddressKHR : T_vkGetBufferOpaqueCaptureAddress;
vkGetBufferDeviceAddressKHR : T_vkGetBufferDeviceAddress;
vkGetDeviceMemoryOpaqueCaptureAddressKHR : T_vkGetDeviceMemoryOpaqueCaptureAddress;

{ from VK_EXT_buffer_device_address }
vkGetBufferDeviceAddressEXT : T_vkGetBufferDeviceAddress;

{ from VK_EXT_headless_surface }
vkCreateHeadlessSurfaceEXT : T_vkCreateHeadlessSurfaceEXT;

{ from VK_NV_coverage_reduction_mode }
vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV : T_vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV;

{ from VK_INTEL_performance_query }
vkInitializePerformanceApiINTEL : T_vkInitializePerformanceApiINTEL;
vkUninitializePerformanceApiINTEL : T_vkUninitializePerformanceApiINTEL;
vkCmdSetPerformanceMarkerINTEL : T_vkCmdSetPerformanceMarkerINTEL;
vkCmdSetPerformanceStreamMarkerINTEL : T_vkCmdSetPerformanceStreamMarkerINTEL;
vkCmdSetPerformanceOverrideINTEL : T_vkCmdSetPerformanceOverrideINTEL;
vkAcquirePerformanceConfigurationINTEL : T_vkAcquirePerformanceConfigurationINTEL;
vkReleasePerformanceConfigurationINTEL : T_vkReleasePerformanceConfigurationINTEL;
vkQueueSetPerformanceConfigurationINTEL : T_vkQueueSetPerformanceConfigurationINTEL;
vkGetPerformanceParameterINTEL : T_vkGetPerformanceParameterINTEL;

{ from VK_KHR_pipeline_executable_properties }
vkGetPipelineExecutablePropertiesKHR : T_vkGetPipelineExecutablePropertiesKHR;
vkGetPipelineExecutableStatisticsKHR : T_vkGetPipelineExecutableStatisticsKHR;
vkGetPipelineExecutableInternalRepresentationsKHR : T_vkGetPipelineExecutableInternalRepresentationsKHR;

{ from VK_EXT_line_rasterization }
vkCmdSetLineStippleEXT : T_vkCmdSetLineStippleEXT;

{ from VK_EXT_tooling_info }
vkGetPhysicalDeviceToolPropertiesEXT : T_vkGetPhysicalDeviceToolPropertiesEXT;

var vulkan_available : boolean;
procedure load_all_vk(i:VkInstance; d:VkDevice);


implementation
uses vulkan_lib;

procedure load_vk(i:VkInstance; d:VkDevice; idx:longint; var p:pointer);
var p2 : pointer;
begin
  p2 := nil;
  if (d=VK_NIL_HANDLE) then begin
    if ((i<>VK_NIL_HANDLE)and(nil<>pointer(vkGetInstanceProcAddr)))
    then begin
      p2 := vkGetInstanceProcAddr(i,known_cmd_names[idx]);
      if not(p2=nil) then p:=p2;
    end else p:=nil;
  end else begin
    p2 := vkGetDeviceProcAddr(d,known_cmd_names[idx]);
    if not(p2=nil) then p:=p2;
  end;
end;

procedure load_VK_VERSION_1_0(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,0,pointer(vkCreateInstance));
  load_vk(i,d,1,pointer(vkDestroyInstance));
  load_vk(i,d,2,pointer(vkEnumeratePhysicalDevices));
  load_vk(i,d,3,pointer(vkGetDeviceProcAddr));
  load_vk(i,d,4,pointer(vkGetInstanceProcAddr));
  load_vk(i,d,5,pointer(vkGetPhysicalDeviceProperties));
  load_vk(i,d,6,pointer(vkGetPhysicalDeviceQueueFamilyProperties));
  load_vk(i,d,7,pointer(vkGetPhysicalDeviceMemoryProperties));
  load_vk(i,d,8,pointer(vkGetPhysicalDeviceFeatures));
  load_vk(i,d,9,pointer(vkGetPhysicalDeviceFormatProperties));
  load_vk(i,d,10,pointer(vkGetPhysicalDeviceImageFormatProperties));
  load_vk(i,d,11,pointer(vkCreateDevice));
  load_vk(i,d,12,pointer(vkDestroyDevice));
  load_vk(i,d,14,pointer(vkEnumerateInstanceLayerProperties));
  load_vk(i,d,15,pointer(vkEnumerateInstanceExtensionProperties));
  load_vk(i,d,16,pointer(vkEnumerateDeviceLayerProperties));
  load_vk(i,d,17,pointer(vkEnumerateDeviceExtensionProperties));
  load_vk(i,d,18,pointer(vkGetDeviceQueue));
  load_vk(i,d,19,pointer(vkQueueSubmit));
  load_vk(i,d,20,pointer(vkQueueWaitIdle));
  load_vk(i,d,21,pointer(vkDeviceWaitIdle));
  load_vk(i,d,22,pointer(vkAllocateMemory));
  load_vk(i,d,23,pointer(vkFreeMemory));
  load_vk(i,d,24,pointer(vkMapMemory));
  load_vk(i,d,25,pointer(vkUnmapMemory));
  load_vk(i,d,26,pointer(vkFlushMappedMemoryRanges));
  load_vk(i,d,27,pointer(vkInvalidateMappedMemoryRanges));
  load_vk(i,d,28,pointer(vkGetDeviceMemoryCommitment));
  load_vk(i,d,29,pointer(vkGetBufferMemoryRequirements));
  load_vk(i,d,30,pointer(vkBindBufferMemory));
  load_vk(i,d,31,pointer(vkGetImageMemoryRequirements));
  load_vk(i,d,32,pointer(vkBindImageMemory));
  load_vk(i,d,33,pointer(vkGetImageSparseMemoryRequirements));
  load_vk(i,d,34,pointer(vkGetPhysicalDeviceSparseImageFormatProperties));
  load_vk(i,d,35,pointer(vkQueueBindSparse));
  load_vk(i,d,36,pointer(vkCreateFence));
  load_vk(i,d,37,pointer(vkDestroyFence));
  load_vk(i,d,38,pointer(vkResetFences));
  load_vk(i,d,39,pointer(vkGetFenceStatus));
  load_vk(i,d,40,pointer(vkWaitForFences));
  load_vk(i,d,41,pointer(vkCreateSemaphore));
  load_vk(i,d,42,pointer(vkDestroySemaphore));
  load_vk(i,d,43,pointer(vkCreateEvent));
  load_vk(i,d,44,pointer(vkDestroyEvent));
  load_vk(i,d,45,pointer(vkGetEventStatus));
  load_vk(i,d,46,pointer(vkSetEvent));
  load_vk(i,d,47,pointer(vkResetEvent));
  load_vk(i,d,48,pointer(vkCreateQueryPool));
  load_vk(i,d,49,pointer(vkDestroyQueryPool));
  load_vk(i,d,50,pointer(vkGetQueryPoolResults));
  load_vk(i,d,53,pointer(vkCreateBuffer));
  load_vk(i,d,54,pointer(vkDestroyBuffer));
  load_vk(i,d,55,pointer(vkCreateBufferView));
  load_vk(i,d,56,pointer(vkDestroyBufferView));
  load_vk(i,d,57,pointer(vkCreateImage));
  load_vk(i,d,58,pointer(vkDestroyImage));
  load_vk(i,d,59,pointer(vkGetImageSubresourceLayout));
  load_vk(i,d,60,pointer(vkCreateImageView));
  load_vk(i,d,61,pointer(vkDestroyImageView));
  load_vk(i,d,62,pointer(vkCreateShaderModule));
  load_vk(i,d,63,pointer(vkDestroyShaderModule));
  load_vk(i,d,64,pointer(vkCreatePipelineCache));
  load_vk(i,d,65,pointer(vkDestroyPipelineCache));
  load_vk(i,d,66,pointer(vkGetPipelineCacheData));
  load_vk(i,d,67,pointer(vkMergePipelineCaches));
  load_vk(i,d,68,pointer(vkCreateGraphicsPipelines));
  load_vk(i,d,69,pointer(vkCreateComputePipelines));
  load_vk(i,d,70,pointer(vkDestroyPipeline));
  load_vk(i,d,71,pointer(vkCreatePipelineLayout));
  load_vk(i,d,72,pointer(vkDestroyPipelineLayout));
  load_vk(i,d,73,pointer(vkCreateSampler));
  load_vk(i,d,74,pointer(vkDestroySampler));
  load_vk(i,d,75,pointer(vkCreateDescriptorSetLayout));
  load_vk(i,d,76,pointer(vkDestroyDescriptorSetLayout));
  load_vk(i,d,77,pointer(vkCreateDescriptorPool));
  load_vk(i,d,78,pointer(vkDestroyDescriptorPool));
  load_vk(i,d,79,pointer(vkResetDescriptorPool));
  load_vk(i,d,80,pointer(vkAllocateDescriptorSets));
  load_vk(i,d,81,pointer(vkFreeDescriptorSets));
  load_vk(i,d,82,pointer(vkUpdateDescriptorSets));
  load_vk(i,d,83,pointer(vkCreateFramebuffer));
  load_vk(i,d,84,pointer(vkDestroyFramebuffer));
  load_vk(i,d,85,pointer(vkCreateRenderPass));
  load_vk(i,d,86,pointer(vkDestroyRenderPass));
  load_vk(i,d,87,pointer(vkGetRenderAreaGranularity));
  load_vk(i,d,88,pointer(vkCreateCommandPool));
  load_vk(i,d,89,pointer(vkDestroyCommandPool));
  load_vk(i,d,90,pointer(vkResetCommandPool));
  load_vk(i,d,91,pointer(vkAllocateCommandBuffers));
  load_vk(i,d,92,pointer(vkFreeCommandBuffers));
  load_vk(i,d,93,pointer(vkBeginCommandBuffer));
  load_vk(i,d,94,pointer(vkEndCommandBuffer));
  load_vk(i,d,95,pointer(vkResetCommandBuffer));
  load_vk(i,d,96,pointer(vkCmdBindPipeline));
  load_vk(i,d,97,pointer(vkCmdSetViewport));
  load_vk(i,d,98,pointer(vkCmdSetScissor));
  load_vk(i,d,99,pointer(vkCmdSetLineWidth));
  load_vk(i,d,100,pointer(vkCmdSetDepthBias));
  load_vk(i,d,101,pointer(vkCmdSetBlendConstants));
  load_vk(i,d,102,pointer(vkCmdSetDepthBounds));
  load_vk(i,d,103,pointer(vkCmdSetStencilCompareMask));
  load_vk(i,d,104,pointer(vkCmdSetStencilWriteMask));
  load_vk(i,d,105,pointer(vkCmdSetStencilReference));
  load_vk(i,d,106,pointer(vkCmdBindDescriptorSets));
  load_vk(i,d,107,pointer(vkCmdBindIndexBuffer));
  load_vk(i,d,108,pointer(vkCmdBindVertexBuffers));
  load_vk(i,d,109,pointer(vkCmdDraw));
  load_vk(i,d,110,pointer(vkCmdDrawIndexed));
  load_vk(i,d,111,pointer(vkCmdDrawIndirect));
  load_vk(i,d,112,pointer(vkCmdDrawIndexedIndirect));
  load_vk(i,d,113,pointer(vkCmdDispatch));
  load_vk(i,d,114,pointer(vkCmdDispatchIndirect));
  load_vk(i,d,115,pointer(vkCmdCopyBuffer));
  load_vk(i,d,116,pointer(vkCmdCopyImage));
  load_vk(i,d,117,pointer(vkCmdBlitImage));
  load_vk(i,d,118,pointer(vkCmdCopyBufferToImage));
  load_vk(i,d,119,pointer(vkCmdCopyImageToBuffer));
  load_vk(i,d,120,pointer(vkCmdUpdateBuffer));
  load_vk(i,d,121,pointer(vkCmdFillBuffer));
  load_vk(i,d,122,pointer(vkCmdClearColorImage));
  load_vk(i,d,123,pointer(vkCmdClearDepthStencilImage));
  load_vk(i,d,124,pointer(vkCmdClearAttachments));
  load_vk(i,d,125,pointer(vkCmdResolveImage));
  load_vk(i,d,126,pointer(vkCmdSetEvent));
  load_vk(i,d,127,pointer(vkCmdResetEvent));
  load_vk(i,d,128,pointer(vkCmdWaitEvents));
  load_vk(i,d,129,pointer(vkCmdPipelineBarrier));
  load_vk(i,d,130,pointer(vkCmdBeginQuery));
  load_vk(i,d,131,pointer(vkCmdEndQuery));
  load_vk(i,d,134,pointer(vkCmdResetQueryPool));
  load_vk(i,d,135,pointer(vkCmdWriteTimestamp));
  load_vk(i,d,136,pointer(vkCmdCopyQueryPoolResults));
  load_vk(i,d,137,pointer(vkCmdPushConstants));
  load_vk(i,d,138,pointer(vkCmdBeginRenderPass));
  load_vk(i,d,139,pointer(vkCmdNextSubpass));
  load_vk(i,d,140,pointer(vkCmdEndRenderPass));
  load_vk(i,d,141,pointer(vkCmdExecuteCommands));
end;

procedure load_VK_VERSION_1_1(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,13,pointer(vkEnumerateInstanceVersion));
  load_vk(i,d,191,pointer(vkGetPhysicalDeviceFeatures2));
  load_vk(i,d,193,pointer(vkGetPhysicalDeviceProperties2));
  load_vk(i,d,195,pointer(vkGetPhysicalDeviceFormatProperties2));
  load_vk(i,d,197,pointer(vkGetPhysicalDeviceImageFormatProperties2));
  load_vk(i,d,199,pointer(vkGetPhysicalDeviceQueueFamilyProperties2));
  load_vk(i,d,201,pointer(vkGetPhysicalDeviceMemoryProperties2));
  load_vk(i,d,203,pointer(vkGetPhysicalDeviceSparseImageFormatProperties2));
  load_vk(i,d,206,pointer(vkTrimCommandPool));
  load_vk(i,d,208,pointer(vkGetPhysicalDeviceExternalBufferProperties));
  load_vk(i,d,214,pointer(vkGetPhysicalDeviceExternalSemaphoreProperties));
  load_vk(i,d,220,pointer(vkGetPhysicalDeviceExternalFenceProperties));
  load_vk(i,d,234,pointer(vkEnumeratePhysicalDeviceGroups));
  load_vk(i,d,236,pointer(vkGetDeviceGroupPeerMemoryFeatures));
  load_vk(i,d,238,pointer(vkBindBufferMemory2));
  load_vk(i,d,240,pointer(vkBindImageMemory2));
  load_vk(i,d,242,pointer(vkCmdSetDeviceMask));
  load_vk(i,d,247,pointer(vkCmdDispatchBase));
  load_vk(i,d,250,pointer(vkCreateDescriptorUpdateTemplate));
  load_vk(i,d,252,pointer(vkDestroyDescriptorUpdateTemplate));
  load_vk(i,d,254,pointer(vkUpdateDescriptorSetWithTemplate));
  load_vk(i,d,274,pointer(vkGetBufferMemoryRequirements2));
  load_vk(i,d,276,pointer(vkGetImageMemoryRequirements2));
  load_vk(i,d,278,pointer(vkGetImageSparseMemoryRequirements2));
  load_vk(i,d,280,pointer(vkCreateSamplerYcbcrConversion));
  load_vk(i,d,282,pointer(vkDestroySamplerYcbcrConversion));
  load_vk(i,d,284,pointer(vkGetDeviceQueue2));
  load_vk(i,d,289,pointer(vkGetDescriptorSetLayoutSupport));
end;

procedure load_VK_VERSION_1_2(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,51,pointer(vkResetQueryPool));
  load_vk(i,d,312,pointer(vkCreateRenderPass2));
  load_vk(i,d,314,pointer(vkCmdBeginRenderPass2));
  load_vk(i,d,316,pointer(vkCmdNextSubpass2));
  load_vk(i,d,318,pointer(vkCmdEndRenderPass2));
  load_vk(i,d,320,pointer(vkGetSemaphoreCounterValue));
  load_vk(i,d,322,pointer(vkWaitSemaphores));
  load_vk(i,d,324,pointer(vkSignalSemaphore));
  load_vk(i,d,328,pointer(vkCmdDrawIndirectCount));
  load_vk(i,d,331,pointer(vkCmdDrawIndexedIndirectCount));
  load_vk(i,d,372,pointer(vkGetBufferOpaqueCaptureAddress));
  load_vk(i,d,374,pointer(vkGetBufferDeviceAddress));
  load_vk(i,d,388,pointer(vkGetDeviceMemoryOpaqueCaptureAddress));
end;

procedure load_VK_EXT_host_query_reset(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,52,pointer(vkResetQueryPoolEXT));
end;

procedure load_VK_EXT_conditional_rendering(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,132,pointer(vkCmdBeginConditionalRenderingEXT));
  load_vk(i,d,133,pointer(vkCmdEndConditionalRenderingEXT));
end;

procedure load_VK_KHR_android_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,142,pointer(vkCreateAndroidSurfaceKHR));
end;

procedure load_VK_KHR_display(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,143,pointer(vkGetPhysicalDeviceDisplayPropertiesKHR));
  load_vk(i,d,144,pointer(vkGetPhysicalDeviceDisplayPlanePropertiesKHR));
  load_vk(i,d,145,pointer(vkGetDisplayPlaneSupportedDisplaysKHR));
  load_vk(i,d,146,pointer(vkGetDisplayModePropertiesKHR));
  load_vk(i,d,147,pointer(vkCreateDisplayModeKHR));
  load_vk(i,d,148,pointer(vkGetDisplayPlaneCapabilitiesKHR));
  load_vk(i,d,149,pointer(vkCreateDisplayPlaneSurfaceKHR));
end;

procedure load_VK_KHR_display_swapchain(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,150,pointer(vkCreateSharedSwapchainsKHR));
end;

procedure load_VK_KHR_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,151,pointer(vkDestroySurfaceKHR));
  load_vk(i,d,152,pointer(vkGetPhysicalDeviceSurfaceSupportKHR));
  load_vk(i,d,153,pointer(vkGetPhysicalDeviceSurfaceCapabilitiesKHR));
  load_vk(i,d,154,pointer(vkGetPhysicalDeviceSurfaceFormatsKHR));
  load_vk(i,d,155,pointer(vkGetPhysicalDeviceSurfacePresentModesKHR));
end;

procedure load_VK_KHR_swapchain(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,156,pointer(vkCreateSwapchainKHR));
  load_vk(i,d,157,pointer(vkDestroySwapchainKHR));
  load_vk(i,d,158,pointer(vkGetSwapchainImagesKHR));
  load_vk(i,d,159,pointer(vkAcquireNextImageKHR));
  load_vk(i,d,160,pointer(vkQueuePresentKHR));
  load_vk(i,d,244,pointer(vkGetDeviceGroupPresentCapabilitiesKHR));
  load_vk(i,d,245,pointer(vkGetDeviceGroupSurfacePresentModesKHR));
  load_vk(i,d,246,pointer(vkAcquireNextImage2KHR));
  load_vk(i,d,249,pointer(vkGetPhysicalDevicePresentRectanglesKHR));
end;

procedure load_VK_NN_vi_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,161,pointer(vkCreateViSurfaceNN));
end;

procedure load_VK_KHR_wayland_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,162,pointer(vkCreateWaylandSurfaceKHR));
  load_vk(i,d,163,pointer(vkGetPhysicalDeviceWaylandPresentationSupportKHR));
end;

procedure load_VK_KHR_win32_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,164,pointer(vkCreateWin32SurfaceKHR));
  load_vk(i,d,165,pointer(vkGetPhysicalDeviceWin32PresentationSupportKHR));
end;

procedure load_VK_KHR_xlib_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,166,pointer(vkCreateXlibSurfaceKHR));
  load_vk(i,d,167,pointer(vkGetPhysicalDeviceXlibPresentationSupportKHR));
end;

procedure load_VK_KHR_xcb_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,168,pointer(vkCreateXcbSurfaceKHR));
  load_vk(i,d,169,pointer(vkGetPhysicalDeviceXcbPresentationSupportKHR));
end;

procedure load_VK_FUCHSIA_imagepipe_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,170,pointer(vkCreateImagePipeSurfaceFUCHSIA));
end;

procedure load_VK_GGP_stream_descriptor_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,171,pointer(vkCreateStreamDescriptorSurfaceGGP));
end;

procedure load_VK_EXT_debug_report(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,172,pointer(vkCreateDebugReportCallbackEXT));
  load_vk(i,d,173,pointer(vkDestroyDebugReportCallbackEXT));
  load_vk(i,d,174,pointer(vkDebugReportMessageEXT));
end;

procedure load_VK_EXT_debug_marker(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,175,pointer(vkDebugMarkerSetObjectNameEXT));
  load_vk(i,d,176,pointer(vkDebugMarkerSetObjectTagEXT));
  load_vk(i,d,177,pointer(vkCmdDebugMarkerBeginEXT));
  load_vk(i,d,178,pointer(vkCmdDebugMarkerEndEXT));
  load_vk(i,d,179,pointer(vkCmdDebugMarkerInsertEXT));
end;

procedure load_VK_NV_external_memory_capabilities(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,180,pointer(vkGetPhysicalDeviceExternalImageFormatPropertiesNV));
end;

procedure load_VK_NV_external_memory_win32(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,181,pointer(vkGetMemoryWin32HandleNV));
end;

procedure load_VK_NVX_device_generated_commands(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,182,pointer(vkCmdProcessCommandsNVX));
  load_vk(i,d,183,pointer(vkCmdReserveSpaceForCommandsNVX));
  load_vk(i,d,184,pointer(vkCreateIndirectCommandsLayoutNVX));
  load_vk(i,d,185,pointer(vkDestroyIndirectCommandsLayoutNVX));
  load_vk(i,d,186,pointer(vkCreateObjectTableNVX));
  load_vk(i,d,187,pointer(vkDestroyObjectTableNVX));
  load_vk(i,d,188,pointer(vkRegisterObjectsNVX));
  load_vk(i,d,189,pointer(vkUnregisterObjectsNVX));
  load_vk(i,d,190,pointer(vkGetPhysicalDeviceGeneratedCommandsPropertiesNVX));
end;

procedure load_VK_KHR_get_physical_device_properties2(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,192,pointer(vkGetPhysicalDeviceFeatures2KHR));
  load_vk(i,d,194,pointer(vkGetPhysicalDeviceProperties2KHR));
  load_vk(i,d,196,pointer(vkGetPhysicalDeviceFormatProperties2KHR));
  load_vk(i,d,198,pointer(vkGetPhysicalDeviceImageFormatProperties2KHR));
  load_vk(i,d,200,pointer(vkGetPhysicalDeviceQueueFamilyProperties2KHR));
  load_vk(i,d,202,pointer(vkGetPhysicalDeviceMemoryProperties2KHR));
  load_vk(i,d,204,pointer(vkGetPhysicalDeviceSparseImageFormatProperties2KHR));
end;

procedure load_VK_KHR_push_descriptor(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,205,pointer(vkCmdPushDescriptorSetKHR));
  load_vk(i,d,256,pointer(vkCmdPushDescriptorSetWithTemplateKHR));
end;

procedure load_VK_KHR_maintenance1(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,207,pointer(vkTrimCommandPoolKHR));
end;

procedure load_VK_KHR_external_memory_capabilities(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,209,pointer(vkGetPhysicalDeviceExternalBufferPropertiesKHR));
end;

procedure load_VK_KHR_external_memory_win32(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,210,pointer(vkGetMemoryWin32HandleKHR));
  load_vk(i,d,211,pointer(vkGetMemoryWin32HandlePropertiesKHR));
end;

procedure load_VK_KHR_external_memory_fd(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,212,pointer(vkGetMemoryFdKHR));
  load_vk(i,d,213,pointer(vkGetMemoryFdPropertiesKHR));
end;

procedure load_VK_KHR_external_semaphore_capabilities(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,215,pointer(vkGetPhysicalDeviceExternalSemaphorePropertiesKHR));
end;

procedure load_VK_KHR_external_semaphore_win32(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,216,pointer(vkGetSemaphoreWin32HandleKHR));
  load_vk(i,d,217,pointer(vkImportSemaphoreWin32HandleKHR));
end;

procedure load_VK_KHR_external_semaphore_fd(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,218,pointer(vkGetSemaphoreFdKHR));
  load_vk(i,d,219,pointer(vkImportSemaphoreFdKHR));
end;

procedure load_VK_KHR_external_fence_capabilities(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,221,pointer(vkGetPhysicalDeviceExternalFencePropertiesKHR));
end;

procedure load_VK_KHR_external_fence_win32(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,222,pointer(vkGetFenceWin32HandleKHR));
  load_vk(i,d,223,pointer(vkImportFenceWin32HandleKHR));
end;

procedure load_VK_KHR_external_fence_fd(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,224,pointer(vkGetFenceFdKHR));
  load_vk(i,d,225,pointer(vkImportFenceFdKHR));
end;

procedure load_VK_EXT_direct_mode_display(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,226,pointer(vkReleaseDisplayEXT));
end;

procedure load_VK_EXT_acquire_xlib_display(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,227,pointer(vkAcquireXlibDisplayEXT));
  load_vk(i,d,228,pointer(vkGetRandROutputDisplayEXT));
end;

procedure load_VK_EXT_display_control(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,229,pointer(vkDisplayPowerControlEXT));
  load_vk(i,d,230,pointer(vkRegisterDeviceEventEXT));
  load_vk(i,d,231,pointer(vkRegisterDisplayEventEXT));
  load_vk(i,d,232,pointer(vkGetSwapchainCounterEXT));
end;

procedure load_VK_EXT_display_surface_counter(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,233,pointer(vkGetPhysicalDeviceSurfaceCapabilities2EXT));
end;

procedure load_VK_KHR_device_group_creation(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,235,pointer(vkEnumeratePhysicalDeviceGroupsKHR));
end;

procedure load_VK_KHR_device_group(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,237,pointer(vkGetDeviceGroupPeerMemoryFeaturesKHR));
  load_vk(i,d,243,pointer(vkCmdSetDeviceMaskKHR));
  load_vk(i,d,248,pointer(vkCmdDispatchBaseKHR));
end;

procedure load_VK_KHR_bind_memory2(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,239,pointer(vkBindBufferMemory2KHR));
  load_vk(i,d,241,pointer(vkBindImageMemory2KHR));
end;

procedure load_VK_KHR_descriptor_update_template(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,251,pointer(vkCreateDescriptorUpdateTemplateKHR));
  load_vk(i,d,253,pointer(vkDestroyDescriptorUpdateTemplateKHR));
  load_vk(i,d,255,pointer(vkUpdateDescriptorSetWithTemplateKHR));
end;

procedure load_VK_EXT_hdr_metadata(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,257,pointer(vkSetHdrMetadataEXT));
end;

procedure load_VK_KHR_shared_presentable_image(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,258,pointer(vkGetSwapchainStatusKHR));
end;

procedure load_VK_GOOGLE_display_timing(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,259,pointer(vkGetRefreshCycleDurationGOOGLE));
  load_vk(i,d,260,pointer(vkGetPastPresentationTimingGOOGLE));
end;

procedure load_VK_MVK_ios_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,261,pointer(vkCreateIOSSurfaceMVK));
end;

procedure load_VK_MVK_macos_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,262,pointer(vkCreateMacOSSurfaceMVK));
end;

procedure load_VK_EXT_metal_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,263,pointer(vkCreateMetalSurfaceEXT));
end;

procedure load_VK_NV_clip_space_w_scaling(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,264,pointer(vkCmdSetViewportWScalingNV));
end;

procedure load_VK_EXT_discard_rectangles(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,265,pointer(vkCmdSetDiscardRectangleEXT));
end;

procedure load_VK_EXT_sample_locations(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,266,pointer(vkCmdSetSampleLocationsEXT));
  load_vk(i,d,267,pointer(vkGetPhysicalDeviceMultisamplePropertiesEXT));
end;

procedure load_VK_KHR_get_surface_capabilities2(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,268,pointer(vkGetPhysicalDeviceSurfaceCapabilities2KHR));
  load_vk(i,d,269,pointer(vkGetPhysicalDeviceSurfaceFormats2KHR));
end;

procedure load_VK_KHR_get_display_properties2(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,270,pointer(vkGetPhysicalDeviceDisplayProperties2KHR));
  load_vk(i,d,271,pointer(vkGetPhysicalDeviceDisplayPlaneProperties2KHR));
  load_vk(i,d,272,pointer(vkGetDisplayModeProperties2KHR));
  load_vk(i,d,273,pointer(vkGetDisplayPlaneCapabilities2KHR));
end;

procedure load_VK_KHR_get_memory_requirements2(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,275,pointer(vkGetBufferMemoryRequirements2KHR));
  load_vk(i,d,277,pointer(vkGetImageMemoryRequirements2KHR));
  load_vk(i,d,279,pointer(vkGetImageSparseMemoryRequirements2KHR));
end;

procedure load_VK_KHR_sampler_ycbcr_conversion(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,281,pointer(vkCreateSamplerYcbcrConversionKHR));
  load_vk(i,d,283,pointer(vkDestroySamplerYcbcrConversionKHR));
end;

procedure load_VK_EXT_validation_cache(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,285,pointer(vkCreateValidationCacheEXT));
  load_vk(i,d,286,pointer(vkDestroyValidationCacheEXT));
  load_vk(i,d,287,pointer(vkGetValidationCacheDataEXT));
  load_vk(i,d,288,pointer(vkMergeValidationCachesEXT));
end;

procedure load_VK_KHR_maintenance3(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,290,pointer(vkGetDescriptorSetLayoutSupportKHR));
end;

procedure load_VK_ANDROID_native_buffer(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,291,pointer(vkGetSwapchainGrallocUsageANDROID));
  load_vk(i,d,292,pointer(vkGetSwapchainGrallocUsage2ANDROID));
  load_vk(i,d,293,pointer(vkAcquireImageANDROID));
  load_vk(i,d,294,pointer(vkQueueSignalReleaseImageANDROID));
end;

procedure load_VK_AMD_shader_info(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,295,pointer(vkGetShaderInfoAMD));
end;

procedure load_VK_AMD_display_native_hdr(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,296,pointer(vkSetLocalDimmingAMD));
end;

procedure load_VK_EXT_calibrated_timestamps(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,297,pointer(vkGetPhysicalDeviceCalibrateableTimeDomainsEXT));
  load_vk(i,d,298,pointer(vkGetCalibratedTimestampsEXT));
end;

procedure load_VK_EXT_debug_utils(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,299,pointer(vkSetDebugUtilsObjectNameEXT));
  load_vk(i,d,300,pointer(vkSetDebugUtilsObjectTagEXT));
  load_vk(i,d,301,pointer(vkQueueBeginDebugUtilsLabelEXT));
  load_vk(i,d,302,pointer(vkQueueEndDebugUtilsLabelEXT));
  load_vk(i,d,303,pointer(vkQueueInsertDebugUtilsLabelEXT));
  load_vk(i,d,304,pointer(vkCmdBeginDebugUtilsLabelEXT));
  load_vk(i,d,305,pointer(vkCmdEndDebugUtilsLabelEXT));
  load_vk(i,d,306,pointer(vkCmdInsertDebugUtilsLabelEXT));
  load_vk(i,d,307,pointer(vkCreateDebugUtilsMessengerEXT));
  load_vk(i,d,308,pointer(vkDestroyDebugUtilsMessengerEXT));
  load_vk(i,d,309,pointer(vkSubmitDebugUtilsMessageEXT));
end;

procedure load_VK_EXT_external_memory_host(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,310,pointer(vkGetMemoryHostPointerPropertiesEXT));
end;

procedure load_VK_AMD_buffer_marker(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,311,pointer(vkCmdWriteBufferMarkerAMD));
end;

procedure load_VK_KHR_create_renderpass2(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,313,pointer(vkCreateRenderPass2KHR));
  load_vk(i,d,315,pointer(vkCmdBeginRenderPass2KHR));
  load_vk(i,d,317,pointer(vkCmdNextSubpass2KHR));
  load_vk(i,d,319,pointer(vkCmdEndRenderPass2KHR));
end;

procedure load_VK_KHR_timeline_semaphore(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,321,pointer(vkGetSemaphoreCounterValueKHR));
  load_vk(i,d,323,pointer(vkWaitSemaphoresKHR));
  load_vk(i,d,325,pointer(vkSignalSemaphoreKHR));
end;

procedure load_VK_ANDROID_external_memory_android_hardware_buffer(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,326,pointer(vkGetAndroidHardwareBufferPropertiesANDROID));
  load_vk(i,d,327,pointer(vkGetMemoryAndroidHardwareBufferANDROID));
end;

procedure load_VK_KHR_draw_indirect_count(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,329,pointer(vkCmdDrawIndirectCountKHR));
  load_vk(i,d,332,pointer(vkCmdDrawIndexedIndirectCountKHR));
end;

procedure load_VK_AMD_draw_indirect_count(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,330,pointer(vkCmdDrawIndirectCountAMD));
  load_vk(i,d,333,pointer(vkCmdDrawIndexedIndirectCountAMD));
end;

procedure load_VK_NV_device_diagnostic_checkpoints(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,334,pointer(vkCmdSetCheckpointNV));
  load_vk(i,d,335,pointer(vkGetQueueCheckpointDataNV));
end;

procedure load_VK_EXT_transform_feedback(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,336,pointer(vkCmdBindTransformFeedbackBuffersEXT));
  load_vk(i,d,337,pointer(vkCmdBeginTransformFeedbackEXT));
  load_vk(i,d,338,pointer(vkCmdEndTransformFeedbackEXT));
  load_vk(i,d,339,pointer(vkCmdBeginQueryIndexedEXT));
  load_vk(i,d,340,pointer(vkCmdEndQueryIndexedEXT));
  load_vk(i,d,341,pointer(vkCmdDrawIndirectByteCountEXT));
end;

procedure load_VK_NV_scissor_exclusive(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,342,pointer(vkCmdSetExclusiveScissorNV));
end;

procedure load_VK_NV_shading_rate_image(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,343,pointer(vkCmdBindShadingRateImageNV));
  load_vk(i,d,344,pointer(vkCmdSetViewportShadingRatePaletteNV));
  load_vk(i,d,345,pointer(vkCmdSetCoarseSampleOrderNV));
end;

procedure load_VK_NV_mesh_shader(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,346,pointer(vkCmdDrawMeshTasksNV));
  load_vk(i,d,347,pointer(vkCmdDrawMeshTasksIndirectNV));
  load_vk(i,d,348,pointer(vkCmdDrawMeshTasksIndirectCountNV));
end;

procedure load_VK_NV_ray_tracing(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,349,pointer(vkCompileDeferredNV));
  load_vk(i,d,350,pointer(vkCreateAccelerationStructureNV));
  load_vk(i,d,351,pointer(vkDestroyAccelerationStructureNV));
  load_vk(i,d,352,pointer(vkGetAccelerationStructureMemoryRequirementsNV));
  load_vk(i,d,353,pointer(vkBindAccelerationStructureMemoryNV));
  load_vk(i,d,354,pointer(vkCmdCopyAccelerationStructureNV));
  load_vk(i,d,355,pointer(vkCmdWriteAccelerationStructuresPropertiesNV));
  load_vk(i,d,356,pointer(vkCmdBuildAccelerationStructureNV));
  load_vk(i,d,357,pointer(vkCmdTraceRaysNV));
  load_vk(i,d,358,pointer(vkGetRayTracingShaderGroupHandlesNV));
  load_vk(i,d,359,pointer(vkGetAccelerationStructureHandleNV));
  load_vk(i,d,360,pointer(vkCreateRayTracingPipelinesNV));
end;

procedure load_VK_NV_cooperative_matrix(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,361,pointer(vkGetPhysicalDeviceCooperativeMatrixPropertiesNV));
end;

procedure load_VK_NVX_image_view_handle(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,362,pointer(vkGetImageViewHandleNVX));
end;

procedure load_VK_EXT_full_screen_exclusive(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,363,pointer(vkGetPhysicalDeviceSurfacePresentModes2EXT));
  load_vk(i,d,364,pointer(vkGetDeviceGroupSurfacePresentModes2EXT));
  load_vk(i,d,365,pointer(vkAcquireFullScreenExclusiveModeEXT));
  load_vk(i,d,366,pointer(vkReleaseFullScreenExclusiveModeEXT));
end;

procedure load_VK_KHR_performance_query(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,367,pointer(vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR));
  load_vk(i,d,368,pointer(vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR));
  load_vk(i,d,369,pointer(vkAcquireProfilingLockKHR));
  load_vk(i,d,370,pointer(vkReleaseProfilingLockKHR));
end;

procedure load_VK_EXT_image_drm_format_modifier(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,371,pointer(vkGetImageDrmFormatModifierPropertiesEXT));
end;

procedure load_VK_KHR_buffer_device_address(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,373,pointer(vkGetBufferOpaqueCaptureAddressKHR));
  load_vk(i,d,375,pointer(vkGetBufferDeviceAddressKHR));
  load_vk(i,d,389,pointer(vkGetDeviceMemoryOpaqueCaptureAddressKHR));
end;

procedure load_VK_EXT_buffer_device_address(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,376,pointer(vkGetBufferDeviceAddressEXT));
end;

procedure load_VK_EXT_headless_surface(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,377,pointer(vkCreateHeadlessSurfaceEXT));
end;

procedure load_VK_NV_coverage_reduction_mode(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,378,pointer(vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV));
end;

procedure load_VK_INTEL_performance_query(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,379,pointer(vkInitializePerformanceApiINTEL));
  load_vk(i,d,380,pointer(vkUninitializePerformanceApiINTEL));
  load_vk(i,d,381,pointer(vkCmdSetPerformanceMarkerINTEL));
  load_vk(i,d,382,pointer(vkCmdSetPerformanceStreamMarkerINTEL));
  load_vk(i,d,383,pointer(vkCmdSetPerformanceOverrideINTEL));
  load_vk(i,d,384,pointer(vkAcquirePerformanceConfigurationINTEL));
  load_vk(i,d,385,pointer(vkReleasePerformanceConfigurationINTEL));
  load_vk(i,d,386,pointer(vkQueueSetPerformanceConfigurationINTEL));
  load_vk(i,d,387,pointer(vkGetPerformanceParameterINTEL));
end;

procedure load_VK_KHR_pipeline_executable_properties(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,390,pointer(vkGetPipelineExecutablePropertiesKHR));
  load_vk(i,d,391,pointer(vkGetPipelineExecutableStatisticsKHR));
  load_vk(i,d,392,pointer(vkGetPipelineExecutableInternalRepresentationsKHR));
end;

procedure load_VK_EXT_line_rasterization(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,393,pointer(vkCmdSetLineStippleEXT));
end;

procedure load_VK_EXT_tooling_info(i:VkInstance; d:VkDevice);
begin
  load_vk(i,d,394,pointer(vkGetPhysicalDeviceToolPropertiesEXT));
end;

procedure load_all_vk(i:VkInstance; d:VkDevice);
begin
  load_VK_VERSION_1_0(i,d);
  load_VK_VERSION_1_1(i,d);
  load_VK_VERSION_1_2(i,d);
  load_VK_EXT_host_query_reset(i,d);
  load_VK_EXT_conditional_rendering(i,d);
  load_VK_KHR_android_surface(i,d);
  load_VK_KHR_display(i,d);
  load_VK_KHR_display_swapchain(i,d);
  load_VK_KHR_surface(i,d);
  load_VK_KHR_swapchain(i,d);
  load_VK_NN_vi_surface(i,d);
  load_VK_KHR_wayland_surface(i,d);
  load_VK_KHR_win32_surface(i,d);
  load_VK_KHR_xlib_surface(i,d);
  load_VK_KHR_xcb_surface(i,d);
  load_VK_FUCHSIA_imagepipe_surface(i,d);
  load_VK_GGP_stream_descriptor_surface(i,d);
  load_VK_EXT_debug_report(i,d);
  load_VK_EXT_debug_marker(i,d);
  load_VK_NV_external_memory_capabilities(i,d);
  load_VK_NV_external_memory_win32(i,d);
  load_VK_NVX_device_generated_commands(i,d);
  load_VK_KHR_get_physical_device_properties2(i,d);
  load_VK_KHR_push_descriptor(i,d);
  load_VK_KHR_maintenance1(i,d);
  load_VK_KHR_external_memory_capabilities(i,d);
  load_VK_KHR_external_memory_win32(i,d);
  load_VK_KHR_external_memory_fd(i,d);
  load_VK_KHR_external_semaphore_capabilities(i,d);
  load_VK_KHR_external_semaphore_win32(i,d);
  load_VK_KHR_external_semaphore_fd(i,d);
  load_VK_KHR_external_fence_capabilities(i,d);
  load_VK_KHR_external_fence_win32(i,d);
  load_VK_KHR_external_fence_fd(i,d);
  load_VK_EXT_direct_mode_display(i,d);
  load_VK_EXT_acquire_xlib_display(i,d);
  load_VK_EXT_display_control(i,d);
  load_VK_EXT_display_surface_counter(i,d);
  load_VK_KHR_device_group_creation(i,d);
  load_VK_KHR_device_group(i,d);
  load_VK_KHR_bind_memory2(i,d);
  load_VK_KHR_descriptor_update_template(i,d);
  load_VK_EXT_hdr_metadata(i,d);
  load_VK_KHR_shared_presentable_image(i,d);
  load_VK_GOOGLE_display_timing(i,d);
  load_VK_MVK_ios_surface(i,d);
  load_VK_MVK_macos_surface(i,d);
  load_VK_EXT_metal_surface(i,d);
  load_VK_NV_clip_space_w_scaling(i,d);
  load_VK_EXT_discard_rectangles(i,d);
  load_VK_EXT_sample_locations(i,d);
  load_VK_KHR_get_surface_capabilities2(i,d);
  load_VK_KHR_get_display_properties2(i,d);
  load_VK_KHR_get_memory_requirements2(i,d);
  load_VK_KHR_sampler_ycbcr_conversion(i,d);
  load_VK_EXT_validation_cache(i,d);
  load_VK_KHR_maintenance3(i,d);
  load_VK_ANDROID_native_buffer(i,d);
  load_VK_AMD_shader_info(i,d);
  load_VK_AMD_display_native_hdr(i,d);
  load_VK_EXT_calibrated_timestamps(i,d);
  load_VK_EXT_debug_utils(i,d);
  load_VK_EXT_external_memory_host(i,d);
  load_VK_AMD_buffer_marker(i,d);
  load_VK_KHR_create_renderpass2(i,d);
  load_VK_KHR_timeline_semaphore(i,d);
  load_VK_ANDROID_external_memory_android_hardware_buffer(i,d);
  load_VK_KHR_draw_indirect_count(i,d);
  load_VK_AMD_draw_indirect_count(i,d);
  load_VK_NV_device_diagnostic_checkpoints(i,d);
  load_VK_EXT_transform_feedback(i,d);
  load_VK_NV_scissor_exclusive(i,d);
  load_VK_NV_shading_rate_image(i,d);
  load_VK_NV_mesh_shader(i,d);
  load_VK_NV_ray_tracing(i,d);
  load_VK_NV_cooperative_matrix(i,d);
  load_VK_NVX_image_view_handle(i,d);
  load_VK_EXT_full_screen_exclusive(i,d);
  load_VK_KHR_performance_query(i,d);
  load_VK_EXT_image_drm_format_modifier(i,d);
  load_VK_KHR_buffer_device_address(i,d);
  load_VK_EXT_buffer_device_address(i,d);
  load_VK_EXT_headless_surface(i,d);
  load_VK_NV_coverage_reduction_mode(i,d);
  load_VK_INTEL_performance_query(i,d);
  load_VK_KHR_pipeline_executable_properties(i,d);
  load_VK_EXT_line_rasterization(i,d);
  load_VK_EXT_tooling_info(i,d);
end;

procedure init_vk();
begin
  vulkan_available := false;
  load_all_vk(VK_NIL_HANDLE,VK_NIL_HANDLE);
  pointer(vkGetInstanceProcAddr) := nil;
  pointer(vkGetInstanceProcAddr) := vulkan_getGIPA();
  if (nil<>pointer(vkGetInstanceProcAddr)) then begin
    pointer(vkEnumerateInstanceExtensionProperties) := vkGetInstanceProcAddr(VK_NIL_HANDLE,'vkEnumerateInstanceExtensionProperties');
    pointer(vkEnumerateInstanceLayerProperties)     := vkGetInstanceProcAddr(VK_NIL_HANDLE,'vkEnumerateInstanceLayerProperties');
    pointer(vkEnumerateInstanceVersion)             := vkGetInstanceProcAddr(VK_NIL_HANDLE,'vkEnumerateInstanceVersion');
    pointer(vkCreateInstance)                       := vkGetInstanceProcAddr(VK_NIL_HANDLE,'vkCreateInstance');
    vulkan_available := (nil<>pointer(vkCreateInstance));
  end;
end;



begin
  init_vk();
end.
