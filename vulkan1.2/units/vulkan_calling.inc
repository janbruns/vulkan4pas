{$macro on}

{$ifdef win32}
  {$define vulkan_callingconv:=stdcall}
  {$define vulkan_cb_callingconv:=stdcall}
{$else}
  {$define vulkan_callingconv:=cdecl}
  {$define vulkan_cb_callingconv:=cdecl}
{$endif}

{$ifdef CPU64}
  {$define vulkan_handle_type := class end}
  {$define vulkan_handle_dtype := class end}
  {$define VK_USING_CLASS_HANDLES}
{$else}
  {$define vulkan_handle_type := type qword}
  {$define VK_USING_QWORD_HANDLES}
  {$ifdef VK_HANDLES32_USE_CLASS}
    {$define vulkan_handle_dtype := class end}
  {$else}
    {$define vulkan_handle_dtype := type dword}
    {$define VK_HANDLES32_USING_DWORD}
  {$endif}
{$endif}
