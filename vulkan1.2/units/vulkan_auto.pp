{$mode objfpc}
unit vulkan_auto;

interface
uses vulkan_h, vulkan_class, vulkan_typeinit;

const maxvkargbytes = 1024*1024;

type
T_vulkan_auto=class(T_vulkan_API_commands_class)
  function vkgetmem(n : dword) : pointer; virtual;
  procedure vkfreemem(p : pointer; n : dword); virtual;

  procedure vknew(out a:Pdword);
  procedure vkdispose(var a:Pdword);
  procedure vkadd(out a:Pdword);
  procedure vkrem(var a:Pdword);

  procedure vknew(out a:Pqword);
  procedure vkdispose(var a:Pqword);
  procedure vkadd(out a:Pqword);
  procedure vkrem(var a:Pqword);

  procedure vknew(out a:Psingle);
  procedure vkdispose(var a:Psingle);
  procedure vkadd(out a:Psingle);
  procedure vkrem(var a:Psingle);

  procedure vkremitem(var a; len : SizeUInt);
  procedure vkadditem(out a; len : SizeUInt);
  procedure on_vk_auto_err(s : ansistring); virtual;

// aotogenerated from vk.xml:
  procedure vknew(out a:PVkInstance);
  procedure vkdispose(var a:PVkInstance);
  procedure vkadd(out a:PVkInstance);
  procedure vkrem(var a:PVkInstance);

  procedure vknew(out a:PVkPhysicalDevice);
  procedure vkdispose(var a:PVkPhysicalDevice);
  procedure vkadd(out a:PVkPhysicalDevice);
  procedure vkrem(var a:PVkPhysicalDevice);
  procedure vknew(out a:PVkDevice);
  procedure vkdispose(var a:PVkDevice);
  procedure vkadd(out a:PVkDevice);
  procedure vkrem(var a:PVkDevice);
  procedure vknew(out a:PVkQueue);
  procedure vkdispose(var a:PVkQueue);
  procedure vkadd(out a:PVkQueue);
  procedure vkrem(var a:PVkQueue);
  procedure vknew(out a:PVkCommandBuffer);
  procedure vkdispose(var a:PVkCommandBuffer);
  procedure vkadd(out a:PVkCommandBuffer);
  procedure vkrem(var a:PVkCommandBuffer);
  procedure vknew(out a:PVkDeviceMemory);
  procedure vkdispose(var a:PVkDeviceMemory);
  procedure vkadd(out a:PVkDeviceMemory);
  procedure vkrem(var a:PVkDeviceMemory);
  procedure vknew(out a:PVkCommandPool);
  procedure vkdispose(var a:PVkCommandPool);
  procedure vkadd(out a:PVkCommandPool);
  procedure vkrem(var a:PVkCommandPool);
  procedure vknew(out a:PVkBuffer);
  procedure vkdispose(var a:PVkBuffer);
  procedure vkadd(out a:PVkBuffer);
  procedure vkrem(var a:PVkBuffer);
  procedure vknew(out a:PVkBufferView);
  procedure vkdispose(var a:PVkBufferView);
  procedure vkadd(out a:PVkBufferView);
  procedure vkrem(var a:PVkBufferView);
  procedure vknew(out a:PVkImage);
  procedure vkdispose(var a:PVkImage);
  procedure vkadd(out a:PVkImage);
  procedure vkrem(var a:PVkImage);
  procedure vknew(out a:PVkImageView);
  procedure vkdispose(var a:PVkImageView);
  procedure vkadd(out a:PVkImageView);
  procedure vkrem(var a:PVkImageView);
  procedure vknew(out a:PVkShaderModule);
  procedure vkdispose(var a:PVkShaderModule);
  procedure vkadd(out a:PVkShaderModule);
  procedure vkrem(var a:PVkShaderModule);
  procedure vknew(out a:PVkPipeline);
  procedure vkdispose(var a:PVkPipeline);
  procedure vkadd(out a:PVkPipeline);
  procedure vkrem(var a:PVkPipeline);
  procedure vknew(out a:PVkPipelineLayout);
  procedure vkdispose(var a:PVkPipelineLayout);
  procedure vkadd(out a:PVkPipelineLayout);
  procedure vkrem(var a:PVkPipelineLayout);
  procedure vknew(out a:PVkSampler);
  procedure vkdispose(var a:PVkSampler);
  procedure vkadd(out a:PVkSampler);
  procedure vkrem(var a:PVkSampler);
  procedure vknew(out a:PVkDescriptorSet);
  procedure vkdispose(var a:PVkDescriptorSet);
  procedure vkadd(out a:PVkDescriptorSet);
  procedure vkrem(var a:PVkDescriptorSet);
  procedure vknew(out a:PVkDescriptorSetLayout);
  procedure vkdispose(var a:PVkDescriptorSetLayout);
  procedure vkadd(out a:PVkDescriptorSetLayout);
  procedure vkrem(var a:PVkDescriptorSetLayout);
  procedure vknew(out a:PVkDescriptorPool);
  procedure vkdispose(var a:PVkDescriptorPool);
  procedure vkadd(out a:PVkDescriptorPool);
  procedure vkrem(var a:PVkDescriptorPool);
  procedure vknew(out a:PVkFence);
  procedure vkdispose(var a:PVkFence);
  procedure vkadd(out a:PVkFence);
  procedure vkrem(var a:PVkFence);
  procedure vknew(out a:PVkSemaphore);
  procedure vkdispose(var a:PVkSemaphore);
  procedure vkadd(out a:PVkSemaphore);
  procedure vkrem(var a:PVkSemaphore);
  procedure vknew(out a:PVkEvent);
  procedure vkdispose(var a:PVkEvent);
  procedure vkadd(out a:PVkEvent);
  procedure vkrem(var a:PVkEvent);
  procedure vknew(out a:PVkQueryPool);
  procedure vkdispose(var a:PVkQueryPool);
  procedure vkadd(out a:PVkQueryPool);
  procedure vkrem(var a:PVkQueryPool);
  procedure vknew(out a:PVkFramebuffer);
  procedure vkdispose(var a:PVkFramebuffer);
  procedure vkadd(out a:PVkFramebuffer);
  procedure vkrem(var a:PVkFramebuffer);
  procedure vknew(out a:PVkRenderPass);
  procedure vkdispose(var a:PVkRenderPass);
  procedure vkadd(out a:PVkRenderPass);
  procedure vkrem(var a:PVkRenderPass);
  procedure vknew(out a:PVkPipelineCache);
  procedure vkdispose(var a:PVkPipelineCache);
  procedure vkadd(out a:PVkPipelineCache);
  procedure vkrem(var a:PVkPipelineCache);
  procedure vknew(out a:PVkObjectTableNVX);
  procedure vkdispose(var a:PVkObjectTableNVX);
  procedure vkadd(out a:PVkObjectTableNVX);
  procedure vkrem(var a:PVkObjectTableNVX);
  procedure vknew(out a:PVkIndirectCommandsLayoutNVX);
  procedure vkdispose(var a:PVkIndirectCommandsLayoutNVX);
  procedure vkadd(out a:PVkIndirectCommandsLayoutNVX);
  procedure vkrem(var a:PVkIndirectCommandsLayoutNVX);
  procedure vknew(out a:PVkDescriptorUpdateTemplate);
  procedure vkdispose(var a:PVkDescriptorUpdateTemplate);
  procedure vkadd(out a:PVkDescriptorUpdateTemplate);
  procedure vkrem(var a:PVkDescriptorUpdateTemplate);
  procedure vknew(out a:PVkSamplerYcbcrConversion);
  procedure vkdispose(var a:PVkSamplerYcbcrConversion);
  procedure vkadd(out a:PVkSamplerYcbcrConversion);
  procedure vkrem(var a:PVkSamplerYcbcrConversion);
  procedure vknew(out a:PVkValidationCacheEXT);
  procedure vkdispose(var a:PVkValidationCacheEXT);
  procedure vkadd(out a:PVkValidationCacheEXT);
  procedure vkrem(var a:PVkValidationCacheEXT);
  procedure vknew(out a:PVkAccelerationStructureNV);
  procedure vkdispose(var a:PVkAccelerationStructureNV);
  procedure vkadd(out a:PVkAccelerationStructureNV);
  procedure vkrem(var a:PVkAccelerationStructureNV);
  procedure vknew(out a:PVkPerformanceConfigurationINTEL);
  procedure vkdispose(var a:PVkPerformanceConfigurationINTEL);
  procedure vkadd(out a:PVkPerformanceConfigurationINTEL);
  procedure vkrem(var a:PVkPerformanceConfigurationINTEL);
  procedure vknew(out a:PVkDisplayKHR);
  procedure vkdispose(var a:PVkDisplayKHR);
  procedure vkadd(out a:PVkDisplayKHR);
  procedure vkrem(var a:PVkDisplayKHR);
  procedure vknew(out a:PVkDisplayModeKHR);
  procedure vkdispose(var a:PVkDisplayModeKHR);
  procedure vkadd(out a:PVkDisplayModeKHR);
  procedure vkrem(var a:PVkDisplayModeKHR);
  procedure vknew(out a:PVkSurfaceKHR);
  procedure vkdispose(var a:PVkSurfaceKHR);
  procedure vkadd(out a:PVkSurfaceKHR);
  procedure vkrem(var a:PVkSurfaceKHR);
  procedure vknew(out a:PVkSwapchainKHR);
  procedure vkdispose(var a:PVkSwapchainKHR);
  procedure vkadd(out a:PVkSwapchainKHR);
  procedure vkrem(var a:PVkSwapchainKHR);
  procedure vknew(out a:PVkDebugReportCallbackEXT);
  procedure vkdispose(var a:PVkDebugReportCallbackEXT);
  procedure vkadd(out a:PVkDebugReportCallbackEXT);
  procedure vkrem(var a:PVkDebugReportCallbackEXT);
  procedure vknew(out a:PVkDebugUtilsMessengerEXT);
  procedure vkdispose(var a:PVkDebugUtilsMessengerEXT);
  procedure vkadd(out a:PVkDebugUtilsMessengerEXT);
  procedure vkrem(var a:PVkDebugUtilsMessengerEXT);
  procedure vknew(out a:PVkOffset2D);
  procedure vkdispose(var a:PVkOffset2D);
  procedure vkadd(out a:PVkOffset2D);
  procedure vkrem(var a:PVkOffset2D);
  procedure vknew(out a:PVkOffset3D);
  procedure vkdispose(var a:PVkOffset3D);
  procedure vkadd(out a:PVkOffset3D);
  procedure vkrem(var a:PVkOffset3D);
  procedure vknew(out a:PVkExtent2D);
  procedure vkdispose(var a:PVkExtent2D);
  procedure vkadd(out a:PVkExtent2D);
  procedure vkrem(var a:PVkExtent2D);
  procedure vknew(out a:PVkExtent3D);
  procedure vkdispose(var a:PVkExtent3D);
  procedure vkadd(out a:PVkExtent3D);
  procedure vkrem(var a:PVkExtent3D);
  procedure vknew(out a:PVkViewport);
  procedure vkdispose(var a:PVkViewport);
  procedure vkadd(out a:PVkViewport);
  procedure vkrem(var a:PVkViewport);
  procedure vknew(out a:PVkRect2D);
  procedure vkdispose(var a:PVkRect2D);
  procedure vkadd(out a:PVkRect2D);
  procedure vkrem(var a:PVkRect2D);
  procedure vknew(out a:PVkClearRect);
  procedure vkdispose(var a:PVkClearRect);
  procedure vkadd(out a:PVkClearRect);
  procedure vkrem(var a:PVkClearRect);
  procedure vknew(out a:PVkComponentMapping);
  procedure vkdispose(var a:PVkComponentMapping);
  procedure vkadd(out a:PVkComponentMapping);
  procedure vkrem(var a:PVkComponentMapping);
  procedure vknew(out a:PVkExtensionProperties);
  procedure vkdispose(var a:PVkExtensionProperties);
  procedure vkadd(out a:PVkExtensionProperties);
  procedure vkrem(var a:PVkExtensionProperties);
  procedure vknew(out a:PVkLayerProperties);
  procedure vkdispose(var a:PVkLayerProperties);
  procedure vkadd(out a:PVkLayerProperties);
  procedure vkrem(var a:PVkLayerProperties);
  procedure vknew(out a:PVkApplicationInfo);
  procedure vkdispose(var a:PVkApplicationInfo);
  procedure vkadd(out a:PVkApplicationInfo);
  procedure vkrem(var a:PVkApplicationInfo);
  procedure vknew(out a:PVkDeviceQueueCreateInfo);
  procedure vkdispose(var a:PVkDeviceQueueCreateInfo);
  procedure vkadd(out a:PVkDeviceQueueCreateInfo);
  procedure vkrem(var a:PVkDeviceQueueCreateInfo);
  procedure vknew(out a:PVkInstanceCreateInfo);
  procedure vkdispose(var a:PVkInstanceCreateInfo);
  procedure vkadd(out a:PVkInstanceCreateInfo);
  procedure vkrem(var a:PVkInstanceCreateInfo);
  procedure vknew(out a:PVkQueueFamilyProperties);
  procedure vkdispose(var a:PVkQueueFamilyProperties);
  procedure vkadd(out a:PVkQueueFamilyProperties);
  procedure vkrem(var a:PVkQueueFamilyProperties);
  procedure vknew(out a:PVkMemoryAllocateInfo);
  procedure vkdispose(var a:PVkMemoryAllocateInfo);
  procedure vkadd(out a:PVkMemoryAllocateInfo);
  procedure vkrem(var a:PVkMemoryAllocateInfo);
  procedure vknew(out a:PVkMemoryRequirements);
  procedure vkdispose(var a:PVkMemoryRequirements);
  procedure vkadd(out a:PVkMemoryRequirements);
  procedure vkrem(var a:PVkMemoryRequirements);
  procedure vknew(out a:PVkSparseImageFormatProperties);
  procedure vkdispose(var a:PVkSparseImageFormatProperties);
  procedure vkadd(out a:PVkSparseImageFormatProperties);
  procedure vkrem(var a:PVkSparseImageFormatProperties);
  procedure vknew(out a:PVkSparseImageMemoryRequirements);
  procedure vkdispose(var a:PVkSparseImageMemoryRequirements);
  procedure vkadd(out a:PVkSparseImageMemoryRequirements);
  procedure vkrem(var a:PVkSparseImageMemoryRequirements);
  procedure vknew(out a:PVkMemoryType);
  procedure vkdispose(var a:PVkMemoryType);
  procedure vkadd(out a:PVkMemoryType);
  procedure vkrem(var a:PVkMemoryType);
  procedure vknew(out a:PVkMemoryHeap);
  procedure vkdispose(var a:PVkMemoryHeap);
  procedure vkadd(out a:PVkMemoryHeap);
  procedure vkrem(var a:PVkMemoryHeap);
  procedure vknew(out a:PVkMappedMemoryRange);
  procedure vkdispose(var a:PVkMappedMemoryRange);
  procedure vkadd(out a:PVkMappedMemoryRange);
  procedure vkrem(var a:PVkMappedMemoryRange);
  procedure vknew(out a:PVkFormatProperties);
  procedure vkdispose(var a:PVkFormatProperties);
  procedure vkadd(out a:PVkFormatProperties);
  procedure vkrem(var a:PVkFormatProperties);
  procedure vknew(out a:PVkImageFormatProperties);
  procedure vkdispose(var a:PVkImageFormatProperties);
  procedure vkadd(out a:PVkImageFormatProperties);
  procedure vkrem(var a:PVkImageFormatProperties);
  procedure vknew(out a:PVkDescriptorBufferInfo);
  procedure vkdispose(var a:PVkDescriptorBufferInfo);
  procedure vkadd(out a:PVkDescriptorBufferInfo);
  procedure vkrem(var a:PVkDescriptorBufferInfo);
  procedure vknew(out a:PVkDescriptorImageInfo);
  procedure vkdispose(var a:PVkDescriptorImageInfo);
  procedure vkadd(out a:PVkDescriptorImageInfo);
  procedure vkrem(var a:PVkDescriptorImageInfo);
  procedure vknew(out a:PVkWriteDescriptorSet);
  procedure vkdispose(var a:PVkWriteDescriptorSet);
  procedure vkadd(out a:PVkWriteDescriptorSet);
  procedure vkrem(var a:PVkWriteDescriptorSet);
  procedure vknew(out a:PVkCopyDescriptorSet);
  procedure vkdispose(var a:PVkCopyDescriptorSet);
  procedure vkadd(out a:PVkCopyDescriptorSet);
  procedure vkrem(var a:PVkCopyDescriptorSet);
  procedure vknew(out a:PVkBufferCreateInfo);
  procedure vkdispose(var a:PVkBufferCreateInfo);
  procedure vkadd(out a:PVkBufferCreateInfo);
  procedure vkrem(var a:PVkBufferCreateInfo);
  procedure vknew(out a:PVkBufferViewCreateInfo);
  procedure vkdispose(var a:PVkBufferViewCreateInfo);
  procedure vkadd(out a:PVkBufferViewCreateInfo);
  procedure vkrem(var a:PVkBufferViewCreateInfo);
  procedure vknew(out a:PVkImageSubresource);
  procedure vkdispose(var a:PVkImageSubresource);
  procedure vkadd(out a:PVkImageSubresource);
  procedure vkrem(var a:PVkImageSubresource);
  procedure vknew(out a:PVkImageSubresourceLayers);
  procedure vkdispose(var a:PVkImageSubresourceLayers);
  procedure vkadd(out a:PVkImageSubresourceLayers);
  procedure vkrem(var a:PVkImageSubresourceLayers);
  procedure vknew(out a:PVkImageSubresourceRange);
  procedure vkdispose(var a:PVkImageSubresourceRange);
  procedure vkadd(out a:PVkImageSubresourceRange);
  procedure vkrem(var a:PVkImageSubresourceRange);
  procedure vknew(out a:PVkMemoryBarrier);
  procedure vkdispose(var a:PVkMemoryBarrier);
  procedure vkadd(out a:PVkMemoryBarrier);
  procedure vkrem(var a:PVkMemoryBarrier);
  procedure vknew(out a:PVkBufferMemoryBarrier);
  procedure vkdispose(var a:PVkBufferMemoryBarrier);
  procedure vkadd(out a:PVkBufferMemoryBarrier);
  procedure vkrem(var a:PVkBufferMemoryBarrier);
  procedure vknew(out a:PVkImageMemoryBarrier);
  procedure vkdispose(var a:PVkImageMemoryBarrier);
  procedure vkadd(out a:PVkImageMemoryBarrier);
  procedure vkrem(var a:PVkImageMemoryBarrier);
  procedure vknew(out a:PVkImageCreateInfo);
  procedure vkdispose(var a:PVkImageCreateInfo);
  procedure vkadd(out a:PVkImageCreateInfo);
  procedure vkrem(var a:PVkImageCreateInfo);
  procedure vknew(out a:PVkSubresourceLayout);
  procedure vkdispose(var a:PVkSubresourceLayout);
  procedure vkadd(out a:PVkSubresourceLayout);
  procedure vkrem(var a:PVkSubresourceLayout);
  procedure vknew(out a:PVkImageViewCreateInfo);
  procedure vkdispose(var a:PVkImageViewCreateInfo);
  procedure vkadd(out a:PVkImageViewCreateInfo);
  procedure vkrem(var a:PVkImageViewCreateInfo);
  procedure vknew(out a:PVkBufferCopy);
  procedure vkdispose(var a:PVkBufferCopy);
  procedure vkadd(out a:PVkBufferCopy);
  procedure vkrem(var a:PVkBufferCopy);
  procedure vknew(out a:PVkSparseMemoryBind);
  procedure vkdispose(var a:PVkSparseMemoryBind);
  procedure vkadd(out a:PVkSparseMemoryBind);
  procedure vkrem(var a:PVkSparseMemoryBind);
  procedure vknew(out a:PVkSparseImageMemoryBind);
  procedure vkdispose(var a:PVkSparseImageMemoryBind);
  procedure vkadd(out a:PVkSparseImageMemoryBind);
  procedure vkrem(var a:PVkSparseImageMemoryBind);
  procedure vknew(out a:PVkSparseBufferMemoryBindInfo);
  procedure vkdispose(var a:PVkSparseBufferMemoryBindInfo);
  procedure vkadd(out a:PVkSparseBufferMemoryBindInfo);
  procedure vkrem(var a:PVkSparseBufferMemoryBindInfo);
  procedure vknew(out a:PVkSparseImageOpaqueMemoryBindInfo);
  procedure vkdispose(var a:PVkSparseImageOpaqueMemoryBindInfo);
  procedure vkadd(out a:PVkSparseImageOpaqueMemoryBindInfo);
  procedure vkrem(var a:PVkSparseImageOpaqueMemoryBindInfo);
  procedure vknew(out a:PVkSparseImageMemoryBindInfo);
  procedure vkdispose(var a:PVkSparseImageMemoryBindInfo);
  procedure vkadd(out a:PVkSparseImageMemoryBindInfo);
  procedure vkrem(var a:PVkSparseImageMemoryBindInfo);
  procedure vknew(out a:PVkBindSparseInfo);
  procedure vkdispose(var a:PVkBindSparseInfo);
  procedure vkadd(out a:PVkBindSparseInfo);
  procedure vkrem(var a:PVkBindSparseInfo);
  procedure vknew(out a:PVkImageCopy);
  procedure vkdispose(var a:PVkImageCopy);
  procedure vkadd(out a:PVkImageCopy);
  procedure vkrem(var a:PVkImageCopy);
  procedure vknew(out a:PVkImageBlit);
  procedure vkdispose(var a:PVkImageBlit);
  procedure vkadd(out a:PVkImageBlit);
  procedure vkrem(var a:PVkImageBlit);
  procedure vknew(out a:PVkBufferImageCopy);
  procedure vkdispose(var a:PVkBufferImageCopy);
  procedure vkadd(out a:PVkBufferImageCopy);
  procedure vkrem(var a:PVkBufferImageCopy);
  procedure vknew(out a:PVkImageResolve);
  procedure vkdispose(var a:PVkImageResolve);
  procedure vkadd(out a:PVkImageResolve);
  procedure vkrem(var a:PVkImageResolve);
  procedure vknew(out a:PVkShaderModuleCreateInfo);
  procedure vkdispose(var a:PVkShaderModuleCreateInfo);
  procedure vkadd(out a:PVkShaderModuleCreateInfo);
  procedure vkrem(var a:PVkShaderModuleCreateInfo);
  procedure vknew(out a:PVkDescriptorSetLayoutBinding);
  procedure vkdispose(var a:PVkDescriptorSetLayoutBinding);
  procedure vkadd(out a:PVkDescriptorSetLayoutBinding);
  procedure vkrem(var a:PVkDescriptorSetLayoutBinding);
  procedure vknew(out a:PVkDescriptorSetLayoutCreateInfo);
  procedure vkdispose(var a:PVkDescriptorSetLayoutCreateInfo);
  procedure vkadd(out a:PVkDescriptorSetLayoutCreateInfo);
  procedure vkrem(var a:PVkDescriptorSetLayoutCreateInfo);
  procedure vknew(out a:PVkDescriptorPoolSize);
  procedure vkdispose(var a:PVkDescriptorPoolSize);
  procedure vkadd(out a:PVkDescriptorPoolSize);
  procedure vkrem(var a:PVkDescriptorPoolSize);
  procedure vknew(out a:PVkDescriptorPoolCreateInfo);
  procedure vkdispose(var a:PVkDescriptorPoolCreateInfo);
  procedure vkadd(out a:PVkDescriptorPoolCreateInfo);
  procedure vkrem(var a:PVkDescriptorPoolCreateInfo);
  procedure vknew(out a:PVkDescriptorSetAllocateInfo);
  procedure vkdispose(var a:PVkDescriptorSetAllocateInfo);
  procedure vkadd(out a:PVkDescriptorSetAllocateInfo);
  procedure vkrem(var a:PVkDescriptorSetAllocateInfo);
  procedure vknew(out a:PVkSpecializationMapEntry);
  procedure vkdispose(var a:PVkSpecializationMapEntry);
  procedure vkadd(out a:PVkSpecializationMapEntry);
  procedure vkrem(var a:PVkSpecializationMapEntry);
  procedure vknew(out a:PVkSpecializationInfo);
  procedure vkdispose(var a:PVkSpecializationInfo);
  procedure vkadd(out a:PVkSpecializationInfo);
  procedure vkrem(var a:PVkSpecializationInfo);
  procedure vknew(out a:PVkPipelineShaderStageCreateInfo);
  procedure vkdispose(var a:PVkPipelineShaderStageCreateInfo);
  procedure vkadd(out a:PVkPipelineShaderStageCreateInfo);
  procedure vkrem(var a:PVkPipelineShaderStageCreateInfo);
  procedure vknew(out a:PVkComputePipelineCreateInfo);
  procedure vkdispose(var a:PVkComputePipelineCreateInfo);
  procedure vkadd(out a:PVkComputePipelineCreateInfo);
  procedure vkrem(var a:PVkComputePipelineCreateInfo);
  procedure vknew(out a:PVkVertexInputBindingDescription);
  procedure vkdispose(var a:PVkVertexInputBindingDescription);
  procedure vkadd(out a:PVkVertexInputBindingDescription);
  procedure vkrem(var a:PVkVertexInputBindingDescription);
  procedure vknew(out a:PVkVertexInputAttributeDescription);
  procedure vkdispose(var a:PVkVertexInputAttributeDescription);
  procedure vkadd(out a:PVkVertexInputAttributeDescription);
  procedure vkrem(var a:PVkVertexInputAttributeDescription);
  procedure vknew(out a:PVkPipelineVertexInputStateCreateInfo);
  procedure vkdispose(var a:PVkPipelineVertexInputStateCreateInfo);
  procedure vkadd(out a:PVkPipelineVertexInputStateCreateInfo);
  procedure vkrem(var a:PVkPipelineVertexInputStateCreateInfo);
  procedure vknew(out a:PVkPipelineInputAssemblyStateCreateInfo);
  procedure vkdispose(var a:PVkPipelineInputAssemblyStateCreateInfo);
  procedure vkadd(out a:PVkPipelineInputAssemblyStateCreateInfo);
  procedure vkrem(var a:PVkPipelineInputAssemblyStateCreateInfo);
  procedure vknew(out a:PVkPipelineTessellationStateCreateInfo);
  procedure vkdispose(var a:PVkPipelineTessellationStateCreateInfo);
  procedure vkadd(out a:PVkPipelineTessellationStateCreateInfo);
  procedure vkrem(var a:PVkPipelineTessellationStateCreateInfo);
  procedure vknew(out a:PVkPipelineViewportStateCreateInfo);
  procedure vkdispose(var a:PVkPipelineViewportStateCreateInfo);
  procedure vkadd(out a:PVkPipelineViewportStateCreateInfo);
  procedure vkrem(var a:PVkPipelineViewportStateCreateInfo);
  procedure vknew(out a:PVkPipelineRasterizationStateCreateInfo);
  procedure vkdispose(var a:PVkPipelineRasterizationStateCreateInfo);
  procedure vkadd(out a:PVkPipelineRasterizationStateCreateInfo);
  procedure vkrem(var a:PVkPipelineRasterizationStateCreateInfo);
  procedure vknew(out a:PVkPipelineMultisampleStateCreateInfo);
  procedure vkdispose(var a:PVkPipelineMultisampleStateCreateInfo);
  procedure vkadd(out a:PVkPipelineMultisampleStateCreateInfo);
  procedure vkrem(var a:PVkPipelineMultisampleStateCreateInfo);
  procedure vknew(out a:PVkPipelineColorBlendAttachmentState);
  procedure vkdispose(var a:PVkPipelineColorBlendAttachmentState);
  procedure vkadd(out a:PVkPipelineColorBlendAttachmentState);
  procedure vkrem(var a:PVkPipelineColorBlendAttachmentState);
  procedure vknew(out a:PVkPipelineColorBlendStateCreateInfo);
  procedure vkdispose(var a:PVkPipelineColorBlendStateCreateInfo);
  procedure vkadd(out a:PVkPipelineColorBlendStateCreateInfo);
  procedure vkrem(var a:PVkPipelineColorBlendStateCreateInfo);
  procedure vknew(out a:PVkPipelineDynamicStateCreateInfo);
  procedure vkdispose(var a:PVkPipelineDynamicStateCreateInfo);
  procedure vkadd(out a:PVkPipelineDynamicStateCreateInfo);
  procedure vkrem(var a:PVkPipelineDynamicStateCreateInfo);
  procedure vknew(out a:PVkStencilOpState);
  procedure vkdispose(var a:PVkStencilOpState);
  procedure vkadd(out a:PVkStencilOpState);
  procedure vkrem(var a:PVkStencilOpState);
  procedure vknew(out a:PVkPipelineDepthStencilStateCreateInfo);
  procedure vkdispose(var a:PVkPipelineDepthStencilStateCreateInfo);
  procedure vkadd(out a:PVkPipelineDepthStencilStateCreateInfo);
  procedure vkrem(var a:PVkPipelineDepthStencilStateCreateInfo);
  procedure vknew(out a:PVkGraphicsPipelineCreateInfo);
  procedure vkdispose(var a:PVkGraphicsPipelineCreateInfo);
  procedure vkadd(out a:PVkGraphicsPipelineCreateInfo);
  procedure vkrem(var a:PVkGraphicsPipelineCreateInfo);
  procedure vknew(out a:PVkPipelineCacheCreateInfo);
  procedure vkdispose(var a:PVkPipelineCacheCreateInfo);
  procedure vkadd(out a:PVkPipelineCacheCreateInfo);
  procedure vkrem(var a:PVkPipelineCacheCreateInfo);
  procedure vknew(out a:PVkPushConstantRange);
  procedure vkdispose(var a:PVkPushConstantRange);
  procedure vkadd(out a:PVkPushConstantRange);
  procedure vkrem(var a:PVkPushConstantRange);
  procedure vknew(out a:PVkPipelineLayoutCreateInfo);
  procedure vkdispose(var a:PVkPipelineLayoutCreateInfo);
  procedure vkadd(out a:PVkPipelineLayoutCreateInfo);
  procedure vkrem(var a:PVkPipelineLayoutCreateInfo);
  procedure vknew(out a:PVkSamplerCreateInfo);
  procedure vkdispose(var a:PVkSamplerCreateInfo);
  procedure vkadd(out a:PVkSamplerCreateInfo);
  procedure vkrem(var a:PVkSamplerCreateInfo);
  procedure vknew(out a:PVkCommandPoolCreateInfo);
  procedure vkdispose(var a:PVkCommandPoolCreateInfo);
  procedure vkadd(out a:PVkCommandPoolCreateInfo);
  procedure vkrem(var a:PVkCommandPoolCreateInfo);
  procedure vknew(out a:PVkCommandBufferAllocateInfo);
  procedure vkdispose(var a:PVkCommandBufferAllocateInfo);
  procedure vkadd(out a:PVkCommandBufferAllocateInfo);
  procedure vkrem(var a:PVkCommandBufferAllocateInfo);
  procedure vknew(out a:PVkCommandBufferInheritanceInfo);
  procedure vkdispose(var a:PVkCommandBufferInheritanceInfo);
  procedure vkadd(out a:PVkCommandBufferInheritanceInfo);
  procedure vkrem(var a:PVkCommandBufferInheritanceInfo);
  procedure vknew(out a:PVkCommandBufferBeginInfo);
  procedure vkdispose(var a:PVkCommandBufferBeginInfo);
  procedure vkadd(out a:PVkCommandBufferBeginInfo);
  procedure vkrem(var a:PVkCommandBufferBeginInfo);
  procedure vknew(out a:PVkClearColorValue);
  procedure vkdispose(var a:PVkClearColorValue);
  procedure vkadd(out a:PVkClearColorValue);
  procedure vkrem(var a:PVkClearColorValue);
  procedure vknew(out a:PVkClearDepthStencilValue);
  procedure vkdispose(var a:PVkClearDepthStencilValue);
  procedure vkadd(out a:PVkClearDepthStencilValue);
  procedure vkrem(var a:PVkClearDepthStencilValue);
  procedure vknew(out a:PVkClearValue);
  procedure vkdispose(var a:PVkClearValue);
  procedure vkadd(out a:PVkClearValue);
  procedure vkrem(var a:PVkClearValue);
  procedure vknew(out a:PVkClearAttachment);
  procedure vkdispose(var a:PVkClearAttachment);
  procedure vkadd(out a:PVkClearAttachment);
  procedure vkrem(var a:PVkClearAttachment);
  procedure vknew(out a:PVkAttachmentDescription);
  procedure vkdispose(var a:PVkAttachmentDescription);
  procedure vkadd(out a:PVkAttachmentDescription);
  procedure vkrem(var a:PVkAttachmentDescription);
  procedure vknew(out a:PVkAttachmentReference);
  procedure vkdispose(var a:PVkAttachmentReference);
  procedure vkadd(out a:PVkAttachmentReference);
  procedure vkrem(var a:PVkAttachmentReference);
  procedure vknew(out a:PVkSubpassDescription);
  procedure vkdispose(var a:PVkSubpassDescription);
  procedure vkadd(out a:PVkSubpassDescription);
  procedure vkrem(var a:PVkSubpassDescription);
  procedure vknew(out a:PVkSubpassDependency);
  procedure vkdispose(var a:PVkSubpassDependency);
  procedure vkadd(out a:PVkSubpassDependency);
  procedure vkrem(var a:PVkSubpassDependency);
  procedure vknew(out a:PVkRenderPassCreateInfo);
  procedure vkdispose(var a:PVkRenderPassCreateInfo);
  procedure vkadd(out a:PVkRenderPassCreateInfo);
  procedure vkrem(var a:PVkRenderPassCreateInfo);
  procedure vknew(out a:PVkEventCreateInfo);
  procedure vkdispose(var a:PVkEventCreateInfo);
  procedure vkadd(out a:PVkEventCreateInfo);
  procedure vkrem(var a:PVkEventCreateInfo);
  procedure vknew(out a:PVkFenceCreateInfo);
  procedure vkdispose(var a:PVkFenceCreateInfo);
  procedure vkadd(out a:PVkFenceCreateInfo);
  procedure vkrem(var a:PVkFenceCreateInfo);
  procedure vknew(out a:PVkPhysicalDeviceFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceFeatures);
  procedure vknew(out a:PVkPhysicalDeviceSparseProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceSparseProperties);
  procedure vkadd(out a:PVkPhysicalDeviceSparseProperties);
  procedure vkrem(var a:PVkPhysicalDeviceSparseProperties);
  procedure vknew(out a:PVkPhysicalDeviceLimits);
  procedure vkdispose(var a:PVkPhysicalDeviceLimits);
  procedure vkadd(out a:PVkPhysicalDeviceLimits);
  procedure vkrem(var a:PVkPhysicalDeviceLimits);
  procedure vknew(out a:PVkSemaphoreCreateInfo);
  procedure vkdispose(var a:PVkSemaphoreCreateInfo);
  procedure vkadd(out a:PVkSemaphoreCreateInfo);
  procedure vkrem(var a:PVkSemaphoreCreateInfo);
  procedure vknew(out a:PVkQueryPoolCreateInfo);
  procedure vkdispose(var a:PVkQueryPoolCreateInfo);
  procedure vkadd(out a:PVkQueryPoolCreateInfo);
  procedure vkrem(var a:PVkQueryPoolCreateInfo);
  procedure vknew(out a:PVkFramebufferCreateInfo);
  procedure vkdispose(var a:PVkFramebufferCreateInfo);
  procedure vkadd(out a:PVkFramebufferCreateInfo);
  procedure vkrem(var a:PVkFramebufferCreateInfo);
  procedure vknew(out a:PVkDrawIndirectCommand);
  procedure vkdispose(var a:PVkDrawIndirectCommand);
  procedure vkadd(out a:PVkDrawIndirectCommand);
  procedure vkrem(var a:PVkDrawIndirectCommand);
  procedure vknew(out a:PVkDrawIndexedIndirectCommand);
  procedure vkdispose(var a:PVkDrawIndexedIndirectCommand);
  procedure vkadd(out a:PVkDrawIndexedIndirectCommand);
  procedure vkrem(var a:PVkDrawIndexedIndirectCommand);
  procedure vknew(out a:PVkDispatchIndirectCommand);
  procedure vkdispose(var a:PVkDispatchIndirectCommand);
  procedure vkadd(out a:PVkDispatchIndirectCommand);
  procedure vkrem(var a:PVkDispatchIndirectCommand);
  procedure vknew(out a:PVkSubmitInfo);
  procedure vkdispose(var a:PVkSubmitInfo);
  procedure vkadd(out a:PVkSubmitInfo);
  procedure vkrem(var a:PVkSubmitInfo);
  procedure vknew(out a:PVkDisplayPropertiesKHR);
  procedure vkdispose(var a:PVkDisplayPropertiesKHR);
  procedure vkadd(out a:PVkDisplayPropertiesKHR);
  procedure vkrem(var a:PVkDisplayPropertiesKHR);
  procedure vknew(out a:PVkDisplayPlanePropertiesKHR);
  procedure vkdispose(var a:PVkDisplayPlanePropertiesKHR);
  procedure vkadd(out a:PVkDisplayPlanePropertiesKHR);
  procedure vkrem(var a:PVkDisplayPlanePropertiesKHR);
  procedure vknew(out a:PVkDisplayModeParametersKHR);
  procedure vkdispose(var a:PVkDisplayModeParametersKHR);
  procedure vkadd(out a:PVkDisplayModeParametersKHR);
  procedure vkrem(var a:PVkDisplayModeParametersKHR);
  procedure vknew(out a:PVkDisplayModePropertiesKHR);
  procedure vkdispose(var a:PVkDisplayModePropertiesKHR);
  procedure vkadd(out a:PVkDisplayModePropertiesKHR);
  procedure vkrem(var a:PVkDisplayModePropertiesKHR);
  procedure vknew(out a:PVkDisplayModeCreateInfoKHR);
  procedure vkdispose(var a:PVkDisplayModeCreateInfoKHR);
  procedure vkadd(out a:PVkDisplayModeCreateInfoKHR);
  procedure vkrem(var a:PVkDisplayModeCreateInfoKHR);
  procedure vknew(out a:PVkDisplayPlaneCapabilitiesKHR);
  procedure vkdispose(var a:PVkDisplayPlaneCapabilitiesKHR);
  procedure vkadd(out a:PVkDisplayPlaneCapabilitiesKHR);
  procedure vkrem(var a:PVkDisplayPlaneCapabilitiesKHR);
  procedure vknew(out a:PVkDisplaySurfaceCreateInfoKHR);
  procedure vkdispose(var a:PVkDisplaySurfaceCreateInfoKHR);
  procedure vkadd(out a:PVkDisplaySurfaceCreateInfoKHR);
  procedure vkrem(var a:PVkDisplaySurfaceCreateInfoKHR);
  procedure vknew(out a:PVkDisplayPresentInfoKHR);
  procedure vkdispose(var a:PVkDisplayPresentInfoKHR);
  procedure vkadd(out a:PVkDisplayPresentInfoKHR);
  procedure vkrem(var a:PVkDisplayPresentInfoKHR);
  procedure vknew(out a:PVkSurfaceCapabilitiesKHR);
  procedure vkdispose(var a:PVkSurfaceCapabilitiesKHR);
  procedure vkadd(out a:PVkSurfaceCapabilitiesKHR);
  procedure vkrem(var a:PVkSurfaceCapabilitiesKHR);
  procedure vknew(out a:PVkAndroidSurfaceCreateInfoKHR);
  procedure vkdispose(var a:PVkAndroidSurfaceCreateInfoKHR);
  procedure vkadd(out a:PVkAndroidSurfaceCreateInfoKHR);
  procedure vkrem(var a:PVkAndroidSurfaceCreateInfoKHR);
  procedure vknew(out a:PVkViSurfaceCreateInfoNN);
  procedure vkdispose(var a:PVkViSurfaceCreateInfoNN);
  procedure vkadd(out a:PVkViSurfaceCreateInfoNN);
  procedure vkrem(var a:PVkViSurfaceCreateInfoNN);
  procedure vknew(out a:PVkWaylandSurfaceCreateInfoKHR);
  procedure vkdispose(var a:PVkWaylandSurfaceCreateInfoKHR);
  procedure vkadd(out a:PVkWaylandSurfaceCreateInfoKHR);
  procedure vkrem(var a:PVkWaylandSurfaceCreateInfoKHR);
  procedure vknew(out a:PVkWin32SurfaceCreateInfoKHR);
  procedure vkdispose(var a:PVkWin32SurfaceCreateInfoKHR);
  procedure vkadd(out a:PVkWin32SurfaceCreateInfoKHR);
  procedure vkrem(var a:PVkWin32SurfaceCreateInfoKHR);
  procedure vknew(out a:PVkXlibSurfaceCreateInfoKHR);
  procedure vkdispose(var a:PVkXlibSurfaceCreateInfoKHR);
  procedure vkadd(out a:PVkXlibSurfaceCreateInfoKHR);
  procedure vkrem(var a:PVkXlibSurfaceCreateInfoKHR);
  procedure vknew(out a:PVkXcbSurfaceCreateInfoKHR);
  procedure vkdispose(var a:PVkXcbSurfaceCreateInfoKHR);
  procedure vkadd(out a:PVkXcbSurfaceCreateInfoKHR);
  procedure vkrem(var a:PVkXcbSurfaceCreateInfoKHR);
  procedure vknew(out a:PVkImagePipeSurfaceCreateInfoFUCHSIA);
  procedure vkdispose(var a:PVkImagePipeSurfaceCreateInfoFUCHSIA);
  procedure vkadd(out a:PVkImagePipeSurfaceCreateInfoFUCHSIA);
  procedure vkrem(var a:PVkImagePipeSurfaceCreateInfoFUCHSIA);
  procedure vknew(out a:PVkStreamDescriptorSurfaceCreateInfoGGP);
  procedure vkdispose(var a:PVkStreamDescriptorSurfaceCreateInfoGGP);
  procedure vkadd(out a:PVkStreamDescriptorSurfaceCreateInfoGGP);
  procedure vkrem(var a:PVkStreamDescriptorSurfaceCreateInfoGGP);
  procedure vknew(out a:PVkSurfaceFormatKHR);
  procedure vkdispose(var a:PVkSurfaceFormatKHR);
  procedure vkadd(out a:PVkSurfaceFormatKHR);
  procedure vkrem(var a:PVkSurfaceFormatKHR);
  procedure vknew(out a:PVkSwapchainCreateInfoKHR);
  procedure vkdispose(var a:PVkSwapchainCreateInfoKHR);
  procedure vkadd(out a:PVkSwapchainCreateInfoKHR);
  procedure vkrem(var a:PVkSwapchainCreateInfoKHR);
  procedure vknew(out a:PVkPresentInfoKHR);
  procedure vkdispose(var a:PVkPresentInfoKHR);
  procedure vkadd(out a:PVkPresentInfoKHR);
  procedure vkrem(var a:PVkPresentInfoKHR);
  procedure vknew(out a:PVkValidationFlagsEXT);
  procedure vkdispose(var a:PVkValidationFlagsEXT);
  procedure vkadd(out a:PVkValidationFlagsEXT);
  procedure vkrem(var a:PVkValidationFlagsEXT);
  procedure vknew(out a:PVkValidationFeaturesEXT);
  procedure vkdispose(var a:PVkValidationFeaturesEXT);
  procedure vkadd(out a:PVkValidationFeaturesEXT);
  procedure vkrem(var a:PVkValidationFeaturesEXT);
  procedure vknew(out a:PVkPipelineRasterizationStateRasterizationOrderAMD);
  procedure vkdispose(var a:PVkPipelineRasterizationStateRasterizationOrderAMD);
  procedure vkadd(out a:PVkPipelineRasterizationStateRasterizationOrderAMD);
  procedure vkrem(var a:PVkPipelineRasterizationStateRasterizationOrderAMD);
  procedure vknew(out a:PVkDebugMarkerObjectNameInfoEXT);
  procedure vkdispose(var a:PVkDebugMarkerObjectNameInfoEXT);
  procedure vkadd(out a:PVkDebugMarkerObjectNameInfoEXT);
  procedure vkrem(var a:PVkDebugMarkerObjectNameInfoEXT);
  procedure vknew(out a:PVkDebugMarkerObjectTagInfoEXT);
  procedure vkdispose(var a:PVkDebugMarkerObjectTagInfoEXT);
  procedure vkadd(out a:PVkDebugMarkerObjectTagInfoEXT);
  procedure vkrem(var a:PVkDebugMarkerObjectTagInfoEXT);
  procedure vknew(out a:PVkDebugMarkerMarkerInfoEXT);
  procedure vkdispose(var a:PVkDebugMarkerMarkerInfoEXT);
  procedure vkadd(out a:PVkDebugMarkerMarkerInfoEXT);
  procedure vkrem(var a:PVkDebugMarkerMarkerInfoEXT);
  procedure vknew(out a:PVkDedicatedAllocationImageCreateInfoNV);
  procedure vkdispose(var a:PVkDedicatedAllocationImageCreateInfoNV);
  procedure vkadd(out a:PVkDedicatedAllocationImageCreateInfoNV);
  procedure vkrem(var a:PVkDedicatedAllocationImageCreateInfoNV);
  procedure vknew(out a:PVkDedicatedAllocationBufferCreateInfoNV);
  procedure vkdispose(var a:PVkDedicatedAllocationBufferCreateInfoNV);
  procedure vkadd(out a:PVkDedicatedAllocationBufferCreateInfoNV);
  procedure vkrem(var a:PVkDedicatedAllocationBufferCreateInfoNV);
  procedure vknew(out a:PVkDedicatedAllocationMemoryAllocateInfoNV);
  procedure vkdispose(var a:PVkDedicatedAllocationMemoryAllocateInfoNV);
  procedure vkadd(out a:PVkDedicatedAllocationMemoryAllocateInfoNV);
  procedure vkrem(var a:PVkDedicatedAllocationMemoryAllocateInfoNV);
  procedure vknew(out a:PVkExternalImageFormatPropertiesNV);
  procedure vkdispose(var a:PVkExternalImageFormatPropertiesNV);
  procedure vkadd(out a:PVkExternalImageFormatPropertiesNV);
  procedure vkrem(var a:PVkExternalImageFormatPropertiesNV);
  procedure vknew(out a:PVkExternalMemoryImageCreateInfoNV);
  procedure vkdispose(var a:PVkExternalMemoryImageCreateInfoNV);
  procedure vkadd(out a:PVkExternalMemoryImageCreateInfoNV);
  procedure vkrem(var a:PVkExternalMemoryImageCreateInfoNV);
  procedure vknew(out a:PVkExportMemoryAllocateInfoNV);
  procedure vkdispose(var a:PVkExportMemoryAllocateInfoNV);
  procedure vkadd(out a:PVkExportMemoryAllocateInfoNV);
  procedure vkrem(var a:PVkExportMemoryAllocateInfoNV);
  procedure vknew(out a:PVkImportMemoryWin32HandleInfoNV);
  procedure vkdispose(var a:PVkImportMemoryWin32HandleInfoNV);
  procedure vkadd(out a:PVkImportMemoryWin32HandleInfoNV);
  procedure vkrem(var a:PVkImportMemoryWin32HandleInfoNV);
  procedure vknew(out a:PVkExportMemoryWin32HandleInfoNV);
  procedure vkdispose(var a:PVkExportMemoryWin32HandleInfoNV);
  procedure vkadd(out a:PVkExportMemoryWin32HandleInfoNV);
  procedure vkrem(var a:PVkExportMemoryWin32HandleInfoNV);
  procedure vknew(out a:PVkWin32KeyedMutexAcquireReleaseInfoNV);
  procedure vkdispose(var a:PVkWin32KeyedMutexAcquireReleaseInfoNV);
  procedure vkadd(out a:PVkWin32KeyedMutexAcquireReleaseInfoNV);
  procedure vkrem(var a:PVkWin32KeyedMutexAcquireReleaseInfoNV);
  procedure vknew(out a:PVkDeviceGeneratedCommandsFeaturesNVX);
  procedure vkdispose(var a:PVkDeviceGeneratedCommandsFeaturesNVX);
  procedure vkadd(out a:PVkDeviceGeneratedCommandsFeaturesNVX);
  procedure vkrem(var a:PVkDeviceGeneratedCommandsFeaturesNVX);
  procedure vknew(out a:PVkDeviceGeneratedCommandsLimitsNVX);
  procedure vkdispose(var a:PVkDeviceGeneratedCommandsLimitsNVX);
  procedure vkadd(out a:PVkDeviceGeneratedCommandsLimitsNVX);
  procedure vkrem(var a:PVkDeviceGeneratedCommandsLimitsNVX);
  procedure vknew(out a:PVkIndirectCommandsTokenNVX);
  procedure vkdispose(var a:PVkIndirectCommandsTokenNVX);
  procedure vkadd(out a:PVkIndirectCommandsTokenNVX);
  procedure vkrem(var a:PVkIndirectCommandsTokenNVX);
  procedure vknew(out a:PVkIndirectCommandsLayoutTokenNVX);
  procedure vkdispose(var a:PVkIndirectCommandsLayoutTokenNVX);
  procedure vkadd(out a:PVkIndirectCommandsLayoutTokenNVX);
  procedure vkrem(var a:PVkIndirectCommandsLayoutTokenNVX);
  procedure vknew(out a:PVkIndirectCommandsLayoutCreateInfoNVX);
  procedure vkdispose(var a:PVkIndirectCommandsLayoutCreateInfoNVX);
  procedure vkadd(out a:PVkIndirectCommandsLayoutCreateInfoNVX);
  procedure vkrem(var a:PVkIndirectCommandsLayoutCreateInfoNVX);
  procedure vknew(out a:PVkCmdProcessCommandsInfoNVX);
  procedure vkdispose(var a:PVkCmdProcessCommandsInfoNVX);
  procedure vkadd(out a:PVkCmdProcessCommandsInfoNVX);
  procedure vkrem(var a:PVkCmdProcessCommandsInfoNVX);
  procedure vknew(out a:PVkCmdReserveSpaceForCommandsInfoNVX);
  procedure vkdispose(var a:PVkCmdReserveSpaceForCommandsInfoNVX);
  procedure vkadd(out a:PVkCmdReserveSpaceForCommandsInfoNVX);
  procedure vkrem(var a:PVkCmdReserveSpaceForCommandsInfoNVX);
  procedure vknew(out a:PVkObjectTableCreateInfoNVX);
  procedure vkdispose(var a:PVkObjectTableCreateInfoNVX);
  procedure vkadd(out a:PVkObjectTableCreateInfoNVX);
  procedure vkrem(var a:PVkObjectTableCreateInfoNVX);
  procedure vknew(out a:PVkObjectTableEntryNVX);
  procedure vkdispose(var a:PVkObjectTableEntryNVX);
  procedure vkadd(out a:PVkObjectTableEntryNVX);
  procedure vkrem(var a:PVkObjectTableEntryNVX);
  procedure vknew(out a:PVkObjectTablePipelineEntryNVX);
  procedure vkdispose(var a:PVkObjectTablePipelineEntryNVX);
  procedure vkadd(out a:PVkObjectTablePipelineEntryNVX);
  procedure vkrem(var a:PVkObjectTablePipelineEntryNVX);
  procedure vknew(out a:PVkObjectTableDescriptorSetEntryNVX);
  procedure vkdispose(var a:PVkObjectTableDescriptorSetEntryNVX);
  procedure vkadd(out a:PVkObjectTableDescriptorSetEntryNVX);
  procedure vkrem(var a:PVkObjectTableDescriptorSetEntryNVX);
  procedure vknew(out a:PVkObjectTableVertexBufferEntryNVX);
  procedure vkdispose(var a:PVkObjectTableVertexBufferEntryNVX);
  procedure vkadd(out a:PVkObjectTableVertexBufferEntryNVX);
  procedure vkrem(var a:PVkObjectTableVertexBufferEntryNVX);
  procedure vknew(out a:PVkObjectTableIndexBufferEntryNVX);
  procedure vkdispose(var a:PVkObjectTableIndexBufferEntryNVX);
  procedure vkadd(out a:PVkObjectTableIndexBufferEntryNVX);
  procedure vkrem(var a:PVkObjectTableIndexBufferEntryNVX);
  procedure vknew(out a:PVkObjectTablePushConstantEntryNVX);
  procedure vkdispose(var a:PVkObjectTablePushConstantEntryNVX);
  procedure vkadd(out a:PVkObjectTablePushConstantEntryNVX);
  procedure vkrem(var a:PVkObjectTablePushConstantEntryNVX);
  procedure vknew(out a:PVkPhysicalDeviceFeatures2);
  procedure vkdispose(var a:PVkPhysicalDeviceFeatures2);
  procedure vkadd(out a:PVkPhysicalDeviceFeatures2);
  procedure vkrem(var a:PVkPhysicalDeviceFeatures2);
  procedure vknew(out a:PVkFormatProperties2);
  procedure vkdispose(var a:PVkFormatProperties2);
  procedure vkadd(out a:PVkFormatProperties2);
  procedure vkrem(var a:PVkFormatProperties2);
  procedure vknew(out a:PVkImageFormatProperties2);
  procedure vkdispose(var a:PVkImageFormatProperties2);
  procedure vkadd(out a:PVkImageFormatProperties2);
  procedure vkrem(var a:PVkImageFormatProperties2);
  procedure vknew(out a:PVkPhysicalDeviceImageFormatInfo2);
  procedure vkdispose(var a:PVkPhysicalDeviceImageFormatInfo2);
  procedure vkadd(out a:PVkPhysicalDeviceImageFormatInfo2);
  procedure vkrem(var a:PVkPhysicalDeviceImageFormatInfo2);
  procedure vknew(out a:PVkQueueFamilyProperties2);
  procedure vkdispose(var a:PVkQueueFamilyProperties2);
  procedure vkadd(out a:PVkQueueFamilyProperties2);
  procedure vkrem(var a:PVkQueueFamilyProperties2);
  procedure vknew(out a:PVkSparseImageFormatProperties2);
  procedure vkdispose(var a:PVkSparseImageFormatProperties2);
  procedure vkadd(out a:PVkSparseImageFormatProperties2);
  procedure vkrem(var a:PVkSparseImageFormatProperties2);
  procedure vknew(out a:PVkPhysicalDeviceSparseImageFormatInfo2);
  procedure vkdispose(var a:PVkPhysicalDeviceSparseImageFormatInfo2);
  procedure vkadd(out a:PVkPhysicalDeviceSparseImageFormatInfo2);
  procedure vkrem(var a:PVkPhysicalDeviceSparseImageFormatInfo2);
  procedure vknew(out a:PVkPhysicalDevicePushDescriptorPropertiesKHR);
  procedure vkdispose(var a:PVkPhysicalDevicePushDescriptorPropertiesKHR);
  procedure vkadd(out a:PVkPhysicalDevicePushDescriptorPropertiesKHR);
  procedure vkrem(var a:PVkPhysicalDevicePushDescriptorPropertiesKHR);
  procedure vknew(out a:PVkConformanceVersion);
  procedure vkdispose(var a:PVkConformanceVersion);
  procedure vkadd(out a:PVkConformanceVersion);
  procedure vkrem(var a:PVkConformanceVersion);
  procedure vknew(out a:PVkPhysicalDeviceDriverProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceDriverProperties);
  procedure vkadd(out a:PVkPhysicalDeviceDriverProperties);
  procedure vkrem(var a:PVkPhysicalDeviceDriverProperties);
  procedure vknew(out a:PVkRectLayerKHR);
  procedure vkdispose(var a:PVkRectLayerKHR);
  procedure vkadd(out a:PVkRectLayerKHR);
  procedure vkrem(var a:PVkRectLayerKHR);
  procedure vknew(out a:PVkPhysicalDeviceVariablePointersFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceVariablePointersFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceVariablePointersFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceVariablePointersFeatures);
  procedure vknew(out a:PVkExternalMemoryProperties);
  procedure vkdispose(var a:PVkExternalMemoryProperties);
  procedure vkadd(out a:PVkExternalMemoryProperties);
  procedure vkrem(var a:PVkExternalMemoryProperties);
  procedure vknew(out a:PVkPhysicalDeviceExternalImageFormatInfo);
  procedure vkdispose(var a:PVkPhysicalDeviceExternalImageFormatInfo);
  procedure vkadd(out a:PVkPhysicalDeviceExternalImageFormatInfo);
  procedure vkrem(var a:PVkPhysicalDeviceExternalImageFormatInfo);
  procedure vknew(out a:PVkExternalImageFormatProperties);
  procedure vkdispose(var a:PVkExternalImageFormatProperties);
  procedure vkadd(out a:PVkExternalImageFormatProperties);
  procedure vkrem(var a:PVkExternalImageFormatProperties);
  procedure vknew(out a:PVkPhysicalDeviceExternalBufferInfo);
  procedure vkdispose(var a:PVkPhysicalDeviceExternalBufferInfo);
  procedure vkadd(out a:PVkPhysicalDeviceExternalBufferInfo);
  procedure vkrem(var a:PVkPhysicalDeviceExternalBufferInfo);
  procedure vknew(out a:PVkExternalBufferProperties);
  procedure vkdispose(var a:PVkExternalBufferProperties);
  procedure vkadd(out a:PVkExternalBufferProperties);
  procedure vkrem(var a:PVkExternalBufferProperties);
  procedure vknew(out a:PVkPhysicalDeviceIDProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceIDProperties);
  procedure vkadd(out a:PVkPhysicalDeviceIDProperties);
  procedure vkrem(var a:PVkPhysicalDeviceIDProperties);
  procedure vknew(out a:PVkExternalMemoryImageCreateInfo);
  procedure vkdispose(var a:PVkExternalMemoryImageCreateInfo);
  procedure vkadd(out a:PVkExternalMemoryImageCreateInfo);
  procedure vkrem(var a:PVkExternalMemoryImageCreateInfo);
  procedure vknew(out a:PVkExternalMemoryBufferCreateInfo);
  procedure vkdispose(var a:PVkExternalMemoryBufferCreateInfo);
  procedure vkadd(out a:PVkExternalMemoryBufferCreateInfo);
  procedure vkrem(var a:PVkExternalMemoryBufferCreateInfo);
  procedure vknew(out a:PVkExportMemoryAllocateInfo);
  procedure vkdispose(var a:PVkExportMemoryAllocateInfo);
  procedure vkadd(out a:PVkExportMemoryAllocateInfo);
  procedure vkrem(var a:PVkExportMemoryAllocateInfo);
  procedure vknew(out a:PVkImportMemoryWin32HandleInfoKHR);
  procedure vkdispose(var a:PVkImportMemoryWin32HandleInfoKHR);
  procedure vkadd(out a:PVkImportMemoryWin32HandleInfoKHR);
  procedure vkrem(var a:PVkImportMemoryWin32HandleInfoKHR);
  procedure vknew(out a:PVkExportMemoryWin32HandleInfoKHR);
  procedure vkdispose(var a:PVkExportMemoryWin32HandleInfoKHR);
  procedure vkadd(out a:PVkExportMemoryWin32HandleInfoKHR);
  procedure vkrem(var a:PVkExportMemoryWin32HandleInfoKHR);
  procedure vknew(out a:PVkMemoryWin32HandlePropertiesKHR);
  procedure vkdispose(var a:PVkMemoryWin32HandlePropertiesKHR);
  procedure vkadd(out a:PVkMemoryWin32HandlePropertiesKHR);
  procedure vkrem(var a:PVkMemoryWin32HandlePropertiesKHR);
  procedure vknew(out a:PVkMemoryGetWin32HandleInfoKHR);
  procedure vkdispose(var a:PVkMemoryGetWin32HandleInfoKHR);
  procedure vkadd(out a:PVkMemoryGetWin32HandleInfoKHR);
  procedure vkrem(var a:PVkMemoryGetWin32HandleInfoKHR);
  procedure vknew(out a:PVkImportMemoryFdInfoKHR);
  procedure vkdispose(var a:PVkImportMemoryFdInfoKHR);
  procedure vkadd(out a:PVkImportMemoryFdInfoKHR);
  procedure vkrem(var a:PVkImportMemoryFdInfoKHR);
  procedure vknew(out a:PVkMemoryFdPropertiesKHR);
  procedure vkdispose(var a:PVkMemoryFdPropertiesKHR);
  procedure vkadd(out a:PVkMemoryFdPropertiesKHR);
  procedure vkrem(var a:PVkMemoryFdPropertiesKHR);
  procedure vknew(out a:PVkMemoryGetFdInfoKHR);
  procedure vkdispose(var a:PVkMemoryGetFdInfoKHR);
  procedure vkadd(out a:PVkMemoryGetFdInfoKHR);
  procedure vkrem(var a:PVkMemoryGetFdInfoKHR);
  procedure vknew(out a:PVkWin32KeyedMutexAcquireReleaseInfoKHR);
  procedure vkdispose(var a:PVkWin32KeyedMutexAcquireReleaseInfoKHR);
  procedure vkadd(out a:PVkWin32KeyedMutexAcquireReleaseInfoKHR);
  procedure vkrem(var a:PVkWin32KeyedMutexAcquireReleaseInfoKHR);
  procedure vknew(out a:PVkPhysicalDeviceExternalSemaphoreInfo);
  procedure vkdispose(var a:PVkPhysicalDeviceExternalSemaphoreInfo);
  procedure vkadd(out a:PVkPhysicalDeviceExternalSemaphoreInfo);
  procedure vkrem(var a:PVkPhysicalDeviceExternalSemaphoreInfo);
  procedure vknew(out a:PVkExternalSemaphoreProperties);
  procedure vkdispose(var a:PVkExternalSemaphoreProperties);
  procedure vkadd(out a:PVkExternalSemaphoreProperties);
  procedure vkrem(var a:PVkExternalSemaphoreProperties);
  procedure vknew(out a:PVkExportSemaphoreCreateInfo);
  procedure vkdispose(var a:PVkExportSemaphoreCreateInfo);
  procedure vkadd(out a:PVkExportSemaphoreCreateInfo);
  procedure vkrem(var a:PVkExportSemaphoreCreateInfo);
  procedure vknew(out a:PVkImportSemaphoreWin32HandleInfoKHR);
  procedure vkdispose(var a:PVkImportSemaphoreWin32HandleInfoKHR);
  procedure vkadd(out a:PVkImportSemaphoreWin32HandleInfoKHR);
  procedure vkrem(var a:PVkImportSemaphoreWin32HandleInfoKHR);
  procedure vknew(out a:PVkExportSemaphoreWin32HandleInfoKHR);
  procedure vkdispose(var a:PVkExportSemaphoreWin32HandleInfoKHR);
  procedure vkadd(out a:PVkExportSemaphoreWin32HandleInfoKHR);
  procedure vkrem(var a:PVkExportSemaphoreWin32HandleInfoKHR);
  procedure vknew(out a:PVkD3D12FenceSubmitInfoKHR);
  procedure vkdispose(var a:PVkD3D12FenceSubmitInfoKHR);
  procedure vkadd(out a:PVkD3D12FenceSubmitInfoKHR);
  procedure vkrem(var a:PVkD3D12FenceSubmitInfoKHR);
  procedure vknew(out a:PVkSemaphoreGetWin32HandleInfoKHR);
  procedure vkdispose(var a:PVkSemaphoreGetWin32HandleInfoKHR);
  procedure vkadd(out a:PVkSemaphoreGetWin32HandleInfoKHR);
  procedure vkrem(var a:PVkSemaphoreGetWin32HandleInfoKHR);
  procedure vknew(out a:PVkImportSemaphoreFdInfoKHR);
  procedure vkdispose(var a:PVkImportSemaphoreFdInfoKHR);
  procedure vkadd(out a:PVkImportSemaphoreFdInfoKHR);
  procedure vkrem(var a:PVkImportSemaphoreFdInfoKHR);
  procedure vknew(out a:PVkSemaphoreGetFdInfoKHR);
  procedure vkdispose(var a:PVkSemaphoreGetFdInfoKHR);
  procedure vkadd(out a:PVkSemaphoreGetFdInfoKHR);
  procedure vkrem(var a:PVkSemaphoreGetFdInfoKHR);
  procedure vknew(out a:PVkPhysicalDeviceExternalFenceInfo);
  procedure vkdispose(var a:PVkPhysicalDeviceExternalFenceInfo);
  procedure vkadd(out a:PVkPhysicalDeviceExternalFenceInfo);
  procedure vkrem(var a:PVkPhysicalDeviceExternalFenceInfo);
  procedure vknew(out a:PVkExternalFenceProperties);
  procedure vkdispose(var a:PVkExternalFenceProperties);
  procedure vkadd(out a:PVkExternalFenceProperties);
  procedure vkrem(var a:PVkExternalFenceProperties);
  procedure vknew(out a:PVkExportFenceCreateInfo);
  procedure vkdispose(var a:PVkExportFenceCreateInfo);
  procedure vkadd(out a:PVkExportFenceCreateInfo);
  procedure vkrem(var a:PVkExportFenceCreateInfo);
  procedure vknew(out a:PVkImportFenceWin32HandleInfoKHR);
  procedure vkdispose(var a:PVkImportFenceWin32HandleInfoKHR);
  procedure vkadd(out a:PVkImportFenceWin32HandleInfoKHR);
  procedure vkrem(var a:PVkImportFenceWin32HandleInfoKHR);
  procedure vknew(out a:PVkExportFenceWin32HandleInfoKHR);
  procedure vkdispose(var a:PVkExportFenceWin32HandleInfoKHR);
  procedure vkadd(out a:PVkExportFenceWin32HandleInfoKHR);
  procedure vkrem(var a:PVkExportFenceWin32HandleInfoKHR);
  procedure vknew(out a:PVkFenceGetWin32HandleInfoKHR);
  procedure vkdispose(var a:PVkFenceGetWin32HandleInfoKHR);
  procedure vkadd(out a:PVkFenceGetWin32HandleInfoKHR);
  procedure vkrem(var a:PVkFenceGetWin32HandleInfoKHR);
  procedure vknew(out a:PVkImportFenceFdInfoKHR);
  procedure vkdispose(var a:PVkImportFenceFdInfoKHR);
  procedure vkadd(out a:PVkImportFenceFdInfoKHR);
  procedure vkrem(var a:PVkImportFenceFdInfoKHR);
  procedure vknew(out a:PVkFenceGetFdInfoKHR);
  procedure vkdispose(var a:PVkFenceGetFdInfoKHR);
  procedure vkadd(out a:PVkFenceGetFdInfoKHR);
  procedure vkrem(var a:PVkFenceGetFdInfoKHR);
  procedure vknew(out a:PVkPhysicalDeviceMultiviewFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceMultiviewFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceMultiviewFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceMultiviewFeatures);
  procedure vknew(out a:PVkPhysicalDeviceMultiviewProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceMultiviewProperties);
  procedure vkadd(out a:PVkPhysicalDeviceMultiviewProperties);
  procedure vkrem(var a:PVkPhysicalDeviceMultiviewProperties);
  procedure vknew(out a:PVkRenderPassMultiviewCreateInfo);
  procedure vkdispose(var a:PVkRenderPassMultiviewCreateInfo);
  procedure vkadd(out a:PVkRenderPassMultiviewCreateInfo);
  procedure vkrem(var a:PVkRenderPassMultiviewCreateInfo);
  procedure vknew(out a:PVkSurfaceCapabilities2EXT);
  procedure vkdispose(var a:PVkSurfaceCapabilities2EXT);
  procedure vkadd(out a:PVkSurfaceCapabilities2EXT);
  procedure vkrem(var a:PVkSurfaceCapabilities2EXT);
  procedure vknew(out a:PVkDisplayPowerInfoEXT);
  procedure vkdispose(var a:PVkDisplayPowerInfoEXT);
  procedure vkadd(out a:PVkDisplayPowerInfoEXT);
  procedure vkrem(var a:PVkDisplayPowerInfoEXT);
  procedure vknew(out a:PVkDeviceEventInfoEXT);
  procedure vkdispose(var a:PVkDeviceEventInfoEXT);
  procedure vkadd(out a:PVkDeviceEventInfoEXT);
  procedure vkrem(var a:PVkDeviceEventInfoEXT);
  procedure vknew(out a:PVkDisplayEventInfoEXT);
  procedure vkdispose(var a:PVkDisplayEventInfoEXT);
  procedure vkadd(out a:PVkDisplayEventInfoEXT);
  procedure vkrem(var a:PVkDisplayEventInfoEXT);
  procedure vknew(out a:PVkSwapchainCounterCreateInfoEXT);
  procedure vkdispose(var a:PVkSwapchainCounterCreateInfoEXT);
  procedure vkadd(out a:PVkSwapchainCounterCreateInfoEXT);
  procedure vkrem(var a:PVkSwapchainCounterCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceGroupProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceGroupProperties);
  procedure vkadd(out a:PVkPhysicalDeviceGroupProperties);
  procedure vkrem(var a:PVkPhysicalDeviceGroupProperties);
  procedure vknew(out a:PVkMemoryAllocateFlagsInfo);
  procedure vkdispose(var a:PVkMemoryAllocateFlagsInfo);
  procedure vkadd(out a:PVkMemoryAllocateFlagsInfo);
  procedure vkrem(var a:PVkMemoryAllocateFlagsInfo);
  procedure vknew(out a:PVkBindBufferMemoryInfo);
  procedure vkdispose(var a:PVkBindBufferMemoryInfo);
  procedure vkadd(out a:PVkBindBufferMemoryInfo);
  procedure vkrem(var a:PVkBindBufferMemoryInfo);
  procedure vknew(out a:PVkBindBufferMemoryDeviceGroupInfo);
  procedure vkdispose(var a:PVkBindBufferMemoryDeviceGroupInfo);
  procedure vkadd(out a:PVkBindBufferMemoryDeviceGroupInfo);
  procedure vkrem(var a:PVkBindBufferMemoryDeviceGroupInfo);
  procedure vknew(out a:PVkBindImageMemoryInfo);
  procedure vkdispose(var a:PVkBindImageMemoryInfo);
  procedure vkadd(out a:PVkBindImageMemoryInfo);
  procedure vkrem(var a:PVkBindImageMemoryInfo);
  procedure vknew(out a:PVkBindImageMemoryDeviceGroupInfo);
  procedure vkdispose(var a:PVkBindImageMemoryDeviceGroupInfo);
  procedure vkadd(out a:PVkBindImageMemoryDeviceGroupInfo);
  procedure vkrem(var a:PVkBindImageMemoryDeviceGroupInfo);
  procedure vknew(out a:PVkDeviceGroupRenderPassBeginInfo);
  procedure vkdispose(var a:PVkDeviceGroupRenderPassBeginInfo);
  procedure vkadd(out a:PVkDeviceGroupRenderPassBeginInfo);
  procedure vkrem(var a:PVkDeviceGroupRenderPassBeginInfo);
  procedure vknew(out a:PVkDeviceGroupCommandBufferBeginInfo);
  procedure vkdispose(var a:PVkDeviceGroupCommandBufferBeginInfo);
  procedure vkadd(out a:PVkDeviceGroupCommandBufferBeginInfo);
  procedure vkrem(var a:PVkDeviceGroupCommandBufferBeginInfo);
  procedure vknew(out a:PVkDeviceGroupSubmitInfo);
  procedure vkdispose(var a:PVkDeviceGroupSubmitInfo);
  procedure vkadd(out a:PVkDeviceGroupSubmitInfo);
  procedure vkrem(var a:PVkDeviceGroupSubmitInfo);
  procedure vknew(out a:PVkDeviceGroupBindSparseInfo);
  procedure vkdispose(var a:PVkDeviceGroupBindSparseInfo);
  procedure vkadd(out a:PVkDeviceGroupBindSparseInfo);
  procedure vkrem(var a:PVkDeviceGroupBindSparseInfo);
  procedure vknew(out a:PVkDeviceGroupPresentCapabilitiesKHR);
  procedure vkdispose(var a:PVkDeviceGroupPresentCapabilitiesKHR);
  procedure vkadd(out a:PVkDeviceGroupPresentCapabilitiesKHR);
  procedure vkrem(var a:PVkDeviceGroupPresentCapabilitiesKHR);
  procedure vknew(out a:PVkImageSwapchainCreateInfoKHR);
  procedure vkdispose(var a:PVkImageSwapchainCreateInfoKHR);
  procedure vkadd(out a:PVkImageSwapchainCreateInfoKHR);
  procedure vkrem(var a:PVkImageSwapchainCreateInfoKHR);
  procedure vknew(out a:PVkBindImageMemorySwapchainInfoKHR);
  procedure vkdispose(var a:PVkBindImageMemorySwapchainInfoKHR);
  procedure vkadd(out a:PVkBindImageMemorySwapchainInfoKHR);
  procedure vkrem(var a:PVkBindImageMemorySwapchainInfoKHR);
  procedure vknew(out a:PVkAcquireNextImageInfoKHR);
  procedure vkdispose(var a:PVkAcquireNextImageInfoKHR);
  procedure vkadd(out a:PVkAcquireNextImageInfoKHR);
  procedure vkrem(var a:PVkAcquireNextImageInfoKHR);
  procedure vknew(out a:PVkDeviceGroupPresentInfoKHR);
  procedure vkdispose(var a:PVkDeviceGroupPresentInfoKHR);
  procedure vkadd(out a:PVkDeviceGroupPresentInfoKHR);
  procedure vkrem(var a:PVkDeviceGroupPresentInfoKHR);
  procedure vknew(out a:PVkDeviceGroupDeviceCreateInfo);
  procedure vkdispose(var a:PVkDeviceGroupDeviceCreateInfo);
  procedure vkadd(out a:PVkDeviceGroupDeviceCreateInfo);
  procedure vkrem(var a:PVkDeviceGroupDeviceCreateInfo);
  procedure vknew(out a:PVkDeviceGroupSwapchainCreateInfoKHR);
  procedure vkdispose(var a:PVkDeviceGroupSwapchainCreateInfoKHR);
  procedure vkadd(out a:PVkDeviceGroupSwapchainCreateInfoKHR);
  procedure vkrem(var a:PVkDeviceGroupSwapchainCreateInfoKHR);
  procedure vknew(out a:PVkDescriptorUpdateTemplateEntry);
  procedure vkdispose(var a:PVkDescriptorUpdateTemplateEntry);
  procedure vkadd(out a:PVkDescriptorUpdateTemplateEntry);
  procedure vkrem(var a:PVkDescriptorUpdateTemplateEntry);
  procedure vknew(out a:PVkDescriptorUpdateTemplateCreateInfo);
  procedure vkdispose(var a:PVkDescriptorUpdateTemplateCreateInfo);
  procedure vkadd(out a:PVkDescriptorUpdateTemplateCreateInfo);
  procedure vkrem(var a:PVkDescriptorUpdateTemplateCreateInfo);
  procedure vknew(out a:PVkXYColorEXT);
  procedure vkdispose(var a:PVkXYColorEXT);
  procedure vkadd(out a:PVkXYColorEXT);
  procedure vkrem(var a:PVkXYColorEXT);
  procedure vknew(out a:PVkHdrMetadataEXT);
  procedure vkdispose(var a:PVkHdrMetadataEXT);
  procedure vkadd(out a:PVkHdrMetadataEXT);
  procedure vkrem(var a:PVkHdrMetadataEXT);
  procedure vknew(out a:PVkDisplayNativeHdrSurfaceCapabilitiesAMD);
  procedure vkdispose(var a:PVkDisplayNativeHdrSurfaceCapabilitiesAMD);
  procedure vkadd(out a:PVkDisplayNativeHdrSurfaceCapabilitiesAMD);
  procedure vkrem(var a:PVkDisplayNativeHdrSurfaceCapabilitiesAMD);
  procedure vknew(out a:PVkSwapchainDisplayNativeHdrCreateInfoAMD);
  procedure vkdispose(var a:PVkSwapchainDisplayNativeHdrCreateInfoAMD);
  procedure vkadd(out a:PVkSwapchainDisplayNativeHdrCreateInfoAMD);
  procedure vkrem(var a:PVkSwapchainDisplayNativeHdrCreateInfoAMD);
  procedure vknew(out a:PVkRefreshCycleDurationGOOGLE);
  procedure vkdispose(var a:PVkRefreshCycleDurationGOOGLE);
  procedure vkadd(out a:PVkRefreshCycleDurationGOOGLE);
  procedure vkrem(var a:PVkRefreshCycleDurationGOOGLE);
  procedure vknew(out a:PVkPastPresentationTimingGOOGLE);
  procedure vkdispose(var a:PVkPastPresentationTimingGOOGLE);
  procedure vkadd(out a:PVkPastPresentationTimingGOOGLE);
  procedure vkrem(var a:PVkPastPresentationTimingGOOGLE);
  procedure vknew(out a:PVkPresentTimeGOOGLE);
  procedure vkdispose(var a:PVkPresentTimeGOOGLE);
  procedure vkadd(out a:PVkPresentTimeGOOGLE);
  procedure vkrem(var a:PVkPresentTimeGOOGLE);
  procedure vknew(out a:PVkIOSSurfaceCreateInfoMVK);
  procedure vkdispose(var a:PVkIOSSurfaceCreateInfoMVK);
  procedure vkadd(out a:PVkIOSSurfaceCreateInfoMVK);
  procedure vkrem(var a:PVkIOSSurfaceCreateInfoMVK);
  procedure vknew(out a:PVkMacOSSurfaceCreateInfoMVK);
  procedure vkdispose(var a:PVkMacOSSurfaceCreateInfoMVK);
  procedure vkadd(out a:PVkMacOSSurfaceCreateInfoMVK);
  procedure vkrem(var a:PVkMacOSSurfaceCreateInfoMVK);
  procedure vknew(out a:PVkMetalSurfaceCreateInfoEXT);
  procedure vkdispose(var a:PVkMetalSurfaceCreateInfoEXT);
  procedure vkadd(out a:PVkMetalSurfaceCreateInfoEXT);
  procedure vkrem(var a:PVkMetalSurfaceCreateInfoEXT);
  procedure vknew(out a:PVkViewportWScalingNV);
  procedure vkdispose(var a:PVkViewportWScalingNV);
  procedure vkadd(out a:PVkViewportWScalingNV);
  procedure vkrem(var a:PVkViewportWScalingNV);
  procedure vknew(out a:PVkPipelineViewportWScalingStateCreateInfoNV);
  procedure vkdispose(var a:PVkPipelineViewportWScalingStateCreateInfoNV);
  procedure vkadd(out a:PVkPipelineViewportWScalingStateCreateInfoNV);
  procedure vkrem(var a:PVkPipelineViewportWScalingStateCreateInfoNV);
  procedure vknew(out a:PVkViewportSwizzleNV);
  procedure vkdispose(var a:PVkViewportSwizzleNV);
  procedure vkadd(out a:PVkViewportSwizzleNV);
  procedure vkrem(var a:PVkViewportSwizzleNV);
  procedure vknew(out a:PVkPipelineViewportSwizzleStateCreateInfoNV);
  procedure vkdispose(var a:PVkPipelineViewportSwizzleStateCreateInfoNV);
  procedure vkadd(out a:PVkPipelineViewportSwizzleStateCreateInfoNV);
  procedure vkrem(var a:PVkPipelineViewportSwizzleStateCreateInfoNV);
  procedure vknew(out a:PVkPhysicalDeviceDiscardRectanglePropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceDiscardRectanglePropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceDiscardRectanglePropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceDiscardRectanglePropertiesEXT);
  procedure vknew(out a:PVkPipelineDiscardRectangleStateCreateInfoEXT);
  procedure vkdispose(var a:PVkPipelineDiscardRectangleStateCreateInfoEXT);
  procedure vkadd(out a:PVkPipelineDiscardRectangleStateCreateInfoEXT);
  procedure vkrem(var a:PVkPipelineDiscardRectangleStateCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX);
  procedure vkdispose(var a:PVkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX);
  procedure vkadd(out a:PVkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX);
  procedure vkrem(var a:PVkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX);
  procedure vknew(out a:PVkInputAttachmentAspectReference);
  procedure vkdispose(var a:PVkInputAttachmentAspectReference);
  procedure vkadd(out a:PVkInputAttachmentAspectReference);
  procedure vkrem(var a:PVkInputAttachmentAspectReference);
  procedure vknew(out a:PVkRenderPassInputAttachmentAspectCreateInfo);
  procedure vkdispose(var a:PVkRenderPassInputAttachmentAspectCreateInfo);
  procedure vkadd(out a:PVkRenderPassInputAttachmentAspectCreateInfo);
  procedure vkrem(var a:PVkRenderPassInputAttachmentAspectCreateInfo);
  procedure vknew(out a:PVkPhysicalDeviceSurfaceInfo2KHR);
  procedure vkdispose(var a:PVkPhysicalDeviceSurfaceInfo2KHR);
  procedure vkadd(out a:PVkPhysicalDeviceSurfaceInfo2KHR);
  procedure vkrem(var a:PVkPhysicalDeviceSurfaceInfo2KHR);
  procedure vknew(out a:PVkSurfaceCapabilities2KHR);
  procedure vkdispose(var a:PVkSurfaceCapabilities2KHR);
  procedure vkadd(out a:PVkSurfaceCapabilities2KHR);
  procedure vkrem(var a:PVkSurfaceCapabilities2KHR);
  procedure vknew(out a:PVkSurfaceFormat2KHR);
  procedure vkdispose(var a:PVkSurfaceFormat2KHR);
  procedure vkadd(out a:PVkSurfaceFormat2KHR);
  procedure vkrem(var a:PVkSurfaceFormat2KHR);
  procedure vknew(out a:PVkDisplayProperties2KHR);
  procedure vkdispose(var a:PVkDisplayProperties2KHR);
  procedure vkadd(out a:PVkDisplayProperties2KHR);
  procedure vkrem(var a:PVkDisplayProperties2KHR);
  procedure vknew(out a:PVkDisplayPlaneProperties2KHR);
  procedure vkdispose(var a:PVkDisplayPlaneProperties2KHR);
  procedure vkadd(out a:PVkDisplayPlaneProperties2KHR);
  procedure vkrem(var a:PVkDisplayPlaneProperties2KHR);
  procedure vknew(out a:PVkDisplayModeProperties2KHR);
  procedure vkdispose(var a:PVkDisplayModeProperties2KHR);
  procedure vkadd(out a:PVkDisplayModeProperties2KHR);
  procedure vkrem(var a:PVkDisplayModeProperties2KHR);
  procedure vknew(out a:PVkDisplayPlaneInfo2KHR);
  procedure vkdispose(var a:PVkDisplayPlaneInfo2KHR);
  procedure vkadd(out a:PVkDisplayPlaneInfo2KHR);
  procedure vkrem(var a:PVkDisplayPlaneInfo2KHR);
  procedure vknew(out a:PVkDisplayPlaneCapabilities2KHR);
  procedure vkdispose(var a:PVkDisplayPlaneCapabilities2KHR);
  procedure vkadd(out a:PVkDisplayPlaneCapabilities2KHR);
  procedure vkrem(var a:PVkDisplayPlaneCapabilities2KHR);
  procedure vknew(out a:PVkSharedPresentSurfaceCapabilitiesKHR);
  procedure vkdispose(var a:PVkSharedPresentSurfaceCapabilitiesKHR);
  procedure vkadd(out a:PVkSharedPresentSurfaceCapabilitiesKHR);
  procedure vkrem(var a:PVkSharedPresentSurfaceCapabilitiesKHR);
  procedure vknew(out a:PVkPhysicalDevice16BitStorageFeatures);
  procedure vkdispose(var a:PVkPhysicalDevice16BitStorageFeatures);
  procedure vkadd(out a:PVkPhysicalDevice16BitStorageFeatures);
  procedure vkrem(var a:PVkPhysicalDevice16BitStorageFeatures);
  procedure vknew(out a:PVkPhysicalDeviceSubgroupProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceSubgroupProperties);
  procedure vkadd(out a:PVkPhysicalDeviceSubgroupProperties);
  procedure vkrem(var a:PVkPhysicalDeviceSubgroupProperties);
  procedure vknew(out a:PVkPhysicalDeviceShaderSubgroupExtendedTypesFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceShaderSubgroupExtendedTypesFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceShaderSubgroupExtendedTypesFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceShaderSubgroupExtendedTypesFeatures);
  procedure vknew(out a:PVkBufferMemoryRequirementsInfo2);
  procedure vkdispose(var a:PVkBufferMemoryRequirementsInfo2);
  procedure vkadd(out a:PVkBufferMemoryRequirementsInfo2);
  procedure vkrem(var a:PVkBufferMemoryRequirementsInfo2);
  procedure vknew(out a:PVkImageMemoryRequirementsInfo2);
  procedure vkdispose(var a:PVkImageMemoryRequirementsInfo2);
  procedure vkadd(out a:PVkImageMemoryRequirementsInfo2);
  procedure vkrem(var a:PVkImageMemoryRequirementsInfo2);
  procedure vknew(out a:PVkImageSparseMemoryRequirementsInfo2);
  procedure vkdispose(var a:PVkImageSparseMemoryRequirementsInfo2);
  procedure vkadd(out a:PVkImageSparseMemoryRequirementsInfo2);
  procedure vkrem(var a:PVkImageSparseMemoryRequirementsInfo2);
  procedure vknew(out a:PVkMemoryRequirements2);
  procedure vkdispose(var a:PVkMemoryRequirements2);
  procedure vkadd(out a:PVkMemoryRequirements2);
  procedure vkrem(var a:PVkMemoryRequirements2);
  procedure vknew(out a:PVkSparseImageMemoryRequirements2);
  procedure vkdispose(var a:PVkSparseImageMemoryRequirements2);
  procedure vkadd(out a:PVkSparseImageMemoryRequirements2);
  procedure vkrem(var a:PVkSparseImageMemoryRequirements2);
  procedure vknew(out a:PVkPhysicalDevicePointClippingProperties);
  procedure vkdispose(var a:PVkPhysicalDevicePointClippingProperties);
  procedure vkadd(out a:PVkPhysicalDevicePointClippingProperties);
  procedure vkrem(var a:PVkPhysicalDevicePointClippingProperties);
  procedure vknew(out a:PVkMemoryDedicatedRequirements);
  procedure vkdispose(var a:PVkMemoryDedicatedRequirements);
  procedure vkadd(out a:PVkMemoryDedicatedRequirements);
  procedure vkrem(var a:PVkMemoryDedicatedRequirements);
  procedure vknew(out a:PVkMemoryDedicatedAllocateInfo);
  procedure vkdispose(var a:PVkMemoryDedicatedAllocateInfo);
  procedure vkadd(out a:PVkMemoryDedicatedAllocateInfo);
  procedure vkrem(var a:PVkMemoryDedicatedAllocateInfo);
  procedure vknew(out a:PVkImageViewUsageCreateInfo);
  procedure vkdispose(var a:PVkImageViewUsageCreateInfo);
  procedure vkadd(out a:PVkImageViewUsageCreateInfo);
  procedure vkrem(var a:PVkImageViewUsageCreateInfo);
  procedure vknew(out a:PVkPipelineTessellationDomainOriginStateCreateInfo);
  procedure vkdispose(var a:PVkPipelineTessellationDomainOriginStateCreateInfo);
  procedure vkadd(out a:PVkPipelineTessellationDomainOriginStateCreateInfo);
  procedure vkrem(var a:PVkPipelineTessellationDomainOriginStateCreateInfo);
  procedure vknew(out a:PVkSamplerYcbcrConversionInfo);
  procedure vkdispose(var a:PVkSamplerYcbcrConversionInfo);
  procedure vkadd(out a:PVkSamplerYcbcrConversionInfo);
  procedure vkrem(var a:PVkSamplerYcbcrConversionInfo);
  procedure vknew(out a:PVkSamplerYcbcrConversionCreateInfo);
  procedure vkdispose(var a:PVkSamplerYcbcrConversionCreateInfo);
  procedure vkadd(out a:PVkSamplerYcbcrConversionCreateInfo);
  procedure vkrem(var a:PVkSamplerYcbcrConversionCreateInfo);
  procedure vknew(out a:PVkBindImagePlaneMemoryInfo);
  procedure vkdispose(var a:PVkBindImagePlaneMemoryInfo);
  procedure vkadd(out a:PVkBindImagePlaneMemoryInfo);
  procedure vkrem(var a:PVkBindImagePlaneMemoryInfo);
  procedure vknew(out a:PVkImagePlaneMemoryRequirementsInfo);
  procedure vkdispose(var a:PVkImagePlaneMemoryRequirementsInfo);
  procedure vkadd(out a:PVkImagePlaneMemoryRequirementsInfo);
  procedure vkrem(var a:PVkImagePlaneMemoryRequirementsInfo);
  procedure vknew(out a:PVkPhysicalDeviceSamplerYcbcrConversionFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceSamplerYcbcrConversionFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceSamplerYcbcrConversionFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceSamplerYcbcrConversionFeatures);
  procedure vknew(out a:PVkSamplerYcbcrConversionImageFormatProperties);
  procedure vkdispose(var a:PVkSamplerYcbcrConversionImageFormatProperties);
  procedure vkadd(out a:PVkSamplerYcbcrConversionImageFormatProperties);
  procedure vkrem(var a:PVkSamplerYcbcrConversionImageFormatProperties);
  procedure vknew(out a:PVkTextureLODGatherFormatPropertiesAMD);
  procedure vkdispose(var a:PVkTextureLODGatherFormatPropertiesAMD);
  procedure vkadd(out a:PVkTextureLODGatherFormatPropertiesAMD);
  procedure vkrem(var a:PVkTextureLODGatherFormatPropertiesAMD);
  procedure vknew(out a:PVkConditionalRenderingBeginInfoEXT);
  procedure vkdispose(var a:PVkConditionalRenderingBeginInfoEXT);
  procedure vkadd(out a:PVkConditionalRenderingBeginInfoEXT);
  procedure vkrem(var a:PVkConditionalRenderingBeginInfoEXT);
  procedure vknew(out a:PVkProtectedSubmitInfo);
  procedure vkdispose(var a:PVkProtectedSubmitInfo);
  procedure vkadd(out a:PVkProtectedSubmitInfo);
  procedure vkrem(var a:PVkProtectedSubmitInfo);
  procedure vknew(out a:PVkPhysicalDeviceProtectedMemoryFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceProtectedMemoryFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceProtectedMemoryFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceProtectedMemoryFeatures);
  procedure vknew(out a:PVkPhysicalDeviceProtectedMemoryProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceProtectedMemoryProperties);
  procedure vkadd(out a:PVkPhysicalDeviceProtectedMemoryProperties);
  procedure vkrem(var a:PVkPhysicalDeviceProtectedMemoryProperties);
  procedure vknew(out a:PVkDeviceQueueInfo2);
  procedure vkdispose(var a:PVkDeviceQueueInfo2);
  procedure vkadd(out a:PVkDeviceQueueInfo2);
  procedure vkrem(var a:PVkDeviceQueueInfo2);
  procedure vknew(out a:PVkPipelineCoverageToColorStateCreateInfoNV);
  procedure vkdispose(var a:PVkPipelineCoverageToColorStateCreateInfoNV);
  procedure vkadd(out a:PVkPipelineCoverageToColorStateCreateInfoNV);
  procedure vkrem(var a:PVkPipelineCoverageToColorStateCreateInfoNV);
  procedure vknew(out a:PVkPhysicalDeviceSamplerFilterMinmaxProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceSamplerFilterMinmaxProperties);
  procedure vkadd(out a:PVkPhysicalDeviceSamplerFilterMinmaxProperties);
  procedure vkrem(var a:PVkPhysicalDeviceSamplerFilterMinmaxProperties);
  procedure vknew(out a:PVkSampleLocationEXT);
  procedure vkdispose(var a:PVkSampleLocationEXT);
  procedure vkadd(out a:PVkSampleLocationEXT);
  procedure vkrem(var a:PVkSampleLocationEXT);
  procedure vknew(out a:PVkSampleLocationsInfoEXT);
  procedure vkdispose(var a:PVkSampleLocationsInfoEXT);
  procedure vkadd(out a:PVkSampleLocationsInfoEXT);
  procedure vkrem(var a:PVkSampleLocationsInfoEXT);
  procedure vknew(out a:PVkAttachmentSampleLocationsEXT);
  procedure vkdispose(var a:PVkAttachmentSampleLocationsEXT);
  procedure vkadd(out a:PVkAttachmentSampleLocationsEXT);
  procedure vkrem(var a:PVkAttachmentSampleLocationsEXT);
  procedure vknew(out a:PVkSubpassSampleLocationsEXT);
  procedure vkdispose(var a:PVkSubpassSampleLocationsEXT);
  procedure vkadd(out a:PVkSubpassSampleLocationsEXT);
  procedure vkrem(var a:PVkSubpassSampleLocationsEXT);
  procedure vknew(out a:PVkRenderPassSampleLocationsBeginInfoEXT);
  procedure vkdispose(var a:PVkRenderPassSampleLocationsBeginInfoEXT);
  procedure vkadd(out a:PVkRenderPassSampleLocationsBeginInfoEXT);
  procedure vkrem(var a:PVkRenderPassSampleLocationsBeginInfoEXT);
  procedure vknew(out a:PVkPipelineSampleLocationsStateCreateInfoEXT);
  procedure vkdispose(var a:PVkPipelineSampleLocationsStateCreateInfoEXT);
  procedure vkadd(out a:PVkPipelineSampleLocationsStateCreateInfoEXT);
  procedure vkrem(var a:PVkPipelineSampleLocationsStateCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceSampleLocationsPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceSampleLocationsPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceSampleLocationsPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceSampleLocationsPropertiesEXT);
  procedure vknew(out a:PVkMultisamplePropertiesEXT);
  procedure vkdispose(var a:PVkMultisamplePropertiesEXT);
  procedure vkadd(out a:PVkMultisamplePropertiesEXT);
  procedure vkrem(var a:PVkMultisamplePropertiesEXT);
  procedure vknew(out a:PVkSamplerReductionModeCreateInfo);
  procedure vkdispose(var a:PVkSamplerReductionModeCreateInfo);
  procedure vkadd(out a:PVkSamplerReductionModeCreateInfo);
  procedure vkrem(var a:PVkSamplerReductionModeCreateInfo);
  procedure vknew(out a:PVkPhysicalDeviceBlendOperationAdvancedFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceBlendOperationAdvancedFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceBlendOperationAdvancedFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceBlendOperationAdvancedFeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceBlendOperationAdvancedPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceBlendOperationAdvancedPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceBlendOperationAdvancedPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceBlendOperationAdvancedPropertiesEXT);
  procedure vknew(out a:PVkPipelineColorBlendAdvancedStateCreateInfoEXT);
  procedure vkdispose(var a:PVkPipelineColorBlendAdvancedStateCreateInfoEXT);
  procedure vkadd(out a:PVkPipelineColorBlendAdvancedStateCreateInfoEXT);
  procedure vkrem(var a:PVkPipelineColorBlendAdvancedStateCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceInlineUniformBlockFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceInlineUniformBlockFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceInlineUniformBlockFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceInlineUniformBlockFeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceInlineUniformBlockPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceInlineUniformBlockPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceInlineUniformBlockPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceInlineUniformBlockPropertiesEXT);
  procedure vknew(out a:PVkWriteDescriptorSetInlineUniformBlockEXT);
  procedure vkdispose(var a:PVkWriteDescriptorSetInlineUniformBlockEXT);
  procedure vkadd(out a:PVkWriteDescriptorSetInlineUniformBlockEXT);
  procedure vkrem(var a:PVkWriteDescriptorSetInlineUniformBlockEXT);
  procedure vknew(out a:PVkDescriptorPoolInlineUniformBlockCreateInfoEXT);
  procedure vkdispose(var a:PVkDescriptorPoolInlineUniformBlockCreateInfoEXT);
  procedure vkadd(out a:PVkDescriptorPoolInlineUniformBlockCreateInfoEXT);
  procedure vkrem(var a:PVkDescriptorPoolInlineUniformBlockCreateInfoEXT);
  procedure vknew(out a:PVkPipelineCoverageModulationStateCreateInfoNV);
  procedure vkdispose(var a:PVkPipelineCoverageModulationStateCreateInfoNV);
  procedure vkadd(out a:PVkPipelineCoverageModulationStateCreateInfoNV);
  procedure vkrem(var a:PVkPipelineCoverageModulationStateCreateInfoNV);
  procedure vknew(out a:PVkImageFormatListCreateInfo);
  procedure vkdispose(var a:PVkImageFormatListCreateInfo);
  procedure vkadd(out a:PVkImageFormatListCreateInfo);
  procedure vkrem(var a:PVkImageFormatListCreateInfo);
  procedure vknew(out a:PVkValidationCacheCreateInfoEXT);
  procedure vkdispose(var a:PVkValidationCacheCreateInfoEXT);
  procedure vkadd(out a:PVkValidationCacheCreateInfoEXT);
  procedure vkrem(var a:PVkValidationCacheCreateInfoEXT);
  procedure vknew(out a:PVkShaderModuleValidationCacheCreateInfoEXT);
  procedure vkdispose(var a:PVkShaderModuleValidationCacheCreateInfoEXT);
  procedure vkadd(out a:PVkShaderModuleValidationCacheCreateInfoEXT);
  procedure vkrem(var a:PVkShaderModuleValidationCacheCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceMaintenance3Properties);
  procedure vkdispose(var a:PVkPhysicalDeviceMaintenance3Properties);
  procedure vkadd(out a:PVkPhysicalDeviceMaintenance3Properties);
  procedure vkrem(var a:PVkPhysicalDeviceMaintenance3Properties);
  procedure vknew(out a:PVkDescriptorSetLayoutSupport);
  procedure vkdispose(var a:PVkDescriptorSetLayoutSupport);
  procedure vkadd(out a:PVkDescriptorSetLayoutSupport);
  procedure vkrem(var a:PVkDescriptorSetLayoutSupport);
  procedure vknew(out a:PVkPhysicalDeviceShaderDrawParametersFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceShaderDrawParametersFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceShaderDrawParametersFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceShaderDrawParametersFeatures);
  procedure vknew(out a:PVkPhysicalDeviceShaderFloat16Int8Features);
  procedure vkdispose(var a:PVkPhysicalDeviceShaderFloat16Int8Features);
  procedure vkadd(out a:PVkPhysicalDeviceShaderFloat16Int8Features);
  procedure vkrem(var a:PVkPhysicalDeviceShaderFloat16Int8Features);
  procedure vknew(out a:PVkPhysicalDeviceFloatControlsProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceFloatControlsProperties);
  procedure vkadd(out a:PVkPhysicalDeviceFloatControlsProperties);
  procedure vkrem(var a:PVkPhysicalDeviceFloatControlsProperties);
  procedure vknew(out a:PVkPhysicalDeviceHostQueryResetFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceHostQueryResetFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceHostQueryResetFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceHostQueryResetFeatures);
  procedure vknew(out a:PVkNativeBufferUsage2ANDROID);
  procedure vkdispose(var a:PVkNativeBufferUsage2ANDROID);
  procedure vkadd(out a:PVkNativeBufferUsage2ANDROID);
  procedure vkrem(var a:PVkNativeBufferUsage2ANDROID);
  procedure vknew(out a:PVkNativeBufferANDROID);
  procedure vkdispose(var a:PVkNativeBufferANDROID);
  procedure vkadd(out a:PVkNativeBufferANDROID);
  procedure vkrem(var a:PVkNativeBufferANDROID);
  procedure vknew(out a:PVkSwapchainImageCreateInfoANDROID);
  procedure vkdispose(var a:PVkSwapchainImageCreateInfoANDROID);
  procedure vkadd(out a:PVkSwapchainImageCreateInfoANDROID);
  procedure vkrem(var a:PVkSwapchainImageCreateInfoANDROID);
  procedure vknew(out a:PVkPhysicalDevicePresentationPropertiesANDROID);
  procedure vkdispose(var a:PVkPhysicalDevicePresentationPropertiesANDROID);
  procedure vkadd(out a:PVkPhysicalDevicePresentationPropertiesANDROID);
  procedure vkrem(var a:PVkPhysicalDevicePresentationPropertiesANDROID);
  procedure vknew(out a:PVkShaderResourceUsageAMD);
  procedure vkdispose(var a:PVkShaderResourceUsageAMD);
  procedure vkadd(out a:PVkShaderResourceUsageAMD);
  procedure vkrem(var a:PVkShaderResourceUsageAMD);
  procedure vknew(out a:PVkShaderStatisticsInfoAMD);
  procedure vkdispose(var a:PVkShaderStatisticsInfoAMD);
  procedure vkadd(out a:PVkShaderStatisticsInfoAMD);
  procedure vkrem(var a:PVkShaderStatisticsInfoAMD);
  procedure vknew(out a:PVkDeviceQueueGlobalPriorityCreateInfoEXT);
  procedure vkdispose(var a:PVkDeviceQueueGlobalPriorityCreateInfoEXT);
  procedure vkadd(out a:PVkDeviceQueueGlobalPriorityCreateInfoEXT);
  procedure vkrem(var a:PVkDeviceQueueGlobalPriorityCreateInfoEXT);
  procedure vknew(out a:PVkDebugUtilsObjectNameInfoEXT);
  procedure vkdispose(var a:PVkDebugUtilsObjectNameInfoEXT);
  procedure vkadd(out a:PVkDebugUtilsObjectNameInfoEXT);
  procedure vkrem(var a:PVkDebugUtilsObjectNameInfoEXT);
  procedure vknew(out a:PVkDebugUtilsObjectTagInfoEXT);
  procedure vkdispose(var a:PVkDebugUtilsObjectTagInfoEXT);
  procedure vkadd(out a:PVkDebugUtilsObjectTagInfoEXT);
  procedure vkrem(var a:PVkDebugUtilsObjectTagInfoEXT);
  procedure vknew(out a:PVkDebugUtilsLabelEXT);
  procedure vkdispose(var a:PVkDebugUtilsLabelEXT);
  procedure vkadd(out a:PVkDebugUtilsLabelEXT);
  procedure vkrem(var a:PVkDebugUtilsLabelEXT);
  procedure vknew(out a:PVkDebugUtilsMessengerCallbackDataEXT);
  procedure vkdispose(var a:PVkDebugUtilsMessengerCallbackDataEXT);
  procedure vkadd(out a:PVkDebugUtilsMessengerCallbackDataEXT);
  procedure vkrem(var a:PVkDebugUtilsMessengerCallbackDataEXT);
  procedure vknew(out a:PVkImportMemoryHostPointerInfoEXT);
  procedure vkdispose(var a:PVkImportMemoryHostPointerInfoEXT);
  procedure vkadd(out a:PVkImportMemoryHostPointerInfoEXT);
  procedure vkrem(var a:PVkImportMemoryHostPointerInfoEXT);
  procedure vknew(out a:PVkMemoryHostPointerPropertiesEXT);
  procedure vkdispose(var a:PVkMemoryHostPointerPropertiesEXT);
  procedure vkadd(out a:PVkMemoryHostPointerPropertiesEXT);
  procedure vkrem(var a:PVkMemoryHostPointerPropertiesEXT);
  procedure vknew(out a:PVkPhysicalDeviceExternalMemoryHostPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceExternalMemoryHostPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceExternalMemoryHostPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceExternalMemoryHostPropertiesEXT);
  procedure vknew(out a:PVkPhysicalDeviceConservativeRasterizationPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceConservativeRasterizationPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceConservativeRasterizationPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceConservativeRasterizationPropertiesEXT);
  procedure vknew(out a:PVkCalibratedTimestampInfoEXT);
  procedure vkdispose(var a:PVkCalibratedTimestampInfoEXT);
  procedure vkadd(out a:PVkCalibratedTimestampInfoEXT);
  procedure vkrem(var a:PVkCalibratedTimestampInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceShaderCorePropertiesAMD);
  procedure vkdispose(var a:PVkPhysicalDeviceShaderCorePropertiesAMD);
  procedure vkadd(out a:PVkPhysicalDeviceShaderCorePropertiesAMD);
  procedure vkrem(var a:PVkPhysicalDeviceShaderCorePropertiesAMD);
  procedure vknew(out a:PVkPhysicalDeviceShaderCoreProperties2AMD);
  procedure vkdispose(var a:PVkPhysicalDeviceShaderCoreProperties2AMD);
  procedure vkadd(out a:PVkPhysicalDeviceShaderCoreProperties2AMD);
  procedure vkrem(var a:PVkPhysicalDeviceShaderCoreProperties2AMD);
  procedure vknew(out a:PVkPipelineRasterizationConservativeStateCreateInfoEXT);
  procedure vkdispose(var a:PVkPipelineRasterizationConservativeStateCreateInfoEXT);
  procedure vkadd(out a:PVkPipelineRasterizationConservativeStateCreateInfoEXT);
  procedure vkrem(var a:PVkPipelineRasterizationConservativeStateCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceDescriptorIndexingFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceDescriptorIndexingFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceDescriptorIndexingFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceDescriptorIndexingFeatures);
  procedure vknew(out a:PVkPhysicalDeviceDescriptorIndexingProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceDescriptorIndexingProperties);
  procedure vkadd(out a:PVkPhysicalDeviceDescriptorIndexingProperties);
  procedure vkrem(var a:PVkPhysicalDeviceDescriptorIndexingProperties);
  procedure vknew(out a:PVkDescriptorSetLayoutBindingFlagsCreateInfo);
  procedure vkdispose(var a:PVkDescriptorSetLayoutBindingFlagsCreateInfo);
  procedure vkadd(out a:PVkDescriptorSetLayoutBindingFlagsCreateInfo);
  procedure vkrem(var a:PVkDescriptorSetLayoutBindingFlagsCreateInfo);
  procedure vknew(out a:PVkDescriptorSetVariableDescriptorCountAllocateInfo);
  procedure vkdispose(var a:PVkDescriptorSetVariableDescriptorCountAllocateInfo);
  procedure vkadd(out a:PVkDescriptorSetVariableDescriptorCountAllocateInfo);
  procedure vkrem(var a:PVkDescriptorSetVariableDescriptorCountAllocateInfo);
  procedure vknew(out a:PVkDescriptorSetVariableDescriptorCountLayoutSupport);
  procedure vkdispose(var a:PVkDescriptorSetVariableDescriptorCountLayoutSupport);
  procedure vkadd(out a:PVkDescriptorSetVariableDescriptorCountLayoutSupport);
  procedure vkrem(var a:PVkDescriptorSetVariableDescriptorCountLayoutSupport);
  procedure vknew(out a:PVkAttachmentDescription2);
  procedure vkdispose(var a:PVkAttachmentDescription2);
  procedure vkadd(out a:PVkAttachmentDescription2);
  procedure vkrem(var a:PVkAttachmentDescription2);
  procedure vknew(out a:PVkAttachmentReference2);
  procedure vkdispose(var a:PVkAttachmentReference2);
  procedure vkadd(out a:PVkAttachmentReference2);
  procedure vkrem(var a:PVkAttachmentReference2);
  procedure vknew(out a:PVkSubpassDescription2);
  procedure vkdispose(var a:PVkSubpassDescription2);
  procedure vkadd(out a:PVkSubpassDescription2);
  procedure vkrem(var a:PVkSubpassDescription2);
  procedure vknew(out a:PVkSubpassDependency2);
  procedure vkdispose(var a:PVkSubpassDependency2);
  procedure vkadd(out a:PVkSubpassDependency2);
  procedure vkrem(var a:PVkSubpassDependency2);
  procedure vknew(out a:PVkRenderPassCreateInfo2);
  procedure vkdispose(var a:PVkRenderPassCreateInfo2);
  procedure vkadd(out a:PVkRenderPassCreateInfo2);
  procedure vkrem(var a:PVkRenderPassCreateInfo2);
  procedure vknew(out a:PVkSubpassBeginInfo);
  procedure vkdispose(var a:PVkSubpassBeginInfo);
  procedure vkadd(out a:PVkSubpassBeginInfo);
  procedure vkrem(var a:PVkSubpassBeginInfo);
  procedure vknew(out a:PVkSubpassEndInfo);
  procedure vkdispose(var a:PVkSubpassEndInfo);
  procedure vkadd(out a:PVkSubpassEndInfo);
  procedure vkrem(var a:PVkSubpassEndInfo);
  procedure vknew(out a:PVkPhysicalDeviceTimelineSemaphoreFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceTimelineSemaphoreFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceTimelineSemaphoreFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceTimelineSemaphoreFeatures);
  procedure vknew(out a:PVkPhysicalDeviceTimelineSemaphoreProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceTimelineSemaphoreProperties);
  procedure vkadd(out a:PVkPhysicalDeviceTimelineSemaphoreProperties);
  procedure vkrem(var a:PVkPhysicalDeviceTimelineSemaphoreProperties);
  procedure vknew(out a:PVkSemaphoreTypeCreateInfo);
  procedure vkdispose(var a:PVkSemaphoreTypeCreateInfo);
  procedure vkadd(out a:PVkSemaphoreTypeCreateInfo);
  procedure vkrem(var a:PVkSemaphoreTypeCreateInfo);
  procedure vknew(out a:PVkTimelineSemaphoreSubmitInfo);
  procedure vkdispose(var a:PVkTimelineSemaphoreSubmitInfo);
  procedure vkadd(out a:PVkTimelineSemaphoreSubmitInfo);
  procedure vkrem(var a:PVkTimelineSemaphoreSubmitInfo);
  procedure vknew(out a:PVkSemaphoreWaitInfo);
  procedure vkdispose(var a:PVkSemaphoreWaitInfo);
  procedure vkadd(out a:PVkSemaphoreWaitInfo);
  procedure vkrem(var a:PVkSemaphoreWaitInfo);
  procedure vknew(out a:PVkSemaphoreSignalInfo);
  procedure vkdispose(var a:PVkSemaphoreSignalInfo);
  procedure vkadd(out a:PVkSemaphoreSignalInfo);
  procedure vkrem(var a:PVkSemaphoreSignalInfo);
  procedure vknew(out a:PVkVertexInputBindingDivisorDescriptionEXT);
  procedure vkdispose(var a:PVkVertexInputBindingDivisorDescriptionEXT);
  procedure vkadd(out a:PVkVertexInputBindingDivisorDescriptionEXT);
  procedure vkrem(var a:PVkVertexInputBindingDivisorDescriptionEXT);
  procedure vknew(out a:PVkPipelineVertexInputDivisorStateCreateInfoEXT);
  procedure vkdispose(var a:PVkPipelineVertexInputDivisorStateCreateInfoEXT);
  procedure vkadd(out a:PVkPipelineVertexInputDivisorStateCreateInfoEXT);
  procedure vkrem(var a:PVkPipelineVertexInputDivisorStateCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceVertexAttributeDivisorPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceVertexAttributeDivisorPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceVertexAttributeDivisorPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceVertexAttributeDivisorPropertiesEXT);
  procedure vknew(out a:PVkPhysicalDevicePCIBusInfoPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDevicePCIBusInfoPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDevicePCIBusInfoPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDevicePCIBusInfoPropertiesEXT);
  procedure vknew(out a:PVkImportAndroidHardwareBufferInfoANDROID);
  procedure vkdispose(var a:PVkImportAndroidHardwareBufferInfoANDROID);
  procedure vkadd(out a:PVkImportAndroidHardwareBufferInfoANDROID);
  procedure vkrem(var a:PVkImportAndroidHardwareBufferInfoANDROID);
  procedure vknew(out a:PVkAndroidHardwareBufferUsageANDROID);
  procedure vkdispose(var a:PVkAndroidHardwareBufferUsageANDROID);
  procedure vkadd(out a:PVkAndroidHardwareBufferUsageANDROID);
  procedure vkrem(var a:PVkAndroidHardwareBufferUsageANDROID);
  procedure vknew(out a:PVkAndroidHardwareBufferPropertiesANDROID);
  procedure vkdispose(var a:PVkAndroidHardwareBufferPropertiesANDROID);
  procedure vkadd(out a:PVkAndroidHardwareBufferPropertiesANDROID);
  procedure vkrem(var a:PVkAndroidHardwareBufferPropertiesANDROID);
  procedure vknew(out a:PVkMemoryGetAndroidHardwareBufferInfoANDROID);
  procedure vkdispose(var a:PVkMemoryGetAndroidHardwareBufferInfoANDROID);
  procedure vkadd(out a:PVkMemoryGetAndroidHardwareBufferInfoANDROID);
  procedure vkrem(var a:PVkMemoryGetAndroidHardwareBufferInfoANDROID);
  procedure vknew(out a:PVkAndroidHardwareBufferFormatPropertiesANDROID);
  procedure vkdispose(var a:PVkAndroidHardwareBufferFormatPropertiesANDROID);
  procedure vkadd(out a:PVkAndroidHardwareBufferFormatPropertiesANDROID);
  procedure vkrem(var a:PVkAndroidHardwareBufferFormatPropertiesANDROID);
  procedure vknew(out a:PVkCommandBufferInheritanceConditionalRenderingInfoEXT);
  procedure vkdispose(var a:PVkCommandBufferInheritanceConditionalRenderingInfoEXT);
  procedure vkadd(out a:PVkCommandBufferInheritanceConditionalRenderingInfoEXT);
  procedure vkrem(var a:PVkCommandBufferInheritanceConditionalRenderingInfoEXT);
  procedure vknew(out a:PVkExternalFormatANDROID);
  procedure vkdispose(var a:PVkExternalFormatANDROID);
  procedure vkadd(out a:PVkExternalFormatANDROID);
  procedure vkrem(var a:PVkExternalFormatANDROID);
  procedure vknew(out a:PVkPhysicalDevice8BitStorageFeatures);
  procedure vkdispose(var a:PVkPhysicalDevice8BitStorageFeatures);
  procedure vkadd(out a:PVkPhysicalDevice8BitStorageFeatures);
  procedure vkrem(var a:PVkPhysicalDevice8BitStorageFeatures);
  procedure vknew(out a:PVkPhysicalDeviceConditionalRenderingFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceConditionalRenderingFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceConditionalRenderingFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceConditionalRenderingFeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceVulkanMemoryModelFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceVulkanMemoryModelFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceVulkanMemoryModelFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceVulkanMemoryModelFeatures);
  procedure vknew(out a:PVkPhysicalDeviceShaderAtomicInt64Features);
  procedure vkdispose(var a:PVkPhysicalDeviceShaderAtomicInt64Features);
  procedure vkadd(out a:PVkPhysicalDeviceShaderAtomicInt64Features);
  procedure vkrem(var a:PVkPhysicalDeviceShaderAtomicInt64Features);
  procedure vknew(out a:PVkPhysicalDeviceVertexAttributeDivisorFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceVertexAttributeDivisorFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceVertexAttributeDivisorFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceVertexAttributeDivisorFeaturesEXT);
  procedure vknew(out a:PVkQueueFamilyCheckpointPropertiesNV);
  procedure vkdispose(var a:PVkQueueFamilyCheckpointPropertiesNV);
  procedure vkadd(out a:PVkQueueFamilyCheckpointPropertiesNV);
  procedure vkrem(var a:PVkQueueFamilyCheckpointPropertiesNV);
  procedure vknew(out a:PVkCheckpointDataNV);
  procedure vkdispose(var a:PVkCheckpointDataNV);
  procedure vkadd(out a:PVkCheckpointDataNV);
  procedure vkrem(var a:PVkCheckpointDataNV);
  procedure vknew(out a:PVkPhysicalDeviceDepthStencilResolveProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceDepthStencilResolveProperties);
  procedure vkadd(out a:PVkPhysicalDeviceDepthStencilResolveProperties);
  procedure vkrem(var a:PVkPhysicalDeviceDepthStencilResolveProperties);
  procedure vknew(out a:PVkSubpassDescriptionDepthStencilResolve);
  procedure vkdispose(var a:PVkSubpassDescriptionDepthStencilResolve);
  procedure vkadd(out a:PVkSubpassDescriptionDepthStencilResolve);
  procedure vkrem(var a:PVkSubpassDescriptionDepthStencilResolve);
  procedure vknew(out a:PVkImageViewASTCDecodeModeEXT);
  procedure vkdispose(var a:PVkImageViewASTCDecodeModeEXT);
  procedure vkadd(out a:PVkImageViewASTCDecodeModeEXT);
  procedure vkrem(var a:PVkImageViewASTCDecodeModeEXT);
  procedure vknew(out a:PVkPhysicalDeviceASTCDecodeFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceASTCDecodeFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceASTCDecodeFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceASTCDecodeFeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceTransformFeedbackFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceTransformFeedbackFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceTransformFeedbackFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceTransformFeedbackFeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceTransformFeedbackPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceTransformFeedbackPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceTransformFeedbackPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceTransformFeedbackPropertiesEXT);
  procedure vknew(out a:PVkPipelineRasterizationStateStreamCreateInfoEXT);
  procedure vkdispose(var a:PVkPipelineRasterizationStateStreamCreateInfoEXT);
  procedure vkadd(out a:PVkPipelineRasterizationStateStreamCreateInfoEXT);
  procedure vkrem(var a:PVkPipelineRasterizationStateStreamCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceRepresentativeFragmentTestFeaturesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceRepresentativeFragmentTestFeaturesNV);
  procedure vkadd(out a:PVkPhysicalDeviceRepresentativeFragmentTestFeaturesNV);
  procedure vkrem(var a:PVkPhysicalDeviceRepresentativeFragmentTestFeaturesNV);
  procedure vknew(out a:PVkPipelineRepresentativeFragmentTestStateCreateInfoNV);
  procedure vkdispose(var a:PVkPipelineRepresentativeFragmentTestStateCreateInfoNV);
  procedure vkadd(out a:PVkPipelineRepresentativeFragmentTestStateCreateInfoNV);
  procedure vkrem(var a:PVkPipelineRepresentativeFragmentTestStateCreateInfoNV);
  procedure vknew(out a:PVkPhysicalDeviceExclusiveScissorFeaturesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceExclusiveScissorFeaturesNV);
  procedure vkadd(out a:PVkPhysicalDeviceExclusiveScissorFeaturesNV);
  procedure vkrem(var a:PVkPhysicalDeviceExclusiveScissorFeaturesNV);
  procedure vknew(out a:PVkPipelineViewportExclusiveScissorStateCreateInfoNV);
  procedure vkdispose(var a:PVkPipelineViewportExclusiveScissorStateCreateInfoNV);
  procedure vkadd(out a:PVkPipelineViewportExclusiveScissorStateCreateInfoNV);
  procedure vkrem(var a:PVkPipelineViewportExclusiveScissorStateCreateInfoNV);
  procedure vknew(out a:PVkPhysicalDeviceCornerSampledImageFeaturesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceCornerSampledImageFeaturesNV);
  procedure vkadd(out a:PVkPhysicalDeviceCornerSampledImageFeaturesNV);
  procedure vkrem(var a:PVkPhysicalDeviceCornerSampledImageFeaturesNV);
  procedure vknew(out a:PVkPhysicalDeviceComputeShaderDerivativesFeaturesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceComputeShaderDerivativesFeaturesNV);
  procedure vkadd(out a:PVkPhysicalDeviceComputeShaderDerivativesFeaturesNV);
  procedure vkrem(var a:PVkPhysicalDeviceComputeShaderDerivativesFeaturesNV);
  procedure vknew(out a:PVkPhysicalDeviceFragmentShaderBarycentricFeaturesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceFragmentShaderBarycentricFeaturesNV);
  procedure vkadd(out a:PVkPhysicalDeviceFragmentShaderBarycentricFeaturesNV);
  procedure vkrem(var a:PVkPhysicalDeviceFragmentShaderBarycentricFeaturesNV);
  procedure vknew(out a:PVkPhysicalDeviceShaderImageFootprintFeaturesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceShaderImageFootprintFeaturesNV);
  procedure vkadd(out a:PVkPhysicalDeviceShaderImageFootprintFeaturesNV);
  procedure vkrem(var a:PVkPhysicalDeviceShaderImageFootprintFeaturesNV);
  procedure vknew(out a:PVkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV);
  procedure vkadd(out a:PVkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV);
  procedure vkrem(var a:PVkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV);
  procedure vknew(out a:PVkShadingRatePaletteNV);
  procedure vkdispose(var a:PVkShadingRatePaletteNV);
  procedure vkadd(out a:PVkShadingRatePaletteNV);
  procedure vkrem(var a:PVkShadingRatePaletteNV);
  procedure vknew(out a:PVkPipelineViewportShadingRateImageStateCreateInfoNV);
  procedure vkdispose(var a:PVkPipelineViewportShadingRateImageStateCreateInfoNV);
  procedure vkadd(out a:PVkPipelineViewportShadingRateImageStateCreateInfoNV);
  procedure vkrem(var a:PVkPipelineViewportShadingRateImageStateCreateInfoNV);
  procedure vknew(out a:PVkPhysicalDeviceShadingRateImageFeaturesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceShadingRateImageFeaturesNV);
  procedure vkadd(out a:PVkPhysicalDeviceShadingRateImageFeaturesNV);
  procedure vkrem(var a:PVkPhysicalDeviceShadingRateImageFeaturesNV);
  procedure vknew(out a:PVkPhysicalDeviceShadingRateImagePropertiesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceShadingRateImagePropertiesNV);
  procedure vkadd(out a:PVkPhysicalDeviceShadingRateImagePropertiesNV);
  procedure vkrem(var a:PVkPhysicalDeviceShadingRateImagePropertiesNV);
  procedure vknew(out a:PVkCoarseSampleLocationNV);
  procedure vkdispose(var a:PVkCoarseSampleLocationNV);
  procedure vkadd(out a:PVkCoarseSampleLocationNV);
  procedure vkrem(var a:PVkCoarseSampleLocationNV);
  procedure vknew(out a:PVkCoarseSampleOrderCustomNV);
  procedure vkdispose(var a:PVkCoarseSampleOrderCustomNV);
  procedure vkadd(out a:PVkCoarseSampleOrderCustomNV);
  procedure vkrem(var a:PVkCoarseSampleOrderCustomNV);
  procedure vknew(out a:PVkPipelineViewportCoarseSampleOrderStateCreateInfoNV);
  procedure vkdispose(var a:PVkPipelineViewportCoarseSampleOrderStateCreateInfoNV);
  procedure vkadd(out a:PVkPipelineViewportCoarseSampleOrderStateCreateInfoNV);
  procedure vkrem(var a:PVkPipelineViewportCoarseSampleOrderStateCreateInfoNV);
  procedure vknew(out a:PVkPhysicalDeviceMeshShaderFeaturesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceMeshShaderFeaturesNV);
  procedure vkadd(out a:PVkPhysicalDeviceMeshShaderFeaturesNV);
  procedure vkrem(var a:PVkPhysicalDeviceMeshShaderFeaturesNV);
  procedure vknew(out a:PVkPhysicalDeviceMeshShaderPropertiesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceMeshShaderPropertiesNV);
  procedure vkadd(out a:PVkPhysicalDeviceMeshShaderPropertiesNV);
  procedure vkrem(var a:PVkPhysicalDeviceMeshShaderPropertiesNV);
  procedure vknew(out a:PVkDrawMeshTasksIndirectCommandNV);
  procedure vkdispose(var a:PVkDrawMeshTasksIndirectCommandNV);
  procedure vkadd(out a:PVkDrawMeshTasksIndirectCommandNV);
  procedure vkrem(var a:PVkDrawMeshTasksIndirectCommandNV);
  procedure vknew(out a:PVkRayTracingShaderGroupCreateInfoNV);
  procedure vkdispose(var a:PVkRayTracingShaderGroupCreateInfoNV);
  procedure vkadd(out a:PVkRayTracingShaderGroupCreateInfoNV);
  procedure vkrem(var a:PVkRayTracingShaderGroupCreateInfoNV);
  procedure vknew(out a:PVkRayTracingPipelineCreateInfoNV);
  procedure vkdispose(var a:PVkRayTracingPipelineCreateInfoNV);
  procedure vkadd(out a:PVkRayTracingPipelineCreateInfoNV);
  procedure vkrem(var a:PVkRayTracingPipelineCreateInfoNV);
  procedure vknew(out a:PVkGeometryTrianglesNV);
  procedure vkdispose(var a:PVkGeometryTrianglesNV);
  procedure vkadd(out a:PVkGeometryTrianglesNV);
  procedure vkrem(var a:PVkGeometryTrianglesNV);
  procedure vknew(out a:PVkGeometryAABBNV);
  procedure vkdispose(var a:PVkGeometryAABBNV);
  procedure vkadd(out a:PVkGeometryAABBNV);
  procedure vkrem(var a:PVkGeometryAABBNV);
  procedure vknew(out a:PVkGeometryDataNV);
  procedure vkdispose(var a:PVkGeometryDataNV);
  procedure vkadd(out a:PVkGeometryDataNV);
  procedure vkrem(var a:PVkGeometryDataNV);
  procedure vknew(out a:PVkGeometryNV);
  procedure vkdispose(var a:PVkGeometryNV);
  procedure vkadd(out a:PVkGeometryNV);
  procedure vkrem(var a:PVkGeometryNV);
  procedure vknew(out a:PVkAccelerationStructureInfoNV);
  procedure vkdispose(var a:PVkAccelerationStructureInfoNV);
  procedure vkadd(out a:PVkAccelerationStructureInfoNV);
  procedure vkrem(var a:PVkAccelerationStructureInfoNV);
  procedure vknew(out a:PVkAccelerationStructureCreateInfoNV);
  procedure vkdispose(var a:PVkAccelerationStructureCreateInfoNV);
  procedure vkadd(out a:PVkAccelerationStructureCreateInfoNV);
  procedure vkrem(var a:PVkAccelerationStructureCreateInfoNV);
  procedure vknew(out a:PVkBindAccelerationStructureMemoryInfoNV);
  procedure vkdispose(var a:PVkBindAccelerationStructureMemoryInfoNV);
  procedure vkadd(out a:PVkBindAccelerationStructureMemoryInfoNV);
  procedure vkrem(var a:PVkBindAccelerationStructureMemoryInfoNV);
  procedure vknew(out a:PVkWriteDescriptorSetAccelerationStructureNV);
  procedure vkdispose(var a:PVkWriteDescriptorSetAccelerationStructureNV);
  procedure vkadd(out a:PVkWriteDescriptorSetAccelerationStructureNV);
  procedure vkrem(var a:PVkWriteDescriptorSetAccelerationStructureNV);
  procedure vknew(out a:PVkAccelerationStructureMemoryRequirementsInfoNV);
  procedure vkdispose(var a:PVkAccelerationStructureMemoryRequirementsInfoNV);
  procedure vkadd(out a:PVkAccelerationStructureMemoryRequirementsInfoNV);
  procedure vkrem(var a:PVkAccelerationStructureMemoryRequirementsInfoNV);
  procedure vknew(out a:PVkPhysicalDeviceRayTracingPropertiesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceRayTracingPropertiesNV);
  procedure vkadd(out a:PVkPhysicalDeviceRayTracingPropertiesNV);
  procedure vkrem(var a:PVkPhysicalDeviceRayTracingPropertiesNV);
  procedure vknew(out a:PVkDrmFormatModifierPropertiesEXT);
  procedure vkdispose(var a:PVkDrmFormatModifierPropertiesEXT);
  procedure vkadd(out a:PVkDrmFormatModifierPropertiesEXT);
  procedure vkrem(var a:PVkDrmFormatModifierPropertiesEXT);
  procedure vknew(out a:PVkPhysicalDeviceImageDrmFormatModifierInfoEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceImageDrmFormatModifierInfoEXT);
  procedure vkadd(out a:PVkPhysicalDeviceImageDrmFormatModifierInfoEXT);
  procedure vkrem(var a:PVkPhysicalDeviceImageDrmFormatModifierInfoEXT);
  procedure vknew(out a:PVkImageDrmFormatModifierListCreateInfoEXT);
  procedure vkdispose(var a:PVkImageDrmFormatModifierListCreateInfoEXT);
  procedure vkadd(out a:PVkImageDrmFormatModifierListCreateInfoEXT);
  procedure vkrem(var a:PVkImageDrmFormatModifierListCreateInfoEXT);
  procedure vknew(out a:PVkImageDrmFormatModifierExplicitCreateInfoEXT);
  procedure vkdispose(var a:PVkImageDrmFormatModifierExplicitCreateInfoEXT);
  procedure vkadd(out a:PVkImageDrmFormatModifierExplicitCreateInfoEXT);
  procedure vkrem(var a:PVkImageDrmFormatModifierExplicitCreateInfoEXT);
  procedure vknew(out a:PVkImageDrmFormatModifierPropertiesEXT);
  procedure vkdispose(var a:PVkImageDrmFormatModifierPropertiesEXT);
  procedure vkadd(out a:PVkImageDrmFormatModifierPropertiesEXT);
  procedure vkrem(var a:PVkImageDrmFormatModifierPropertiesEXT);
  procedure vknew(out a:PVkImageStencilUsageCreateInfo);
  procedure vkdispose(var a:PVkImageStencilUsageCreateInfo);
  procedure vkadd(out a:PVkImageStencilUsageCreateInfo);
  procedure vkrem(var a:PVkImageStencilUsageCreateInfo);
  procedure vknew(out a:PVkDeviceMemoryOverallocationCreateInfoAMD);
  procedure vkdispose(var a:PVkDeviceMemoryOverallocationCreateInfoAMD);
  procedure vkadd(out a:PVkDeviceMemoryOverallocationCreateInfoAMD);
  procedure vkrem(var a:PVkDeviceMemoryOverallocationCreateInfoAMD);
  procedure vknew(out a:PVkPhysicalDeviceFragmentDensityMapFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceFragmentDensityMapFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceFragmentDensityMapFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceFragmentDensityMapFeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceFragmentDensityMapPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceFragmentDensityMapPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceFragmentDensityMapPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceFragmentDensityMapPropertiesEXT);
  procedure vknew(out a:PVkRenderPassFragmentDensityMapCreateInfoEXT);
  procedure vkdispose(var a:PVkRenderPassFragmentDensityMapCreateInfoEXT);
  procedure vkadd(out a:PVkRenderPassFragmentDensityMapCreateInfoEXT);
  procedure vkrem(var a:PVkRenderPassFragmentDensityMapCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceScalarBlockLayoutFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceScalarBlockLayoutFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceScalarBlockLayoutFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceScalarBlockLayoutFeatures);
  procedure vknew(out a:PVkSurfaceProtectedCapabilitiesKHR);
  procedure vkdispose(var a:PVkSurfaceProtectedCapabilitiesKHR);
  procedure vkadd(out a:PVkSurfaceProtectedCapabilitiesKHR);
  procedure vkrem(var a:PVkSurfaceProtectedCapabilitiesKHR);
  procedure vknew(out a:PVkPhysicalDeviceUniformBufferStandardLayoutFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceUniformBufferStandardLayoutFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceUniformBufferStandardLayoutFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceUniformBufferStandardLayoutFeatures);
  procedure vknew(out a:PVkPhysicalDeviceDepthClipEnableFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceDepthClipEnableFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceDepthClipEnableFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceDepthClipEnableFeaturesEXT);
  procedure vknew(out a:PVkPipelineRasterizationDepthClipStateCreateInfoEXT);
  procedure vkdispose(var a:PVkPipelineRasterizationDepthClipStateCreateInfoEXT);
  procedure vkadd(out a:PVkPipelineRasterizationDepthClipStateCreateInfoEXT);
  procedure vkrem(var a:PVkPipelineRasterizationDepthClipStateCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceMemoryBudgetPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceMemoryBudgetPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceMemoryBudgetPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceMemoryBudgetPropertiesEXT);
  procedure vknew(out a:PVkPhysicalDeviceMemoryPriorityFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceMemoryPriorityFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceMemoryPriorityFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceMemoryPriorityFeaturesEXT);
  procedure vknew(out a:PVkMemoryPriorityAllocateInfoEXT);
  procedure vkdispose(var a:PVkMemoryPriorityAllocateInfoEXT);
  procedure vkadd(out a:PVkMemoryPriorityAllocateInfoEXT);
  procedure vkrem(var a:PVkMemoryPriorityAllocateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceBufferDeviceAddressFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceBufferDeviceAddressFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceBufferDeviceAddressFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceBufferDeviceAddressFeatures);
  procedure vknew(out a:PVkPhysicalDeviceBufferDeviceAddressFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceBufferDeviceAddressFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceBufferDeviceAddressFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceBufferDeviceAddressFeaturesEXT);
  procedure vknew(out a:PVkBufferDeviceAddressInfo);
  procedure vkdispose(var a:PVkBufferDeviceAddressInfo);
  procedure vkadd(out a:PVkBufferDeviceAddressInfo);
  procedure vkrem(var a:PVkBufferDeviceAddressInfo);
  procedure vknew(out a:PVkBufferOpaqueCaptureAddressCreateInfo);
  procedure vkdispose(var a:PVkBufferOpaqueCaptureAddressCreateInfo);
  procedure vkadd(out a:PVkBufferOpaqueCaptureAddressCreateInfo);
  procedure vkrem(var a:PVkBufferOpaqueCaptureAddressCreateInfo);
  procedure vknew(out a:PVkBufferDeviceAddressCreateInfoEXT);
  procedure vkdispose(var a:PVkBufferDeviceAddressCreateInfoEXT);
  procedure vkadd(out a:PVkBufferDeviceAddressCreateInfoEXT);
  procedure vkrem(var a:PVkBufferDeviceAddressCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceImageViewImageFormatInfoEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceImageViewImageFormatInfoEXT);
  procedure vkadd(out a:PVkPhysicalDeviceImageViewImageFormatInfoEXT);
  procedure vkrem(var a:PVkPhysicalDeviceImageViewImageFormatInfoEXT);
  procedure vknew(out a:PVkFilterCubicImageViewImageFormatPropertiesEXT);
  procedure vkdispose(var a:PVkFilterCubicImageViewImageFormatPropertiesEXT);
  procedure vkadd(out a:PVkFilterCubicImageViewImageFormatPropertiesEXT);
  procedure vkrem(var a:PVkFilterCubicImageViewImageFormatPropertiesEXT);
  procedure vknew(out a:PVkPhysicalDeviceImagelessFramebufferFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceImagelessFramebufferFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceImagelessFramebufferFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceImagelessFramebufferFeatures);
  procedure vknew(out a:PVkFramebufferAttachmentImageInfo);
  procedure vkdispose(var a:PVkFramebufferAttachmentImageInfo);
  procedure vkadd(out a:PVkFramebufferAttachmentImageInfo);
  procedure vkrem(var a:PVkFramebufferAttachmentImageInfo);
  procedure vknew(out a:PVkRenderPassAttachmentBeginInfo);
  procedure vkdispose(var a:PVkRenderPassAttachmentBeginInfo);
  procedure vkadd(out a:PVkRenderPassAttachmentBeginInfo);
  procedure vkrem(var a:PVkRenderPassAttachmentBeginInfo);
  procedure vknew(out a:PVkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceCooperativeMatrixFeaturesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceCooperativeMatrixFeaturesNV);
  procedure vkadd(out a:PVkPhysicalDeviceCooperativeMatrixFeaturesNV);
  procedure vkrem(var a:PVkPhysicalDeviceCooperativeMatrixFeaturesNV);
  procedure vknew(out a:PVkPhysicalDeviceCooperativeMatrixPropertiesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceCooperativeMatrixPropertiesNV);
  procedure vkadd(out a:PVkPhysicalDeviceCooperativeMatrixPropertiesNV);
  procedure vkrem(var a:PVkPhysicalDeviceCooperativeMatrixPropertiesNV);
  procedure vknew(out a:PVkCooperativeMatrixPropertiesNV);
  procedure vkdispose(var a:PVkCooperativeMatrixPropertiesNV);
  procedure vkadd(out a:PVkCooperativeMatrixPropertiesNV);
  procedure vkrem(var a:PVkCooperativeMatrixPropertiesNV);
  procedure vknew(out a:PVkPhysicalDeviceYcbcrImageArraysFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceYcbcrImageArraysFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceYcbcrImageArraysFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceYcbcrImageArraysFeaturesEXT);
  procedure vknew(out a:PVkImageViewHandleInfoNVX);
  procedure vkdispose(var a:PVkImageViewHandleInfoNVX);
  procedure vkadd(out a:PVkImageViewHandleInfoNVX);
  procedure vkrem(var a:PVkImageViewHandleInfoNVX);
  procedure vknew(out a:PVkPresentFrameTokenGGP);
  procedure vkdispose(var a:PVkPresentFrameTokenGGP);
  procedure vkadd(out a:PVkPresentFrameTokenGGP);
  procedure vkrem(var a:PVkPresentFrameTokenGGP);
  procedure vknew(out a:PVkPipelineCreationFeedbackEXT);
  procedure vkdispose(var a:PVkPipelineCreationFeedbackEXT);
  procedure vkadd(out a:PVkPipelineCreationFeedbackEXT);
  procedure vkrem(var a:PVkPipelineCreationFeedbackEXT);
  procedure vknew(out a:PVkPipelineCreationFeedbackCreateInfoEXT);
  procedure vkdispose(var a:PVkPipelineCreationFeedbackCreateInfoEXT);
  procedure vkadd(out a:PVkPipelineCreationFeedbackCreateInfoEXT);
  procedure vkrem(var a:PVkPipelineCreationFeedbackCreateInfoEXT);
  procedure vknew(out a:PVkSurfaceFullScreenExclusiveInfoEXT);
  procedure vkdispose(var a:PVkSurfaceFullScreenExclusiveInfoEXT);
  procedure vkadd(out a:PVkSurfaceFullScreenExclusiveInfoEXT);
  procedure vkrem(var a:PVkSurfaceFullScreenExclusiveInfoEXT);
  procedure vknew(out a:PVkSurfaceFullScreenExclusiveWin32InfoEXT);
  procedure vkdispose(var a:PVkSurfaceFullScreenExclusiveWin32InfoEXT);
  procedure vkadd(out a:PVkSurfaceFullScreenExclusiveWin32InfoEXT);
  procedure vkrem(var a:PVkSurfaceFullScreenExclusiveWin32InfoEXT);
  procedure vknew(out a:PVkSurfaceCapabilitiesFullScreenExclusiveEXT);
  procedure vkdispose(var a:PVkSurfaceCapabilitiesFullScreenExclusiveEXT);
  procedure vkadd(out a:PVkSurfaceCapabilitiesFullScreenExclusiveEXT);
  procedure vkrem(var a:PVkSurfaceCapabilitiesFullScreenExclusiveEXT);
  procedure vknew(out a:PVkPhysicalDevicePerformanceQueryFeaturesKHR);
  procedure vkdispose(var a:PVkPhysicalDevicePerformanceQueryFeaturesKHR);
  procedure vkadd(out a:PVkPhysicalDevicePerformanceQueryFeaturesKHR);
  procedure vkrem(var a:PVkPhysicalDevicePerformanceQueryFeaturesKHR);
  procedure vknew(out a:PVkPhysicalDevicePerformanceQueryPropertiesKHR);
  procedure vkdispose(var a:PVkPhysicalDevicePerformanceQueryPropertiesKHR);
  procedure vkadd(out a:PVkPhysicalDevicePerformanceQueryPropertiesKHR);
  procedure vkrem(var a:PVkPhysicalDevicePerformanceQueryPropertiesKHR);
  procedure vknew(out a:PVkPerformanceCounterKHR);
  procedure vkdispose(var a:PVkPerformanceCounterKHR);
  procedure vkadd(out a:PVkPerformanceCounterKHR);
  procedure vkrem(var a:PVkPerformanceCounterKHR);
  procedure vknew(out a:PVkPerformanceCounterDescriptionKHR);
  procedure vkdispose(var a:PVkPerformanceCounterDescriptionKHR);
  procedure vkadd(out a:PVkPerformanceCounterDescriptionKHR);
  procedure vkrem(var a:PVkPerformanceCounterDescriptionKHR);
  procedure vknew(out a:PVkQueryPoolPerformanceCreateInfoKHR);
  procedure vkdispose(var a:PVkQueryPoolPerformanceCreateInfoKHR);
  procedure vkadd(out a:PVkQueryPoolPerformanceCreateInfoKHR);
  procedure vkrem(var a:PVkQueryPoolPerformanceCreateInfoKHR);
  procedure vknew(out a:PVkPerformanceCounterResultKHR);
  procedure vkdispose(var a:PVkPerformanceCounterResultKHR);
  procedure vkadd(out a:PVkPerformanceCounterResultKHR);
  procedure vkrem(var a:PVkPerformanceCounterResultKHR);
  procedure vknew(out a:PVkAcquireProfilingLockInfoKHR);
  procedure vkdispose(var a:PVkAcquireProfilingLockInfoKHR);
  procedure vkadd(out a:PVkAcquireProfilingLockInfoKHR);
  procedure vkrem(var a:PVkAcquireProfilingLockInfoKHR);
  procedure vknew(out a:PVkPerformanceQuerySubmitInfoKHR);
  procedure vkdispose(var a:PVkPerformanceQuerySubmitInfoKHR);
  procedure vkadd(out a:PVkPerformanceQuerySubmitInfoKHR);
  procedure vkrem(var a:PVkPerformanceQuerySubmitInfoKHR);
  procedure vknew(out a:PVkHeadlessSurfaceCreateInfoEXT);
  procedure vkdispose(var a:PVkHeadlessSurfaceCreateInfoEXT);
  procedure vkadd(out a:PVkHeadlessSurfaceCreateInfoEXT);
  procedure vkrem(var a:PVkHeadlessSurfaceCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceCoverageReductionModeFeaturesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceCoverageReductionModeFeaturesNV);
  procedure vkadd(out a:PVkPhysicalDeviceCoverageReductionModeFeaturesNV);
  procedure vkrem(var a:PVkPhysicalDeviceCoverageReductionModeFeaturesNV);
  procedure vknew(out a:PVkPipelineCoverageReductionStateCreateInfoNV);
  procedure vkdispose(var a:PVkPipelineCoverageReductionStateCreateInfoNV);
  procedure vkadd(out a:PVkPipelineCoverageReductionStateCreateInfoNV);
  procedure vkrem(var a:PVkPipelineCoverageReductionStateCreateInfoNV);
  procedure vknew(out a:PVkFramebufferMixedSamplesCombinationNV);
  procedure vkdispose(var a:PVkFramebufferMixedSamplesCombinationNV);
  procedure vkadd(out a:PVkFramebufferMixedSamplesCombinationNV);
  procedure vkrem(var a:PVkFramebufferMixedSamplesCombinationNV);
  procedure vknew(out a:PVkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL);
  procedure vkdispose(var a:PVkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL);
  procedure vkadd(out a:PVkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL);
  procedure vkrem(var a:PVkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL);
  procedure vknew(out a:PVkPerformanceValueDataINTEL);
  procedure vkdispose(var a:PVkPerformanceValueDataINTEL);
  procedure vkadd(out a:PVkPerformanceValueDataINTEL);
  procedure vkrem(var a:PVkPerformanceValueDataINTEL);
  procedure vknew(out a:PVkPerformanceValueINTEL);
  procedure vkdispose(var a:PVkPerformanceValueINTEL);
  procedure vkadd(out a:PVkPerformanceValueINTEL);
  procedure vkrem(var a:PVkPerformanceValueINTEL);
  procedure vknew(out a:PVkInitializePerformanceApiInfoINTEL);
  procedure vkdispose(var a:PVkInitializePerformanceApiInfoINTEL);
  procedure vkadd(out a:PVkInitializePerformanceApiInfoINTEL);
  procedure vkrem(var a:PVkInitializePerformanceApiInfoINTEL);
  procedure vknew(out a:PVkQueryPoolCreateInfoINTEL);
  procedure vkdispose(var a:PVkQueryPoolCreateInfoINTEL);
  procedure vkadd(out a:PVkQueryPoolCreateInfoINTEL);
  procedure vkrem(var a:PVkQueryPoolCreateInfoINTEL);
  procedure vknew(out a:PVkPerformanceMarkerInfoINTEL);
  procedure vkdispose(var a:PVkPerformanceMarkerInfoINTEL);
  procedure vkadd(out a:PVkPerformanceMarkerInfoINTEL);
  procedure vkrem(var a:PVkPerformanceMarkerInfoINTEL);
  procedure vknew(out a:PVkPerformanceStreamMarkerInfoINTEL);
  procedure vkdispose(var a:PVkPerformanceStreamMarkerInfoINTEL);
  procedure vkadd(out a:PVkPerformanceStreamMarkerInfoINTEL);
  procedure vkrem(var a:PVkPerformanceStreamMarkerInfoINTEL);
  procedure vknew(out a:PVkPerformanceOverrideInfoINTEL);
  procedure vkdispose(var a:PVkPerformanceOverrideInfoINTEL);
  procedure vkadd(out a:PVkPerformanceOverrideInfoINTEL);
  procedure vkrem(var a:PVkPerformanceOverrideInfoINTEL);
  procedure vknew(out a:PVkPerformanceConfigurationAcquireInfoINTEL);
  procedure vkdispose(var a:PVkPerformanceConfigurationAcquireInfoINTEL);
  procedure vkadd(out a:PVkPerformanceConfigurationAcquireInfoINTEL);
  procedure vkrem(var a:PVkPerformanceConfigurationAcquireInfoINTEL);
  procedure vknew(out a:PVkPhysicalDeviceShaderClockFeaturesKHR);
  procedure vkdispose(var a:PVkPhysicalDeviceShaderClockFeaturesKHR);
  procedure vkadd(out a:PVkPhysicalDeviceShaderClockFeaturesKHR);
  procedure vkrem(var a:PVkPhysicalDeviceShaderClockFeaturesKHR);
  procedure vknew(out a:PVkPhysicalDeviceIndexTypeUint8FeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceIndexTypeUint8FeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceIndexTypeUint8FeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceIndexTypeUint8FeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceShaderSMBuiltinsPropertiesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceShaderSMBuiltinsPropertiesNV);
  procedure vkadd(out a:PVkPhysicalDeviceShaderSMBuiltinsPropertiesNV);
  procedure vkrem(var a:PVkPhysicalDeviceShaderSMBuiltinsPropertiesNV);
  procedure vknew(out a:PVkPhysicalDeviceShaderSMBuiltinsFeaturesNV);
  procedure vkdispose(var a:PVkPhysicalDeviceShaderSMBuiltinsFeaturesNV);
  procedure vkadd(out a:PVkPhysicalDeviceShaderSMBuiltinsFeaturesNV);
  procedure vkrem(var a:PVkPhysicalDeviceShaderSMBuiltinsFeaturesNV);
  procedure vknew(out a:PVkPhysicalDeviceFragmentShaderInterlockFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceFragmentShaderInterlockFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceFragmentShaderInterlockFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceFragmentShaderInterlockFeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceSeparateDepthStencilLayoutsFeatures);
  procedure vkdispose(var a:PVkPhysicalDeviceSeparateDepthStencilLayoutsFeatures);
  procedure vkadd(out a:PVkPhysicalDeviceSeparateDepthStencilLayoutsFeatures);
  procedure vkrem(var a:PVkPhysicalDeviceSeparateDepthStencilLayoutsFeatures);
  procedure vknew(out a:PVkAttachmentReferenceStencilLayout);
  procedure vkdispose(var a:PVkAttachmentReferenceStencilLayout);
  procedure vkadd(out a:PVkAttachmentReferenceStencilLayout);
  procedure vkrem(var a:PVkAttachmentReferenceStencilLayout);
  procedure vknew(out a:PVkAttachmentDescriptionStencilLayout);
  procedure vkdispose(var a:PVkAttachmentDescriptionStencilLayout);
  procedure vkadd(out a:PVkAttachmentDescriptionStencilLayout);
  procedure vkrem(var a:PVkAttachmentDescriptionStencilLayout);
  procedure vknew(out a:PVkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR);
  procedure vkdispose(var a:PVkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR);
  procedure vkadd(out a:PVkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR);
  procedure vkrem(var a:PVkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR);
  procedure vknew(out a:PVkPipelineInfoKHR);
  procedure vkdispose(var a:PVkPipelineInfoKHR);
  procedure vkadd(out a:PVkPipelineInfoKHR);
  procedure vkrem(var a:PVkPipelineInfoKHR);
  procedure vknew(out a:PVkPipelineExecutablePropertiesKHR);
  procedure vkdispose(var a:PVkPipelineExecutablePropertiesKHR);
  procedure vkadd(out a:PVkPipelineExecutablePropertiesKHR);
  procedure vkrem(var a:PVkPipelineExecutablePropertiesKHR);
  procedure vknew(out a:PVkPipelineExecutableInfoKHR);
  procedure vkdispose(var a:PVkPipelineExecutableInfoKHR);
  procedure vkadd(out a:PVkPipelineExecutableInfoKHR);
  procedure vkrem(var a:PVkPipelineExecutableInfoKHR);
  procedure vknew(out a:PVkPipelineExecutableStatisticValueKHR);
  procedure vkdispose(var a:PVkPipelineExecutableStatisticValueKHR);
  procedure vkadd(out a:PVkPipelineExecutableStatisticValueKHR);
  procedure vkrem(var a:PVkPipelineExecutableStatisticValueKHR);
  procedure vknew(out a:PVkPipelineExecutableStatisticKHR);
  procedure vkdispose(var a:PVkPipelineExecutableStatisticKHR);
  procedure vkadd(out a:PVkPipelineExecutableStatisticKHR);
  procedure vkrem(var a:PVkPipelineExecutableStatisticKHR);
  procedure vknew(out a:PVkPipelineExecutableInternalRepresentationKHR);
  procedure vkdispose(var a:PVkPipelineExecutableInternalRepresentationKHR);
  procedure vkadd(out a:PVkPipelineExecutableInternalRepresentationKHR);
  procedure vkrem(var a:PVkPipelineExecutableInternalRepresentationKHR);
  procedure vknew(out a:PVkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceTexelBufferAlignmentFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceTexelBufferAlignmentFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceTexelBufferAlignmentFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceTexelBufferAlignmentFeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceTexelBufferAlignmentPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceTexelBufferAlignmentPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceTexelBufferAlignmentPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceTexelBufferAlignmentPropertiesEXT);
  procedure vknew(out a:PVkPhysicalDeviceSubgroupSizeControlFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceSubgroupSizeControlFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceSubgroupSizeControlFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceSubgroupSizeControlFeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceSubgroupSizeControlPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceSubgroupSizeControlPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceSubgroupSizeControlPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceSubgroupSizeControlPropertiesEXT);
  procedure vknew(out a:PVkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT);
  procedure vkdispose(var a:PVkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT);
  procedure vkadd(out a:PVkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT);
  procedure vkrem(var a:PVkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT);
  procedure vknew(out a:PVkMemoryOpaqueCaptureAddressAllocateInfo);
  procedure vkdispose(var a:PVkMemoryOpaqueCaptureAddressAllocateInfo);
  procedure vkadd(out a:PVkMemoryOpaqueCaptureAddressAllocateInfo);
  procedure vkrem(var a:PVkMemoryOpaqueCaptureAddressAllocateInfo);
  procedure vknew(out a:PVkDeviceMemoryOpaqueCaptureAddressInfo);
  procedure vkdispose(var a:PVkDeviceMemoryOpaqueCaptureAddressInfo);
  procedure vkadd(out a:PVkDeviceMemoryOpaqueCaptureAddressInfo);
  procedure vkrem(var a:PVkDeviceMemoryOpaqueCaptureAddressInfo);
  procedure vknew(out a:PVkPhysicalDeviceLineRasterizationFeaturesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceLineRasterizationFeaturesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceLineRasterizationFeaturesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceLineRasterizationFeaturesEXT);
  procedure vknew(out a:PVkPhysicalDeviceLineRasterizationPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceLineRasterizationPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceLineRasterizationPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceLineRasterizationPropertiesEXT);
  procedure vknew(out a:PVkPipelineRasterizationLineStateCreateInfoEXT);
  procedure vkdispose(var a:PVkPipelineRasterizationLineStateCreateInfoEXT);
  procedure vkadd(out a:PVkPipelineRasterizationLineStateCreateInfoEXT);
  procedure vkrem(var a:PVkPipelineRasterizationLineStateCreateInfoEXT);
  procedure vknew(out a:PVkPhysicalDeviceVulkan11Features);
  procedure vkdispose(var a:PVkPhysicalDeviceVulkan11Features);
  procedure vkadd(out a:PVkPhysicalDeviceVulkan11Features);
  procedure vkrem(var a:PVkPhysicalDeviceVulkan11Features);
  procedure vknew(out a:PVkPhysicalDeviceVulkan11Properties);
  procedure vkdispose(var a:PVkPhysicalDeviceVulkan11Properties);
  procedure vkadd(out a:PVkPhysicalDeviceVulkan11Properties);
  procedure vkrem(var a:PVkPhysicalDeviceVulkan11Properties);
  procedure vknew(out a:PVkPhysicalDeviceVulkan12Features);
  procedure vkdispose(var a:PVkPhysicalDeviceVulkan12Features);
  procedure vkadd(out a:PVkPhysicalDeviceVulkan12Features);
  procedure vkrem(var a:PVkPhysicalDeviceVulkan12Features);
  procedure vknew(out a:PVkPhysicalDeviceVulkan12Properties);
  procedure vkdispose(var a:PVkPhysicalDeviceVulkan12Properties);
  procedure vkadd(out a:PVkPhysicalDeviceVulkan12Properties);
  procedure vkrem(var a:PVkPhysicalDeviceVulkan12Properties);
  procedure vknew(out a:PVkPipelineCompilerControlCreateInfoAMD);
  procedure vkdispose(var a:PVkPipelineCompilerControlCreateInfoAMD);
  procedure vkadd(out a:PVkPipelineCompilerControlCreateInfoAMD);
  procedure vkrem(var a:PVkPipelineCompilerControlCreateInfoAMD);
  procedure vknew(out a:PVkPhysicalDeviceCoherentMemoryFeaturesAMD);
  procedure vkdispose(var a:PVkPhysicalDeviceCoherentMemoryFeaturesAMD);
  procedure vkadd(out a:PVkPhysicalDeviceCoherentMemoryFeaturesAMD);
  procedure vkrem(var a:PVkPhysicalDeviceCoherentMemoryFeaturesAMD);
  procedure vknew(out a:PVkPhysicalDeviceToolPropertiesEXT);
  procedure vkdispose(var a:PVkPhysicalDeviceToolPropertiesEXT);
  procedure vkadd(out a:PVkPhysicalDeviceToolPropertiesEXT);
  procedure vkrem(var a:PVkPhysicalDeviceToolPropertiesEXT);
  procedure vknew(out a:PVkPhysicalDeviceProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceProperties);
  procedure vkadd(out a:PVkPhysicalDeviceProperties);
  procedure vkrem(var a:PVkPhysicalDeviceProperties);
  procedure vknew(out a:PVkDeviceCreateInfo);
  procedure vkdispose(var a:PVkDeviceCreateInfo);
  procedure vkadd(out a:PVkDeviceCreateInfo);
  procedure vkrem(var a:PVkDeviceCreateInfo);
  procedure vknew(out a:PVkPhysicalDeviceMemoryProperties);
  procedure vkdispose(var a:PVkPhysicalDeviceMemoryProperties);
  procedure vkadd(out a:PVkPhysicalDeviceMemoryProperties);
  procedure vkrem(var a:PVkPhysicalDeviceMemoryProperties);
  procedure vknew(out a:PVkRenderPassBeginInfo);
  procedure vkdispose(var a:PVkRenderPassBeginInfo);
  procedure vkadd(out a:PVkRenderPassBeginInfo);
  procedure vkrem(var a:PVkRenderPassBeginInfo);
  procedure vknew(out a:PVkPhysicalDeviceProperties2);
  procedure vkdispose(var a:PVkPhysicalDeviceProperties2);
  procedure vkadd(out a:PVkPhysicalDeviceProperties2);
  procedure vkrem(var a:PVkPhysicalDeviceProperties2);
  procedure vknew(out a:PVkPhysicalDeviceMemoryProperties2);
  procedure vkdispose(var a:PVkPhysicalDeviceMemoryProperties2);
  procedure vkadd(out a:PVkPhysicalDeviceMemoryProperties2);
  procedure vkrem(var a:PVkPhysicalDeviceMemoryProperties2);
  procedure vknew(out a:PVkPresentRegionKHR);
  procedure vkdispose(var a:PVkPresentRegionKHR);
  procedure vkadd(out a:PVkPresentRegionKHR);
  procedure vkrem(var a:PVkPresentRegionKHR);
  procedure vknew(out a:PVkPresentTimesInfoGOOGLE);
  procedure vkdispose(var a:PVkPresentTimesInfoGOOGLE);
  procedure vkadd(out a:PVkPresentTimesInfoGOOGLE);
  procedure vkrem(var a:PVkPresentTimesInfoGOOGLE);
  procedure vknew(out a:PVkDrmFormatModifierPropertiesListEXT);
  procedure vkdispose(var a:PVkDrmFormatModifierPropertiesListEXT);
  procedure vkadd(out a:PVkDrmFormatModifierPropertiesListEXT);
  procedure vkrem(var a:PVkDrmFormatModifierPropertiesListEXT);
  procedure vknew(out a:PVkFramebufferAttachmentsCreateInfo);
  procedure vkdispose(var a:PVkFramebufferAttachmentsCreateInfo);
  procedure vkadd(out a:PVkFramebufferAttachmentsCreateInfo);
  procedure vkrem(var a:PVkFramebufferAttachmentsCreateInfo);
  procedure vknew(out a:PVkPresentRegionsKHR);
  procedure vkdispose(var a:PVkPresentRegionsKHR);
  procedure vkadd(out a:PVkPresentRegionsKHR);
  procedure vkrem(var a:PVkPresentRegionsKHR);
  procedure vknew(out a:PVkAllocationCallbacks);
  procedure vkdispose(var a:PVkAllocationCallbacks);
  procedure vkadd(out a:PVkAllocationCallbacks);
  procedure vkrem(var a:PVkAllocationCallbacks);
  procedure vknew(out a:PVkDebugReportCallbackCreateInfoEXT);
  procedure vkdispose(var a:PVkDebugReportCallbackCreateInfoEXT);
  procedure vkadd(out a:PVkDebugReportCallbackCreateInfoEXT);
  procedure vkrem(var a:PVkDebugReportCallbackCreateInfoEXT);
  procedure vknew(out a:PVkDebugUtilsMessengerCreateInfoEXT);
  procedure vkdispose(var a:PVkDebugUtilsMessengerCreateInfoEXT);
  procedure vkadd(out a:PVkDebugUtilsMessengerCreateInfoEXT);
  procedure vkrem(var a:PVkDebugUtilsMessengerCreateInfoEXT);

  public
  vkfreepos : sizeuint;
  vkargdata : array[0..maxvkargbytes-1] of byte; 
end;


implementation



function T_vulkan_auto.vkgetmem(n : dword) : pointer;
var p : pbyte;
begin
  getmem(p,n);
  vkgetmem := p;
end;

procedure T_vulkan_auto.vkfreemem(p : pointer; n : dword);
begin
  freemem(p,n);
end;




procedure T_vulkan_auto.vknew(out a:Pdword);
begin
  a:=vkgetmem(sizeof(dword));
  a^:=0;
end;

procedure T_vulkan_auto.vkdispose(var a:Pdword);
begin
  vkfreemem(a,sizeof(dword));
  a:=nil;
end;

procedure T_vulkan_auto.vkadd(out a:Pdword);
begin
  vkadditem(a,sizeof(dword));
  a^:=0;
end;

procedure T_vulkan_auto.vkrem(var a:Pdword);
begin
  vkremitem(a,sizeof(dword));
  a:=nil;
end;




procedure T_vulkan_auto.vknew(out a:Pqword);
begin
  a:=vkgetmem(sizeof(qword));
  a^:=0;
end;

procedure T_vulkan_auto.vkdispose(var a:Pqword);
begin
  vkfreemem(a,sizeof(qword));
  a:=nil;
end;

procedure T_vulkan_auto.vkadd(out a:Pqword);
begin
  vkadditem(a,sizeof(qword));
  a^:=0;
end;

procedure T_vulkan_auto.vkrem(var a:Pqword);
begin
  vkremitem(a,sizeof(qword));
  a:=nil;
end;



procedure T_vulkan_auto.vknew(out a:Psingle);
begin
  a:=vkgetmem(sizeof(single));
  a^:=0.0;
end;

procedure T_vulkan_auto.vkdispose(var a:Psingle);
begin
  vkfreemem(a,sizeof(single));
  a:=nil;
end;

procedure T_vulkan_auto.vkadd(out a:Psingle);
begin
  vkadditem(a,sizeof(single));
  a^:=0.0;
end;

procedure T_vulkan_auto.vkrem(var a:Psingle);
begin
  vkremitem(a,sizeof(single));
  a:=nil;
end;


function alignlen(l : SizeUInt) : SizeUInt; inline;
begin
  if (l and 7)=0 then alignlen := l else begin
    alignlen := (l -(l and 7)) +8;
  end;
end;

procedure T_vulkan_auto.vkremitem(var a; len : SizeUInt);
begin
  len := alignlen(len);
  if (vkfreepos<len) then begin
    on_vk_auto_err('remove more than added vk structures than secondary stack?');
  end;
  vkfreepos := vkfreepos -len;
  pointer(a) := nil;
  //writeln(len,'>> balance of secondary arg stack = ',vkfreepos);
end;

procedure T_vulkan_auto.vkadditem(out a; len : SizeUInt);
begin
  len := alignlen(len);
  pointer(a) := @vkargdata[vkfreepos];
  vkfreepos := vkfreepos +len;
  if (vkfreepos>=maxvkargbytes) then begin
    on_vk_auto_err('too many vk structures on secondary stack!');
  end;
  //writeln(len, '<< balance of secondary arg stack = ',vkfreepos);
end;

procedure T_vulkan_auto.on_vk_auto_err(s : ansistring);
begin
  writeln(s);
  halt(1);
end;








procedure T_vulkan_auto.vknew(out a:PVkInstance);
begin
  a:=vkgetmem(sizeof(VkInstance));
  FillByte(a^,sizeof(VkInstance),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkInstance);
begin
  vkfreemem(a,sizeof(VkInstance));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkInstance);
begin
  vkadditem(a,sizeof(VkInstance));
  FillByte(a^,sizeof(VkInstance),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkInstance);
begin
  vkremitem(a,sizeof(VkInstance));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDevice);
begin
  a:=vkgetmem(sizeof(VkPhysicalDevice));
  FillByte(a^,sizeof(VkPhysicalDevice),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDevice);
begin
  vkfreemem(a,sizeof(VkPhysicalDevice));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDevice);
begin
  vkadditem(a,sizeof(VkPhysicalDevice));
  FillByte(a^,sizeof(VkPhysicalDevice),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDevice);
begin
  vkremitem(a,sizeof(VkPhysicalDevice));
end;

procedure T_vulkan_auto.vknew(out a:PVkDevice);
begin
  a:=vkgetmem(sizeof(VkDevice));
  FillByte(a^,sizeof(VkDevice),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDevice);
begin
  vkfreemem(a,sizeof(VkDevice));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDevice);
begin
  vkadditem(a,sizeof(VkDevice));
  FillByte(a^,sizeof(VkDevice),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDevice);
begin
  vkremitem(a,sizeof(VkDevice));
end;

procedure T_vulkan_auto.vknew(out a:PVkQueue);
begin
  a:=vkgetmem(sizeof(VkQueue));
  FillByte(a^,sizeof(VkQueue),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkQueue);
begin
  vkfreemem(a,sizeof(VkQueue));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkQueue);
begin
  vkadditem(a,sizeof(VkQueue));
  FillByte(a^,sizeof(VkQueue),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkQueue);
begin
  vkremitem(a,sizeof(VkQueue));
end;

procedure T_vulkan_auto.vknew(out a:PVkCommandBuffer);
begin
  a:=vkgetmem(sizeof(VkCommandBuffer));
  FillByte(a^,sizeof(VkCommandBuffer),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCommandBuffer);
begin
  vkfreemem(a,sizeof(VkCommandBuffer));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCommandBuffer);
begin
  vkadditem(a,sizeof(VkCommandBuffer));
  FillByte(a^,sizeof(VkCommandBuffer),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkCommandBuffer);
begin
  vkremitem(a,sizeof(VkCommandBuffer));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceMemory);
begin
  a:=vkgetmem(sizeof(VkDeviceMemory));
  FillByte(a^,sizeof(VkDeviceMemory),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceMemory);
begin
  vkfreemem(a,sizeof(VkDeviceMemory));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceMemory);
begin
  vkadditem(a,sizeof(VkDeviceMemory));
  FillByte(a^,sizeof(VkDeviceMemory),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceMemory);
begin
  vkremitem(a,sizeof(VkDeviceMemory));
end;

procedure T_vulkan_auto.vknew(out a:PVkCommandPool);
begin
  a:=vkgetmem(sizeof(VkCommandPool));
  FillByte(a^,sizeof(VkCommandPool),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCommandPool);
begin
  vkfreemem(a,sizeof(VkCommandPool));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCommandPool);
begin
  vkadditem(a,sizeof(VkCommandPool));
  FillByte(a^,sizeof(VkCommandPool),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkCommandPool);
begin
  vkremitem(a,sizeof(VkCommandPool));
end;

procedure T_vulkan_auto.vknew(out a:PVkBuffer);
begin
  a:=vkgetmem(sizeof(VkBuffer));
  FillByte(a^,sizeof(VkBuffer),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBuffer);
begin
  vkfreemem(a,sizeof(VkBuffer));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBuffer);
begin
  vkadditem(a,sizeof(VkBuffer));
  FillByte(a^,sizeof(VkBuffer),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkBuffer);
begin
  vkremitem(a,sizeof(VkBuffer));
end;

procedure T_vulkan_auto.vknew(out a:PVkBufferView);
begin
  a:=vkgetmem(sizeof(VkBufferView));
  FillByte(a^,sizeof(VkBufferView),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBufferView);
begin
  vkfreemem(a,sizeof(VkBufferView));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBufferView);
begin
  vkadditem(a,sizeof(VkBufferView));
  FillByte(a^,sizeof(VkBufferView),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkBufferView);
begin
  vkremitem(a,sizeof(VkBufferView));
end;

procedure T_vulkan_auto.vknew(out a:PVkImage);
begin
  a:=vkgetmem(sizeof(VkImage));
  FillByte(a^,sizeof(VkImage),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImage);
begin
  vkfreemem(a,sizeof(VkImage));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImage);
begin
  vkadditem(a,sizeof(VkImage));
  FillByte(a^,sizeof(VkImage),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkImage);
begin
  vkremitem(a,sizeof(VkImage));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageView);
begin
  a:=vkgetmem(sizeof(VkImageView));
  FillByte(a^,sizeof(VkImageView),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageView);
begin
  vkfreemem(a,sizeof(VkImageView));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageView);
begin
  vkadditem(a,sizeof(VkImageView));
  FillByte(a^,sizeof(VkImageView),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageView);
begin
  vkremitem(a,sizeof(VkImageView));
end;

procedure T_vulkan_auto.vknew(out a:PVkShaderModule);
begin
  a:=vkgetmem(sizeof(VkShaderModule));
  FillByte(a^,sizeof(VkShaderModule),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkShaderModule);
begin
  vkfreemem(a,sizeof(VkShaderModule));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkShaderModule);
begin
  vkadditem(a,sizeof(VkShaderModule));
  FillByte(a^,sizeof(VkShaderModule),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkShaderModule);
begin
  vkremitem(a,sizeof(VkShaderModule));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipeline);
begin
  a:=vkgetmem(sizeof(VkPipeline));
  FillByte(a^,sizeof(VkPipeline),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipeline);
begin
  vkfreemem(a,sizeof(VkPipeline));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipeline);
begin
  vkadditem(a,sizeof(VkPipeline));
  FillByte(a^,sizeof(VkPipeline),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipeline);
begin
  vkremitem(a,sizeof(VkPipeline));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineLayout);
begin
  a:=vkgetmem(sizeof(VkPipelineLayout));
  FillByte(a^,sizeof(VkPipelineLayout),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineLayout);
begin
  vkfreemem(a,sizeof(VkPipelineLayout));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineLayout);
begin
  vkadditem(a,sizeof(VkPipelineLayout));
  FillByte(a^,sizeof(VkPipelineLayout),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineLayout);
begin
  vkremitem(a,sizeof(VkPipelineLayout));
end;

procedure T_vulkan_auto.vknew(out a:PVkSampler);
begin
  a:=vkgetmem(sizeof(VkSampler));
  FillByte(a^,sizeof(VkSampler),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSampler);
begin
  vkfreemem(a,sizeof(VkSampler));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSampler);
begin
  vkadditem(a,sizeof(VkSampler));
  FillByte(a^,sizeof(VkSampler),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSampler);
begin
  vkremitem(a,sizeof(VkSampler));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorSet);
begin
  a:=vkgetmem(sizeof(VkDescriptorSet));
  FillByte(a^,sizeof(VkDescriptorSet),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorSet);
begin
  vkfreemem(a,sizeof(VkDescriptorSet));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorSet);
begin
  vkadditem(a,sizeof(VkDescriptorSet));
  FillByte(a^,sizeof(VkDescriptorSet),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorSet);
begin
  vkremitem(a,sizeof(VkDescriptorSet));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorSetLayout);
begin
  a:=vkgetmem(sizeof(VkDescriptorSetLayout));
  FillByte(a^,sizeof(VkDescriptorSetLayout),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorSetLayout);
begin
  vkfreemem(a,sizeof(VkDescriptorSetLayout));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorSetLayout);
begin
  vkadditem(a,sizeof(VkDescriptorSetLayout));
  FillByte(a^,sizeof(VkDescriptorSetLayout),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorSetLayout);
begin
  vkremitem(a,sizeof(VkDescriptorSetLayout));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorPool);
begin
  a:=vkgetmem(sizeof(VkDescriptorPool));
  FillByte(a^,sizeof(VkDescriptorPool),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorPool);
begin
  vkfreemem(a,sizeof(VkDescriptorPool));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorPool);
begin
  vkadditem(a,sizeof(VkDescriptorPool));
  FillByte(a^,sizeof(VkDescriptorPool),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorPool);
begin
  vkremitem(a,sizeof(VkDescriptorPool));
end;

procedure T_vulkan_auto.vknew(out a:PVkFence);
begin
  a:=vkgetmem(sizeof(VkFence));
  FillByte(a^,sizeof(VkFence),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkFence);
begin
  vkfreemem(a,sizeof(VkFence));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkFence);
begin
  vkadditem(a,sizeof(VkFence));
  FillByte(a^,sizeof(VkFence),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkFence);
begin
  vkremitem(a,sizeof(VkFence));
end;

procedure T_vulkan_auto.vknew(out a:PVkSemaphore);
begin
  a:=vkgetmem(sizeof(VkSemaphore));
  FillByte(a^,sizeof(VkSemaphore),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSemaphore);
begin
  vkfreemem(a,sizeof(VkSemaphore));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSemaphore);
begin
  vkadditem(a,sizeof(VkSemaphore));
  FillByte(a^,sizeof(VkSemaphore),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSemaphore);
begin
  vkremitem(a,sizeof(VkSemaphore));
end;

procedure T_vulkan_auto.vknew(out a:PVkEvent);
begin
  a:=vkgetmem(sizeof(VkEvent));
  FillByte(a^,sizeof(VkEvent),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkEvent);
begin
  vkfreemem(a,sizeof(VkEvent));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkEvent);
begin
  vkadditem(a,sizeof(VkEvent));
  FillByte(a^,sizeof(VkEvent),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkEvent);
begin
  vkremitem(a,sizeof(VkEvent));
end;

procedure T_vulkan_auto.vknew(out a:PVkQueryPool);
begin
  a:=vkgetmem(sizeof(VkQueryPool));
  FillByte(a^,sizeof(VkQueryPool),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkQueryPool);
begin
  vkfreemem(a,sizeof(VkQueryPool));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkQueryPool);
begin
  vkadditem(a,sizeof(VkQueryPool));
  FillByte(a^,sizeof(VkQueryPool),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkQueryPool);
begin
  vkremitem(a,sizeof(VkQueryPool));
end;

procedure T_vulkan_auto.vknew(out a:PVkFramebuffer);
begin
  a:=vkgetmem(sizeof(VkFramebuffer));
  FillByte(a^,sizeof(VkFramebuffer),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkFramebuffer);
begin
  vkfreemem(a,sizeof(VkFramebuffer));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkFramebuffer);
begin
  vkadditem(a,sizeof(VkFramebuffer));
  FillByte(a^,sizeof(VkFramebuffer),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkFramebuffer);
begin
  vkremitem(a,sizeof(VkFramebuffer));
end;

procedure T_vulkan_auto.vknew(out a:PVkRenderPass);
begin
  a:=vkgetmem(sizeof(VkRenderPass));
  FillByte(a^,sizeof(VkRenderPass),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRenderPass);
begin
  vkfreemem(a,sizeof(VkRenderPass));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRenderPass);
begin
  vkadditem(a,sizeof(VkRenderPass));
  FillByte(a^,sizeof(VkRenderPass),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkRenderPass);
begin
  vkremitem(a,sizeof(VkRenderPass));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineCache);
begin
  a:=vkgetmem(sizeof(VkPipelineCache));
  FillByte(a^,sizeof(VkPipelineCache),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineCache);
begin
  vkfreemem(a,sizeof(VkPipelineCache));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineCache);
begin
  vkadditem(a,sizeof(VkPipelineCache));
  FillByte(a^,sizeof(VkPipelineCache),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineCache);
begin
  vkremitem(a,sizeof(VkPipelineCache));
end;

procedure T_vulkan_auto.vknew(out a:PVkObjectTableNVX);
begin
  a:=vkgetmem(sizeof(VkObjectTableNVX));
  FillByte(a^,sizeof(VkObjectTableNVX),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkObjectTableNVX);
begin
  vkfreemem(a,sizeof(VkObjectTableNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkObjectTableNVX);
begin
  vkadditem(a,sizeof(VkObjectTableNVX));
  FillByte(a^,sizeof(VkObjectTableNVX),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkObjectTableNVX);
begin
  vkremitem(a,sizeof(VkObjectTableNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkIndirectCommandsLayoutNVX);
begin
  a:=vkgetmem(sizeof(VkIndirectCommandsLayoutNVX));
  FillByte(a^,sizeof(VkIndirectCommandsLayoutNVX),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkIndirectCommandsLayoutNVX);
begin
  vkfreemem(a,sizeof(VkIndirectCommandsLayoutNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkIndirectCommandsLayoutNVX);
begin
  vkadditem(a,sizeof(VkIndirectCommandsLayoutNVX));
  FillByte(a^,sizeof(VkIndirectCommandsLayoutNVX),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkIndirectCommandsLayoutNVX);
begin
  vkremitem(a,sizeof(VkIndirectCommandsLayoutNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorUpdateTemplate);
begin
  a:=vkgetmem(sizeof(VkDescriptorUpdateTemplate));
  FillByte(a^,sizeof(VkDescriptorUpdateTemplate),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorUpdateTemplate);
begin
  vkfreemem(a,sizeof(VkDescriptorUpdateTemplate));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorUpdateTemplate);
begin
  vkadditem(a,sizeof(VkDescriptorUpdateTemplate));
  FillByte(a^,sizeof(VkDescriptorUpdateTemplate),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorUpdateTemplate);
begin
  vkremitem(a,sizeof(VkDescriptorUpdateTemplate));
end;

procedure T_vulkan_auto.vknew(out a:PVkSamplerYcbcrConversion);
begin
  a:=vkgetmem(sizeof(VkSamplerYcbcrConversion));
  FillByte(a^,sizeof(VkSamplerYcbcrConversion),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSamplerYcbcrConversion);
begin
  vkfreemem(a,sizeof(VkSamplerYcbcrConversion));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSamplerYcbcrConversion);
begin
  vkadditem(a,sizeof(VkSamplerYcbcrConversion));
  FillByte(a^,sizeof(VkSamplerYcbcrConversion),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSamplerYcbcrConversion);
begin
  vkremitem(a,sizeof(VkSamplerYcbcrConversion));
end;

procedure T_vulkan_auto.vknew(out a:PVkValidationCacheEXT);
begin
  a:=vkgetmem(sizeof(VkValidationCacheEXT));
  FillByte(a^,sizeof(VkValidationCacheEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkValidationCacheEXT);
begin
  vkfreemem(a,sizeof(VkValidationCacheEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkValidationCacheEXT);
begin
  vkadditem(a,sizeof(VkValidationCacheEXT));
  FillByte(a^,sizeof(VkValidationCacheEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkValidationCacheEXT);
begin
  vkremitem(a,sizeof(VkValidationCacheEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkAccelerationStructureNV);
begin
  a:=vkgetmem(sizeof(VkAccelerationStructureNV));
  FillByte(a^,sizeof(VkAccelerationStructureNV),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAccelerationStructureNV);
begin
  vkfreemem(a,sizeof(VkAccelerationStructureNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAccelerationStructureNV);
begin
  vkadditem(a,sizeof(VkAccelerationStructureNV));
  FillByte(a^,sizeof(VkAccelerationStructureNV),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkAccelerationStructureNV);
begin
  vkremitem(a,sizeof(VkAccelerationStructureNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPerformanceConfigurationINTEL);
begin
  a:=vkgetmem(sizeof(VkPerformanceConfigurationINTEL));
  FillByte(a^,sizeof(VkPerformanceConfigurationINTEL),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPerformanceConfigurationINTEL);
begin
  vkfreemem(a,sizeof(VkPerformanceConfigurationINTEL));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPerformanceConfigurationINTEL);
begin
  vkadditem(a,sizeof(VkPerformanceConfigurationINTEL));
  FillByte(a^,sizeof(VkPerformanceConfigurationINTEL),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPerformanceConfigurationINTEL);
begin
  vkremitem(a,sizeof(VkPerformanceConfigurationINTEL));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayKHR);
begin
  a:=vkgetmem(sizeof(VkDisplayKHR));
  FillByte(a^,sizeof(VkDisplayKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayKHR);
begin
  vkfreemem(a,sizeof(VkDisplayKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayKHR);
begin
  vkadditem(a,sizeof(VkDisplayKHR));
  FillByte(a^,sizeof(VkDisplayKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayKHR);
begin
  vkremitem(a,sizeof(VkDisplayKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayModeKHR);
begin
  a:=vkgetmem(sizeof(VkDisplayModeKHR));
  FillByte(a^,sizeof(VkDisplayModeKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayModeKHR);
begin
  vkfreemem(a,sizeof(VkDisplayModeKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayModeKHR);
begin
  vkadditem(a,sizeof(VkDisplayModeKHR));
  FillByte(a^,sizeof(VkDisplayModeKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayModeKHR);
begin
  vkremitem(a,sizeof(VkDisplayModeKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkSurfaceKHR);
begin
  a:=vkgetmem(sizeof(VkSurfaceKHR));
  FillByte(a^,sizeof(VkSurfaceKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSurfaceKHR);
begin
  vkfreemem(a,sizeof(VkSurfaceKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSurfaceKHR);
begin
  vkadditem(a,sizeof(VkSurfaceKHR));
  FillByte(a^,sizeof(VkSurfaceKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSurfaceKHR);
begin
  vkremitem(a,sizeof(VkSurfaceKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkSwapchainKHR);
begin
  a:=vkgetmem(sizeof(VkSwapchainKHR));
  FillByte(a^,sizeof(VkSwapchainKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSwapchainKHR);
begin
  vkfreemem(a,sizeof(VkSwapchainKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSwapchainKHR);
begin
  vkadditem(a,sizeof(VkSwapchainKHR));
  FillByte(a^,sizeof(VkSwapchainKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSwapchainKHR);
begin
  vkremitem(a,sizeof(VkSwapchainKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDebugReportCallbackEXT);
begin
  a:=vkgetmem(sizeof(VkDebugReportCallbackEXT));
  FillByte(a^,sizeof(VkDebugReportCallbackEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDebugReportCallbackEXT);
begin
  vkfreemem(a,sizeof(VkDebugReportCallbackEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDebugReportCallbackEXT);
begin
  vkadditem(a,sizeof(VkDebugReportCallbackEXT));
  FillByte(a^,sizeof(VkDebugReportCallbackEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDebugReportCallbackEXT);
begin
  vkremitem(a,sizeof(VkDebugReportCallbackEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDebugUtilsMessengerEXT);
begin
  a:=vkgetmem(sizeof(VkDebugUtilsMessengerEXT));
  FillByte(a^,sizeof(VkDebugUtilsMessengerEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDebugUtilsMessengerEXT);
begin
  vkfreemem(a,sizeof(VkDebugUtilsMessengerEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDebugUtilsMessengerEXT);
begin
  vkadditem(a,sizeof(VkDebugUtilsMessengerEXT));
  FillByte(a^,sizeof(VkDebugUtilsMessengerEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDebugUtilsMessengerEXT);
begin
  vkremitem(a,sizeof(VkDebugUtilsMessengerEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkOffset2D);
begin
  a:=vkgetmem(sizeof(VkOffset2D));
  a^:=default_VkOffset2D;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkOffset2D);
begin
  vkfreemem(a,sizeof(VkOffset2D));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkOffset2D);
begin
  vkadditem(a,sizeof(VkOffset2D));
  a^:=default_VkOffset2D;
end;
procedure T_vulkan_auto.vkrem(var a:PVkOffset2D);
begin
  vkremitem(a,sizeof(VkOffset2D));
end;

procedure T_vulkan_auto.vknew(out a:PVkOffset3D);
begin
  a:=vkgetmem(sizeof(VkOffset3D));
  a^:=default_VkOffset3D;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkOffset3D);
begin
  vkfreemem(a,sizeof(VkOffset3D));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkOffset3D);
begin
  vkadditem(a,sizeof(VkOffset3D));
  a^:=default_VkOffset3D;
end;
procedure T_vulkan_auto.vkrem(var a:PVkOffset3D);
begin
  vkremitem(a,sizeof(VkOffset3D));
end;

procedure T_vulkan_auto.vknew(out a:PVkExtent2D);
begin
  a:=vkgetmem(sizeof(VkExtent2D));
  a^:=default_VkExtent2D;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExtent2D);
begin
  vkfreemem(a,sizeof(VkExtent2D));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExtent2D);
begin
  vkadditem(a,sizeof(VkExtent2D));
  a^:=default_VkExtent2D;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExtent2D);
begin
  vkremitem(a,sizeof(VkExtent2D));
end;

procedure T_vulkan_auto.vknew(out a:PVkExtent3D);
begin
  a:=vkgetmem(sizeof(VkExtent3D));
  a^:=default_VkExtent3D;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExtent3D);
begin
  vkfreemem(a,sizeof(VkExtent3D));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExtent3D);
begin
  vkadditem(a,sizeof(VkExtent3D));
  a^:=default_VkExtent3D;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExtent3D);
begin
  vkremitem(a,sizeof(VkExtent3D));
end;

procedure T_vulkan_auto.vknew(out a:PVkViewport);
begin
  a:=vkgetmem(sizeof(VkViewport));
  a^:=default_VkViewport;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkViewport);
begin
  vkfreemem(a,sizeof(VkViewport));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkViewport);
begin
  vkadditem(a,sizeof(VkViewport));
  a^:=default_VkViewport;
end;
procedure T_vulkan_auto.vkrem(var a:PVkViewport);
begin
  vkremitem(a,sizeof(VkViewport));
end;

procedure T_vulkan_auto.vknew(out a:PVkRect2D);
begin
  a:=vkgetmem(sizeof(VkRect2D));
  a^:=default_VkRect2D;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRect2D);
begin
  vkfreemem(a,sizeof(VkRect2D));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRect2D);
begin
  vkadditem(a,sizeof(VkRect2D));
  a^:=default_VkRect2D;
end;
procedure T_vulkan_auto.vkrem(var a:PVkRect2D);
begin
  vkremitem(a,sizeof(VkRect2D));
end;

procedure T_vulkan_auto.vknew(out a:PVkClearRect);
begin
  a:=vkgetmem(sizeof(VkClearRect));
  a^:=default_VkClearRect;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkClearRect);
begin
  vkfreemem(a,sizeof(VkClearRect));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkClearRect);
begin
  vkadditem(a,sizeof(VkClearRect));
  a^:=default_VkClearRect;
end;
procedure T_vulkan_auto.vkrem(var a:PVkClearRect);
begin
  vkremitem(a,sizeof(VkClearRect));
end;

procedure T_vulkan_auto.vknew(out a:PVkComponentMapping);
begin
  a:=vkgetmem(sizeof(VkComponentMapping));
  a^:=default_VkComponentMapping;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkComponentMapping);
begin
  vkfreemem(a,sizeof(VkComponentMapping));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkComponentMapping);
begin
  vkadditem(a,sizeof(VkComponentMapping));
  a^:=default_VkComponentMapping;
end;
procedure T_vulkan_auto.vkrem(var a:PVkComponentMapping);
begin
  vkremitem(a,sizeof(VkComponentMapping));
end;

procedure T_vulkan_auto.vknew(out a:PVkExtensionProperties);
begin
  a:=vkgetmem(sizeof(VkExtensionProperties));
  FillByte(a^,sizeof(VkExtensionProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExtensionProperties);
begin
  vkfreemem(a,sizeof(VkExtensionProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExtensionProperties);
begin
  vkadditem(a,sizeof(VkExtensionProperties));
  FillByte(a^,sizeof(VkExtensionProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkExtensionProperties);
begin
  vkremitem(a,sizeof(VkExtensionProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkLayerProperties);
begin
  a:=vkgetmem(sizeof(VkLayerProperties));
  FillByte(a^,sizeof(VkLayerProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkLayerProperties);
begin
  vkfreemem(a,sizeof(VkLayerProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkLayerProperties);
begin
  vkadditem(a,sizeof(VkLayerProperties));
  FillByte(a^,sizeof(VkLayerProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkLayerProperties);
begin
  vkremitem(a,sizeof(VkLayerProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkApplicationInfo);
begin
  a:=vkgetmem(sizeof(VkApplicationInfo));
  a^:=default_VkApplicationInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkApplicationInfo);
begin
  vkfreemem(a,sizeof(VkApplicationInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkApplicationInfo);
begin
  vkadditem(a,sizeof(VkApplicationInfo));
  a^:=default_VkApplicationInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkApplicationInfo);
begin
  vkremitem(a,sizeof(VkApplicationInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceQueueCreateInfo);
begin
  a:=vkgetmem(sizeof(VkDeviceQueueCreateInfo));
  a^:=default_VkDeviceQueueCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceQueueCreateInfo);
begin
  vkfreemem(a,sizeof(VkDeviceQueueCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceQueueCreateInfo);
begin
  vkadditem(a,sizeof(VkDeviceQueueCreateInfo));
  a^:=default_VkDeviceQueueCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceQueueCreateInfo);
begin
  vkremitem(a,sizeof(VkDeviceQueueCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkInstanceCreateInfo);
begin
  a:=vkgetmem(sizeof(VkInstanceCreateInfo));
  a^:=default_VkInstanceCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkInstanceCreateInfo);
begin
  vkfreemem(a,sizeof(VkInstanceCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkInstanceCreateInfo);
begin
  vkadditem(a,sizeof(VkInstanceCreateInfo));
  a^:=default_VkInstanceCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkInstanceCreateInfo);
begin
  vkremitem(a,sizeof(VkInstanceCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkQueueFamilyProperties);
begin
  a:=vkgetmem(sizeof(VkQueueFamilyProperties));
  FillByte(a^,sizeof(VkQueueFamilyProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkQueueFamilyProperties);
begin
  vkfreemem(a,sizeof(VkQueueFamilyProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkQueueFamilyProperties);
begin
  vkadditem(a,sizeof(VkQueueFamilyProperties));
  FillByte(a^,sizeof(VkQueueFamilyProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkQueueFamilyProperties);
begin
  vkremitem(a,sizeof(VkQueueFamilyProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryAllocateInfo);
begin
  a:=vkgetmem(sizeof(VkMemoryAllocateInfo));
  a^:=default_VkMemoryAllocateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryAllocateInfo);
begin
  vkfreemem(a,sizeof(VkMemoryAllocateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryAllocateInfo);
begin
  vkadditem(a,sizeof(VkMemoryAllocateInfo));
  a^:=default_VkMemoryAllocateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryAllocateInfo);
begin
  vkremitem(a,sizeof(VkMemoryAllocateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryRequirements);
begin
  a:=vkgetmem(sizeof(VkMemoryRequirements));
  FillByte(a^,sizeof(VkMemoryRequirements),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryRequirements);
begin
  vkfreemem(a,sizeof(VkMemoryRequirements));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryRequirements);
begin
  vkadditem(a,sizeof(VkMemoryRequirements));
  FillByte(a^,sizeof(VkMemoryRequirements),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryRequirements);
begin
  vkremitem(a,sizeof(VkMemoryRequirements));
end;

procedure T_vulkan_auto.vknew(out a:PVkSparseImageFormatProperties);
begin
  a:=vkgetmem(sizeof(VkSparseImageFormatProperties));
  FillByte(a^,sizeof(VkSparseImageFormatProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSparseImageFormatProperties);
begin
  vkfreemem(a,sizeof(VkSparseImageFormatProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSparseImageFormatProperties);
begin
  vkadditem(a,sizeof(VkSparseImageFormatProperties));
  FillByte(a^,sizeof(VkSparseImageFormatProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSparseImageFormatProperties);
begin
  vkremitem(a,sizeof(VkSparseImageFormatProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkSparseImageMemoryRequirements);
begin
  a:=vkgetmem(sizeof(VkSparseImageMemoryRequirements));
  FillByte(a^,sizeof(VkSparseImageMemoryRequirements),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSparseImageMemoryRequirements);
begin
  vkfreemem(a,sizeof(VkSparseImageMemoryRequirements));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSparseImageMemoryRequirements);
begin
  vkadditem(a,sizeof(VkSparseImageMemoryRequirements));
  FillByte(a^,sizeof(VkSparseImageMemoryRequirements),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSparseImageMemoryRequirements);
begin
  vkremitem(a,sizeof(VkSparseImageMemoryRequirements));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryType);
begin
  a:=vkgetmem(sizeof(VkMemoryType));
  FillByte(a^,sizeof(VkMemoryType),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryType);
begin
  vkfreemem(a,sizeof(VkMemoryType));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryType);
begin
  vkadditem(a,sizeof(VkMemoryType));
  FillByte(a^,sizeof(VkMemoryType),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryType);
begin
  vkremitem(a,sizeof(VkMemoryType));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryHeap);
begin
  a:=vkgetmem(sizeof(VkMemoryHeap));
  FillByte(a^,sizeof(VkMemoryHeap),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryHeap);
begin
  vkfreemem(a,sizeof(VkMemoryHeap));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryHeap);
begin
  vkadditem(a,sizeof(VkMemoryHeap));
  FillByte(a^,sizeof(VkMemoryHeap),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryHeap);
begin
  vkremitem(a,sizeof(VkMemoryHeap));
end;

procedure T_vulkan_auto.vknew(out a:PVkMappedMemoryRange);
begin
  a:=vkgetmem(sizeof(VkMappedMemoryRange));
  a^:=default_VkMappedMemoryRange;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMappedMemoryRange);
begin
  vkfreemem(a,sizeof(VkMappedMemoryRange));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMappedMemoryRange);
begin
  vkadditem(a,sizeof(VkMappedMemoryRange));
  a^:=default_VkMappedMemoryRange;
end;
procedure T_vulkan_auto.vkrem(var a:PVkMappedMemoryRange);
begin
  vkremitem(a,sizeof(VkMappedMemoryRange));
end;

procedure T_vulkan_auto.vknew(out a:PVkFormatProperties);
begin
  a:=vkgetmem(sizeof(VkFormatProperties));
  FillByte(a^,sizeof(VkFormatProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkFormatProperties);
begin
  vkfreemem(a,sizeof(VkFormatProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkFormatProperties);
begin
  vkadditem(a,sizeof(VkFormatProperties));
  FillByte(a^,sizeof(VkFormatProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkFormatProperties);
begin
  vkremitem(a,sizeof(VkFormatProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageFormatProperties);
begin
  a:=vkgetmem(sizeof(VkImageFormatProperties));
  FillByte(a^,sizeof(VkImageFormatProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageFormatProperties);
begin
  vkfreemem(a,sizeof(VkImageFormatProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageFormatProperties);
begin
  vkadditem(a,sizeof(VkImageFormatProperties));
  FillByte(a^,sizeof(VkImageFormatProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageFormatProperties);
begin
  vkremitem(a,sizeof(VkImageFormatProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorBufferInfo);
begin
  a:=vkgetmem(sizeof(VkDescriptorBufferInfo));
  a^:=default_VkDescriptorBufferInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorBufferInfo);
begin
  vkfreemem(a,sizeof(VkDescriptorBufferInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorBufferInfo);
begin
  vkadditem(a,sizeof(VkDescriptorBufferInfo));
  a^:=default_VkDescriptorBufferInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorBufferInfo);
begin
  vkremitem(a,sizeof(VkDescriptorBufferInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorImageInfo);
begin
  a:=vkgetmem(sizeof(VkDescriptorImageInfo));
  a^:=default_VkDescriptorImageInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorImageInfo);
begin
  vkfreemem(a,sizeof(VkDescriptorImageInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorImageInfo);
begin
  vkadditem(a,sizeof(VkDescriptorImageInfo));
  a^:=default_VkDescriptorImageInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorImageInfo);
begin
  vkremitem(a,sizeof(VkDescriptorImageInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkWriteDescriptorSet);
begin
  a:=vkgetmem(sizeof(VkWriteDescriptorSet));
  a^:=default_VkWriteDescriptorSet;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkWriteDescriptorSet);
begin
  vkfreemem(a,sizeof(VkWriteDescriptorSet));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkWriteDescriptorSet);
begin
  vkadditem(a,sizeof(VkWriteDescriptorSet));
  a^:=default_VkWriteDescriptorSet;
end;
procedure T_vulkan_auto.vkrem(var a:PVkWriteDescriptorSet);
begin
  vkremitem(a,sizeof(VkWriteDescriptorSet));
end;

procedure T_vulkan_auto.vknew(out a:PVkCopyDescriptorSet);
begin
  a:=vkgetmem(sizeof(VkCopyDescriptorSet));
  a^:=default_VkCopyDescriptorSet;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCopyDescriptorSet);
begin
  vkfreemem(a,sizeof(VkCopyDescriptorSet));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCopyDescriptorSet);
begin
  vkadditem(a,sizeof(VkCopyDescriptorSet));
  a^:=default_VkCopyDescriptorSet;
end;
procedure T_vulkan_auto.vkrem(var a:PVkCopyDescriptorSet);
begin
  vkremitem(a,sizeof(VkCopyDescriptorSet));
end;

procedure T_vulkan_auto.vknew(out a:PVkBufferCreateInfo);
begin
  a:=vkgetmem(sizeof(VkBufferCreateInfo));
  a^:=default_VkBufferCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBufferCreateInfo);
begin
  vkfreemem(a,sizeof(VkBufferCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBufferCreateInfo);
begin
  vkadditem(a,sizeof(VkBufferCreateInfo));
  a^:=default_VkBufferCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBufferCreateInfo);
begin
  vkremitem(a,sizeof(VkBufferCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkBufferViewCreateInfo);
begin
  a:=vkgetmem(sizeof(VkBufferViewCreateInfo));
  a^:=default_VkBufferViewCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBufferViewCreateInfo);
begin
  vkfreemem(a,sizeof(VkBufferViewCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBufferViewCreateInfo);
begin
  vkadditem(a,sizeof(VkBufferViewCreateInfo));
  a^:=default_VkBufferViewCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBufferViewCreateInfo);
begin
  vkremitem(a,sizeof(VkBufferViewCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageSubresource);
begin
  a:=vkgetmem(sizeof(VkImageSubresource));
  a^:=default_VkImageSubresource;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageSubresource);
begin
  vkfreemem(a,sizeof(VkImageSubresource));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageSubresource);
begin
  vkadditem(a,sizeof(VkImageSubresource));
  a^:=default_VkImageSubresource;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageSubresource);
begin
  vkremitem(a,sizeof(VkImageSubresource));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageSubresourceLayers);
begin
  a:=vkgetmem(sizeof(VkImageSubresourceLayers));
  a^:=default_VkImageSubresourceLayers;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageSubresourceLayers);
begin
  vkfreemem(a,sizeof(VkImageSubresourceLayers));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageSubresourceLayers);
begin
  vkadditem(a,sizeof(VkImageSubresourceLayers));
  a^:=default_VkImageSubresourceLayers;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageSubresourceLayers);
begin
  vkremitem(a,sizeof(VkImageSubresourceLayers));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageSubresourceRange);
begin
  a:=vkgetmem(sizeof(VkImageSubresourceRange));
  a^:=default_VkImageSubresourceRange;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageSubresourceRange);
begin
  vkfreemem(a,sizeof(VkImageSubresourceRange));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageSubresourceRange);
begin
  vkadditem(a,sizeof(VkImageSubresourceRange));
  a^:=default_VkImageSubresourceRange;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageSubresourceRange);
begin
  vkremitem(a,sizeof(VkImageSubresourceRange));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryBarrier);
begin
  a:=vkgetmem(sizeof(VkMemoryBarrier));
  a^:=default_VkMemoryBarrier;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryBarrier);
begin
  vkfreemem(a,sizeof(VkMemoryBarrier));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryBarrier);
begin
  vkadditem(a,sizeof(VkMemoryBarrier));
  a^:=default_VkMemoryBarrier;
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryBarrier);
begin
  vkremitem(a,sizeof(VkMemoryBarrier));
end;

procedure T_vulkan_auto.vknew(out a:PVkBufferMemoryBarrier);
begin
  a:=vkgetmem(sizeof(VkBufferMemoryBarrier));
  a^:=default_VkBufferMemoryBarrier;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBufferMemoryBarrier);
begin
  vkfreemem(a,sizeof(VkBufferMemoryBarrier));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBufferMemoryBarrier);
begin
  vkadditem(a,sizeof(VkBufferMemoryBarrier));
  a^:=default_VkBufferMemoryBarrier;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBufferMemoryBarrier);
begin
  vkremitem(a,sizeof(VkBufferMemoryBarrier));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageMemoryBarrier);
begin
  a:=vkgetmem(sizeof(VkImageMemoryBarrier));
  a^:=default_VkImageMemoryBarrier;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageMemoryBarrier);
begin
  vkfreemem(a,sizeof(VkImageMemoryBarrier));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageMemoryBarrier);
begin
  vkadditem(a,sizeof(VkImageMemoryBarrier));
  a^:=default_VkImageMemoryBarrier;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageMemoryBarrier);
begin
  vkremitem(a,sizeof(VkImageMemoryBarrier));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageCreateInfo);
begin
  a:=vkgetmem(sizeof(VkImageCreateInfo));
  a^:=default_VkImageCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageCreateInfo);
begin
  vkfreemem(a,sizeof(VkImageCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageCreateInfo);
begin
  vkadditem(a,sizeof(VkImageCreateInfo));
  a^:=default_VkImageCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageCreateInfo);
begin
  vkremitem(a,sizeof(VkImageCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkSubresourceLayout);
begin
  a:=vkgetmem(sizeof(VkSubresourceLayout));
  FillByte(a^,sizeof(VkSubresourceLayout),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSubresourceLayout);
begin
  vkfreemem(a,sizeof(VkSubresourceLayout));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSubresourceLayout);
begin
  vkadditem(a,sizeof(VkSubresourceLayout));
  FillByte(a^,sizeof(VkSubresourceLayout),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSubresourceLayout);
begin
  vkremitem(a,sizeof(VkSubresourceLayout));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageViewCreateInfo);
begin
  a:=vkgetmem(sizeof(VkImageViewCreateInfo));
  a^:=default_VkImageViewCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageViewCreateInfo);
begin
  vkfreemem(a,sizeof(VkImageViewCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageViewCreateInfo);
begin
  vkadditem(a,sizeof(VkImageViewCreateInfo));
  a^:=default_VkImageViewCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageViewCreateInfo);
begin
  vkremitem(a,sizeof(VkImageViewCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkBufferCopy);
begin
  a:=vkgetmem(sizeof(VkBufferCopy));
  a^:=default_VkBufferCopy;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBufferCopy);
begin
  vkfreemem(a,sizeof(VkBufferCopy));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBufferCopy);
begin
  vkadditem(a,sizeof(VkBufferCopy));
  a^:=default_VkBufferCopy;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBufferCopy);
begin
  vkremitem(a,sizeof(VkBufferCopy));
end;

procedure T_vulkan_auto.vknew(out a:PVkSparseMemoryBind);
begin
  a:=vkgetmem(sizeof(VkSparseMemoryBind));
  a^:=default_VkSparseMemoryBind;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSparseMemoryBind);
begin
  vkfreemem(a,sizeof(VkSparseMemoryBind));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSparseMemoryBind);
begin
  vkadditem(a,sizeof(VkSparseMemoryBind));
  a^:=default_VkSparseMemoryBind;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSparseMemoryBind);
begin
  vkremitem(a,sizeof(VkSparseMemoryBind));
end;

procedure T_vulkan_auto.vknew(out a:PVkSparseImageMemoryBind);
begin
  a:=vkgetmem(sizeof(VkSparseImageMemoryBind));
  a^:=default_VkSparseImageMemoryBind;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSparseImageMemoryBind);
begin
  vkfreemem(a,sizeof(VkSparseImageMemoryBind));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSparseImageMemoryBind);
begin
  vkadditem(a,sizeof(VkSparseImageMemoryBind));
  a^:=default_VkSparseImageMemoryBind;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSparseImageMemoryBind);
begin
  vkremitem(a,sizeof(VkSparseImageMemoryBind));
end;

procedure T_vulkan_auto.vknew(out a:PVkSparseBufferMemoryBindInfo);
begin
  a:=vkgetmem(sizeof(VkSparseBufferMemoryBindInfo));
  a^:=default_VkSparseBufferMemoryBindInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSparseBufferMemoryBindInfo);
begin
  vkfreemem(a,sizeof(VkSparseBufferMemoryBindInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSparseBufferMemoryBindInfo);
begin
  vkadditem(a,sizeof(VkSparseBufferMemoryBindInfo));
  a^:=default_VkSparseBufferMemoryBindInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSparseBufferMemoryBindInfo);
begin
  vkremitem(a,sizeof(VkSparseBufferMemoryBindInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkSparseImageOpaqueMemoryBindInfo);
begin
  a:=vkgetmem(sizeof(VkSparseImageOpaqueMemoryBindInfo));
  a^:=default_VkSparseImageOpaqueMemoryBindInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSparseImageOpaqueMemoryBindInfo);
begin
  vkfreemem(a,sizeof(VkSparseImageOpaqueMemoryBindInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSparseImageOpaqueMemoryBindInfo);
begin
  vkadditem(a,sizeof(VkSparseImageOpaqueMemoryBindInfo));
  a^:=default_VkSparseImageOpaqueMemoryBindInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSparseImageOpaqueMemoryBindInfo);
begin
  vkremitem(a,sizeof(VkSparseImageOpaqueMemoryBindInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkSparseImageMemoryBindInfo);
begin
  a:=vkgetmem(sizeof(VkSparseImageMemoryBindInfo));
  a^:=default_VkSparseImageMemoryBindInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSparseImageMemoryBindInfo);
begin
  vkfreemem(a,sizeof(VkSparseImageMemoryBindInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSparseImageMemoryBindInfo);
begin
  vkadditem(a,sizeof(VkSparseImageMemoryBindInfo));
  a^:=default_VkSparseImageMemoryBindInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSparseImageMemoryBindInfo);
begin
  vkremitem(a,sizeof(VkSparseImageMemoryBindInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkBindSparseInfo);
begin
  a:=vkgetmem(sizeof(VkBindSparseInfo));
  a^:=default_VkBindSparseInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBindSparseInfo);
begin
  vkfreemem(a,sizeof(VkBindSparseInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBindSparseInfo);
begin
  vkadditem(a,sizeof(VkBindSparseInfo));
  a^:=default_VkBindSparseInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBindSparseInfo);
begin
  vkremitem(a,sizeof(VkBindSparseInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageCopy);
begin
  a:=vkgetmem(sizeof(VkImageCopy));
  a^:=default_VkImageCopy;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageCopy);
begin
  vkfreemem(a,sizeof(VkImageCopy));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageCopy);
begin
  vkadditem(a,sizeof(VkImageCopy));
  a^:=default_VkImageCopy;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageCopy);
begin
  vkremitem(a,sizeof(VkImageCopy));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageBlit);
begin
  a:=vkgetmem(sizeof(VkImageBlit));
  a^:=default_VkImageBlit;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageBlit);
begin
  vkfreemem(a,sizeof(VkImageBlit));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageBlit);
begin
  vkadditem(a,sizeof(VkImageBlit));
  a^:=default_VkImageBlit;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageBlit);
begin
  vkremitem(a,sizeof(VkImageBlit));
end;

procedure T_vulkan_auto.vknew(out a:PVkBufferImageCopy);
begin
  a:=vkgetmem(sizeof(VkBufferImageCopy));
  a^:=default_VkBufferImageCopy;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBufferImageCopy);
begin
  vkfreemem(a,sizeof(VkBufferImageCopy));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBufferImageCopy);
begin
  vkadditem(a,sizeof(VkBufferImageCopy));
  a^:=default_VkBufferImageCopy;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBufferImageCopy);
begin
  vkremitem(a,sizeof(VkBufferImageCopy));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageResolve);
begin
  a:=vkgetmem(sizeof(VkImageResolve));
  a^:=default_VkImageResolve;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageResolve);
begin
  vkfreemem(a,sizeof(VkImageResolve));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageResolve);
begin
  vkadditem(a,sizeof(VkImageResolve));
  a^:=default_VkImageResolve;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageResolve);
begin
  vkremitem(a,sizeof(VkImageResolve));
end;

procedure T_vulkan_auto.vknew(out a:PVkShaderModuleCreateInfo);
begin
  a:=vkgetmem(sizeof(VkShaderModuleCreateInfo));
  a^:=default_VkShaderModuleCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkShaderModuleCreateInfo);
begin
  vkfreemem(a,sizeof(VkShaderModuleCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkShaderModuleCreateInfo);
begin
  vkadditem(a,sizeof(VkShaderModuleCreateInfo));
  a^:=default_VkShaderModuleCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkShaderModuleCreateInfo);
begin
  vkremitem(a,sizeof(VkShaderModuleCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorSetLayoutBinding);
begin
  a:=vkgetmem(sizeof(VkDescriptorSetLayoutBinding));
  a^:=default_VkDescriptorSetLayoutBinding;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorSetLayoutBinding);
begin
  vkfreemem(a,sizeof(VkDescriptorSetLayoutBinding));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorSetLayoutBinding);
begin
  vkadditem(a,sizeof(VkDescriptorSetLayoutBinding));
  a^:=default_VkDescriptorSetLayoutBinding;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorSetLayoutBinding);
begin
  vkremitem(a,sizeof(VkDescriptorSetLayoutBinding));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorSetLayoutCreateInfo);
begin
  a:=vkgetmem(sizeof(VkDescriptorSetLayoutCreateInfo));
  a^:=default_VkDescriptorSetLayoutCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorSetLayoutCreateInfo);
begin
  vkfreemem(a,sizeof(VkDescriptorSetLayoutCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorSetLayoutCreateInfo);
begin
  vkadditem(a,sizeof(VkDescriptorSetLayoutCreateInfo));
  a^:=default_VkDescriptorSetLayoutCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorSetLayoutCreateInfo);
begin
  vkremitem(a,sizeof(VkDescriptorSetLayoutCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorPoolSize);
begin
  a:=vkgetmem(sizeof(VkDescriptorPoolSize));
  a^:=default_VkDescriptorPoolSize;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorPoolSize);
begin
  vkfreemem(a,sizeof(VkDescriptorPoolSize));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorPoolSize);
begin
  vkadditem(a,sizeof(VkDescriptorPoolSize));
  a^:=default_VkDescriptorPoolSize;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorPoolSize);
begin
  vkremitem(a,sizeof(VkDescriptorPoolSize));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorPoolCreateInfo);
begin
  a:=vkgetmem(sizeof(VkDescriptorPoolCreateInfo));
  a^:=default_VkDescriptorPoolCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorPoolCreateInfo);
begin
  vkfreemem(a,sizeof(VkDescriptorPoolCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorPoolCreateInfo);
begin
  vkadditem(a,sizeof(VkDescriptorPoolCreateInfo));
  a^:=default_VkDescriptorPoolCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorPoolCreateInfo);
begin
  vkremitem(a,sizeof(VkDescriptorPoolCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorSetAllocateInfo);
begin
  a:=vkgetmem(sizeof(VkDescriptorSetAllocateInfo));
  a^:=default_VkDescriptorSetAllocateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorSetAllocateInfo);
begin
  vkfreemem(a,sizeof(VkDescriptorSetAllocateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorSetAllocateInfo);
begin
  vkadditem(a,sizeof(VkDescriptorSetAllocateInfo));
  a^:=default_VkDescriptorSetAllocateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorSetAllocateInfo);
begin
  vkremitem(a,sizeof(VkDescriptorSetAllocateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkSpecializationMapEntry);
begin
  a:=vkgetmem(sizeof(VkSpecializationMapEntry));
  a^:=default_VkSpecializationMapEntry;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSpecializationMapEntry);
begin
  vkfreemem(a,sizeof(VkSpecializationMapEntry));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSpecializationMapEntry);
begin
  vkadditem(a,sizeof(VkSpecializationMapEntry));
  a^:=default_VkSpecializationMapEntry;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSpecializationMapEntry);
begin
  vkremitem(a,sizeof(VkSpecializationMapEntry));
end;

procedure T_vulkan_auto.vknew(out a:PVkSpecializationInfo);
begin
  a:=vkgetmem(sizeof(VkSpecializationInfo));
  a^:=default_VkSpecializationInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSpecializationInfo);
begin
  vkfreemem(a,sizeof(VkSpecializationInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSpecializationInfo);
begin
  vkadditem(a,sizeof(VkSpecializationInfo));
  a^:=default_VkSpecializationInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSpecializationInfo);
begin
  vkremitem(a,sizeof(VkSpecializationInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineShaderStageCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineShaderStageCreateInfo));
  a^:=default_VkPipelineShaderStageCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineShaderStageCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineShaderStageCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineShaderStageCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineShaderStageCreateInfo));
  a^:=default_VkPipelineShaderStageCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineShaderStageCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineShaderStageCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkComputePipelineCreateInfo);
begin
  a:=vkgetmem(sizeof(VkComputePipelineCreateInfo));
  a^:=default_VkComputePipelineCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkComputePipelineCreateInfo);
begin
  vkfreemem(a,sizeof(VkComputePipelineCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkComputePipelineCreateInfo);
begin
  vkadditem(a,sizeof(VkComputePipelineCreateInfo));
  a^:=default_VkComputePipelineCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkComputePipelineCreateInfo);
begin
  vkremitem(a,sizeof(VkComputePipelineCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkVertexInputBindingDescription);
begin
  a:=vkgetmem(sizeof(VkVertexInputBindingDescription));
  a^:=default_VkVertexInputBindingDescription;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkVertexInputBindingDescription);
begin
  vkfreemem(a,sizeof(VkVertexInputBindingDescription));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkVertexInputBindingDescription);
begin
  vkadditem(a,sizeof(VkVertexInputBindingDescription));
  a^:=default_VkVertexInputBindingDescription;
end;
procedure T_vulkan_auto.vkrem(var a:PVkVertexInputBindingDescription);
begin
  vkremitem(a,sizeof(VkVertexInputBindingDescription));
end;

procedure T_vulkan_auto.vknew(out a:PVkVertexInputAttributeDescription);
begin
  a:=vkgetmem(sizeof(VkVertexInputAttributeDescription));
  a^:=default_VkVertexInputAttributeDescription;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkVertexInputAttributeDescription);
begin
  vkfreemem(a,sizeof(VkVertexInputAttributeDescription));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkVertexInputAttributeDescription);
begin
  vkadditem(a,sizeof(VkVertexInputAttributeDescription));
  a^:=default_VkVertexInputAttributeDescription;
end;
procedure T_vulkan_auto.vkrem(var a:PVkVertexInputAttributeDescription);
begin
  vkremitem(a,sizeof(VkVertexInputAttributeDescription));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineVertexInputStateCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineVertexInputStateCreateInfo));
  a^:=default_VkPipelineVertexInputStateCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineVertexInputStateCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineVertexInputStateCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineVertexInputStateCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineVertexInputStateCreateInfo));
  a^:=default_VkPipelineVertexInputStateCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineVertexInputStateCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineVertexInputStateCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineInputAssemblyStateCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineInputAssemblyStateCreateInfo));
  a^:=default_VkPipelineInputAssemblyStateCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineInputAssemblyStateCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineInputAssemblyStateCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineInputAssemblyStateCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineInputAssemblyStateCreateInfo));
  a^:=default_VkPipelineInputAssemblyStateCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineInputAssemblyStateCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineInputAssemblyStateCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineTessellationStateCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineTessellationStateCreateInfo));
  a^:=default_VkPipelineTessellationStateCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineTessellationStateCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineTessellationStateCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineTessellationStateCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineTessellationStateCreateInfo));
  a^:=default_VkPipelineTessellationStateCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineTessellationStateCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineTessellationStateCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineViewportStateCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineViewportStateCreateInfo));
  a^:=default_VkPipelineViewportStateCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineViewportStateCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineViewportStateCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineViewportStateCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineViewportStateCreateInfo));
  a^:=default_VkPipelineViewportStateCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineViewportStateCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineViewportStateCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineRasterizationStateCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineRasterizationStateCreateInfo));
  a^:=default_VkPipelineRasterizationStateCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineRasterizationStateCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineRasterizationStateCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineRasterizationStateCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineRasterizationStateCreateInfo));
  a^:=default_VkPipelineRasterizationStateCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineRasterizationStateCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineRasterizationStateCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineMultisampleStateCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineMultisampleStateCreateInfo));
  a^:=default_VkPipelineMultisampleStateCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineMultisampleStateCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineMultisampleStateCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineMultisampleStateCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineMultisampleStateCreateInfo));
  a^:=default_VkPipelineMultisampleStateCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineMultisampleStateCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineMultisampleStateCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineColorBlendAttachmentState);
begin
  a:=vkgetmem(sizeof(VkPipelineColorBlendAttachmentState));
  a^:=default_VkPipelineColorBlendAttachmentState;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineColorBlendAttachmentState);
begin
  vkfreemem(a,sizeof(VkPipelineColorBlendAttachmentState));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineColorBlendAttachmentState);
begin
  vkadditem(a,sizeof(VkPipelineColorBlendAttachmentState));
  a^:=default_VkPipelineColorBlendAttachmentState;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineColorBlendAttachmentState);
begin
  vkremitem(a,sizeof(VkPipelineColorBlendAttachmentState));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineColorBlendStateCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineColorBlendStateCreateInfo));
  a^:=default_VkPipelineColorBlendStateCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineColorBlendStateCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineColorBlendStateCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineColorBlendStateCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineColorBlendStateCreateInfo));
  a^:=default_VkPipelineColorBlendStateCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineColorBlendStateCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineColorBlendStateCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineDynamicStateCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineDynamicStateCreateInfo));
  a^:=default_VkPipelineDynamicStateCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineDynamicStateCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineDynamicStateCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineDynamicStateCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineDynamicStateCreateInfo));
  a^:=default_VkPipelineDynamicStateCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineDynamicStateCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineDynamicStateCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkStencilOpState);
begin
  a:=vkgetmem(sizeof(VkStencilOpState));
  a^:=default_VkStencilOpState;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkStencilOpState);
begin
  vkfreemem(a,sizeof(VkStencilOpState));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkStencilOpState);
begin
  vkadditem(a,sizeof(VkStencilOpState));
  a^:=default_VkStencilOpState;
end;
procedure T_vulkan_auto.vkrem(var a:PVkStencilOpState);
begin
  vkremitem(a,sizeof(VkStencilOpState));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineDepthStencilStateCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineDepthStencilStateCreateInfo));
  a^:=default_VkPipelineDepthStencilStateCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineDepthStencilStateCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineDepthStencilStateCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineDepthStencilStateCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineDepthStencilStateCreateInfo));
  a^:=default_VkPipelineDepthStencilStateCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineDepthStencilStateCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineDepthStencilStateCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkGraphicsPipelineCreateInfo);
begin
  a:=vkgetmem(sizeof(VkGraphicsPipelineCreateInfo));
  a^:=default_VkGraphicsPipelineCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkGraphicsPipelineCreateInfo);
begin
  vkfreemem(a,sizeof(VkGraphicsPipelineCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkGraphicsPipelineCreateInfo);
begin
  vkadditem(a,sizeof(VkGraphicsPipelineCreateInfo));
  a^:=default_VkGraphicsPipelineCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkGraphicsPipelineCreateInfo);
begin
  vkremitem(a,sizeof(VkGraphicsPipelineCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineCacheCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineCacheCreateInfo));
  a^:=default_VkPipelineCacheCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineCacheCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineCacheCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineCacheCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineCacheCreateInfo));
  a^:=default_VkPipelineCacheCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineCacheCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineCacheCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPushConstantRange);
begin
  a:=vkgetmem(sizeof(VkPushConstantRange));
  a^:=default_VkPushConstantRange;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPushConstantRange);
begin
  vkfreemem(a,sizeof(VkPushConstantRange));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPushConstantRange);
begin
  vkadditem(a,sizeof(VkPushConstantRange));
  a^:=default_VkPushConstantRange;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPushConstantRange);
begin
  vkremitem(a,sizeof(VkPushConstantRange));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineLayoutCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineLayoutCreateInfo));
  a^:=default_VkPipelineLayoutCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineLayoutCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineLayoutCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineLayoutCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineLayoutCreateInfo));
  a^:=default_VkPipelineLayoutCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineLayoutCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineLayoutCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkSamplerCreateInfo);
begin
  a:=vkgetmem(sizeof(VkSamplerCreateInfo));
  a^:=default_VkSamplerCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSamplerCreateInfo);
begin
  vkfreemem(a,sizeof(VkSamplerCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSamplerCreateInfo);
begin
  vkadditem(a,sizeof(VkSamplerCreateInfo));
  a^:=default_VkSamplerCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSamplerCreateInfo);
begin
  vkremitem(a,sizeof(VkSamplerCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkCommandPoolCreateInfo);
begin
  a:=vkgetmem(sizeof(VkCommandPoolCreateInfo));
  a^:=default_VkCommandPoolCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCommandPoolCreateInfo);
begin
  vkfreemem(a,sizeof(VkCommandPoolCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCommandPoolCreateInfo);
begin
  vkadditem(a,sizeof(VkCommandPoolCreateInfo));
  a^:=default_VkCommandPoolCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkCommandPoolCreateInfo);
begin
  vkremitem(a,sizeof(VkCommandPoolCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkCommandBufferAllocateInfo);
begin
  a:=vkgetmem(sizeof(VkCommandBufferAllocateInfo));
  a^:=default_VkCommandBufferAllocateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCommandBufferAllocateInfo);
begin
  vkfreemem(a,sizeof(VkCommandBufferAllocateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCommandBufferAllocateInfo);
begin
  vkadditem(a,sizeof(VkCommandBufferAllocateInfo));
  a^:=default_VkCommandBufferAllocateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkCommandBufferAllocateInfo);
begin
  vkremitem(a,sizeof(VkCommandBufferAllocateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkCommandBufferInheritanceInfo);
begin
  a:=vkgetmem(sizeof(VkCommandBufferInheritanceInfo));
  a^:=default_VkCommandBufferInheritanceInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCommandBufferInheritanceInfo);
begin
  vkfreemem(a,sizeof(VkCommandBufferInheritanceInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCommandBufferInheritanceInfo);
begin
  vkadditem(a,sizeof(VkCommandBufferInheritanceInfo));
  a^:=default_VkCommandBufferInheritanceInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkCommandBufferInheritanceInfo);
begin
  vkremitem(a,sizeof(VkCommandBufferInheritanceInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkCommandBufferBeginInfo);
begin
  a:=vkgetmem(sizeof(VkCommandBufferBeginInfo));
  a^:=default_VkCommandBufferBeginInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCommandBufferBeginInfo);
begin
  vkfreemem(a,sizeof(VkCommandBufferBeginInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCommandBufferBeginInfo);
begin
  vkadditem(a,sizeof(VkCommandBufferBeginInfo));
  a^:=default_VkCommandBufferBeginInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkCommandBufferBeginInfo);
begin
  vkremitem(a,sizeof(VkCommandBufferBeginInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkClearColorValue);
begin
  a:=vkgetmem(sizeof(VkClearColorValue));
  a^:=default_VkClearColorValue;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkClearColorValue);
begin
  vkfreemem(a,sizeof(VkClearColorValue));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkClearColorValue);
begin
  vkadditem(a,sizeof(VkClearColorValue));
  a^:=default_VkClearColorValue;
end;
procedure T_vulkan_auto.vkrem(var a:PVkClearColorValue);
begin
  vkremitem(a,sizeof(VkClearColorValue));
end;

procedure T_vulkan_auto.vknew(out a:PVkClearDepthStencilValue);
begin
  a:=vkgetmem(sizeof(VkClearDepthStencilValue));
  a^:=default_VkClearDepthStencilValue;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkClearDepthStencilValue);
begin
  vkfreemem(a,sizeof(VkClearDepthStencilValue));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkClearDepthStencilValue);
begin
  vkadditem(a,sizeof(VkClearDepthStencilValue));
  a^:=default_VkClearDepthStencilValue;
end;
procedure T_vulkan_auto.vkrem(var a:PVkClearDepthStencilValue);
begin
  vkremitem(a,sizeof(VkClearDepthStencilValue));
end;

procedure T_vulkan_auto.vknew(out a:PVkClearValue);
begin
  a:=vkgetmem(sizeof(VkClearValue));
  a^:=default_VkClearValue;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkClearValue);
begin
  vkfreemem(a,sizeof(VkClearValue));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkClearValue);
begin
  vkadditem(a,sizeof(VkClearValue));
  a^:=default_VkClearValue;
end;
procedure T_vulkan_auto.vkrem(var a:PVkClearValue);
begin
  vkremitem(a,sizeof(VkClearValue));
end;

procedure T_vulkan_auto.vknew(out a:PVkClearAttachment);
begin
  a:=vkgetmem(sizeof(VkClearAttachment));
  a^:=default_VkClearAttachment;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkClearAttachment);
begin
  vkfreemem(a,sizeof(VkClearAttachment));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkClearAttachment);
begin
  vkadditem(a,sizeof(VkClearAttachment));
  a^:=default_VkClearAttachment;
end;
procedure T_vulkan_auto.vkrem(var a:PVkClearAttachment);
begin
  vkremitem(a,sizeof(VkClearAttachment));
end;

procedure T_vulkan_auto.vknew(out a:PVkAttachmentDescription);
begin
  a:=vkgetmem(sizeof(VkAttachmentDescription));
  a^:=default_VkAttachmentDescription;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAttachmentDescription);
begin
  vkfreemem(a,sizeof(VkAttachmentDescription));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAttachmentDescription);
begin
  vkadditem(a,sizeof(VkAttachmentDescription));
  a^:=default_VkAttachmentDescription;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAttachmentDescription);
begin
  vkremitem(a,sizeof(VkAttachmentDescription));
end;

procedure T_vulkan_auto.vknew(out a:PVkAttachmentReference);
begin
  a:=vkgetmem(sizeof(VkAttachmentReference));
  a^:=default_VkAttachmentReference;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAttachmentReference);
begin
  vkfreemem(a,sizeof(VkAttachmentReference));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAttachmentReference);
begin
  vkadditem(a,sizeof(VkAttachmentReference));
  a^:=default_VkAttachmentReference;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAttachmentReference);
begin
  vkremitem(a,sizeof(VkAttachmentReference));
end;

procedure T_vulkan_auto.vknew(out a:PVkSubpassDescription);
begin
  a:=vkgetmem(sizeof(VkSubpassDescription));
  a^:=default_VkSubpassDescription;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSubpassDescription);
begin
  vkfreemem(a,sizeof(VkSubpassDescription));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSubpassDescription);
begin
  vkadditem(a,sizeof(VkSubpassDescription));
  a^:=default_VkSubpassDescription;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSubpassDescription);
begin
  vkremitem(a,sizeof(VkSubpassDescription));
end;

procedure T_vulkan_auto.vknew(out a:PVkSubpassDependency);
begin
  a:=vkgetmem(sizeof(VkSubpassDependency));
  a^:=default_VkSubpassDependency;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSubpassDependency);
begin
  vkfreemem(a,sizeof(VkSubpassDependency));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSubpassDependency);
begin
  vkadditem(a,sizeof(VkSubpassDependency));
  a^:=default_VkSubpassDependency;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSubpassDependency);
begin
  vkremitem(a,sizeof(VkSubpassDependency));
end;

procedure T_vulkan_auto.vknew(out a:PVkRenderPassCreateInfo);
begin
  a:=vkgetmem(sizeof(VkRenderPassCreateInfo));
  a^:=default_VkRenderPassCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRenderPassCreateInfo);
begin
  vkfreemem(a,sizeof(VkRenderPassCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRenderPassCreateInfo);
begin
  vkadditem(a,sizeof(VkRenderPassCreateInfo));
  a^:=default_VkRenderPassCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkRenderPassCreateInfo);
begin
  vkremitem(a,sizeof(VkRenderPassCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkEventCreateInfo);
begin
  a:=vkgetmem(sizeof(VkEventCreateInfo));
  a^:=default_VkEventCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkEventCreateInfo);
begin
  vkfreemem(a,sizeof(VkEventCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkEventCreateInfo);
begin
  vkadditem(a,sizeof(VkEventCreateInfo));
  a^:=default_VkEventCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkEventCreateInfo);
begin
  vkremitem(a,sizeof(VkEventCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkFenceCreateInfo);
begin
  a:=vkgetmem(sizeof(VkFenceCreateInfo));
  a^:=default_VkFenceCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkFenceCreateInfo);
begin
  vkfreemem(a,sizeof(VkFenceCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkFenceCreateInfo);
begin
  vkadditem(a,sizeof(VkFenceCreateInfo));
  a^:=default_VkFenceCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkFenceCreateInfo);
begin
  vkremitem(a,sizeof(VkFenceCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceFeatures));
  a^:=default_VkPhysicalDeviceFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceFeatures));
  a^:=default_VkPhysicalDeviceFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceSparseProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceSparseProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceSparseProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceSparseProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceSparseProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceSparseProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceSparseProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceSparseProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceSparseProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceSparseProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceLimits);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceLimits));
  FillByte(a^,sizeof(VkPhysicalDeviceLimits),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceLimits);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceLimits));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceLimits);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceLimits));
  FillByte(a^,sizeof(VkPhysicalDeviceLimits),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceLimits);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceLimits));
end;

procedure T_vulkan_auto.vknew(out a:PVkSemaphoreCreateInfo);
begin
  a:=vkgetmem(sizeof(VkSemaphoreCreateInfo));
  a^:=default_VkSemaphoreCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSemaphoreCreateInfo);
begin
  vkfreemem(a,sizeof(VkSemaphoreCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSemaphoreCreateInfo);
begin
  vkadditem(a,sizeof(VkSemaphoreCreateInfo));
  a^:=default_VkSemaphoreCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSemaphoreCreateInfo);
begin
  vkremitem(a,sizeof(VkSemaphoreCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkQueryPoolCreateInfo);
begin
  a:=vkgetmem(sizeof(VkQueryPoolCreateInfo));
  a^:=default_VkQueryPoolCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkQueryPoolCreateInfo);
begin
  vkfreemem(a,sizeof(VkQueryPoolCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkQueryPoolCreateInfo);
begin
  vkadditem(a,sizeof(VkQueryPoolCreateInfo));
  a^:=default_VkQueryPoolCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkQueryPoolCreateInfo);
begin
  vkremitem(a,sizeof(VkQueryPoolCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkFramebufferCreateInfo);
begin
  a:=vkgetmem(sizeof(VkFramebufferCreateInfo));
  a^:=default_VkFramebufferCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkFramebufferCreateInfo);
begin
  vkfreemem(a,sizeof(VkFramebufferCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkFramebufferCreateInfo);
begin
  vkadditem(a,sizeof(VkFramebufferCreateInfo));
  a^:=default_VkFramebufferCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkFramebufferCreateInfo);
begin
  vkremitem(a,sizeof(VkFramebufferCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDrawIndirectCommand);
begin
  a:=vkgetmem(sizeof(VkDrawIndirectCommand));
  a^:=default_VkDrawIndirectCommand;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDrawIndirectCommand);
begin
  vkfreemem(a,sizeof(VkDrawIndirectCommand));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDrawIndirectCommand);
begin
  vkadditem(a,sizeof(VkDrawIndirectCommand));
  a^:=default_VkDrawIndirectCommand;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDrawIndirectCommand);
begin
  vkremitem(a,sizeof(VkDrawIndirectCommand));
end;

procedure T_vulkan_auto.vknew(out a:PVkDrawIndexedIndirectCommand);
begin
  a:=vkgetmem(sizeof(VkDrawIndexedIndirectCommand));
  a^:=default_VkDrawIndexedIndirectCommand;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDrawIndexedIndirectCommand);
begin
  vkfreemem(a,sizeof(VkDrawIndexedIndirectCommand));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDrawIndexedIndirectCommand);
begin
  vkadditem(a,sizeof(VkDrawIndexedIndirectCommand));
  a^:=default_VkDrawIndexedIndirectCommand;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDrawIndexedIndirectCommand);
begin
  vkremitem(a,sizeof(VkDrawIndexedIndirectCommand));
end;

procedure T_vulkan_auto.vknew(out a:PVkDispatchIndirectCommand);
begin
  a:=vkgetmem(sizeof(VkDispatchIndirectCommand));
  a^:=default_VkDispatchIndirectCommand;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDispatchIndirectCommand);
begin
  vkfreemem(a,sizeof(VkDispatchIndirectCommand));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDispatchIndirectCommand);
begin
  vkadditem(a,sizeof(VkDispatchIndirectCommand));
  a^:=default_VkDispatchIndirectCommand;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDispatchIndirectCommand);
begin
  vkremitem(a,sizeof(VkDispatchIndirectCommand));
end;

procedure T_vulkan_auto.vknew(out a:PVkSubmitInfo);
begin
  a:=vkgetmem(sizeof(VkSubmitInfo));
  a^:=default_VkSubmitInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSubmitInfo);
begin
  vkfreemem(a,sizeof(VkSubmitInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSubmitInfo);
begin
  vkadditem(a,sizeof(VkSubmitInfo));
  a^:=default_VkSubmitInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSubmitInfo);
begin
  vkremitem(a,sizeof(VkSubmitInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayPropertiesKHR);
begin
  a:=vkgetmem(sizeof(VkDisplayPropertiesKHR));
  FillByte(a^,sizeof(VkDisplayPropertiesKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayPropertiesKHR);
begin
  vkfreemem(a,sizeof(VkDisplayPropertiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayPropertiesKHR);
begin
  vkadditem(a,sizeof(VkDisplayPropertiesKHR));
  FillByte(a^,sizeof(VkDisplayPropertiesKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayPropertiesKHR);
begin
  vkremitem(a,sizeof(VkDisplayPropertiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayPlanePropertiesKHR);
begin
  a:=vkgetmem(sizeof(VkDisplayPlanePropertiesKHR));
  FillByte(a^,sizeof(VkDisplayPlanePropertiesKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayPlanePropertiesKHR);
begin
  vkfreemem(a,sizeof(VkDisplayPlanePropertiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayPlanePropertiesKHR);
begin
  vkadditem(a,sizeof(VkDisplayPlanePropertiesKHR));
  FillByte(a^,sizeof(VkDisplayPlanePropertiesKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayPlanePropertiesKHR);
begin
  vkremitem(a,sizeof(VkDisplayPlanePropertiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayModeParametersKHR);
begin
  a:=vkgetmem(sizeof(VkDisplayModeParametersKHR));
  a^:=default_VkDisplayModeParametersKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayModeParametersKHR);
begin
  vkfreemem(a,sizeof(VkDisplayModeParametersKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayModeParametersKHR);
begin
  vkadditem(a,sizeof(VkDisplayModeParametersKHR));
  a^:=default_VkDisplayModeParametersKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayModeParametersKHR);
begin
  vkremitem(a,sizeof(VkDisplayModeParametersKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayModePropertiesKHR);
begin
  a:=vkgetmem(sizeof(VkDisplayModePropertiesKHR));
  FillByte(a^,sizeof(VkDisplayModePropertiesKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayModePropertiesKHR);
begin
  vkfreemem(a,sizeof(VkDisplayModePropertiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayModePropertiesKHR);
begin
  vkadditem(a,sizeof(VkDisplayModePropertiesKHR));
  FillByte(a^,sizeof(VkDisplayModePropertiesKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayModePropertiesKHR);
begin
  vkremitem(a,sizeof(VkDisplayModePropertiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayModeCreateInfoKHR);
begin
  a:=vkgetmem(sizeof(VkDisplayModeCreateInfoKHR));
  a^:=default_VkDisplayModeCreateInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayModeCreateInfoKHR);
begin
  vkfreemem(a,sizeof(VkDisplayModeCreateInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayModeCreateInfoKHR);
begin
  vkadditem(a,sizeof(VkDisplayModeCreateInfoKHR));
  a^:=default_VkDisplayModeCreateInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayModeCreateInfoKHR);
begin
  vkremitem(a,sizeof(VkDisplayModeCreateInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayPlaneCapabilitiesKHR);
begin
  a:=vkgetmem(sizeof(VkDisplayPlaneCapabilitiesKHR));
  FillByte(a^,sizeof(VkDisplayPlaneCapabilitiesKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayPlaneCapabilitiesKHR);
begin
  vkfreemem(a,sizeof(VkDisplayPlaneCapabilitiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayPlaneCapabilitiesKHR);
begin
  vkadditem(a,sizeof(VkDisplayPlaneCapabilitiesKHR));
  FillByte(a^,sizeof(VkDisplayPlaneCapabilitiesKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayPlaneCapabilitiesKHR);
begin
  vkremitem(a,sizeof(VkDisplayPlaneCapabilitiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplaySurfaceCreateInfoKHR);
begin
  a:=vkgetmem(sizeof(VkDisplaySurfaceCreateInfoKHR));
  a^:=default_VkDisplaySurfaceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplaySurfaceCreateInfoKHR);
begin
  vkfreemem(a,sizeof(VkDisplaySurfaceCreateInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplaySurfaceCreateInfoKHR);
begin
  vkadditem(a,sizeof(VkDisplaySurfaceCreateInfoKHR));
  a^:=default_VkDisplaySurfaceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplaySurfaceCreateInfoKHR);
begin
  vkremitem(a,sizeof(VkDisplaySurfaceCreateInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayPresentInfoKHR);
begin
  a:=vkgetmem(sizeof(VkDisplayPresentInfoKHR));
  a^:=default_VkDisplayPresentInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayPresentInfoKHR);
begin
  vkfreemem(a,sizeof(VkDisplayPresentInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayPresentInfoKHR);
begin
  vkadditem(a,sizeof(VkDisplayPresentInfoKHR));
  a^:=default_VkDisplayPresentInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayPresentInfoKHR);
begin
  vkremitem(a,sizeof(VkDisplayPresentInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkSurfaceCapabilitiesKHR);
begin
  a:=vkgetmem(sizeof(VkSurfaceCapabilitiesKHR));
  FillByte(a^,sizeof(VkSurfaceCapabilitiesKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSurfaceCapabilitiesKHR);
begin
  vkfreemem(a,sizeof(VkSurfaceCapabilitiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSurfaceCapabilitiesKHR);
begin
  vkadditem(a,sizeof(VkSurfaceCapabilitiesKHR));
  FillByte(a^,sizeof(VkSurfaceCapabilitiesKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSurfaceCapabilitiesKHR);
begin
  vkremitem(a,sizeof(VkSurfaceCapabilitiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkAndroidSurfaceCreateInfoKHR);
begin
  a:=vkgetmem(sizeof(VkAndroidSurfaceCreateInfoKHR));
  a^:=default_VkAndroidSurfaceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAndroidSurfaceCreateInfoKHR);
begin
  vkfreemem(a,sizeof(VkAndroidSurfaceCreateInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAndroidSurfaceCreateInfoKHR);
begin
  vkadditem(a,sizeof(VkAndroidSurfaceCreateInfoKHR));
  a^:=default_VkAndroidSurfaceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAndroidSurfaceCreateInfoKHR);
begin
  vkremitem(a,sizeof(VkAndroidSurfaceCreateInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkViSurfaceCreateInfoNN);
begin
  a:=vkgetmem(sizeof(VkViSurfaceCreateInfoNN));
  a^:=default_VkViSurfaceCreateInfoNN;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkViSurfaceCreateInfoNN);
begin
  vkfreemem(a,sizeof(VkViSurfaceCreateInfoNN));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkViSurfaceCreateInfoNN);
begin
  vkadditem(a,sizeof(VkViSurfaceCreateInfoNN));
  a^:=default_VkViSurfaceCreateInfoNN;
end;
procedure T_vulkan_auto.vkrem(var a:PVkViSurfaceCreateInfoNN);
begin
  vkremitem(a,sizeof(VkViSurfaceCreateInfoNN));
end;

procedure T_vulkan_auto.vknew(out a:PVkWaylandSurfaceCreateInfoKHR);
begin
  a:=vkgetmem(sizeof(VkWaylandSurfaceCreateInfoKHR));
  a^:=default_VkWaylandSurfaceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkWaylandSurfaceCreateInfoKHR);
begin
  vkfreemem(a,sizeof(VkWaylandSurfaceCreateInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkWaylandSurfaceCreateInfoKHR);
begin
  vkadditem(a,sizeof(VkWaylandSurfaceCreateInfoKHR));
  a^:=default_VkWaylandSurfaceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkWaylandSurfaceCreateInfoKHR);
begin
  vkremitem(a,sizeof(VkWaylandSurfaceCreateInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkWin32SurfaceCreateInfoKHR);
begin
  a:=vkgetmem(sizeof(VkWin32SurfaceCreateInfoKHR));
  a^:=default_VkWin32SurfaceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkWin32SurfaceCreateInfoKHR);
begin
  vkfreemem(a,sizeof(VkWin32SurfaceCreateInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkWin32SurfaceCreateInfoKHR);
begin
  vkadditem(a,sizeof(VkWin32SurfaceCreateInfoKHR));
  a^:=default_VkWin32SurfaceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkWin32SurfaceCreateInfoKHR);
begin
  vkremitem(a,sizeof(VkWin32SurfaceCreateInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkXlibSurfaceCreateInfoKHR);
begin
  a:=vkgetmem(sizeof(VkXlibSurfaceCreateInfoKHR));
  a^:=default_VkXlibSurfaceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkXlibSurfaceCreateInfoKHR);
begin
  vkfreemem(a,sizeof(VkXlibSurfaceCreateInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkXlibSurfaceCreateInfoKHR);
begin
  vkadditem(a,sizeof(VkXlibSurfaceCreateInfoKHR));
  a^:=default_VkXlibSurfaceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkXlibSurfaceCreateInfoKHR);
begin
  vkremitem(a,sizeof(VkXlibSurfaceCreateInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkXcbSurfaceCreateInfoKHR);
begin
  a:=vkgetmem(sizeof(VkXcbSurfaceCreateInfoKHR));
  a^:=default_VkXcbSurfaceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkXcbSurfaceCreateInfoKHR);
begin
  vkfreemem(a,sizeof(VkXcbSurfaceCreateInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkXcbSurfaceCreateInfoKHR);
begin
  vkadditem(a,sizeof(VkXcbSurfaceCreateInfoKHR));
  a^:=default_VkXcbSurfaceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkXcbSurfaceCreateInfoKHR);
begin
  vkremitem(a,sizeof(VkXcbSurfaceCreateInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkImagePipeSurfaceCreateInfoFUCHSIA);
begin
  a:=vkgetmem(sizeof(VkImagePipeSurfaceCreateInfoFUCHSIA));
  a^:=default_VkImagePipeSurfaceCreateInfoFUCHSIA;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImagePipeSurfaceCreateInfoFUCHSIA);
begin
  vkfreemem(a,sizeof(VkImagePipeSurfaceCreateInfoFUCHSIA));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImagePipeSurfaceCreateInfoFUCHSIA);
begin
  vkadditem(a,sizeof(VkImagePipeSurfaceCreateInfoFUCHSIA));
  a^:=default_VkImagePipeSurfaceCreateInfoFUCHSIA;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImagePipeSurfaceCreateInfoFUCHSIA);
begin
  vkremitem(a,sizeof(VkImagePipeSurfaceCreateInfoFUCHSIA));
end;

procedure T_vulkan_auto.vknew(out a:PVkStreamDescriptorSurfaceCreateInfoGGP);
begin
  a:=vkgetmem(sizeof(VkStreamDescriptorSurfaceCreateInfoGGP));
  a^:=default_VkStreamDescriptorSurfaceCreateInfoGGP;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkStreamDescriptorSurfaceCreateInfoGGP);
begin
  vkfreemem(a,sizeof(VkStreamDescriptorSurfaceCreateInfoGGP));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkStreamDescriptorSurfaceCreateInfoGGP);
begin
  vkadditem(a,sizeof(VkStreamDescriptorSurfaceCreateInfoGGP));
  a^:=default_VkStreamDescriptorSurfaceCreateInfoGGP;
end;
procedure T_vulkan_auto.vkrem(var a:PVkStreamDescriptorSurfaceCreateInfoGGP);
begin
  vkremitem(a,sizeof(VkStreamDescriptorSurfaceCreateInfoGGP));
end;

procedure T_vulkan_auto.vknew(out a:PVkSurfaceFormatKHR);
begin
  a:=vkgetmem(sizeof(VkSurfaceFormatKHR));
  FillByte(a^,sizeof(VkSurfaceFormatKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSurfaceFormatKHR);
begin
  vkfreemem(a,sizeof(VkSurfaceFormatKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSurfaceFormatKHR);
begin
  vkadditem(a,sizeof(VkSurfaceFormatKHR));
  FillByte(a^,sizeof(VkSurfaceFormatKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSurfaceFormatKHR);
begin
  vkremitem(a,sizeof(VkSurfaceFormatKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkSwapchainCreateInfoKHR);
begin
  a:=vkgetmem(sizeof(VkSwapchainCreateInfoKHR));
  a^:=default_VkSwapchainCreateInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSwapchainCreateInfoKHR);
begin
  vkfreemem(a,sizeof(VkSwapchainCreateInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSwapchainCreateInfoKHR);
begin
  vkadditem(a,sizeof(VkSwapchainCreateInfoKHR));
  a^:=default_VkSwapchainCreateInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSwapchainCreateInfoKHR);
begin
  vkremitem(a,sizeof(VkSwapchainCreateInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPresentInfoKHR);
begin
  a:=vkgetmem(sizeof(VkPresentInfoKHR));
  a^:=default_VkPresentInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPresentInfoKHR);
begin
  vkfreemem(a,sizeof(VkPresentInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPresentInfoKHR);
begin
  vkadditem(a,sizeof(VkPresentInfoKHR));
  a^:=default_VkPresentInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPresentInfoKHR);
begin
  vkremitem(a,sizeof(VkPresentInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkValidationFlagsEXT);
begin
  a:=vkgetmem(sizeof(VkValidationFlagsEXT));
  a^:=default_VkValidationFlagsEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkValidationFlagsEXT);
begin
  vkfreemem(a,sizeof(VkValidationFlagsEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkValidationFlagsEXT);
begin
  vkadditem(a,sizeof(VkValidationFlagsEXT));
  a^:=default_VkValidationFlagsEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkValidationFlagsEXT);
begin
  vkremitem(a,sizeof(VkValidationFlagsEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkValidationFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkValidationFeaturesEXT));
  a^:=default_VkValidationFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkValidationFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkValidationFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkValidationFeaturesEXT);
begin
  vkadditem(a,sizeof(VkValidationFeaturesEXT));
  a^:=default_VkValidationFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkValidationFeaturesEXT);
begin
  vkremitem(a,sizeof(VkValidationFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineRasterizationStateRasterizationOrderAMD);
begin
  a:=vkgetmem(sizeof(VkPipelineRasterizationStateRasterizationOrderAMD));
  a^:=default_VkPipelineRasterizationStateRasterizationOrderAMD;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineRasterizationStateRasterizationOrderAMD);
begin
  vkfreemem(a,sizeof(VkPipelineRasterizationStateRasterizationOrderAMD));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineRasterizationStateRasterizationOrderAMD);
begin
  vkadditem(a,sizeof(VkPipelineRasterizationStateRasterizationOrderAMD));
  a^:=default_VkPipelineRasterizationStateRasterizationOrderAMD;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineRasterizationStateRasterizationOrderAMD);
begin
  vkremitem(a,sizeof(VkPipelineRasterizationStateRasterizationOrderAMD));
end;

procedure T_vulkan_auto.vknew(out a:PVkDebugMarkerObjectNameInfoEXT);
begin
  a:=vkgetmem(sizeof(VkDebugMarkerObjectNameInfoEXT));
  a^:=default_VkDebugMarkerObjectNameInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDebugMarkerObjectNameInfoEXT);
begin
  vkfreemem(a,sizeof(VkDebugMarkerObjectNameInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDebugMarkerObjectNameInfoEXT);
begin
  vkadditem(a,sizeof(VkDebugMarkerObjectNameInfoEXT));
  a^:=default_VkDebugMarkerObjectNameInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDebugMarkerObjectNameInfoEXT);
begin
  vkremitem(a,sizeof(VkDebugMarkerObjectNameInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDebugMarkerObjectTagInfoEXT);
begin
  a:=vkgetmem(sizeof(VkDebugMarkerObjectTagInfoEXT));
  a^:=default_VkDebugMarkerObjectTagInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDebugMarkerObjectTagInfoEXT);
begin
  vkfreemem(a,sizeof(VkDebugMarkerObjectTagInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDebugMarkerObjectTagInfoEXT);
begin
  vkadditem(a,sizeof(VkDebugMarkerObjectTagInfoEXT));
  a^:=default_VkDebugMarkerObjectTagInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDebugMarkerObjectTagInfoEXT);
begin
  vkremitem(a,sizeof(VkDebugMarkerObjectTagInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDebugMarkerMarkerInfoEXT);
begin
  a:=vkgetmem(sizeof(VkDebugMarkerMarkerInfoEXT));
  a^:=default_VkDebugMarkerMarkerInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDebugMarkerMarkerInfoEXT);
begin
  vkfreemem(a,sizeof(VkDebugMarkerMarkerInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDebugMarkerMarkerInfoEXT);
begin
  vkadditem(a,sizeof(VkDebugMarkerMarkerInfoEXT));
  a^:=default_VkDebugMarkerMarkerInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDebugMarkerMarkerInfoEXT);
begin
  vkremitem(a,sizeof(VkDebugMarkerMarkerInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDedicatedAllocationImageCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkDedicatedAllocationImageCreateInfoNV));
  a^:=default_VkDedicatedAllocationImageCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDedicatedAllocationImageCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkDedicatedAllocationImageCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDedicatedAllocationImageCreateInfoNV);
begin
  vkadditem(a,sizeof(VkDedicatedAllocationImageCreateInfoNV));
  a^:=default_VkDedicatedAllocationImageCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDedicatedAllocationImageCreateInfoNV);
begin
  vkremitem(a,sizeof(VkDedicatedAllocationImageCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkDedicatedAllocationBufferCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkDedicatedAllocationBufferCreateInfoNV));
  a^:=default_VkDedicatedAllocationBufferCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDedicatedAllocationBufferCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkDedicatedAllocationBufferCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDedicatedAllocationBufferCreateInfoNV);
begin
  vkadditem(a,sizeof(VkDedicatedAllocationBufferCreateInfoNV));
  a^:=default_VkDedicatedAllocationBufferCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDedicatedAllocationBufferCreateInfoNV);
begin
  vkremitem(a,sizeof(VkDedicatedAllocationBufferCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkDedicatedAllocationMemoryAllocateInfoNV);
begin
  a:=vkgetmem(sizeof(VkDedicatedAllocationMemoryAllocateInfoNV));
  a^:=default_VkDedicatedAllocationMemoryAllocateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDedicatedAllocationMemoryAllocateInfoNV);
begin
  vkfreemem(a,sizeof(VkDedicatedAllocationMemoryAllocateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDedicatedAllocationMemoryAllocateInfoNV);
begin
  vkadditem(a,sizeof(VkDedicatedAllocationMemoryAllocateInfoNV));
  a^:=default_VkDedicatedAllocationMemoryAllocateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDedicatedAllocationMemoryAllocateInfoNV);
begin
  vkremitem(a,sizeof(VkDedicatedAllocationMemoryAllocateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkExternalImageFormatPropertiesNV);
begin
  a:=vkgetmem(sizeof(VkExternalImageFormatPropertiesNV));
  FillByte(a^,sizeof(VkExternalImageFormatPropertiesNV),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExternalImageFormatPropertiesNV);
begin
  vkfreemem(a,sizeof(VkExternalImageFormatPropertiesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExternalImageFormatPropertiesNV);
begin
  vkadditem(a,sizeof(VkExternalImageFormatPropertiesNV));
  FillByte(a^,sizeof(VkExternalImageFormatPropertiesNV),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkExternalImageFormatPropertiesNV);
begin
  vkremitem(a,sizeof(VkExternalImageFormatPropertiesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkExternalMemoryImageCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkExternalMemoryImageCreateInfoNV));
  a^:=default_VkExternalMemoryImageCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExternalMemoryImageCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkExternalMemoryImageCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExternalMemoryImageCreateInfoNV);
begin
  vkadditem(a,sizeof(VkExternalMemoryImageCreateInfoNV));
  a^:=default_VkExternalMemoryImageCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExternalMemoryImageCreateInfoNV);
begin
  vkremitem(a,sizeof(VkExternalMemoryImageCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkExportMemoryAllocateInfoNV);
begin
  a:=vkgetmem(sizeof(VkExportMemoryAllocateInfoNV));
  a^:=default_VkExportMemoryAllocateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExportMemoryAllocateInfoNV);
begin
  vkfreemem(a,sizeof(VkExportMemoryAllocateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExportMemoryAllocateInfoNV);
begin
  vkadditem(a,sizeof(VkExportMemoryAllocateInfoNV));
  a^:=default_VkExportMemoryAllocateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExportMemoryAllocateInfoNV);
begin
  vkremitem(a,sizeof(VkExportMemoryAllocateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkImportMemoryWin32HandleInfoNV);
begin
  a:=vkgetmem(sizeof(VkImportMemoryWin32HandleInfoNV));
  a^:=default_VkImportMemoryWin32HandleInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImportMemoryWin32HandleInfoNV);
begin
  vkfreemem(a,sizeof(VkImportMemoryWin32HandleInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImportMemoryWin32HandleInfoNV);
begin
  vkadditem(a,sizeof(VkImportMemoryWin32HandleInfoNV));
  a^:=default_VkImportMemoryWin32HandleInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImportMemoryWin32HandleInfoNV);
begin
  vkremitem(a,sizeof(VkImportMemoryWin32HandleInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkExportMemoryWin32HandleInfoNV);
begin
  a:=vkgetmem(sizeof(VkExportMemoryWin32HandleInfoNV));
  a^:=default_VkExportMemoryWin32HandleInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExportMemoryWin32HandleInfoNV);
begin
  vkfreemem(a,sizeof(VkExportMemoryWin32HandleInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExportMemoryWin32HandleInfoNV);
begin
  vkadditem(a,sizeof(VkExportMemoryWin32HandleInfoNV));
  a^:=default_VkExportMemoryWin32HandleInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExportMemoryWin32HandleInfoNV);
begin
  vkremitem(a,sizeof(VkExportMemoryWin32HandleInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkWin32KeyedMutexAcquireReleaseInfoNV);
begin
  a:=vkgetmem(sizeof(VkWin32KeyedMutexAcquireReleaseInfoNV));
  a^:=default_VkWin32KeyedMutexAcquireReleaseInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkWin32KeyedMutexAcquireReleaseInfoNV);
begin
  vkfreemem(a,sizeof(VkWin32KeyedMutexAcquireReleaseInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkWin32KeyedMutexAcquireReleaseInfoNV);
begin
  vkadditem(a,sizeof(VkWin32KeyedMutexAcquireReleaseInfoNV));
  a^:=default_VkWin32KeyedMutexAcquireReleaseInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkWin32KeyedMutexAcquireReleaseInfoNV);
begin
  vkremitem(a,sizeof(VkWin32KeyedMutexAcquireReleaseInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceGeneratedCommandsFeaturesNVX);
begin
  a:=vkgetmem(sizeof(VkDeviceGeneratedCommandsFeaturesNVX));
  a^:=default_VkDeviceGeneratedCommandsFeaturesNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceGeneratedCommandsFeaturesNVX);
begin
  vkfreemem(a,sizeof(VkDeviceGeneratedCommandsFeaturesNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceGeneratedCommandsFeaturesNVX);
begin
  vkadditem(a,sizeof(VkDeviceGeneratedCommandsFeaturesNVX));
  a^:=default_VkDeviceGeneratedCommandsFeaturesNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceGeneratedCommandsFeaturesNVX);
begin
  vkremitem(a,sizeof(VkDeviceGeneratedCommandsFeaturesNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceGeneratedCommandsLimitsNVX);
begin
  a:=vkgetmem(sizeof(VkDeviceGeneratedCommandsLimitsNVX));
  a^:=default_VkDeviceGeneratedCommandsLimitsNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceGeneratedCommandsLimitsNVX);
begin
  vkfreemem(a,sizeof(VkDeviceGeneratedCommandsLimitsNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceGeneratedCommandsLimitsNVX);
begin
  vkadditem(a,sizeof(VkDeviceGeneratedCommandsLimitsNVX));
  a^:=default_VkDeviceGeneratedCommandsLimitsNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceGeneratedCommandsLimitsNVX);
begin
  vkremitem(a,sizeof(VkDeviceGeneratedCommandsLimitsNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkIndirectCommandsTokenNVX);
begin
  a:=vkgetmem(sizeof(VkIndirectCommandsTokenNVX));
  a^:=default_VkIndirectCommandsTokenNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkIndirectCommandsTokenNVX);
begin
  vkfreemem(a,sizeof(VkIndirectCommandsTokenNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkIndirectCommandsTokenNVX);
begin
  vkadditem(a,sizeof(VkIndirectCommandsTokenNVX));
  a^:=default_VkIndirectCommandsTokenNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkIndirectCommandsTokenNVX);
begin
  vkremitem(a,sizeof(VkIndirectCommandsTokenNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkIndirectCommandsLayoutTokenNVX);
begin
  a:=vkgetmem(sizeof(VkIndirectCommandsLayoutTokenNVX));
  a^:=default_VkIndirectCommandsLayoutTokenNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkIndirectCommandsLayoutTokenNVX);
begin
  vkfreemem(a,sizeof(VkIndirectCommandsLayoutTokenNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkIndirectCommandsLayoutTokenNVX);
begin
  vkadditem(a,sizeof(VkIndirectCommandsLayoutTokenNVX));
  a^:=default_VkIndirectCommandsLayoutTokenNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkIndirectCommandsLayoutTokenNVX);
begin
  vkremitem(a,sizeof(VkIndirectCommandsLayoutTokenNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkIndirectCommandsLayoutCreateInfoNVX);
begin
  a:=vkgetmem(sizeof(VkIndirectCommandsLayoutCreateInfoNVX));
  a^:=default_VkIndirectCommandsLayoutCreateInfoNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkIndirectCommandsLayoutCreateInfoNVX);
begin
  vkfreemem(a,sizeof(VkIndirectCommandsLayoutCreateInfoNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkIndirectCommandsLayoutCreateInfoNVX);
begin
  vkadditem(a,sizeof(VkIndirectCommandsLayoutCreateInfoNVX));
  a^:=default_VkIndirectCommandsLayoutCreateInfoNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkIndirectCommandsLayoutCreateInfoNVX);
begin
  vkremitem(a,sizeof(VkIndirectCommandsLayoutCreateInfoNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkCmdProcessCommandsInfoNVX);
begin
  a:=vkgetmem(sizeof(VkCmdProcessCommandsInfoNVX));
  a^:=default_VkCmdProcessCommandsInfoNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCmdProcessCommandsInfoNVX);
begin
  vkfreemem(a,sizeof(VkCmdProcessCommandsInfoNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCmdProcessCommandsInfoNVX);
begin
  vkadditem(a,sizeof(VkCmdProcessCommandsInfoNVX));
  a^:=default_VkCmdProcessCommandsInfoNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkCmdProcessCommandsInfoNVX);
begin
  vkremitem(a,sizeof(VkCmdProcessCommandsInfoNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkCmdReserveSpaceForCommandsInfoNVX);
begin
  a:=vkgetmem(sizeof(VkCmdReserveSpaceForCommandsInfoNVX));
  a^:=default_VkCmdReserveSpaceForCommandsInfoNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCmdReserveSpaceForCommandsInfoNVX);
begin
  vkfreemem(a,sizeof(VkCmdReserveSpaceForCommandsInfoNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCmdReserveSpaceForCommandsInfoNVX);
begin
  vkadditem(a,sizeof(VkCmdReserveSpaceForCommandsInfoNVX));
  a^:=default_VkCmdReserveSpaceForCommandsInfoNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkCmdReserveSpaceForCommandsInfoNVX);
begin
  vkremitem(a,sizeof(VkCmdReserveSpaceForCommandsInfoNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkObjectTableCreateInfoNVX);
begin
  a:=vkgetmem(sizeof(VkObjectTableCreateInfoNVX));
  a^:=default_VkObjectTableCreateInfoNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkObjectTableCreateInfoNVX);
begin
  vkfreemem(a,sizeof(VkObjectTableCreateInfoNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkObjectTableCreateInfoNVX);
begin
  vkadditem(a,sizeof(VkObjectTableCreateInfoNVX));
  a^:=default_VkObjectTableCreateInfoNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkObjectTableCreateInfoNVX);
begin
  vkremitem(a,sizeof(VkObjectTableCreateInfoNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkObjectTableEntryNVX);
begin
  a:=vkgetmem(sizeof(VkObjectTableEntryNVX));
  a^:=default_VkObjectTableEntryNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkObjectTableEntryNVX);
begin
  vkfreemem(a,sizeof(VkObjectTableEntryNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkObjectTableEntryNVX);
begin
  vkadditem(a,sizeof(VkObjectTableEntryNVX));
  a^:=default_VkObjectTableEntryNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkObjectTableEntryNVX);
begin
  vkremitem(a,sizeof(VkObjectTableEntryNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkObjectTablePipelineEntryNVX);
begin
  a:=vkgetmem(sizeof(VkObjectTablePipelineEntryNVX));
  a^:=default_VkObjectTablePipelineEntryNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkObjectTablePipelineEntryNVX);
begin
  vkfreemem(a,sizeof(VkObjectTablePipelineEntryNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkObjectTablePipelineEntryNVX);
begin
  vkadditem(a,sizeof(VkObjectTablePipelineEntryNVX));
  a^:=default_VkObjectTablePipelineEntryNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkObjectTablePipelineEntryNVX);
begin
  vkremitem(a,sizeof(VkObjectTablePipelineEntryNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkObjectTableDescriptorSetEntryNVX);
begin
  a:=vkgetmem(sizeof(VkObjectTableDescriptorSetEntryNVX));
  a^:=default_VkObjectTableDescriptorSetEntryNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkObjectTableDescriptorSetEntryNVX);
begin
  vkfreemem(a,sizeof(VkObjectTableDescriptorSetEntryNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkObjectTableDescriptorSetEntryNVX);
begin
  vkadditem(a,sizeof(VkObjectTableDescriptorSetEntryNVX));
  a^:=default_VkObjectTableDescriptorSetEntryNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkObjectTableDescriptorSetEntryNVX);
begin
  vkremitem(a,sizeof(VkObjectTableDescriptorSetEntryNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkObjectTableVertexBufferEntryNVX);
begin
  a:=vkgetmem(sizeof(VkObjectTableVertexBufferEntryNVX));
  a^:=default_VkObjectTableVertexBufferEntryNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkObjectTableVertexBufferEntryNVX);
begin
  vkfreemem(a,sizeof(VkObjectTableVertexBufferEntryNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkObjectTableVertexBufferEntryNVX);
begin
  vkadditem(a,sizeof(VkObjectTableVertexBufferEntryNVX));
  a^:=default_VkObjectTableVertexBufferEntryNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkObjectTableVertexBufferEntryNVX);
begin
  vkremitem(a,sizeof(VkObjectTableVertexBufferEntryNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkObjectTableIndexBufferEntryNVX);
begin
  a:=vkgetmem(sizeof(VkObjectTableIndexBufferEntryNVX));
  a^:=default_VkObjectTableIndexBufferEntryNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkObjectTableIndexBufferEntryNVX);
begin
  vkfreemem(a,sizeof(VkObjectTableIndexBufferEntryNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkObjectTableIndexBufferEntryNVX);
begin
  vkadditem(a,sizeof(VkObjectTableIndexBufferEntryNVX));
  a^:=default_VkObjectTableIndexBufferEntryNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkObjectTableIndexBufferEntryNVX);
begin
  vkremitem(a,sizeof(VkObjectTableIndexBufferEntryNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkObjectTablePushConstantEntryNVX);
begin
  a:=vkgetmem(sizeof(VkObjectTablePushConstantEntryNVX));
  a^:=default_VkObjectTablePushConstantEntryNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkObjectTablePushConstantEntryNVX);
begin
  vkfreemem(a,sizeof(VkObjectTablePushConstantEntryNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkObjectTablePushConstantEntryNVX);
begin
  vkadditem(a,sizeof(VkObjectTablePushConstantEntryNVX));
  a^:=default_VkObjectTablePushConstantEntryNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkObjectTablePushConstantEntryNVX);
begin
  vkremitem(a,sizeof(VkObjectTablePushConstantEntryNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceFeatures2);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceFeatures2));
  a^:=default_VkPhysicalDeviceFeatures2;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceFeatures2);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceFeatures2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceFeatures2);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceFeatures2));
  a^:=default_VkPhysicalDeviceFeatures2;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceFeatures2);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceFeatures2));
end;

procedure T_vulkan_auto.vknew(out a:PVkFormatProperties2);
begin
  a:=vkgetmem(sizeof(VkFormatProperties2));
  FillByte(a^,sizeof(VkFormatProperties2),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkFormatProperties2);
begin
  vkfreemem(a,sizeof(VkFormatProperties2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkFormatProperties2);
begin
  vkadditem(a,sizeof(VkFormatProperties2));
  FillByte(a^,sizeof(VkFormatProperties2),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkFormatProperties2);
begin
  vkremitem(a,sizeof(VkFormatProperties2));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageFormatProperties2);
begin
  a:=vkgetmem(sizeof(VkImageFormatProperties2));
  FillByte(a^,sizeof(VkImageFormatProperties2),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageFormatProperties2);
begin
  vkfreemem(a,sizeof(VkImageFormatProperties2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageFormatProperties2);
begin
  vkadditem(a,sizeof(VkImageFormatProperties2));
  FillByte(a^,sizeof(VkImageFormatProperties2),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageFormatProperties2);
begin
  vkremitem(a,sizeof(VkImageFormatProperties2));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceImageFormatInfo2);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceImageFormatInfo2));
  a^:=default_VkPhysicalDeviceImageFormatInfo2;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceImageFormatInfo2);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceImageFormatInfo2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceImageFormatInfo2);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceImageFormatInfo2));
  a^:=default_VkPhysicalDeviceImageFormatInfo2;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceImageFormatInfo2);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceImageFormatInfo2));
end;

procedure T_vulkan_auto.vknew(out a:PVkQueueFamilyProperties2);
begin
  a:=vkgetmem(sizeof(VkQueueFamilyProperties2));
  FillByte(a^,sizeof(VkQueueFamilyProperties2),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkQueueFamilyProperties2);
begin
  vkfreemem(a,sizeof(VkQueueFamilyProperties2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkQueueFamilyProperties2);
begin
  vkadditem(a,sizeof(VkQueueFamilyProperties2));
  FillByte(a^,sizeof(VkQueueFamilyProperties2),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkQueueFamilyProperties2);
begin
  vkremitem(a,sizeof(VkQueueFamilyProperties2));
end;

procedure T_vulkan_auto.vknew(out a:PVkSparseImageFormatProperties2);
begin
  a:=vkgetmem(sizeof(VkSparseImageFormatProperties2));
  FillByte(a^,sizeof(VkSparseImageFormatProperties2),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSparseImageFormatProperties2);
begin
  vkfreemem(a,sizeof(VkSparseImageFormatProperties2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSparseImageFormatProperties2);
begin
  vkadditem(a,sizeof(VkSparseImageFormatProperties2));
  FillByte(a^,sizeof(VkSparseImageFormatProperties2),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSparseImageFormatProperties2);
begin
  vkremitem(a,sizeof(VkSparseImageFormatProperties2));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceSparseImageFormatInfo2);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceSparseImageFormatInfo2));
  a^:=default_VkPhysicalDeviceSparseImageFormatInfo2;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceSparseImageFormatInfo2);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceSparseImageFormatInfo2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceSparseImageFormatInfo2);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceSparseImageFormatInfo2));
  a^:=default_VkPhysicalDeviceSparseImageFormatInfo2;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceSparseImageFormatInfo2);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceSparseImageFormatInfo2));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDevicePushDescriptorPropertiesKHR);
begin
  a:=vkgetmem(sizeof(VkPhysicalDevicePushDescriptorPropertiesKHR));
  FillByte(a^,sizeof(VkPhysicalDevicePushDescriptorPropertiesKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDevicePushDescriptorPropertiesKHR);
begin
  vkfreemem(a,sizeof(VkPhysicalDevicePushDescriptorPropertiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDevicePushDescriptorPropertiesKHR);
begin
  vkadditem(a,sizeof(VkPhysicalDevicePushDescriptorPropertiesKHR));
  FillByte(a^,sizeof(VkPhysicalDevicePushDescriptorPropertiesKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDevicePushDescriptorPropertiesKHR);
begin
  vkremitem(a,sizeof(VkPhysicalDevicePushDescriptorPropertiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkConformanceVersion);
begin
  a:=vkgetmem(sizeof(VkConformanceVersion));
  a^:=default_VkConformanceVersion;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkConformanceVersion);
begin
  vkfreemem(a,sizeof(VkConformanceVersion));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkConformanceVersion);
begin
  vkadditem(a,sizeof(VkConformanceVersion));
  a^:=default_VkConformanceVersion;
end;
procedure T_vulkan_auto.vkrem(var a:PVkConformanceVersion);
begin
  vkremitem(a,sizeof(VkConformanceVersion));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceDriverProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceDriverProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceDriverProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceDriverProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceDriverProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceDriverProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceDriverProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceDriverProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceDriverProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceDriverProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkRectLayerKHR);
begin
  a:=vkgetmem(sizeof(VkRectLayerKHR));
  a^:=default_VkRectLayerKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRectLayerKHR);
begin
  vkfreemem(a,sizeof(VkRectLayerKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRectLayerKHR);
begin
  vkadditem(a,sizeof(VkRectLayerKHR));
  a^:=default_VkRectLayerKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkRectLayerKHR);
begin
  vkremitem(a,sizeof(VkRectLayerKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceVariablePointersFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceVariablePointersFeatures));
  a^:=default_VkPhysicalDeviceVariablePointersFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceVariablePointersFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceVariablePointersFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceVariablePointersFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceVariablePointersFeatures));
  a^:=default_VkPhysicalDeviceVariablePointersFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceVariablePointersFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceVariablePointersFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkExternalMemoryProperties);
begin
  a:=vkgetmem(sizeof(VkExternalMemoryProperties));
  FillByte(a^,sizeof(VkExternalMemoryProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExternalMemoryProperties);
begin
  vkfreemem(a,sizeof(VkExternalMemoryProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExternalMemoryProperties);
begin
  vkadditem(a,sizeof(VkExternalMemoryProperties));
  FillByte(a^,sizeof(VkExternalMemoryProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkExternalMemoryProperties);
begin
  vkremitem(a,sizeof(VkExternalMemoryProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceExternalImageFormatInfo);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceExternalImageFormatInfo));
  a^:=default_VkPhysicalDeviceExternalImageFormatInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceExternalImageFormatInfo);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceExternalImageFormatInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceExternalImageFormatInfo);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceExternalImageFormatInfo));
  a^:=default_VkPhysicalDeviceExternalImageFormatInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceExternalImageFormatInfo);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceExternalImageFormatInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkExternalImageFormatProperties);
begin
  a:=vkgetmem(sizeof(VkExternalImageFormatProperties));
  FillByte(a^,sizeof(VkExternalImageFormatProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExternalImageFormatProperties);
begin
  vkfreemem(a,sizeof(VkExternalImageFormatProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExternalImageFormatProperties);
begin
  vkadditem(a,sizeof(VkExternalImageFormatProperties));
  FillByte(a^,sizeof(VkExternalImageFormatProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkExternalImageFormatProperties);
begin
  vkremitem(a,sizeof(VkExternalImageFormatProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceExternalBufferInfo);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceExternalBufferInfo));
  a^:=default_VkPhysicalDeviceExternalBufferInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceExternalBufferInfo);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceExternalBufferInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceExternalBufferInfo);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceExternalBufferInfo));
  a^:=default_VkPhysicalDeviceExternalBufferInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceExternalBufferInfo);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceExternalBufferInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkExternalBufferProperties);
begin
  a:=vkgetmem(sizeof(VkExternalBufferProperties));
  FillByte(a^,sizeof(VkExternalBufferProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExternalBufferProperties);
begin
  vkfreemem(a,sizeof(VkExternalBufferProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExternalBufferProperties);
begin
  vkadditem(a,sizeof(VkExternalBufferProperties));
  FillByte(a^,sizeof(VkExternalBufferProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkExternalBufferProperties);
begin
  vkremitem(a,sizeof(VkExternalBufferProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceIDProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceIDProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceIDProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceIDProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceIDProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceIDProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceIDProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceIDProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceIDProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceIDProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkExternalMemoryImageCreateInfo);
begin
  a:=vkgetmem(sizeof(VkExternalMemoryImageCreateInfo));
  a^:=default_VkExternalMemoryImageCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExternalMemoryImageCreateInfo);
begin
  vkfreemem(a,sizeof(VkExternalMemoryImageCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExternalMemoryImageCreateInfo);
begin
  vkadditem(a,sizeof(VkExternalMemoryImageCreateInfo));
  a^:=default_VkExternalMemoryImageCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExternalMemoryImageCreateInfo);
begin
  vkremitem(a,sizeof(VkExternalMemoryImageCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkExternalMemoryBufferCreateInfo);
begin
  a:=vkgetmem(sizeof(VkExternalMemoryBufferCreateInfo));
  a^:=default_VkExternalMemoryBufferCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExternalMemoryBufferCreateInfo);
begin
  vkfreemem(a,sizeof(VkExternalMemoryBufferCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExternalMemoryBufferCreateInfo);
begin
  vkadditem(a,sizeof(VkExternalMemoryBufferCreateInfo));
  a^:=default_VkExternalMemoryBufferCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExternalMemoryBufferCreateInfo);
begin
  vkremitem(a,sizeof(VkExternalMemoryBufferCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkExportMemoryAllocateInfo);
begin
  a:=vkgetmem(sizeof(VkExportMemoryAllocateInfo));
  a^:=default_VkExportMemoryAllocateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExportMemoryAllocateInfo);
begin
  vkfreemem(a,sizeof(VkExportMemoryAllocateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExportMemoryAllocateInfo);
begin
  vkadditem(a,sizeof(VkExportMemoryAllocateInfo));
  a^:=default_VkExportMemoryAllocateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExportMemoryAllocateInfo);
begin
  vkremitem(a,sizeof(VkExportMemoryAllocateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkImportMemoryWin32HandleInfoKHR);
begin
  a:=vkgetmem(sizeof(VkImportMemoryWin32HandleInfoKHR));
  a^:=default_VkImportMemoryWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImportMemoryWin32HandleInfoKHR);
begin
  vkfreemem(a,sizeof(VkImportMemoryWin32HandleInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImportMemoryWin32HandleInfoKHR);
begin
  vkadditem(a,sizeof(VkImportMemoryWin32HandleInfoKHR));
  a^:=default_VkImportMemoryWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImportMemoryWin32HandleInfoKHR);
begin
  vkremitem(a,sizeof(VkImportMemoryWin32HandleInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkExportMemoryWin32HandleInfoKHR);
begin
  a:=vkgetmem(sizeof(VkExportMemoryWin32HandleInfoKHR));
  a^:=default_VkExportMemoryWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExportMemoryWin32HandleInfoKHR);
begin
  vkfreemem(a,sizeof(VkExportMemoryWin32HandleInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExportMemoryWin32HandleInfoKHR);
begin
  vkadditem(a,sizeof(VkExportMemoryWin32HandleInfoKHR));
  a^:=default_VkExportMemoryWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExportMemoryWin32HandleInfoKHR);
begin
  vkremitem(a,sizeof(VkExportMemoryWin32HandleInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryWin32HandlePropertiesKHR);
begin
  a:=vkgetmem(sizeof(VkMemoryWin32HandlePropertiesKHR));
  FillByte(a^,sizeof(VkMemoryWin32HandlePropertiesKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryWin32HandlePropertiesKHR);
begin
  vkfreemem(a,sizeof(VkMemoryWin32HandlePropertiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryWin32HandlePropertiesKHR);
begin
  vkadditem(a,sizeof(VkMemoryWin32HandlePropertiesKHR));
  FillByte(a^,sizeof(VkMemoryWin32HandlePropertiesKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryWin32HandlePropertiesKHR);
begin
  vkremitem(a,sizeof(VkMemoryWin32HandlePropertiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryGetWin32HandleInfoKHR);
begin
  a:=vkgetmem(sizeof(VkMemoryGetWin32HandleInfoKHR));
  a^:=default_VkMemoryGetWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryGetWin32HandleInfoKHR);
begin
  vkfreemem(a,sizeof(VkMemoryGetWin32HandleInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryGetWin32HandleInfoKHR);
begin
  vkadditem(a,sizeof(VkMemoryGetWin32HandleInfoKHR));
  a^:=default_VkMemoryGetWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryGetWin32HandleInfoKHR);
begin
  vkremitem(a,sizeof(VkMemoryGetWin32HandleInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkImportMemoryFdInfoKHR);
begin
  a:=vkgetmem(sizeof(VkImportMemoryFdInfoKHR));
  a^:=default_VkImportMemoryFdInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImportMemoryFdInfoKHR);
begin
  vkfreemem(a,sizeof(VkImportMemoryFdInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImportMemoryFdInfoKHR);
begin
  vkadditem(a,sizeof(VkImportMemoryFdInfoKHR));
  a^:=default_VkImportMemoryFdInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImportMemoryFdInfoKHR);
begin
  vkremitem(a,sizeof(VkImportMemoryFdInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryFdPropertiesKHR);
begin
  a:=vkgetmem(sizeof(VkMemoryFdPropertiesKHR));
  FillByte(a^,sizeof(VkMemoryFdPropertiesKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryFdPropertiesKHR);
begin
  vkfreemem(a,sizeof(VkMemoryFdPropertiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryFdPropertiesKHR);
begin
  vkadditem(a,sizeof(VkMemoryFdPropertiesKHR));
  FillByte(a^,sizeof(VkMemoryFdPropertiesKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryFdPropertiesKHR);
begin
  vkremitem(a,sizeof(VkMemoryFdPropertiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryGetFdInfoKHR);
begin
  a:=vkgetmem(sizeof(VkMemoryGetFdInfoKHR));
  a^:=default_VkMemoryGetFdInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryGetFdInfoKHR);
begin
  vkfreemem(a,sizeof(VkMemoryGetFdInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryGetFdInfoKHR);
begin
  vkadditem(a,sizeof(VkMemoryGetFdInfoKHR));
  a^:=default_VkMemoryGetFdInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryGetFdInfoKHR);
begin
  vkremitem(a,sizeof(VkMemoryGetFdInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkWin32KeyedMutexAcquireReleaseInfoKHR);
begin
  a:=vkgetmem(sizeof(VkWin32KeyedMutexAcquireReleaseInfoKHR));
  a^:=default_VkWin32KeyedMutexAcquireReleaseInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkWin32KeyedMutexAcquireReleaseInfoKHR);
begin
  vkfreemem(a,sizeof(VkWin32KeyedMutexAcquireReleaseInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkWin32KeyedMutexAcquireReleaseInfoKHR);
begin
  vkadditem(a,sizeof(VkWin32KeyedMutexAcquireReleaseInfoKHR));
  a^:=default_VkWin32KeyedMutexAcquireReleaseInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkWin32KeyedMutexAcquireReleaseInfoKHR);
begin
  vkremitem(a,sizeof(VkWin32KeyedMutexAcquireReleaseInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceExternalSemaphoreInfo);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceExternalSemaphoreInfo));
  a^:=default_VkPhysicalDeviceExternalSemaphoreInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceExternalSemaphoreInfo);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceExternalSemaphoreInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceExternalSemaphoreInfo);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceExternalSemaphoreInfo));
  a^:=default_VkPhysicalDeviceExternalSemaphoreInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceExternalSemaphoreInfo);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceExternalSemaphoreInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkExternalSemaphoreProperties);
begin
  a:=vkgetmem(sizeof(VkExternalSemaphoreProperties));
  FillByte(a^,sizeof(VkExternalSemaphoreProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExternalSemaphoreProperties);
begin
  vkfreemem(a,sizeof(VkExternalSemaphoreProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExternalSemaphoreProperties);
begin
  vkadditem(a,sizeof(VkExternalSemaphoreProperties));
  FillByte(a^,sizeof(VkExternalSemaphoreProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkExternalSemaphoreProperties);
begin
  vkremitem(a,sizeof(VkExternalSemaphoreProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkExportSemaphoreCreateInfo);
begin
  a:=vkgetmem(sizeof(VkExportSemaphoreCreateInfo));
  a^:=default_VkExportSemaphoreCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExportSemaphoreCreateInfo);
begin
  vkfreemem(a,sizeof(VkExportSemaphoreCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExportSemaphoreCreateInfo);
begin
  vkadditem(a,sizeof(VkExportSemaphoreCreateInfo));
  a^:=default_VkExportSemaphoreCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExportSemaphoreCreateInfo);
begin
  vkremitem(a,sizeof(VkExportSemaphoreCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkImportSemaphoreWin32HandleInfoKHR);
begin
  a:=vkgetmem(sizeof(VkImportSemaphoreWin32HandleInfoKHR));
  a^:=default_VkImportSemaphoreWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImportSemaphoreWin32HandleInfoKHR);
begin
  vkfreemem(a,sizeof(VkImportSemaphoreWin32HandleInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImportSemaphoreWin32HandleInfoKHR);
begin
  vkadditem(a,sizeof(VkImportSemaphoreWin32HandleInfoKHR));
  a^:=default_VkImportSemaphoreWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImportSemaphoreWin32HandleInfoKHR);
begin
  vkremitem(a,sizeof(VkImportSemaphoreWin32HandleInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkExportSemaphoreWin32HandleInfoKHR);
begin
  a:=vkgetmem(sizeof(VkExportSemaphoreWin32HandleInfoKHR));
  a^:=default_VkExportSemaphoreWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExportSemaphoreWin32HandleInfoKHR);
begin
  vkfreemem(a,sizeof(VkExportSemaphoreWin32HandleInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExportSemaphoreWin32HandleInfoKHR);
begin
  vkadditem(a,sizeof(VkExportSemaphoreWin32HandleInfoKHR));
  a^:=default_VkExportSemaphoreWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExportSemaphoreWin32HandleInfoKHR);
begin
  vkremitem(a,sizeof(VkExportSemaphoreWin32HandleInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkD3D12FenceSubmitInfoKHR);
begin
  a:=vkgetmem(sizeof(VkD3D12FenceSubmitInfoKHR));
  a^:=default_VkD3D12FenceSubmitInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkD3D12FenceSubmitInfoKHR);
begin
  vkfreemem(a,sizeof(VkD3D12FenceSubmitInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkD3D12FenceSubmitInfoKHR);
begin
  vkadditem(a,sizeof(VkD3D12FenceSubmitInfoKHR));
  a^:=default_VkD3D12FenceSubmitInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkD3D12FenceSubmitInfoKHR);
begin
  vkremitem(a,sizeof(VkD3D12FenceSubmitInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkSemaphoreGetWin32HandleInfoKHR);
begin
  a:=vkgetmem(sizeof(VkSemaphoreGetWin32HandleInfoKHR));
  a^:=default_VkSemaphoreGetWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSemaphoreGetWin32HandleInfoKHR);
begin
  vkfreemem(a,sizeof(VkSemaphoreGetWin32HandleInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSemaphoreGetWin32HandleInfoKHR);
begin
  vkadditem(a,sizeof(VkSemaphoreGetWin32HandleInfoKHR));
  a^:=default_VkSemaphoreGetWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSemaphoreGetWin32HandleInfoKHR);
begin
  vkremitem(a,sizeof(VkSemaphoreGetWin32HandleInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkImportSemaphoreFdInfoKHR);
begin
  a:=vkgetmem(sizeof(VkImportSemaphoreFdInfoKHR));
  a^:=default_VkImportSemaphoreFdInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImportSemaphoreFdInfoKHR);
begin
  vkfreemem(a,sizeof(VkImportSemaphoreFdInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImportSemaphoreFdInfoKHR);
begin
  vkadditem(a,sizeof(VkImportSemaphoreFdInfoKHR));
  a^:=default_VkImportSemaphoreFdInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImportSemaphoreFdInfoKHR);
begin
  vkremitem(a,sizeof(VkImportSemaphoreFdInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkSemaphoreGetFdInfoKHR);
begin
  a:=vkgetmem(sizeof(VkSemaphoreGetFdInfoKHR));
  a^:=default_VkSemaphoreGetFdInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSemaphoreGetFdInfoKHR);
begin
  vkfreemem(a,sizeof(VkSemaphoreGetFdInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSemaphoreGetFdInfoKHR);
begin
  vkadditem(a,sizeof(VkSemaphoreGetFdInfoKHR));
  a^:=default_VkSemaphoreGetFdInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSemaphoreGetFdInfoKHR);
begin
  vkremitem(a,sizeof(VkSemaphoreGetFdInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceExternalFenceInfo);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceExternalFenceInfo));
  a^:=default_VkPhysicalDeviceExternalFenceInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceExternalFenceInfo);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceExternalFenceInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceExternalFenceInfo);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceExternalFenceInfo));
  a^:=default_VkPhysicalDeviceExternalFenceInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceExternalFenceInfo);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceExternalFenceInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkExternalFenceProperties);
begin
  a:=vkgetmem(sizeof(VkExternalFenceProperties));
  FillByte(a^,sizeof(VkExternalFenceProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExternalFenceProperties);
begin
  vkfreemem(a,sizeof(VkExternalFenceProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExternalFenceProperties);
begin
  vkadditem(a,sizeof(VkExternalFenceProperties));
  FillByte(a^,sizeof(VkExternalFenceProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkExternalFenceProperties);
begin
  vkremitem(a,sizeof(VkExternalFenceProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkExportFenceCreateInfo);
begin
  a:=vkgetmem(sizeof(VkExportFenceCreateInfo));
  a^:=default_VkExportFenceCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExportFenceCreateInfo);
begin
  vkfreemem(a,sizeof(VkExportFenceCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExportFenceCreateInfo);
begin
  vkadditem(a,sizeof(VkExportFenceCreateInfo));
  a^:=default_VkExportFenceCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExportFenceCreateInfo);
begin
  vkremitem(a,sizeof(VkExportFenceCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkImportFenceWin32HandleInfoKHR);
begin
  a:=vkgetmem(sizeof(VkImportFenceWin32HandleInfoKHR));
  a^:=default_VkImportFenceWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImportFenceWin32HandleInfoKHR);
begin
  vkfreemem(a,sizeof(VkImportFenceWin32HandleInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImportFenceWin32HandleInfoKHR);
begin
  vkadditem(a,sizeof(VkImportFenceWin32HandleInfoKHR));
  a^:=default_VkImportFenceWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImportFenceWin32HandleInfoKHR);
begin
  vkremitem(a,sizeof(VkImportFenceWin32HandleInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkExportFenceWin32HandleInfoKHR);
begin
  a:=vkgetmem(sizeof(VkExportFenceWin32HandleInfoKHR));
  a^:=default_VkExportFenceWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExportFenceWin32HandleInfoKHR);
begin
  vkfreemem(a,sizeof(VkExportFenceWin32HandleInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExportFenceWin32HandleInfoKHR);
begin
  vkadditem(a,sizeof(VkExportFenceWin32HandleInfoKHR));
  a^:=default_VkExportFenceWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExportFenceWin32HandleInfoKHR);
begin
  vkremitem(a,sizeof(VkExportFenceWin32HandleInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkFenceGetWin32HandleInfoKHR);
begin
  a:=vkgetmem(sizeof(VkFenceGetWin32HandleInfoKHR));
  a^:=default_VkFenceGetWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkFenceGetWin32HandleInfoKHR);
begin
  vkfreemem(a,sizeof(VkFenceGetWin32HandleInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkFenceGetWin32HandleInfoKHR);
begin
  vkadditem(a,sizeof(VkFenceGetWin32HandleInfoKHR));
  a^:=default_VkFenceGetWin32HandleInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkFenceGetWin32HandleInfoKHR);
begin
  vkremitem(a,sizeof(VkFenceGetWin32HandleInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkImportFenceFdInfoKHR);
begin
  a:=vkgetmem(sizeof(VkImportFenceFdInfoKHR));
  a^:=default_VkImportFenceFdInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImportFenceFdInfoKHR);
begin
  vkfreemem(a,sizeof(VkImportFenceFdInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImportFenceFdInfoKHR);
begin
  vkadditem(a,sizeof(VkImportFenceFdInfoKHR));
  a^:=default_VkImportFenceFdInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImportFenceFdInfoKHR);
begin
  vkremitem(a,sizeof(VkImportFenceFdInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkFenceGetFdInfoKHR);
begin
  a:=vkgetmem(sizeof(VkFenceGetFdInfoKHR));
  a^:=default_VkFenceGetFdInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkFenceGetFdInfoKHR);
begin
  vkfreemem(a,sizeof(VkFenceGetFdInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkFenceGetFdInfoKHR);
begin
  vkadditem(a,sizeof(VkFenceGetFdInfoKHR));
  a^:=default_VkFenceGetFdInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkFenceGetFdInfoKHR);
begin
  vkremitem(a,sizeof(VkFenceGetFdInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceMultiviewFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceMultiviewFeatures));
  a^:=default_VkPhysicalDeviceMultiviewFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceMultiviewFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceMultiviewFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceMultiviewFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceMultiviewFeatures));
  a^:=default_VkPhysicalDeviceMultiviewFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceMultiviewFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceMultiviewFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceMultiviewProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceMultiviewProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceMultiviewProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceMultiviewProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceMultiviewProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceMultiviewProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceMultiviewProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceMultiviewProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceMultiviewProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceMultiviewProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkRenderPassMultiviewCreateInfo);
begin
  a:=vkgetmem(sizeof(VkRenderPassMultiviewCreateInfo));
  a^:=default_VkRenderPassMultiviewCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRenderPassMultiviewCreateInfo);
begin
  vkfreemem(a,sizeof(VkRenderPassMultiviewCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRenderPassMultiviewCreateInfo);
begin
  vkadditem(a,sizeof(VkRenderPassMultiviewCreateInfo));
  a^:=default_VkRenderPassMultiviewCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkRenderPassMultiviewCreateInfo);
begin
  vkremitem(a,sizeof(VkRenderPassMultiviewCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkSurfaceCapabilities2EXT);
begin
  a:=vkgetmem(sizeof(VkSurfaceCapabilities2EXT));
  FillByte(a^,sizeof(VkSurfaceCapabilities2EXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSurfaceCapabilities2EXT);
begin
  vkfreemem(a,sizeof(VkSurfaceCapabilities2EXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSurfaceCapabilities2EXT);
begin
  vkadditem(a,sizeof(VkSurfaceCapabilities2EXT));
  FillByte(a^,sizeof(VkSurfaceCapabilities2EXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSurfaceCapabilities2EXT);
begin
  vkremitem(a,sizeof(VkSurfaceCapabilities2EXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayPowerInfoEXT);
begin
  a:=vkgetmem(sizeof(VkDisplayPowerInfoEXT));
  a^:=default_VkDisplayPowerInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayPowerInfoEXT);
begin
  vkfreemem(a,sizeof(VkDisplayPowerInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayPowerInfoEXT);
begin
  vkadditem(a,sizeof(VkDisplayPowerInfoEXT));
  a^:=default_VkDisplayPowerInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayPowerInfoEXT);
begin
  vkremitem(a,sizeof(VkDisplayPowerInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceEventInfoEXT);
begin
  a:=vkgetmem(sizeof(VkDeviceEventInfoEXT));
  a^:=default_VkDeviceEventInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceEventInfoEXT);
begin
  vkfreemem(a,sizeof(VkDeviceEventInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceEventInfoEXT);
begin
  vkadditem(a,sizeof(VkDeviceEventInfoEXT));
  a^:=default_VkDeviceEventInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceEventInfoEXT);
begin
  vkremitem(a,sizeof(VkDeviceEventInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayEventInfoEXT);
begin
  a:=vkgetmem(sizeof(VkDisplayEventInfoEXT));
  a^:=default_VkDisplayEventInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayEventInfoEXT);
begin
  vkfreemem(a,sizeof(VkDisplayEventInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayEventInfoEXT);
begin
  vkadditem(a,sizeof(VkDisplayEventInfoEXT));
  a^:=default_VkDisplayEventInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayEventInfoEXT);
begin
  vkremitem(a,sizeof(VkDisplayEventInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkSwapchainCounterCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkSwapchainCounterCreateInfoEXT));
  a^:=default_VkSwapchainCounterCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSwapchainCounterCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkSwapchainCounterCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSwapchainCounterCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkSwapchainCounterCreateInfoEXT));
  a^:=default_VkSwapchainCounterCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSwapchainCounterCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkSwapchainCounterCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceGroupProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceGroupProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceGroupProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceGroupProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceGroupProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceGroupProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceGroupProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceGroupProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceGroupProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceGroupProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryAllocateFlagsInfo);
begin
  a:=vkgetmem(sizeof(VkMemoryAllocateFlagsInfo));
  a^:=default_VkMemoryAllocateFlagsInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryAllocateFlagsInfo);
begin
  vkfreemem(a,sizeof(VkMemoryAllocateFlagsInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryAllocateFlagsInfo);
begin
  vkadditem(a,sizeof(VkMemoryAllocateFlagsInfo));
  a^:=default_VkMemoryAllocateFlagsInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryAllocateFlagsInfo);
begin
  vkremitem(a,sizeof(VkMemoryAllocateFlagsInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkBindBufferMemoryInfo);
begin
  a:=vkgetmem(sizeof(VkBindBufferMemoryInfo));
  a^:=default_VkBindBufferMemoryInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBindBufferMemoryInfo);
begin
  vkfreemem(a,sizeof(VkBindBufferMemoryInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBindBufferMemoryInfo);
begin
  vkadditem(a,sizeof(VkBindBufferMemoryInfo));
  a^:=default_VkBindBufferMemoryInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBindBufferMemoryInfo);
begin
  vkremitem(a,sizeof(VkBindBufferMemoryInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkBindBufferMemoryDeviceGroupInfo);
begin
  a:=vkgetmem(sizeof(VkBindBufferMemoryDeviceGroupInfo));
  a^:=default_VkBindBufferMemoryDeviceGroupInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBindBufferMemoryDeviceGroupInfo);
begin
  vkfreemem(a,sizeof(VkBindBufferMemoryDeviceGroupInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBindBufferMemoryDeviceGroupInfo);
begin
  vkadditem(a,sizeof(VkBindBufferMemoryDeviceGroupInfo));
  a^:=default_VkBindBufferMemoryDeviceGroupInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBindBufferMemoryDeviceGroupInfo);
begin
  vkremitem(a,sizeof(VkBindBufferMemoryDeviceGroupInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkBindImageMemoryInfo);
begin
  a:=vkgetmem(sizeof(VkBindImageMemoryInfo));
  a^:=default_VkBindImageMemoryInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBindImageMemoryInfo);
begin
  vkfreemem(a,sizeof(VkBindImageMemoryInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBindImageMemoryInfo);
begin
  vkadditem(a,sizeof(VkBindImageMemoryInfo));
  a^:=default_VkBindImageMemoryInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBindImageMemoryInfo);
begin
  vkremitem(a,sizeof(VkBindImageMemoryInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkBindImageMemoryDeviceGroupInfo);
begin
  a:=vkgetmem(sizeof(VkBindImageMemoryDeviceGroupInfo));
  a^:=default_VkBindImageMemoryDeviceGroupInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBindImageMemoryDeviceGroupInfo);
begin
  vkfreemem(a,sizeof(VkBindImageMemoryDeviceGroupInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBindImageMemoryDeviceGroupInfo);
begin
  vkadditem(a,sizeof(VkBindImageMemoryDeviceGroupInfo));
  a^:=default_VkBindImageMemoryDeviceGroupInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBindImageMemoryDeviceGroupInfo);
begin
  vkremitem(a,sizeof(VkBindImageMemoryDeviceGroupInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceGroupRenderPassBeginInfo);
begin
  a:=vkgetmem(sizeof(VkDeviceGroupRenderPassBeginInfo));
  a^:=default_VkDeviceGroupRenderPassBeginInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceGroupRenderPassBeginInfo);
begin
  vkfreemem(a,sizeof(VkDeviceGroupRenderPassBeginInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceGroupRenderPassBeginInfo);
begin
  vkadditem(a,sizeof(VkDeviceGroupRenderPassBeginInfo));
  a^:=default_VkDeviceGroupRenderPassBeginInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceGroupRenderPassBeginInfo);
begin
  vkremitem(a,sizeof(VkDeviceGroupRenderPassBeginInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceGroupCommandBufferBeginInfo);
begin
  a:=vkgetmem(sizeof(VkDeviceGroupCommandBufferBeginInfo));
  a^:=default_VkDeviceGroupCommandBufferBeginInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceGroupCommandBufferBeginInfo);
begin
  vkfreemem(a,sizeof(VkDeviceGroupCommandBufferBeginInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceGroupCommandBufferBeginInfo);
begin
  vkadditem(a,sizeof(VkDeviceGroupCommandBufferBeginInfo));
  a^:=default_VkDeviceGroupCommandBufferBeginInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceGroupCommandBufferBeginInfo);
begin
  vkremitem(a,sizeof(VkDeviceGroupCommandBufferBeginInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceGroupSubmitInfo);
begin
  a:=vkgetmem(sizeof(VkDeviceGroupSubmitInfo));
  a^:=default_VkDeviceGroupSubmitInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceGroupSubmitInfo);
begin
  vkfreemem(a,sizeof(VkDeviceGroupSubmitInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceGroupSubmitInfo);
begin
  vkadditem(a,sizeof(VkDeviceGroupSubmitInfo));
  a^:=default_VkDeviceGroupSubmitInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceGroupSubmitInfo);
begin
  vkremitem(a,sizeof(VkDeviceGroupSubmitInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceGroupBindSparseInfo);
begin
  a:=vkgetmem(sizeof(VkDeviceGroupBindSparseInfo));
  a^:=default_VkDeviceGroupBindSparseInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceGroupBindSparseInfo);
begin
  vkfreemem(a,sizeof(VkDeviceGroupBindSparseInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceGroupBindSparseInfo);
begin
  vkadditem(a,sizeof(VkDeviceGroupBindSparseInfo));
  a^:=default_VkDeviceGroupBindSparseInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceGroupBindSparseInfo);
begin
  vkremitem(a,sizeof(VkDeviceGroupBindSparseInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceGroupPresentCapabilitiesKHR);
begin
  a:=vkgetmem(sizeof(VkDeviceGroupPresentCapabilitiesKHR));
  FillByte(a^,sizeof(VkDeviceGroupPresentCapabilitiesKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceGroupPresentCapabilitiesKHR);
begin
  vkfreemem(a,sizeof(VkDeviceGroupPresentCapabilitiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceGroupPresentCapabilitiesKHR);
begin
  vkadditem(a,sizeof(VkDeviceGroupPresentCapabilitiesKHR));
  FillByte(a^,sizeof(VkDeviceGroupPresentCapabilitiesKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceGroupPresentCapabilitiesKHR);
begin
  vkremitem(a,sizeof(VkDeviceGroupPresentCapabilitiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageSwapchainCreateInfoKHR);
begin
  a:=vkgetmem(sizeof(VkImageSwapchainCreateInfoKHR));
  a^:=default_VkImageSwapchainCreateInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageSwapchainCreateInfoKHR);
begin
  vkfreemem(a,sizeof(VkImageSwapchainCreateInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageSwapchainCreateInfoKHR);
begin
  vkadditem(a,sizeof(VkImageSwapchainCreateInfoKHR));
  a^:=default_VkImageSwapchainCreateInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageSwapchainCreateInfoKHR);
begin
  vkremitem(a,sizeof(VkImageSwapchainCreateInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkBindImageMemorySwapchainInfoKHR);
begin
  a:=vkgetmem(sizeof(VkBindImageMemorySwapchainInfoKHR));
  a^:=default_VkBindImageMemorySwapchainInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBindImageMemorySwapchainInfoKHR);
begin
  vkfreemem(a,sizeof(VkBindImageMemorySwapchainInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBindImageMemorySwapchainInfoKHR);
begin
  vkadditem(a,sizeof(VkBindImageMemorySwapchainInfoKHR));
  a^:=default_VkBindImageMemorySwapchainInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBindImageMemorySwapchainInfoKHR);
begin
  vkremitem(a,sizeof(VkBindImageMemorySwapchainInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkAcquireNextImageInfoKHR);
begin
  a:=vkgetmem(sizeof(VkAcquireNextImageInfoKHR));
  a^:=default_VkAcquireNextImageInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAcquireNextImageInfoKHR);
begin
  vkfreemem(a,sizeof(VkAcquireNextImageInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAcquireNextImageInfoKHR);
begin
  vkadditem(a,sizeof(VkAcquireNextImageInfoKHR));
  a^:=default_VkAcquireNextImageInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAcquireNextImageInfoKHR);
begin
  vkremitem(a,sizeof(VkAcquireNextImageInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceGroupPresentInfoKHR);
begin
  a:=vkgetmem(sizeof(VkDeviceGroupPresentInfoKHR));
  a^:=default_VkDeviceGroupPresentInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceGroupPresentInfoKHR);
begin
  vkfreemem(a,sizeof(VkDeviceGroupPresentInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceGroupPresentInfoKHR);
begin
  vkadditem(a,sizeof(VkDeviceGroupPresentInfoKHR));
  a^:=default_VkDeviceGroupPresentInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceGroupPresentInfoKHR);
begin
  vkremitem(a,sizeof(VkDeviceGroupPresentInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceGroupDeviceCreateInfo);
begin
  a:=vkgetmem(sizeof(VkDeviceGroupDeviceCreateInfo));
  a^:=default_VkDeviceGroupDeviceCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceGroupDeviceCreateInfo);
begin
  vkfreemem(a,sizeof(VkDeviceGroupDeviceCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceGroupDeviceCreateInfo);
begin
  vkadditem(a,sizeof(VkDeviceGroupDeviceCreateInfo));
  a^:=default_VkDeviceGroupDeviceCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceGroupDeviceCreateInfo);
begin
  vkremitem(a,sizeof(VkDeviceGroupDeviceCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceGroupSwapchainCreateInfoKHR);
begin
  a:=vkgetmem(sizeof(VkDeviceGroupSwapchainCreateInfoKHR));
  a^:=default_VkDeviceGroupSwapchainCreateInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceGroupSwapchainCreateInfoKHR);
begin
  vkfreemem(a,sizeof(VkDeviceGroupSwapchainCreateInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceGroupSwapchainCreateInfoKHR);
begin
  vkadditem(a,sizeof(VkDeviceGroupSwapchainCreateInfoKHR));
  a^:=default_VkDeviceGroupSwapchainCreateInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceGroupSwapchainCreateInfoKHR);
begin
  vkremitem(a,sizeof(VkDeviceGroupSwapchainCreateInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorUpdateTemplateEntry);
begin
  a:=vkgetmem(sizeof(VkDescriptorUpdateTemplateEntry));
  a^:=default_VkDescriptorUpdateTemplateEntry;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorUpdateTemplateEntry);
begin
  vkfreemem(a,sizeof(VkDescriptorUpdateTemplateEntry));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorUpdateTemplateEntry);
begin
  vkadditem(a,sizeof(VkDescriptorUpdateTemplateEntry));
  a^:=default_VkDescriptorUpdateTemplateEntry;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorUpdateTemplateEntry);
begin
  vkremitem(a,sizeof(VkDescriptorUpdateTemplateEntry));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorUpdateTemplateCreateInfo);
begin
  a:=vkgetmem(sizeof(VkDescriptorUpdateTemplateCreateInfo));
  a^:=default_VkDescriptorUpdateTemplateCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorUpdateTemplateCreateInfo);
begin
  vkfreemem(a,sizeof(VkDescriptorUpdateTemplateCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorUpdateTemplateCreateInfo);
begin
  vkadditem(a,sizeof(VkDescriptorUpdateTemplateCreateInfo));
  a^:=default_VkDescriptorUpdateTemplateCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorUpdateTemplateCreateInfo);
begin
  vkremitem(a,sizeof(VkDescriptorUpdateTemplateCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkXYColorEXT);
begin
  a:=vkgetmem(sizeof(VkXYColorEXT));
  a^:=default_VkXYColorEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkXYColorEXT);
begin
  vkfreemem(a,sizeof(VkXYColorEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkXYColorEXT);
begin
  vkadditem(a,sizeof(VkXYColorEXT));
  a^:=default_VkXYColorEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkXYColorEXT);
begin
  vkremitem(a,sizeof(VkXYColorEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkHdrMetadataEXT);
begin
  a:=vkgetmem(sizeof(VkHdrMetadataEXT));
  a^:=default_VkHdrMetadataEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkHdrMetadataEXT);
begin
  vkfreemem(a,sizeof(VkHdrMetadataEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkHdrMetadataEXT);
begin
  vkadditem(a,sizeof(VkHdrMetadataEXT));
  a^:=default_VkHdrMetadataEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkHdrMetadataEXT);
begin
  vkremitem(a,sizeof(VkHdrMetadataEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayNativeHdrSurfaceCapabilitiesAMD);
begin
  a:=vkgetmem(sizeof(VkDisplayNativeHdrSurfaceCapabilitiesAMD));
  FillByte(a^,sizeof(VkDisplayNativeHdrSurfaceCapabilitiesAMD),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayNativeHdrSurfaceCapabilitiesAMD);
begin
  vkfreemem(a,sizeof(VkDisplayNativeHdrSurfaceCapabilitiesAMD));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayNativeHdrSurfaceCapabilitiesAMD);
begin
  vkadditem(a,sizeof(VkDisplayNativeHdrSurfaceCapabilitiesAMD));
  FillByte(a^,sizeof(VkDisplayNativeHdrSurfaceCapabilitiesAMD),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayNativeHdrSurfaceCapabilitiesAMD);
begin
  vkremitem(a,sizeof(VkDisplayNativeHdrSurfaceCapabilitiesAMD));
end;

procedure T_vulkan_auto.vknew(out a:PVkSwapchainDisplayNativeHdrCreateInfoAMD);
begin
  a:=vkgetmem(sizeof(VkSwapchainDisplayNativeHdrCreateInfoAMD));
  a^:=default_VkSwapchainDisplayNativeHdrCreateInfoAMD;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSwapchainDisplayNativeHdrCreateInfoAMD);
begin
  vkfreemem(a,sizeof(VkSwapchainDisplayNativeHdrCreateInfoAMD));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSwapchainDisplayNativeHdrCreateInfoAMD);
begin
  vkadditem(a,sizeof(VkSwapchainDisplayNativeHdrCreateInfoAMD));
  a^:=default_VkSwapchainDisplayNativeHdrCreateInfoAMD;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSwapchainDisplayNativeHdrCreateInfoAMD);
begin
  vkremitem(a,sizeof(VkSwapchainDisplayNativeHdrCreateInfoAMD));
end;

procedure T_vulkan_auto.vknew(out a:PVkRefreshCycleDurationGOOGLE);
begin
  a:=vkgetmem(sizeof(VkRefreshCycleDurationGOOGLE));
  FillByte(a^,sizeof(VkRefreshCycleDurationGOOGLE),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRefreshCycleDurationGOOGLE);
begin
  vkfreemem(a,sizeof(VkRefreshCycleDurationGOOGLE));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRefreshCycleDurationGOOGLE);
begin
  vkadditem(a,sizeof(VkRefreshCycleDurationGOOGLE));
  FillByte(a^,sizeof(VkRefreshCycleDurationGOOGLE),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkRefreshCycleDurationGOOGLE);
begin
  vkremitem(a,sizeof(VkRefreshCycleDurationGOOGLE));
end;

procedure T_vulkan_auto.vknew(out a:PVkPastPresentationTimingGOOGLE);
begin
  a:=vkgetmem(sizeof(VkPastPresentationTimingGOOGLE));
  FillByte(a^,sizeof(VkPastPresentationTimingGOOGLE),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPastPresentationTimingGOOGLE);
begin
  vkfreemem(a,sizeof(VkPastPresentationTimingGOOGLE));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPastPresentationTimingGOOGLE);
begin
  vkadditem(a,sizeof(VkPastPresentationTimingGOOGLE));
  FillByte(a^,sizeof(VkPastPresentationTimingGOOGLE),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPastPresentationTimingGOOGLE);
begin
  vkremitem(a,sizeof(VkPastPresentationTimingGOOGLE));
end;

procedure T_vulkan_auto.vknew(out a:PVkPresentTimeGOOGLE);
begin
  a:=vkgetmem(sizeof(VkPresentTimeGOOGLE));
  a^:=default_VkPresentTimeGOOGLE;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPresentTimeGOOGLE);
begin
  vkfreemem(a,sizeof(VkPresentTimeGOOGLE));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPresentTimeGOOGLE);
begin
  vkadditem(a,sizeof(VkPresentTimeGOOGLE));
  a^:=default_VkPresentTimeGOOGLE;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPresentTimeGOOGLE);
begin
  vkremitem(a,sizeof(VkPresentTimeGOOGLE));
end;

procedure T_vulkan_auto.vknew(out a:PVkIOSSurfaceCreateInfoMVK);
begin
  a:=vkgetmem(sizeof(VkIOSSurfaceCreateInfoMVK));
  a^:=default_VkIOSSurfaceCreateInfoMVK;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkIOSSurfaceCreateInfoMVK);
begin
  vkfreemem(a,sizeof(VkIOSSurfaceCreateInfoMVK));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkIOSSurfaceCreateInfoMVK);
begin
  vkadditem(a,sizeof(VkIOSSurfaceCreateInfoMVK));
  a^:=default_VkIOSSurfaceCreateInfoMVK;
end;
procedure T_vulkan_auto.vkrem(var a:PVkIOSSurfaceCreateInfoMVK);
begin
  vkremitem(a,sizeof(VkIOSSurfaceCreateInfoMVK));
end;

procedure T_vulkan_auto.vknew(out a:PVkMacOSSurfaceCreateInfoMVK);
begin
  a:=vkgetmem(sizeof(VkMacOSSurfaceCreateInfoMVK));
  a^:=default_VkMacOSSurfaceCreateInfoMVK;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMacOSSurfaceCreateInfoMVK);
begin
  vkfreemem(a,sizeof(VkMacOSSurfaceCreateInfoMVK));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMacOSSurfaceCreateInfoMVK);
begin
  vkadditem(a,sizeof(VkMacOSSurfaceCreateInfoMVK));
  a^:=default_VkMacOSSurfaceCreateInfoMVK;
end;
procedure T_vulkan_auto.vkrem(var a:PVkMacOSSurfaceCreateInfoMVK);
begin
  vkremitem(a,sizeof(VkMacOSSurfaceCreateInfoMVK));
end;

procedure T_vulkan_auto.vknew(out a:PVkMetalSurfaceCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkMetalSurfaceCreateInfoEXT));
  a^:=default_VkMetalSurfaceCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMetalSurfaceCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkMetalSurfaceCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMetalSurfaceCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkMetalSurfaceCreateInfoEXT));
  a^:=default_VkMetalSurfaceCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkMetalSurfaceCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkMetalSurfaceCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkViewportWScalingNV);
begin
  a:=vkgetmem(sizeof(VkViewportWScalingNV));
  a^:=default_VkViewportWScalingNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkViewportWScalingNV);
begin
  vkfreemem(a,sizeof(VkViewportWScalingNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkViewportWScalingNV);
begin
  vkadditem(a,sizeof(VkViewportWScalingNV));
  a^:=default_VkViewportWScalingNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkViewportWScalingNV);
begin
  vkremitem(a,sizeof(VkViewportWScalingNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineViewportWScalingStateCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkPipelineViewportWScalingStateCreateInfoNV));
  a^:=default_VkPipelineViewportWScalingStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineViewportWScalingStateCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkPipelineViewportWScalingStateCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineViewportWScalingStateCreateInfoNV);
begin
  vkadditem(a,sizeof(VkPipelineViewportWScalingStateCreateInfoNV));
  a^:=default_VkPipelineViewportWScalingStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineViewportWScalingStateCreateInfoNV);
begin
  vkremitem(a,sizeof(VkPipelineViewportWScalingStateCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkViewportSwizzleNV);
begin
  a:=vkgetmem(sizeof(VkViewportSwizzleNV));
  a^:=default_VkViewportSwizzleNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkViewportSwizzleNV);
begin
  vkfreemem(a,sizeof(VkViewportSwizzleNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkViewportSwizzleNV);
begin
  vkadditem(a,sizeof(VkViewportSwizzleNV));
  a^:=default_VkViewportSwizzleNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkViewportSwizzleNV);
begin
  vkremitem(a,sizeof(VkViewportSwizzleNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineViewportSwizzleStateCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkPipelineViewportSwizzleStateCreateInfoNV));
  a^:=default_VkPipelineViewportSwizzleStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineViewportSwizzleStateCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkPipelineViewportSwizzleStateCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineViewportSwizzleStateCreateInfoNV);
begin
  vkadditem(a,sizeof(VkPipelineViewportSwizzleStateCreateInfoNV));
  a^:=default_VkPipelineViewportSwizzleStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineViewportSwizzleStateCreateInfoNV);
begin
  vkremitem(a,sizeof(VkPipelineViewportSwizzleStateCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceDiscardRectanglePropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceDiscardRectanglePropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceDiscardRectanglePropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceDiscardRectanglePropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceDiscardRectanglePropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceDiscardRectanglePropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceDiscardRectanglePropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceDiscardRectanglePropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceDiscardRectanglePropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceDiscardRectanglePropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineDiscardRectangleStateCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkPipelineDiscardRectangleStateCreateInfoEXT));
  a^:=default_VkPipelineDiscardRectangleStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineDiscardRectangleStateCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkPipelineDiscardRectangleStateCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineDiscardRectangleStateCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkPipelineDiscardRectangleStateCreateInfoEXT));
  a^:=default_VkPipelineDiscardRectangleStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineDiscardRectangleStateCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkPipelineDiscardRectangleStateCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX));
  FillByte(a^,sizeof(VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX));
  FillByte(a^,sizeof(VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkInputAttachmentAspectReference);
begin
  a:=vkgetmem(sizeof(VkInputAttachmentAspectReference));
  a^:=default_VkInputAttachmentAspectReference;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkInputAttachmentAspectReference);
begin
  vkfreemem(a,sizeof(VkInputAttachmentAspectReference));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkInputAttachmentAspectReference);
begin
  vkadditem(a,sizeof(VkInputAttachmentAspectReference));
  a^:=default_VkInputAttachmentAspectReference;
end;
procedure T_vulkan_auto.vkrem(var a:PVkInputAttachmentAspectReference);
begin
  vkremitem(a,sizeof(VkInputAttachmentAspectReference));
end;

procedure T_vulkan_auto.vknew(out a:PVkRenderPassInputAttachmentAspectCreateInfo);
begin
  a:=vkgetmem(sizeof(VkRenderPassInputAttachmentAspectCreateInfo));
  a^:=default_VkRenderPassInputAttachmentAspectCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRenderPassInputAttachmentAspectCreateInfo);
begin
  vkfreemem(a,sizeof(VkRenderPassInputAttachmentAspectCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRenderPassInputAttachmentAspectCreateInfo);
begin
  vkadditem(a,sizeof(VkRenderPassInputAttachmentAspectCreateInfo));
  a^:=default_VkRenderPassInputAttachmentAspectCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkRenderPassInputAttachmentAspectCreateInfo);
begin
  vkremitem(a,sizeof(VkRenderPassInputAttachmentAspectCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceSurfaceInfo2KHR);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceSurfaceInfo2KHR));
  a^:=default_VkPhysicalDeviceSurfaceInfo2KHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceSurfaceInfo2KHR);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceSurfaceInfo2KHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceSurfaceInfo2KHR);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceSurfaceInfo2KHR));
  a^:=default_VkPhysicalDeviceSurfaceInfo2KHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceSurfaceInfo2KHR);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceSurfaceInfo2KHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkSurfaceCapabilities2KHR);
begin
  a:=vkgetmem(sizeof(VkSurfaceCapabilities2KHR));
  FillByte(a^,sizeof(VkSurfaceCapabilities2KHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSurfaceCapabilities2KHR);
begin
  vkfreemem(a,sizeof(VkSurfaceCapabilities2KHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSurfaceCapabilities2KHR);
begin
  vkadditem(a,sizeof(VkSurfaceCapabilities2KHR));
  FillByte(a^,sizeof(VkSurfaceCapabilities2KHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSurfaceCapabilities2KHR);
begin
  vkremitem(a,sizeof(VkSurfaceCapabilities2KHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkSurfaceFormat2KHR);
begin
  a:=vkgetmem(sizeof(VkSurfaceFormat2KHR));
  FillByte(a^,sizeof(VkSurfaceFormat2KHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSurfaceFormat2KHR);
begin
  vkfreemem(a,sizeof(VkSurfaceFormat2KHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSurfaceFormat2KHR);
begin
  vkadditem(a,sizeof(VkSurfaceFormat2KHR));
  FillByte(a^,sizeof(VkSurfaceFormat2KHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSurfaceFormat2KHR);
begin
  vkremitem(a,sizeof(VkSurfaceFormat2KHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayProperties2KHR);
begin
  a:=vkgetmem(sizeof(VkDisplayProperties2KHR));
  FillByte(a^,sizeof(VkDisplayProperties2KHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayProperties2KHR);
begin
  vkfreemem(a,sizeof(VkDisplayProperties2KHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayProperties2KHR);
begin
  vkadditem(a,sizeof(VkDisplayProperties2KHR));
  FillByte(a^,sizeof(VkDisplayProperties2KHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayProperties2KHR);
begin
  vkremitem(a,sizeof(VkDisplayProperties2KHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayPlaneProperties2KHR);
begin
  a:=vkgetmem(sizeof(VkDisplayPlaneProperties2KHR));
  FillByte(a^,sizeof(VkDisplayPlaneProperties2KHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayPlaneProperties2KHR);
begin
  vkfreemem(a,sizeof(VkDisplayPlaneProperties2KHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayPlaneProperties2KHR);
begin
  vkadditem(a,sizeof(VkDisplayPlaneProperties2KHR));
  FillByte(a^,sizeof(VkDisplayPlaneProperties2KHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayPlaneProperties2KHR);
begin
  vkremitem(a,sizeof(VkDisplayPlaneProperties2KHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayModeProperties2KHR);
begin
  a:=vkgetmem(sizeof(VkDisplayModeProperties2KHR));
  FillByte(a^,sizeof(VkDisplayModeProperties2KHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayModeProperties2KHR);
begin
  vkfreemem(a,sizeof(VkDisplayModeProperties2KHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayModeProperties2KHR);
begin
  vkadditem(a,sizeof(VkDisplayModeProperties2KHR));
  FillByte(a^,sizeof(VkDisplayModeProperties2KHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayModeProperties2KHR);
begin
  vkremitem(a,sizeof(VkDisplayModeProperties2KHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayPlaneInfo2KHR);
begin
  a:=vkgetmem(sizeof(VkDisplayPlaneInfo2KHR));
  a^:=default_VkDisplayPlaneInfo2KHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayPlaneInfo2KHR);
begin
  vkfreemem(a,sizeof(VkDisplayPlaneInfo2KHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayPlaneInfo2KHR);
begin
  vkadditem(a,sizeof(VkDisplayPlaneInfo2KHR));
  a^:=default_VkDisplayPlaneInfo2KHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayPlaneInfo2KHR);
begin
  vkremitem(a,sizeof(VkDisplayPlaneInfo2KHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkDisplayPlaneCapabilities2KHR);
begin
  a:=vkgetmem(sizeof(VkDisplayPlaneCapabilities2KHR));
  FillByte(a^,sizeof(VkDisplayPlaneCapabilities2KHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDisplayPlaneCapabilities2KHR);
begin
  vkfreemem(a,sizeof(VkDisplayPlaneCapabilities2KHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDisplayPlaneCapabilities2KHR);
begin
  vkadditem(a,sizeof(VkDisplayPlaneCapabilities2KHR));
  FillByte(a^,sizeof(VkDisplayPlaneCapabilities2KHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDisplayPlaneCapabilities2KHR);
begin
  vkremitem(a,sizeof(VkDisplayPlaneCapabilities2KHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkSharedPresentSurfaceCapabilitiesKHR);
begin
  a:=vkgetmem(sizeof(VkSharedPresentSurfaceCapabilitiesKHR));
  FillByte(a^,sizeof(VkSharedPresentSurfaceCapabilitiesKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSharedPresentSurfaceCapabilitiesKHR);
begin
  vkfreemem(a,sizeof(VkSharedPresentSurfaceCapabilitiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSharedPresentSurfaceCapabilitiesKHR);
begin
  vkadditem(a,sizeof(VkSharedPresentSurfaceCapabilitiesKHR));
  FillByte(a^,sizeof(VkSharedPresentSurfaceCapabilitiesKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSharedPresentSurfaceCapabilitiesKHR);
begin
  vkremitem(a,sizeof(VkSharedPresentSurfaceCapabilitiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDevice16BitStorageFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDevice16BitStorageFeatures));
  a^:=default_VkPhysicalDevice16BitStorageFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDevice16BitStorageFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDevice16BitStorageFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDevice16BitStorageFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDevice16BitStorageFeatures));
  a^:=default_VkPhysicalDevice16BitStorageFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDevice16BitStorageFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDevice16BitStorageFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceSubgroupProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceSubgroupProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceSubgroupProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceSubgroupProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceSubgroupProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceSubgroupProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceSubgroupProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceSubgroupProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceSubgroupProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceSubgroupProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShaderSubgroupExtendedTypesFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures));
  a^:=default_VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShaderSubgroupExtendedTypesFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShaderSubgroupExtendedTypesFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures));
  a^:=default_VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShaderSubgroupExtendedTypesFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkBufferMemoryRequirementsInfo2);
begin
  a:=vkgetmem(sizeof(VkBufferMemoryRequirementsInfo2));
  a^:=default_VkBufferMemoryRequirementsInfo2;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBufferMemoryRequirementsInfo2);
begin
  vkfreemem(a,sizeof(VkBufferMemoryRequirementsInfo2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBufferMemoryRequirementsInfo2);
begin
  vkadditem(a,sizeof(VkBufferMemoryRequirementsInfo2));
  a^:=default_VkBufferMemoryRequirementsInfo2;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBufferMemoryRequirementsInfo2);
begin
  vkremitem(a,sizeof(VkBufferMemoryRequirementsInfo2));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageMemoryRequirementsInfo2);
begin
  a:=vkgetmem(sizeof(VkImageMemoryRequirementsInfo2));
  a^:=default_VkImageMemoryRequirementsInfo2;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageMemoryRequirementsInfo2);
begin
  vkfreemem(a,sizeof(VkImageMemoryRequirementsInfo2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageMemoryRequirementsInfo2);
begin
  vkadditem(a,sizeof(VkImageMemoryRequirementsInfo2));
  a^:=default_VkImageMemoryRequirementsInfo2;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageMemoryRequirementsInfo2);
begin
  vkremitem(a,sizeof(VkImageMemoryRequirementsInfo2));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageSparseMemoryRequirementsInfo2);
begin
  a:=vkgetmem(sizeof(VkImageSparseMemoryRequirementsInfo2));
  a^:=default_VkImageSparseMemoryRequirementsInfo2;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageSparseMemoryRequirementsInfo2);
begin
  vkfreemem(a,sizeof(VkImageSparseMemoryRequirementsInfo2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageSparseMemoryRequirementsInfo2);
begin
  vkadditem(a,sizeof(VkImageSparseMemoryRequirementsInfo2));
  a^:=default_VkImageSparseMemoryRequirementsInfo2;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageSparseMemoryRequirementsInfo2);
begin
  vkremitem(a,sizeof(VkImageSparseMemoryRequirementsInfo2));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryRequirements2);
begin
  a:=vkgetmem(sizeof(VkMemoryRequirements2));
  FillByte(a^,sizeof(VkMemoryRequirements2),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryRequirements2);
begin
  vkfreemem(a,sizeof(VkMemoryRequirements2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryRequirements2);
begin
  vkadditem(a,sizeof(VkMemoryRequirements2));
  FillByte(a^,sizeof(VkMemoryRequirements2),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryRequirements2);
begin
  vkremitem(a,sizeof(VkMemoryRequirements2));
end;

procedure T_vulkan_auto.vknew(out a:PVkSparseImageMemoryRequirements2);
begin
  a:=vkgetmem(sizeof(VkSparseImageMemoryRequirements2));
  FillByte(a^,sizeof(VkSparseImageMemoryRequirements2),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSparseImageMemoryRequirements2);
begin
  vkfreemem(a,sizeof(VkSparseImageMemoryRequirements2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSparseImageMemoryRequirements2);
begin
  vkadditem(a,sizeof(VkSparseImageMemoryRequirements2));
  FillByte(a^,sizeof(VkSparseImageMemoryRequirements2),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSparseImageMemoryRequirements2);
begin
  vkremitem(a,sizeof(VkSparseImageMemoryRequirements2));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDevicePointClippingProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDevicePointClippingProperties));
  FillByte(a^,sizeof(VkPhysicalDevicePointClippingProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDevicePointClippingProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDevicePointClippingProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDevicePointClippingProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDevicePointClippingProperties));
  FillByte(a^,sizeof(VkPhysicalDevicePointClippingProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDevicePointClippingProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDevicePointClippingProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryDedicatedRequirements);
begin
  a:=vkgetmem(sizeof(VkMemoryDedicatedRequirements));
  FillByte(a^,sizeof(VkMemoryDedicatedRequirements),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryDedicatedRequirements);
begin
  vkfreemem(a,sizeof(VkMemoryDedicatedRequirements));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryDedicatedRequirements);
begin
  vkadditem(a,sizeof(VkMemoryDedicatedRequirements));
  FillByte(a^,sizeof(VkMemoryDedicatedRequirements),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryDedicatedRequirements);
begin
  vkremitem(a,sizeof(VkMemoryDedicatedRequirements));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryDedicatedAllocateInfo);
begin
  a:=vkgetmem(sizeof(VkMemoryDedicatedAllocateInfo));
  a^:=default_VkMemoryDedicatedAllocateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryDedicatedAllocateInfo);
begin
  vkfreemem(a,sizeof(VkMemoryDedicatedAllocateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryDedicatedAllocateInfo);
begin
  vkadditem(a,sizeof(VkMemoryDedicatedAllocateInfo));
  a^:=default_VkMemoryDedicatedAllocateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryDedicatedAllocateInfo);
begin
  vkremitem(a,sizeof(VkMemoryDedicatedAllocateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageViewUsageCreateInfo);
begin
  a:=vkgetmem(sizeof(VkImageViewUsageCreateInfo));
  a^:=default_VkImageViewUsageCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageViewUsageCreateInfo);
begin
  vkfreemem(a,sizeof(VkImageViewUsageCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageViewUsageCreateInfo);
begin
  vkadditem(a,sizeof(VkImageViewUsageCreateInfo));
  a^:=default_VkImageViewUsageCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageViewUsageCreateInfo);
begin
  vkremitem(a,sizeof(VkImageViewUsageCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineTessellationDomainOriginStateCreateInfo);
begin
  a:=vkgetmem(sizeof(VkPipelineTessellationDomainOriginStateCreateInfo));
  a^:=default_VkPipelineTessellationDomainOriginStateCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineTessellationDomainOriginStateCreateInfo);
begin
  vkfreemem(a,sizeof(VkPipelineTessellationDomainOriginStateCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineTessellationDomainOriginStateCreateInfo);
begin
  vkadditem(a,sizeof(VkPipelineTessellationDomainOriginStateCreateInfo));
  a^:=default_VkPipelineTessellationDomainOriginStateCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineTessellationDomainOriginStateCreateInfo);
begin
  vkremitem(a,sizeof(VkPipelineTessellationDomainOriginStateCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkSamplerYcbcrConversionInfo);
begin
  a:=vkgetmem(sizeof(VkSamplerYcbcrConversionInfo));
  a^:=default_VkSamplerYcbcrConversionInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSamplerYcbcrConversionInfo);
begin
  vkfreemem(a,sizeof(VkSamplerYcbcrConversionInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSamplerYcbcrConversionInfo);
begin
  vkadditem(a,sizeof(VkSamplerYcbcrConversionInfo));
  a^:=default_VkSamplerYcbcrConversionInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSamplerYcbcrConversionInfo);
begin
  vkremitem(a,sizeof(VkSamplerYcbcrConversionInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkSamplerYcbcrConversionCreateInfo);
begin
  a:=vkgetmem(sizeof(VkSamplerYcbcrConversionCreateInfo));
  a^:=default_VkSamplerYcbcrConversionCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSamplerYcbcrConversionCreateInfo);
begin
  vkfreemem(a,sizeof(VkSamplerYcbcrConversionCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSamplerYcbcrConversionCreateInfo);
begin
  vkadditem(a,sizeof(VkSamplerYcbcrConversionCreateInfo));
  a^:=default_VkSamplerYcbcrConversionCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSamplerYcbcrConversionCreateInfo);
begin
  vkremitem(a,sizeof(VkSamplerYcbcrConversionCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkBindImagePlaneMemoryInfo);
begin
  a:=vkgetmem(sizeof(VkBindImagePlaneMemoryInfo));
  a^:=default_VkBindImagePlaneMemoryInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBindImagePlaneMemoryInfo);
begin
  vkfreemem(a,sizeof(VkBindImagePlaneMemoryInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBindImagePlaneMemoryInfo);
begin
  vkadditem(a,sizeof(VkBindImagePlaneMemoryInfo));
  a^:=default_VkBindImagePlaneMemoryInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBindImagePlaneMemoryInfo);
begin
  vkremitem(a,sizeof(VkBindImagePlaneMemoryInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkImagePlaneMemoryRequirementsInfo);
begin
  a:=vkgetmem(sizeof(VkImagePlaneMemoryRequirementsInfo));
  a^:=default_VkImagePlaneMemoryRequirementsInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImagePlaneMemoryRequirementsInfo);
begin
  vkfreemem(a,sizeof(VkImagePlaneMemoryRequirementsInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImagePlaneMemoryRequirementsInfo);
begin
  vkadditem(a,sizeof(VkImagePlaneMemoryRequirementsInfo));
  a^:=default_VkImagePlaneMemoryRequirementsInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImagePlaneMemoryRequirementsInfo);
begin
  vkremitem(a,sizeof(VkImagePlaneMemoryRequirementsInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceSamplerYcbcrConversionFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceSamplerYcbcrConversionFeatures));
  a^:=default_VkPhysicalDeviceSamplerYcbcrConversionFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceSamplerYcbcrConversionFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceSamplerYcbcrConversionFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceSamplerYcbcrConversionFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceSamplerYcbcrConversionFeatures));
  a^:=default_VkPhysicalDeviceSamplerYcbcrConversionFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceSamplerYcbcrConversionFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceSamplerYcbcrConversionFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkSamplerYcbcrConversionImageFormatProperties);
begin
  a:=vkgetmem(sizeof(VkSamplerYcbcrConversionImageFormatProperties));
  FillByte(a^,sizeof(VkSamplerYcbcrConversionImageFormatProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSamplerYcbcrConversionImageFormatProperties);
begin
  vkfreemem(a,sizeof(VkSamplerYcbcrConversionImageFormatProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSamplerYcbcrConversionImageFormatProperties);
begin
  vkadditem(a,sizeof(VkSamplerYcbcrConversionImageFormatProperties));
  FillByte(a^,sizeof(VkSamplerYcbcrConversionImageFormatProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkSamplerYcbcrConversionImageFormatProperties);
begin
  vkremitem(a,sizeof(VkSamplerYcbcrConversionImageFormatProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkTextureLODGatherFormatPropertiesAMD);
begin
  a:=vkgetmem(sizeof(VkTextureLODGatherFormatPropertiesAMD));
  FillByte(a^,sizeof(VkTextureLODGatherFormatPropertiesAMD),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkTextureLODGatherFormatPropertiesAMD);
begin
  vkfreemem(a,sizeof(VkTextureLODGatherFormatPropertiesAMD));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkTextureLODGatherFormatPropertiesAMD);
begin
  vkadditem(a,sizeof(VkTextureLODGatherFormatPropertiesAMD));
  FillByte(a^,sizeof(VkTextureLODGatherFormatPropertiesAMD),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkTextureLODGatherFormatPropertiesAMD);
begin
  vkremitem(a,sizeof(VkTextureLODGatherFormatPropertiesAMD));
end;

procedure T_vulkan_auto.vknew(out a:PVkConditionalRenderingBeginInfoEXT);
begin
  a:=vkgetmem(sizeof(VkConditionalRenderingBeginInfoEXT));
  a^:=default_VkConditionalRenderingBeginInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkConditionalRenderingBeginInfoEXT);
begin
  vkfreemem(a,sizeof(VkConditionalRenderingBeginInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkConditionalRenderingBeginInfoEXT);
begin
  vkadditem(a,sizeof(VkConditionalRenderingBeginInfoEXT));
  a^:=default_VkConditionalRenderingBeginInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkConditionalRenderingBeginInfoEXT);
begin
  vkremitem(a,sizeof(VkConditionalRenderingBeginInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkProtectedSubmitInfo);
begin
  a:=vkgetmem(sizeof(VkProtectedSubmitInfo));
  a^:=default_VkProtectedSubmitInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkProtectedSubmitInfo);
begin
  vkfreemem(a,sizeof(VkProtectedSubmitInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkProtectedSubmitInfo);
begin
  vkadditem(a,sizeof(VkProtectedSubmitInfo));
  a^:=default_VkProtectedSubmitInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkProtectedSubmitInfo);
begin
  vkremitem(a,sizeof(VkProtectedSubmitInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceProtectedMemoryFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceProtectedMemoryFeatures));
  a^:=default_VkPhysicalDeviceProtectedMemoryFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceProtectedMemoryFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceProtectedMemoryFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceProtectedMemoryFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceProtectedMemoryFeatures));
  a^:=default_VkPhysicalDeviceProtectedMemoryFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceProtectedMemoryFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceProtectedMemoryFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceProtectedMemoryProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceProtectedMemoryProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceProtectedMemoryProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceProtectedMemoryProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceProtectedMemoryProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceProtectedMemoryProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceProtectedMemoryProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceProtectedMemoryProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceProtectedMemoryProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceProtectedMemoryProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceQueueInfo2);
begin
  a:=vkgetmem(sizeof(VkDeviceQueueInfo2));
  a^:=default_VkDeviceQueueInfo2;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceQueueInfo2);
begin
  vkfreemem(a,sizeof(VkDeviceQueueInfo2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceQueueInfo2);
begin
  vkadditem(a,sizeof(VkDeviceQueueInfo2));
  a^:=default_VkDeviceQueueInfo2;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceQueueInfo2);
begin
  vkremitem(a,sizeof(VkDeviceQueueInfo2));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineCoverageToColorStateCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkPipelineCoverageToColorStateCreateInfoNV));
  a^:=default_VkPipelineCoverageToColorStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineCoverageToColorStateCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkPipelineCoverageToColorStateCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineCoverageToColorStateCreateInfoNV);
begin
  vkadditem(a,sizeof(VkPipelineCoverageToColorStateCreateInfoNV));
  a^:=default_VkPipelineCoverageToColorStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineCoverageToColorStateCreateInfoNV);
begin
  vkremitem(a,sizeof(VkPipelineCoverageToColorStateCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceSamplerFilterMinmaxProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceSamplerFilterMinmaxProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceSamplerFilterMinmaxProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceSamplerFilterMinmaxProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceSamplerFilterMinmaxProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceSamplerFilterMinmaxProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceSamplerFilterMinmaxProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceSamplerFilterMinmaxProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceSamplerFilterMinmaxProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceSamplerFilterMinmaxProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkSampleLocationEXT);
begin
  a:=vkgetmem(sizeof(VkSampleLocationEXT));
  a^:=default_VkSampleLocationEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSampleLocationEXT);
begin
  vkfreemem(a,sizeof(VkSampleLocationEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSampleLocationEXT);
begin
  vkadditem(a,sizeof(VkSampleLocationEXT));
  a^:=default_VkSampleLocationEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSampleLocationEXT);
begin
  vkremitem(a,sizeof(VkSampleLocationEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkSampleLocationsInfoEXT);
begin
  a:=vkgetmem(sizeof(VkSampleLocationsInfoEXT));
  a^:=default_VkSampleLocationsInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSampleLocationsInfoEXT);
begin
  vkfreemem(a,sizeof(VkSampleLocationsInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSampleLocationsInfoEXT);
begin
  vkadditem(a,sizeof(VkSampleLocationsInfoEXT));
  a^:=default_VkSampleLocationsInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSampleLocationsInfoEXT);
begin
  vkremitem(a,sizeof(VkSampleLocationsInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkAttachmentSampleLocationsEXT);
begin
  a:=vkgetmem(sizeof(VkAttachmentSampleLocationsEXT));
  a^:=default_VkAttachmentSampleLocationsEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAttachmentSampleLocationsEXT);
begin
  vkfreemem(a,sizeof(VkAttachmentSampleLocationsEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAttachmentSampleLocationsEXT);
begin
  vkadditem(a,sizeof(VkAttachmentSampleLocationsEXT));
  a^:=default_VkAttachmentSampleLocationsEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAttachmentSampleLocationsEXT);
begin
  vkremitem(a,sizeof(VkAttachmentSampleLocationsEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkSubpassSampleLocationsEXT);
begin
  a:=vkgetmem(sizeof(VkSubpassSampleLocationsEXT));
  a^:=default_VkSubpassSampleLocationsEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSubpassSampleLocationsEXT);
begin
  vkfreemem(a,sizeof(VkSubpassSampleLocationsEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSubpassSampleLocationsEXT);
begin
  vkadditem(a,sizeof(VkSubpassSampleLocationsEXT));
  a^:=default_VkSubpassSampleLocationsEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSubpassSampleLocationsEXT);
begin
  vkremitem(a,sizeof(VkSubpassSampleLocationsEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkRenderPassSampleLocationsBeginInfoEXT);
begin
  a:=vkgetmem(sizeof(VkRenderPassSampleLocationsBeginInfoEXT));
  a^:=default_VkRenderPassSampleLocationsBeginInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRenderPassSampleLocationsBeginInfoEXT);
begin
  vkfreemem(a,sizeof(VkRenderPassSampleLocationsBeginInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRenderPassSampleLocationsBeginInfoEXT);
begin
  vkadditem(a,sizeof(VkRenderPassSampleLocationsBeginInfoEXT));
  a^:=default_VkRenderPassSampleLocationsBeginInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkRenderPassSampleLocationsBeginInfoEXT);
begin
  vkremitem(a,sizeof(VkRenderPassSampleLocationsBeginInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineSampleLocationsStateCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkPipelineSampleLocationsStateCreateInfoEXT));
  a^:=default_VkPipelineSampleLocationsStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineSampleLocationsStateCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkPipelineSampleLocationsStateCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineSampleLocationsStateCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkPipelineSampleLocationsStateCreateInfoEXT));
  a^:=default_VkPipelineSampleLocationsStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineSampleLocationsStateCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkPipelineSampleLocationsStateCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceSampleLocationsPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceSampleLocationsPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceSampleLocationsPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceSampleLocationsPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceSampleLocationsPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceSampleLocationsPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceSampleLocationsPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceSampleLocationsPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceSampleLocationsPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceSampleLocationsPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkMultisamplePropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkMultisamplePropertiesEXT));
  FillByte(a^,sizeof(VkMultisamplePropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMultisamplePropertiesEXT);
begin
  vkfreemem(a,sizeof(VkMultisamplePropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMultisamplePropertiesEXT);
begin
  vkadditem(a,sizeof(VkMultisamplePropertiesEXT));
  FillByte(a^,sizeof(VkMultisamplePropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkMultisamplePropertiesEXT);
begin
  vkremitem(a,sizeof(VkMultisamplePropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkSamplerReductionModeCreateInfo);
begin
  a:=vkgetmem(sizeof(VkSamplerReductionModeCreateInfo));
  a^:=default_VkSamplerReductionModeCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSamplerReductionModeCreateInfo);
begin
  vkfreemem(a,sizeof(VkSamplerReductionModeCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSamplerReductionModeCreateInfo);
begin
  vkadditem(a,sizeof(VkSamplerReductionModeCreateInfo));
  a^:=default_VkSamplerReductionModeCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSamplerReductionModeCreateInfo);
begin
  vkremitem(a,sizeof(VkSamplerReductionModeCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceBlendOperationAdvancedFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT));
  a^:=default_VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceBlendOperationAdvancedFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceBlendOperationAdvancedFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT));
  a^:=default_VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceBlendOperationAdvancedFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceBlendOperationAdvancedPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceBlendOperationAdvancedPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceBlendOperationAdvancedPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceBlendOperationAdvancedPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineColorBlendAdvancedStateCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkPipelineColorBlendAdvancedStateCreateInfoEXT));
  a^:=default_VkPipelineColorBlendAdvancedStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineColorBlendAdvancedStateCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkPipelineColorBlendAdvancedStateCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineColorBlendAdvancedStateCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkPipelineColorBlendAdvancedStateCreateInfoEXT));
  a^:=default_VkPipelineColorBlendAdvancedStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineColorBlendAdvancedStateCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkPipelineColorBlendAdvancedStateCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceInlineUniformBlockFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceInlineUniformBlockFeaturesEXT));
  a^:=default_VkPhysicalDeviceInlineUniformBlockFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceInlineUniformBlockFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceInlineUniformBlockFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceInlineUniformBlockFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceInlineUniformBlockFeaturesEXT));
  a^:=default_VkPhysicalDeviceInlineUniformBlockFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceInlineUniformBlockFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceInlineUniformBlockFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceInlineUniformBlockPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceInlineUniformBlockPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceInlineUniformBlockPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceInlineUniformBlockPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceInlineUniformBlockPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceInlineUniformBlockPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceInlineUniformBlockPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceInlineUniformBlockPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceInlineUniformBlockPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceInlineUniformBlockPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkWriteDescriptorSetInlineUniformBlockEXT);
begin
  a:=vkgetmem(sizeof(VkWriteDescriptorSetInlineUniformBlockEXT));
  a^:=default_VkWriteDescriptorSetInlineUniformBlockEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkWriteDescriptorSetInlineUniformBlockEXT);
begin
  vkfreemem(a,sizeof(VkWriteDescriptorSetInlineUniformBlockEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkWriteDescriptorSetInlineUniformBlockEXT);
begin
  vkadditem(a,sizeof(VkWriteDescriptorSetInlineUniformBlockEXT));
  a^:=default_VkWriteDescriptorSetInlineUniformBlockEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkWriteDescriptorSetInlineUniformBlockEXT);
begin
  vkremitem(a,sizeof(VkWriteDescriptorSetInlineUniformBlockEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorPoolInlineUniformBlockCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkDescriptorPoolInlineUniformBlockCreateInfoEXT));
  a^:=default_VkDescriptorPoolInlineUniformBlockCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorPoolInlineUniformBlockCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkDescriptorPoolInlineUniformBlockCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorPoolInlineUniformBlockCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkDescriptorPoolInlineUniformBlockCreateInfoEXT));
  a^:=default_VkDescriptorPoolInlineUniformBlockCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorPoolInlineUniformBlockCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkDescriptorPoolInlineUniformBlockCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineCoverageModulationStateCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkPipelineCoverageModulationStateCreateInfoNV));
  a^:=default_VkPipelineCoverageModulationStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineCoverageModulationStateCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkPipelineCoverageModulationStateCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineCoverageModulationStateCreateInfoNV);
begin
  vkadditem(a,sizeof(VkPipelineCoverageModulationStateCreateInfoNV));
  a^:=default_VkPipelineCoverageModulationStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineCoverageModulationStateCreateInfoNV);
begin
  vkremitem(a,sizeof(VkPipelineCoverageModulationStateCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageFormatListCreateInfo);
begin
  a:=vkgetmem(sizeof(VkImageFormatListCreateInfo));
  a^:=default_VkImageFormatListCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageFormatListCreateInfo);
begin
  vkfreemem(a,sizeof(VkImageFormatListCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageFormatListCreateInfo);
begin
  vkadditem(a,sizeof(VkImageFormatListCreateInfo));
  a^:=default_VkImageFormatListCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageFormatListCreateInfo);
begin
  vkremitem(a,sizeof(VkImageFormatListCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkValidationCacheCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkValidationCacheCreateInfoEXT));
  a^:=default_VkValidationCacheCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkValidationCacheCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkValidationCacheCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkValidationCacheCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkValidationCacheCreateInfoEXT));
  a^:=default_VkValidationCacheCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkValidationCacheCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkValidationCacheCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkShaderModuleValidationCacheCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkShaderModuleValidationCacheCreateInfoEXT));
  a^:=default_VkShaderModuleValidationCacheCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkShaderModuleValidationCacheCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkShaderModuleValidationCacheCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkShaderModuleValidationCacheCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkShaderModuleValidationCacheCreateInfoEXT));
  a^:=default_VkShaderModuleValidationCacheCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkShaderModuleValidationCacheCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkShaderModuleValidationCacheCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceMaintenance3Properties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceMaintenance3Properties));
  FillByte(a^,sizeof(VkPhysicalDeviceMaintenance3Properties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceMaintenance3Properties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceMaintenance3Properties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceMaintenance3Properties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceMaintenance3Properties));
  FillByte(a^,sizeof(VkPhysicalDeviceMaintenance3Properties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceMaintenance3Properties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceMaintenance3Properties));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorSetLayoutSupport);
begin
  a:=vkgetmem(sizeof(VkDescriptorSetLayoutSupport));
  FillByte(a^,sizeof(VkDescriptorSetLayoutSupport),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorSetLayoutSupport);
begin
  vkfreemem(a,sizeof(VkDescriptorSetLayoutSupport));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorSetLayoutSupport);
begin
  vkadditem(a,sizeof(VkDescriptorSetLayoutSupport));
  FillByte(a^,sizeof(VkDescriptorSetLayoutSupport),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorSetLayoutSupport);
begin
  vkremitem(a,sizeof(VkDescriptorSetLayoutSupport));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShaderDrawParametersFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShaderDrawParametersFeatures));
  a^:=default_VkPhysicalDeviceShaderDrawParametersFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShaderDrawParametersFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShaderDrawParametersFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShaderDrawParametersFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShaderDrawParametersFeatures));
  a^:=default_VkPhysicalDeviceShaderDrawParametersFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShaderDrawParametersFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShaderDrawParametersFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShaderFloat16Int8Features);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShaderFloat16Int8Features));
  a^:=default_VkPhysicalDeviceShaderFloat16Int8Features;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShaderFloat16Int8Features);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShaderFloat16Int8Features));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShaderFloat16Int8Features);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShaderFloat16Int8Features));
  a^:=default_VkPhysicalDeviceShaderFloat16Int8Features;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShaderFloat16Int8Features);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShaderFloat16Int8Features));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceFloatControlsProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceFloatControlsProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceFloatControlsProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceFloatControlsProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceFloatControlsProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceFloatControlsProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceFloatControlsProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceFloatControlsProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceFloatControlsProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceFloatControlsProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceHostQueryResetFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceHostQueryResetFeatures));
  a^:=default_VkPhysicalDeviceHostQueryResetFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceHostQueryResetFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceHostQueryResetFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceHostQueryResetFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceHostQueryResetFeatures));
  a^:=default_VkPhysicalDeviceHostQueryResetFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceHostQueryResetFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceHostQueryResetFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkNativeBufferUsage2ANDROID);
begin
  a:=vkgetmem(sizeof(VkNativeBufferUsage2ANDROID));
  a^:=default_VkNativeBufferUsage2ANDROID;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkNativeBufferUsage2ANDROID);
begin
  vkfreemem(a,sizeof(VkNativeBufferUsage2ANDROID));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkNativeBufferUsage2ANDROID);
begin
  vkadditem(a,sizeof(VkNativeBufferUsage2ANDROID));
  a^:=default_VkNativeBufferUsage2ANDROID;
end;
procedure T_vulkan_auto.vkrem(var a:PVkNativeBufferUsage2ANDROID);
begin
  vkremitem(a,sizeof(VkNativeBufferUsage2ANDROID));
end;

procedure T_vulkan_auto.vknew(out a:PVkNativeBufferANDROID);
begin
  a:=vkgetmem(sizeof(VkNativeBufferANDROID));
  a^:=default_VkNativeBufferANDROID;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkNativeBufferANDROID);
begin
  vkfreemem(a,sizeof(VkNativeBufferANDROID));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkNativeBufferANDROID);
begin
  vkadditem(a,sizeof(VkNativeBufferANDROID));
  a^:=default_VkNativeBufferANDROID;
end;
procedure T_vulkan_auto.vkrem(var a:PVkNativeBufferANDROID);
begin
  vkremitem(a,sizeof(VkNativeBufferANDROID));
end;

procedure T_vulkan_auto.vknew(out a:PVkSwapchainImageCreateInfoANDROID);
begin
  a:=vkgetmem(sizeof(VkSwapchainImageCreateInfoANDROID));
  a^:=default_VkSwapchainImageCreateInfoANDROID;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSwapchainImageCreateInfoANDROID);
begin
  vkfreemem(a,sizeof(VkSwapchainImageCreateInfoANDROID));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSwapchainImageCreateInfoANDROID);
begin
  vkadditem(a,sizeof(VkSwapchainImageCreateInfoANDROID));
  a^:=default_VkSwapchainImageCreateInfoANDROID;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSwapchainImageCreateInfoANDROID);
begin
  vkremitem(a,sizeof(VkSwapchainImageCreateInfoANDROID));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDevicePresentationPropertiesANDROID);
begin
  a:=vkgetmem(sizeof(VkPhysicalDevicePresentationPropertiesANDROID));
  a^:=default_VkPhysicalDevicePresentationPropertiesANDROID;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDevicePresentationPropertiesANDROID);
begin
  vkfreemem(a,sizeof(VkPhysicalDevicePresentationPropertiesANDROID));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDevicePresentationPropertiesANDROID);
begin
  vkadditem(a,sizeof(VkPhysicalDevicePresentationPropertiesANDROID));
  a^:=default_VkPhysicalDevicePresentationPropertiesANDROID;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDevicePresentationPropertiesANDROID);
begin
  vkremitem(a,sizeof(VkPhysicalDevicePresentationPropertiesANDROID));
end;

procedure T_vulkan_auto.vknew(out a:PVkShaderResourceUsageAMD);
begin
  a:=vkgetmem(sizeof(VkShaderResourceUsageAMD));
  FillByte(a^,sizeof(VkShaderResourceUsageAMD),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkShaderResourceUsageAMD);
begin
  vkfreemem(a,sizeof(VkShaderResourceUsageAMD));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkShaderResourceUsageAMD);
begin
  vkadditem(a,sizeof(VkShaderResourceUsageAMD));
  FillByte(a^,sizeof(VkShaderResourceUsageAMD),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkShaderResourceUsageAMD);
begin
  vkremitem(a,sizeof(VkShaderResourceUsageAMD));
end;

procedure T_vulkan_auto.vknew(out a:PVkShaderStatisticsInfoAMD);
begin
  a:=vkgetmem(sizeof(VkShaderStatisticsInfoAMD));
  FillByte(a^,sizeof(VkShaderStatisticsInfoAMD),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkShaderStatisticsInfoAMD);
begin
  vkfreemem(a,sizeof(VkShaderStatisticsInfoAMD));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkShaderStatisticsInfoAMD);
begin
  vkadditem(a,sizeof(VkShaderStatisticsInfoAMD));
  FillByte(a^,sizeof(VkShaderStatisticsInfoAMD),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkShaderStatisticsInfoAMD);
begin
  vkremitem(a,sizeof(VkShaderStatisticsInfoAMD));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceQueueGlobalPriorityCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkDeviceQueueGlobalPriorityCreateInfoEXT));
  a^:=default_VkDeviceQueueGlobalPriorityCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceQueueGlobalPriorityCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkDeviceQueueGlobalPriorityCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceQueueGlobalPriorityCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkDeviceQueueGlobalPriorityCreateInfoEXT));
  a^:=default_VkDeviceQueueGlobalPriorityCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceQueueGlobalPriorityCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkDeviceQueueGlobalPriorityCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDebugUtilsObjectNameInfoEXT);
begin
  a:=vkgetmem(sizeof(VkDebugUtilsObjectNameInfoEXT));
  a^:=default_VkDebugUtilsObjectNameInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDebugUtilsObjectNameInfoEXT);
begin
  vkfreemem(a,sizeof(VkDebugUtilsObjectNameInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDebugUtilsObjectNameInfoEXT);
begin
  vkadditem(a,sizeof(VkDebugUtilsObjectNameInfoEXT));
  a^:=default_VkDebugUtilsObjectNameInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDebugUtilsObjectNameInfoEXT);
begin
  vkremitem(a,sizeof(VkDebugUtilsObjectNameInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDebugUtilsObjectTagInfoEXT);
begin
  a:=vkgetmem(sizeof(VkDebugUtilsObjectTagInfoEXT));
  a^:=default_VkDebugUtilsObjectTagInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDebugUtilsObjectTagInfoEXT);
begin
  vkfreemem(a,sizeof(VkDebugUtilsObjectTagInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDebugUtilsObjectTagInfoEXT);
begin
  vkadditem(a,sizeof(VkDebugUtilsObjectTagInfoEXT));
  a^:=default_VkDebugUtilsObjectTagInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDebugUtilsObjectTagInfoEXT);
begin
  vkremitem(a,sizeof(VkDebugUtilsObjectTagInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDebugUtilsLabelEXT);
begin
  a:=vkgetmem(sizeof(VkDebugUtilsLabelEXT));
  a^:=default_VkDebugUtilsLabelEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDebugUtilsLabelEXT);
begin
  vkfreemem(a,sizeof(VkDebugUtilsLabelEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDebugUtilsLabelEXT);
begin
  vkadditem(a,sizeof(VkDebugUtilsLabelEXT));
  a^:=default_VkDebugUtilsLabelEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDebugUtilsLabelEXT);
begin
  vkremitem(a,sizeof(VkDebugUtilsLabelEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDebugUtilsMessengerCallbackDataEXT);
begin
  a:=vkgetmem(sizeof(VkDebugUtilsMessengerCallbackDataEXT));
  a^:=default_VkDebugUtilsMessengerCallbackDataEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDebugUtilsMessengerCallbackDataEXT);
begin
  vkfreemem(a,sizeof(VkDebugUtilsMessengerCallbackDataEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDebugUtilsMessengerCallbackDataEXT);
begin
  vkadditem(a,sizeof(VkDebugUtilsMessengerCallbackDataEXT));
  a^:=default_VkDebugUtilsMessengerCallbackDataEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDebugUtilsMessengerCallbackDataEXT);
begin
  vkremitem(a,sizeof(VkDebugUtilsMessengerCallbackDataEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkImportMemoryHostPointerInfoEXT);
begin
  a:=vkgetmem(sizeof(VkImportMemoryHostPointerInfoEXT));
  a^:=default_VkImportMemoryHostPointerInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImportMemoryHostPointerInfoEXT);
begin
  vkfreemem(a,sizeof(VkImportMemoryHostPointerInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImportMemoryHostPointerInfoEXT);
begin
  vkadditem(a,sizeof(VkImportMemoryHostPointerInfoEXT));
  a^:=default_VkImportMemoryHostPointerInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImportMemoryHostPointerInfoEXT);
begin
  vkremitem(a,sizeof(VkImportMemoryHostPointerInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryHostPointerPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkMemoryHostPointerPropertiesEXT));
  FillByte(a^,sizeof(VkMemoryHostPointerPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryHostPointerPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkMemoryHostPointerPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryHostPointerPropertiesEXT);
begin
  vkadditem(a,sizeof(VkMemoryHostPointerPropertiesEXT));
  FillByte(a^,sizeof(VkMemoryHostPointerPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryHostPointerPropertiesEXT);
begin
  vkremitem(a,sizeof(VkMemoryHostPointerPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceExternalMemoryHostPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceExternalMemoryHostPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceExternalMemoryHostPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceExternalMemoryHostPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceExternalMemoryHostPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceExternalMemoryHostPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceExternalMemoryHostPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceExternalMemoryHostPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceExternalMemoryHostPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceExternalMemoryHostPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceConservativeRasterizationPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceConservativeRasterizationPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceConservativeRasterizationPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceConservativeRasterizationPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceConservativeRasterizationPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceConservativeRasterizationPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceConservativeRasterizationPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceConservativeRasterizationPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceConservativeRasterizationPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceConservativeRasterizationPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkCalibratedTimestampInfoEXT);
begin
  a:=vkgetmem(sizeof(VkCalibratedTimestampInfoEXT));
  a^:=default_VkCalibratedTimestampInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCalibratedTimestampInfoEXT);
begin
  vkfreemem(a,sizeof(VkCalibratedTimestampInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCalibratedTimestampInfoEXT);
begin
  vkadditem(a,sizeof(VkCalibratedTimestampInfoEXT));
  a^:=default_VkCalibratedTimestampInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkCalibratedTimestampInfoEXT);
begin
  vkremitem(a,sizeof(VkCalibratedTimestampInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShaderCorePropertiesAMD);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShaderCorePropertiesAMD));
  FillByte(a^,sizeof(VkPhysicalDeviceShaderCorePropertiesAMD),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShaderCorePropertiesAMD);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShaderCorePropertiesAMD));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShaderCorePropertiesAMD);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShaderCorePropertiesAMD));
  FillByte(a^,sizeof(VkPhysicalDeviceShaderCorePropertiesAMD),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShaderCorePropertiesAMD);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShaderCorePropertiesAMD));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShaderCoreProperties2AMD);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShaderCoreProperties2AMD));
  FillByte(a^,sizeof(VkPhysicalDeviceShaderCoreProperties2AMD),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShaderCoreProperties2AMD);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShaderCoreProperties2AMD));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShaderCoreProperties2AMD);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShaderCoreProperties2AMD));
  FillByte(a^,sizeof(VkPhysicalDeviceShaderCoreProperties2AMD),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShaderCoreProperties2AMD);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShaderCoreProperties2AMD));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineRasterizationConservativeStateCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkPipelineRasterizationConservativeStateCreateInfoEXT));
  a^:=default_VkPipelineRasterizationConservativeStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineRasterizationConservativeStateCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkPipelineRasterizationConservativeStateCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineRasterizationConservativeStateCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkPipelineRasterizationConservativeStateCreateInfoEXT));
  a^:=default_VkPipelineRasterizationConservativeStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineRasterizationConservativeStateCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkPipelineRasterizationConservativeStateCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceDescriptorIndexingFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceDescriptorIndexingFeatures));
  a^:=default_VkPhysicalDeviceDescriptorIndexingFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceDescriptorIndexingFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceDescriptorIndexingFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceDescriptorIndexingFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceDescriptorIndexingFeatures));
  a^:=default_VkPhysicalDeviceDescriptorIndexingFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceDescriptorIndexingFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceDescriptorIndexingFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceDescriptorIndexingProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceDescriptorIndexingProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceDescriptorIndexingProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceDescriptorIndexingProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceDescriptorIndexingProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceDescriptorIndexingProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceDescriptorIndexingProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceDescriptorIndexingProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceDescriptorIndexingProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceDescriptorIndexingProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorSetLayoutBindingFlagsCreateInfo);
begin
  a:=vkgetmem(sizeof(VkDescriptorSetLayoutBindingFlagsCreateInfo));
  a^:=default_VkDescriptorSetLayoutBindingFlagsCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorSetLayoutBindingFlagsCreateInfo);
begin
  vkfreemem(a,sizeof(VkDescriptorSetLayoutBindingFlagsCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorSetLayoutBindingFlagsCreateInfo);
begin
  vkadditem(a,sizeof(VkDescriptorSetLayoutBindingFlagsCreateInfo));
  a^:=default_VkDescriptorSetLayoutBindingFlagsCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorSetLayoutBindingFlagsCreateInfo);
begin
  vkremitem(a,sizeof(VkDescriptorSetLayoutBindingFlagsCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorSetVariableDescriptorCountAllocateInfo);
begin
  a:=vkgetmem(sizeof(VkDescriptorSetVariableDescriptorCountAllocateInfo));
  a^:=default_VkDescriptorSetVariableDescriptorCountAllocateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorSetVariableDescriptorCountAllocateInfo);
begin
  vkfreemem(a,sizeof(VkDescriptorSetVariableDescriptorCountAllocateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorSetVariableDescriptorCountAllocateInfo);
begin
  vkadditem(a,sizeof(VkDescriptorSetVariableDescriptorCountAllocateInfo));
  a^:=default_VkDescriptorSetVariableDescriptorCountAllocateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorSetVariableDescriptorCountAllocateInfo);
begin
  vkremitem(a,sizeof(VkDescriptorSetVariableDescriptorCountAllocateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDescriptorSetVariableDescriptorCountLayoutSupport);
begin
  a:=vkgetmem(sizeof(VkDescriptorSetVariableDescriptorCountLayoutSupport));
  FillByte(a^,sizeof(VkDescriptorSetVariableDescriptorCountLayoutSupport),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDescriptorSetVariableDescriptorCountLayoutSupport);
begin
  vkfreemem(a,sizeof(VkDescriptorSetVariableDescriptorCountLayoutSupport));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDescriptorSetVariableDescriptorCountLayoutSupport);
begin
  vkadditem(a,sizeof(VkDescriptorSetVariableDescriptorCountLayoutSupport));
  FillByte(a^,sizeof(VkDescriptorSetVariableDescriptorCountLayoutSupport),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDescriptorSetVariableDescriptorCountLayoutSupport);
begin
  vkremitem(a,sizeof(VkDescriptorSetVariableDescriptorCountLayoutSupport));
end;

procedure T_vulkan_auto.vknew(out a:PVkAttachmentDescription2);
begin
  a:=vkgetmem(sizeof(VkAttachmentDescription2));
  a^:=default_VkAttachmentDescription2;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAttachmentDescription2);
begin
  vkfreemem(a,sizeof(VkAttachmentDescription2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAttachmentDescription2);
begin
  vkadditem(a,sizeof(VkAttachmentDescription2));
  a^:=default_VkAttachmentDescription2;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAttachmentDescription2);
begin
  vkremitem(a,sizeof(VkAttachmentDescription2));
end;

procedure T_vulkan_auto.vknew(out a:PVkAttachmentReference2);
begin
  a:=vkgetmem(sizeof(VkAttachmentReference2));
  a^:=default_VkAttachmentReference2;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAttachmentReference2);
begin
  vkfreemem(a,sizeof(VkAttachmentReference2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAttachmentReference2);
begin
  vkadditem(a,sizeof(VkAttachmentReference2));
  a^:=default_VkAttachmentReference2;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAttachmentReference2);
begin
  vkremitem(a,sizeof(VkAttachmentReference2));
end;

procedure T_vulkan_auto.vknew(out a:PVkSubpassDescription2);
begin
  a:=vkgetmem(sizeof(VkSubpassDescription2));
  a^:=default_VkSubpassDescription2;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSubpassDescription2);
begin
  vkfreemem(a,sizeof(VkSubpassDescription2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSubpassDescription2);
begin
  vkadditem(a,sizeof(VkSubpassDescription2));
  a^:=default_VkSubpassDescription2;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSubpassDescription2);
begin
  vkremitem(a,sizeof(VkSubpassDescription2));
end;

procedure T_vulkan_auto.vknew(out a:PVkSubpassDependency2);
begin
  a:=vkgetmem(sizeof(VkSubpassDependency2));
  a^:=default_VkSubpassDependency2;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSubpassDependency2);
begin
  vkfreemem(a,sizeof(VkSubpassDependency2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSubpassDependency2);
begin
  vkadditem(a,sizeof(VkSubpassDependency2));
  a^:=default_VkSubpassDependency2;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSubpassDependency2);
begin
  vkremitem(a,sizeof(VkSubpassDependency2));
end;

procedure T_vulkan_auto.vknew(out a:PVkRenderPassCreateInfo2);
begin
  a:=vkgetmem(sizeof(VkRenderPassCreateInfo2));
  a^:=default_VkRenderPassCreateInfo2;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRenderPassCreateInfo2);
begin
  vkfreemem(a,sizeof(VkRenderPassCreateInfo2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRenderPassCreateInfo2);
begin
  vkadditem(a,sizeof(VkRenderPassCreateInfo2));
  a^:=default_VkRenderPassCreateInfo2;
end;
procedure T_vulkan_auto.vkrem(var a:PVkRenderPassCreateInfo2);
begin
  vkremitem(a,sizeof(VkRenderPassCreateInfo2));
end;

procedure T_vulkan_auto.vknew(out a:PVkSubpassBeginInfo);
begin
  a:=vkgetmem(sizeof(VkSubpassBeginInfo));
  a^:=default_VkSubpassBeginInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSubpassBeginInfo);
begin
  vkfreemem(a,sizeof(VkSubpassBeginInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSubpassBeginInfo);
begin
  vkadditem(a,sizeof(VkSubpassBeginInfo));
  a^:=default_VkSubpassBeginInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSubpassBeginInfo);
begin
  vkremitem(a,sizeof(VkSubpassBeginInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkSubpassEndInfo);
begin
  a:=vkgetmem(sizeof(VkSubpassEndInfo));
  a^:=default_VkSubpassEndInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSubpassEndInfo);
begin
  vkfreemem(a,sizeof(VkSubpassEndInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSubpassEndInfo);
begin
  vkadditem(a,sizeof(VkSubpassEndInfo));
  a^:=default_VkSubpassEndInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSubpassEndInfo);
begin
  vkremitem(a,sizeof(VkSubpassEndInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceTimelineSemaphoreFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceTimelineSemaphoreFeatures));
  a^:=default_VkPhysicalDeviceTimelineSemaphoreFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceTimelineSemaphoreFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceTimelineSemaphoreFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceTimelineSemaphoreFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceTimelineSemaphoreFeatures));
  a^:=default_VkPhysicalDeviceTimelineSemaphoreFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceTimelineSemaphoreFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceTimelineSemaphoreFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceTimelineSemaphoreProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceTimelineSemaphoreProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceTimelineSemaphoreProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceTimelineSemaphoreProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceTimelineSemaphoreProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceTimelineSemaphoreProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceTimelineSemaphoreProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceTimelineSemaphoreProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceTimelineSemaphoreProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceTimelineSemaphoreProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkSemaphoreTypeCreateInfo);
begin
  a:=vkgetmem(sizeof(VkSemaphoreTypeCreateInfo));
  a^:=default_VkSemaphoreTypeCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSemaphoreTypeCreateInfo);
begin
  vkfreemem(a,sizeof(VkSemaphoreTypeCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSemaphoreTypeCreateInfo);
begin
  vkadditem(a,sizeof(VkSemaphoreTypeCreateInfo));
  a^:=default_VkSemaphoreTypeCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSemaphoreTypeCreateInfo);
begin
  vkremitem(a,sizeof(VkSemaphoreTypeCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkTimelineSemaphoreSubmitInfo);
begin
  a:=vkgetmem(sizeof(VkTimelineSemaphoreSubmitInfo));
  a^:=default_VkTimelineSemaphoreSubmitInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkTimelineSemaphoreSubmitInfo);
begin
  vkfreemem(a,sizeof(VkTimelineSemaphoreSubmitInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkTimelineSemaphoreSubmitInfo);
begin
  vkadditem(a,sizeof(VkTimelineSemaphoreSubmitInfo));
  a^:=default_VkTimelineSemaphoreSubmitInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkTimelineSemaphoreSubmitInfo);
begin
  vkremitem(a,sizeof(VkTimelineSemaphoreSubmitInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkSemaphoreWaitInfo);
begin
  a:=vkgetmem(sizeof(VkSemaphoreWaitInfo));
  a^:=default_VkSemaphoreWaitInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSemaphoreWaitInfo);
begin
  vkfreemem(a,sizeof(VkSemaphoreWaitInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSemaphoreWaitInfo);
begin
  vkadditem(a,sizeof(VkSemaphoreWaitInfo));
  a^:=default_VkSemaphoreWaitInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSemaphoreWaitInfo);
begin
  vkremitem(a,sizeof(VkSemaphoreWaitInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkSemaphoreSignalInfo);
begin
  a:=vkgetmem(sizeof(VkSemaphoreSignalInfo));
  a^:=default_VkSemaphoreSignalInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSemaphoreSignalInfo);
begin
  vkfreemem(a,sizeof(VkSemaphoreSignalInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSemaphoreSignalInfo);
begin
  vkadditem(a,sizeof(VkSemaphoreSignalInfo));
  a^:=default_VkSemaphoreSignalInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSemaphoreSignalInfo);
begin
  vkremitem(a,sizeof(VkSemaphoreSignalInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkVertexInputBindingDivisorDescriptionEXT);
begin
  a:=vkgetmem(sizeof(VkVertexInputBindingDivisorDescriptionEXT));
  a^:=default_VkVertexInputBindingDivisorDescriptionEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkVertexInputBindingDivisorDescriptionEXT);
begin
  vkfreemem(a,sizeof(VkVertexInputBindingDivisorDescriptionEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkVertexInputBindingDivisorDescriptionEXT);
begin
  vkadditem(a,sizeof(VkVertexInputBindingDivisorDescriptionEXT));
  a^:=default_VkVertexInputBindingDivisorDescriptionEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkVertexInputBindingDivisorDescriptionEXT);
begin
  vkremitem(a,sizeof(VkVertexInputBindingDivisorDescriptionEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineVertexInputDivisorStateCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkPipelineVertexInputDivisorStateCreateInfoEXT));
  a^:=default_VkPipelineVertexInputDivisorStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineVertexInputDivisorStateCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkPipelineVertexInputDivisorStateCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineVertexInputDivisorStateCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkPipelineVertexInputDivisorStateCreateInfoEXT));
  a^:=default_VkPipelineVertexInputDivisorStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineVertexInputDivisorStateCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkPipelineVertexInputDivisorStateCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceVertexAttributeDivisorPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceVertexAttributeDivisorPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceVertexAttributeDivisorPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceVertexAttributeDivisorPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDevicePCIBusInfoPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDevicePCIBusInfoPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDevicePCIBusInfoPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDevicePCIBusInfoPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDevicePCIBusInfoPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDevicePCIBusInfoPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDevicePCIBusInfoPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDevicePCIBusInfoPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDevicePCIBusInfoPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDevicePCIBusInfoPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkImportAndroidHardwareBufferInfoANDROID);
begin
  a:=vkgetmem(sizeof(VkImportAndroidHardwareBufferInfoANDROID));
  a^:=default_VkImportAndroidHardwareBufferInfoANDROID;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImportAndroidHardwareBufferInfoANDROID);
begin
  vkfreemem(a,sizeof(VkImportAndroidHardwareBufferInfoANDROID));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImportAndroidHardwareBufferInfoANDROID);
begin
  vkadditem(a,sizeof(VkImportAndroidHardwareBufferInfoANDROID));
  a^:=default_VkImportAndroidHardwareBufferInfoANDROID;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImportAndroidHardwareBufferInfoANDROID);
begin
  vkremitem(a,sizeof(VkImportAndroidHardwareBufferInfoANDROID));
end;

procedure T_vulkan_auto.vknew(out a:PVkAndroidHardwareBufferUsageANDROID);
begin
  a:=vkgetmem(sizeof(VkAndroidHardwareBufferUsageANDROID));
  FillByte(a^,sizeof(VkAndroidHardwareBufferUsageANDROID),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAndroidHardwareBufferUsageANDROID);
begin
  vkfreemem(a,sizeof(VkAndroidHardwareBufferUsageANDROID));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAndroidHardwareBufferUsageANDROID);
begin
  vkadditem(a,sizeof(VkAndroidHardwareBufferUsageANDROID));
  FillByte(a^,sizeof(VkAndroidHardwareBufferUsageANDROID),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkAndroidHardwareBufferUsageANDROID);
begin
  vkremitem(a,sizeof(VkAndroidHardwareBufferUsageANDROID));
end;

procedure T_vulkan_auto.vknew(out a:PVkAndroidHardwareBufferPropertiesANDROID);
begin
  a:=vkgetmem(sizeof(VkAndroidHardwareBufferPropertiesANDROID));
  FillByte(a^,sizeof(VkAndroidHardwareBufferPropertiesANDROID),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAndroidHardwareBufferPropertiesANDROID);
begin
  vkfreemem(a,sizeof(VkAndroidHardwareBufferPropertiesANDROID));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAndroidHardwareBufferPropertiesANDROID);
begin
  vkadditem(a,sizeof(VkAndroidHardwareBufferPropertiesANDROID));
  FillByte(a^,sizeof(VkAndroidHardwareBufferPropertiesANDROID),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkAndroidHardwareBufferPropertiesANDROID);
begin
  vkremitem(a,sizeof(VkAndroidHardwareBufferPropertiesANDROID));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryGetAndroidHardwareBufferInfoANDROID);
begin
  a:=vkgetmem(sizeof(VkMemoryGetAndroidHardwareBufferInfoANDROID));
  a^:=default_VkMemoryGetAndroidHardwareBufferInfoANDROID;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryGetAndroidHardwareBufferInfoANDROID);
begin
  vkfreemem(a,sizeof(VkMemoryGetAndroidHardwareBufferInfoANDROID));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryGetAndroidHardwareBufferInfoANDROID);
begin
  vkadditem(a,sizeof(VkMemoryGetAndroidHardwareBufferInfoANDROID));
  a^:=default_VkMemoryGetAndroidHardwareBufferInfoANDROID;
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryGetAndroidHardwareBufferInfoANDROID);
begin
  vkremitem(a,sizeof(VkMemoryGetAndroidHardwareBufferInfoANDROID));
end;

procedure T_vulkan_auto.vknew(out a:PVkAndroidHardwareBufferFormatPropertiesANDROID);
begin
  a:=vkgetmem(sizeof(VkAndroidHardwareBufferFormatPropertiesANDROID));
  FillByte(a^,sizeof(VkAndroidHardwareBufferFormatPropertiesANDROID),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAndroidHardwareBufferFormatPropertiesANDROID);
begin
  vkfreemem(a,sizeof(VkAndroidHardwareBufferFormatPropertiesANDROID));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAndroidHardwareBufferFormatPropertiesANDROID);
begin
  vkadditem(a,sizeof(VkAndroidHardwareBufferFormatPropertiesANDROID));
  FillByte(a^,sizeof(VkAndroidHardwareBufferFormatPropertiesANDROID),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkAndroidHardwareBufferFormatPropertiesANDROID);
begin
  vkremitem(a,sizeof(VkAndroidHardwareBufferFormatPropertiesANDROID));
end;

procedure T_vulkan_auto.vknew(out a:PVkCommandBufferInheritanceConditionalRenderingInfoEXT);
begin
  a:=vkgetmem(sizeof(VkCommandBufferInheritanceConditionalRenderingInfoEXT));
  a^:=default_VkCommandBufferInheritanceConditionalRenderingInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCommandBufferInheritanceConditionalRenderingInfoEXT);
begin
  vkfreemem(a,sizeof(VkCommandBufferInheritanceConditionalRenderingInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCommandBufferInheritanceConditionalRenderingInfoEXT);
begin
  vkadditem(a,sizeof(VkCommandBufferInheritanceConditionalRenderingInfoEXT));
  a^:=default_VkCommandBufferInheritanceConditionalRenderingInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkCommandBufferInheritanceConditionalRenderingInfoEXT);
begin
  vkremitem(a,sizeof(VkCommandBufferInheritanceConditionalRenderingInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkExternalFormatANDROID);
begin
  a:=vkgetmem(sizeof(VkExternalFormatANDROID));
  a^:=default_VkExternalFormatANDROID;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkExternalFormatANDROID);
begin
  vkfreemem(a,sizeof(VkExternalFormatANDROID));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkExternalFormatANDROID);
begin
  vkadditem(a,sizeof(VkExternalFormatANDROID));
  a^:=default_VkExternalFormatANDROID;
end;
procedure T_vulkan_auto.vkrem(var a:PVkExternalFormatANDROID);
begin
  vkremitem(a,sizeof(VkExternalFormatANDROID));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDevice8BitStorageFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDevice8BitStorageFeatures));
  a^:=default_VkPhysicalDevice8BitStorageFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDevice8BitStorageFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDevice8BitStorageFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDevice8BitStorageFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDevice8BitStorageFeatures));
  a^:=default_VkPhysicalDevice8BitStorageFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDevice8BitStorageFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDevice8BitStorageFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceConditionalRenderingFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceConditionalRenderingFeaturesEXT));
  a^:=default_VkPhysicalDeviceConditionalRenderingFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceConditionalRenderingFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceConditionalRenderingFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceConditionalRenderingFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceConditionalRenderingFeaturesEXT));
  a^:=default_VkPhysicalDeviceConditionalRenderingFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceConditionalRenderingFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceConditionalRenderingFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceVulkanMemoryModelFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceVulkanMemoryModelFeatures));
  a^:=default_VkPhysicalDeviceVulkanMemoryModelFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceVulkanMemoryModelFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceVulkanMemoryModelFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceVulkanMemoryModelFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceVulkanMemoryModelFeatures));
  a^:=default_VkPhysicalDeviceVulkanMemoryModelFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceVulkanMemoryModelFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceVulkanMemoryModelFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShaderAtomicInt64Features);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShaderAtomicInt64Features));
  a^:=default_VkPhysicalDeviceShaderAtomicInt64Features;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShaderAtomicInt64Features);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShaderAtomicInt64Features));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShaderAtomicInt64Features);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShaderAtomicInt64Features));
  a^:=default_VkPhysicalDeviceShaderAtomicInt64Features;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShaderAtomicInt64Features);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShaderAtomicInt64Features));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceVertexAttributeDivisorFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT));
  a^:=default_VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceVertexAttributeDivisorFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceVertexAttributeDivisorFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT));
  a^:=default_VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceVertexAttributeDivisorFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkQueueFamilyCheckpointPropertiesNV);
begin
  a:=vkgetmem(sizeof(VkQueueFamilyCheckpointPropertiesNV));
  FillByte(a^,sizeof(VkQueueFamilyCheckpointPropertiesNV),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkQueueFamilyCheckpointPropertiesNV);
begin
  vkfreemem(a,sizeof(VkQueueFamilyCheckpointPropertiesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkQueueFamilyCheckpointPropertiesNV);
begin
  vkadditem(a,sizeof(VkQueueFamilyCheckpointPropertiesNV));
  FillByte(a^,sizeof(VkQueueFamilyCheckpointPropertiesNV),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkQueueFamilyCheckpointPropertiesNV);
begin
  vkremitem(a,sizeof(VkQueueFamilyCheckpointPropertiesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkCheckpointDataNV);
begin
  a:=vkgetmem(sizeof(VkCheckpointDataNV));
  FillByte(a^,sizeof(VkCheckpointDataNV),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCheckpointDataNV);
begin
  vkfreemem(a,sizeof(VkCheckpointDataNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCheckpointDataNV);
begin
  vkadditem(a,sizeof(VkCheckpointDataNV));
  FillByte(a^,sizeof(VkCheckpointDataNV),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkCheckpointDataNV);
begin
  vkremitem(a,sizeof(VkCheckpointDataNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceDepthStencilResolveProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceDepthStencilResolveProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceDepthStencilResolveProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceDepthStencilResolveProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceDepthStencilResolveProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceDepthStencilResolveProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceDepthStencilResolveProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceDepthStencilResolveProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceDepthStencilResolveProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceDepthStencilResolveProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkSubpassDescriptionDepthStencilResolve);
begin
  a:=vkgetmem(sizeof(VkSubpassDescriptionDepthStencilResolve));
  a^:=default_VkSubpassDescriptionDepthStencilResolve;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSubpassDescriptionDepthStencilResolve);
begin
  vkfreemem(a,sizeof(VkSubpassDescriptionDepthStencilResolve));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSubpassDescriptionDepthStencilResolve);
begin
  vkadditem(a,sizeof(VkSubpassDescriptionDepthStencilResolve));
  a^:=default_VkSubpassDescriptionDepthStencilResolve;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSubpassDescriptionDepthStencilResolve);
begin
  vkremitem(a,sizeof(VkSubpassDescriptionDepthStencilResolve));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageViewASTCDecodeModeEXT);
begin
  a:=vkgetmem(sizeof(VkImageViewASTCDecodeModeEXT));
  a^:=default_VkImageViewASTCDecodeModeEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageViewASTCDecodeModeEXT);
begin
  vkfreemem(a,sizeof(VkImageViewASTCDecodeModeEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageViewASTCDecodeModeEXT);
begin
  vkadditem(a,sizeof(VkImageViewASTCDecodeModeEXT));
  a^:=default_VkImageViewASTCDecodeModeEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageViewASTCDecodeModeEXT);
begin
  vkremitem(a,sizeof(VkImageViewASTCDecodeModeEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceASTCDecodeFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceASTCDecodeFeaturesEXT));
  a^:=default_VkPhysicalDeviceASTCDecodeFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceASTCDecodeFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceASTCDecodeFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceASTCDecodeFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceASTCDecodeFeaturesEXT));
  a^:=default_VkPhysicalDeviceASTCDecodeFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceASTCDecodeFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceASTCDecodeFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceTransformFeedbackFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceTransformFeedbackFeaturesEXT));
  a^:=default_VkPhysicalDeviceTransformFeedbackFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceTransformFeedbackFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceTransformFeedbackFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceTransformFeedbackFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceTransformFeedbackFeaturesEXT));
  a^:=default_VkPhysicalDeviceTransformFeedbackFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceTransformFeedbackFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceTransformFeedbackFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceTransformFeedbackPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceTransformFeedbackPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceTransformFeedbackPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceTransformFeedbackPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceTransformFeedbackPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceTransformFeedbackPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceTransformFeedbackPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceTransformFeedbackPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceTransformFeedbackPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceTransformFeedbackPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineRasterizationStateStreamCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkPipelineRasterizationStateStreamCreateInfoEXT));
  a^:=default_VkPipelineRasterizationStateStreamCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineRasterizationStateStreamCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkPipelineRasterizationStateStreamCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineRasterizationStateStreamCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkPipelineRasterizationStateStreamCreateInfoEXT));
  a^:=default_VkPipelineRasterizationStateStreamCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineRasterizationStateStreamCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkPipelineRasterizationStateStreamCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceRepresentativeFragmentTestFeaturesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV));
  a^:=default_VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceRepresentativeFragmentTestFeaturesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceRepresentativeFragmentTestFeaturesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV));
  a^:=default_VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceRepresentativeFragmentTestFeaturesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineRepresentativeFragmentTestStateCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkPipelineRepresentativeFragmentTestStateCreateInfoNV));
  a^:=default_VkPipelineRepresentativeFragmentTestStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineRepresentativeFragmentTestStateCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkPipelineRepresentativeFragmentTestStateCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineRepresentativeFragmentTestStateCreateInfoNV);
begin
  vkadditem(a,sizeof(VkPipelineRepresentativeFragmentTestStateCreateInfoNV));
  a^:=default_VkPipelineRepresentativeFragmentTestStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineRepresentativeFragmentTestStateCreateInfoNV);
begin
  vkremitem(a,sizeof(VkPipelineRepresentativeFragmentTestStateCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceExclusiveScissorFeaturesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceExclusiveScissorFeaturesNV));
  a^:=default_VkPhysicalDeviceExclusiveScissorFeaturesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceExclusiveScissorFeaturesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceExclusiveScissorFeaturesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceExclusiveScissorFeaturesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceExclusiveScissorFeaturesNV));
  a^:=default_VkPhysicalDeviceExclusiveScissorFeaturesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceExclusiveScissorFeaturesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceExclusiveScissorFeaturesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineViewportExclusiveScissorStateCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkPipelineViewportExclusiveScissorStateCreateInfoNV));
  a^:=default_VkPipelineViewportExclusiveScissorStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineViewportExclusiveScissorStateCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkPipelineViewportExclusiveScissorStateCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineViewportExclusiveScissorStateCreateInfoNV);
begin
  vkadditem(a,sizeof(VkPipelineViewportExclusiveScissorStateCreateInfoNV));
  a^:=default_VkPipelineViewportExclusiveScissorStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineViewportExclusiveScissorStateCreateInfoNV);
begin
  vkremitem(a,sizeof(VkPipelineViewportExclusiveScissorStateCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceCornerSampledImageFeaturesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceCornerSampledImageFeaturesNV));
  a^:=default_VkPhysicalDeviceCornerSampledImageFeaturesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceCornerSampledImageFeaturesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceCornerSampledImageFeaturesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceCornerSampledImageFeaturesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceCornerSampledImageFeaturesNV));
  a^:=default_VkPhysicalDeviceCornerSampledImageFeaturesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceCornerSampledImageFeaturesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceCornerSampledImageFeaturesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceComputeShaderDerivativesFeaturesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceComputeShaderDerivativesFeaturesNV));
  a^:=default_VkPhysicalDeviceComputeShaderDerivativesFeaturesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceComputeShaderDerivativesFeaturesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceComputeShaderDerivativesFeaturesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceComputeShaderDerivativesFeaturesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceComputeShaderDerivativesFeaturesNV));
  a^:=default_VkPhysicalDeviceComputeShaderDerivativesFeaturesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceComputeShaderDerivativesFeaturesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceComputeShaderDerivativesFeaturesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceFragmentShaderBarycentricFeaturesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV));
  a^:=default_VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceFragmentShaderBarycentricFeaturesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceFragmentShaderBarycentricFeaturesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV));
  a^:=default_VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceFragmentShaderBarycentricFeaturesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShaderImageFootprintFeaturesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShaderImageFootprintFeaturesNV));
  a^:=default_VkPhysicalDeviceShaderImageFootprintFeaturesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShaderImageFootprintFeaturesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShaderImageFootprintFeaturesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShaderImageFootprintFeaturesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShaderImageFootprintFeaturesNV));
  a^:=default_VkPhysicalDeviceShaderImageFootprintFeaturesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShaderImageFootprintFeaturesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShaderImageFootprintFeaturesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV));
  a^:=default_VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV));
  a^:=default_VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkShadingRatePaletteNV);
begin
  a:=vkgetmem(sizeof(VkShadingRatePaletteNV));
  a^:=default_VkShadingRatePaletteNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkShadingRatePaletteNV);
begin
  vkfreemem(a,sizeof(VkShadingRatePaletteNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkShadingRatePaletteNV);
begin
  vkadditem(a,sizeof(VkShadingRatePaletteNV));
  a^:=default_VkShadingRatePaletteNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkShadingRatePaletteNV);
begin
  vkremitem(a,sizeof(VkShadingRatePaletteNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineViewportShadingRateImageStateCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkPipelineViewportShadingRateImageStateCreateInfoNV));
  a^:=default_VkPipelineViewportShadingRateImageStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineViewportShadingRateImageStateCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkPipelineViewportShadingRateImageStateCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineViewportShadingRateImageStateCreateInfoNV);
begin
  vkadditem(a,sizeof(VkPipelineViewportShadingRateImageStateCreateInfoNV));
  a^:=default_VkPipelineViewportShadingRateImageStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineViewportShadingRateImageStateCreateInfoNV);
begin
  vkremitem(a,sizeof(VkPipelineViewportShadingRateImageStateCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShadingRateImageFeaturesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShadingRateImageFeaturesNV));
  a^:=default_VkPhysicalDeviceShadingRateImageFeaturesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShadingRateImageFeaturesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShadingRateImageFeaturesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShadingRateImageFeaturesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShadingRateImageFeaturesNV));
  a^:=default_VkPhysicalDeviceShadingRateImageFeaturesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShadingRateImageFeaturesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShadingRateImageFeaturesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShadingRateImagePropertiesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShadingRateImagePropertiesNV));
  FillByte(a^,sizeof(VkPhysicalDeviceShadingRateImagePropertiesNV),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShadingRateImagePropertiesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShadingRateImagePropertiesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShadingRateImagePropertiesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShadingRateImagePropertiesNV));
  FillByte(a^,sizeof(VkPhysicalDeviceShadingRateImagePropertiesNV),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShadingRateImagePropertiesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShadingRateImagePropertiesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkCoarseSampleLocationNV);
begin
  a:=vkgetmem(sizeof(VkCoarseSampleLocationNV));
  a^:=default_VkCoarseSampleLocationNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCoarseSampleLocationNV);
begin
  vkfreemem(a,sizeof(VkCoarseSampleLocationNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCoarseSampleLocationNV);
begin
  vkadditem(a,sizeof(VkCoarseSampleLocationNV));
  a^:=default_VkCoarseSampleLocationNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkCoarseSampleLocationNV);
begin
  vkremitem(a,sizeof(VkCoarseSampleLocationNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkCoarseSampleOrderCustomNV);
begin
  a:=vkgetmem(sizeof(VkCoarseSampleOrderCustomNV));
  a^:=default_VkCoarseSampleOrderCustomNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCoarseSampleOrderCustomNV);
begin
  vkfreemem(a,sizeof(VkCoarseSampleOrderCustomNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCoarseSampleOrderCustomNV);
begin
  vkadditem(a,sizeof(VkCoarseSampleOrderCustomNV));
  a^:=default_VkCoarseSampleOrderCustomNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkCoarseSampleOrderCustomNV);
begin
  vkremitem(a,sizeof(VkCoarseSampleOrderCustomNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineViewportCoarseSampleOrderStateCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkPipelineViewportCoarseSampleOrderStateCreateInfoNV));
  a^:=default_VkPipelineViewportCoarseSampleOrderStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineViewportCoarseSampleOrderStateCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkPipelineViewportCoarseSampleOrderStateCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineViewportCoarseSampleOrderStateCreateInfoNV);
begin
  vkadditem(a,sizeof(VkPipelineViewportCoarseSampleOrderStateCreateInfoNV));
  a^:=default_VkPipelineViewportCoarseSampleOrderStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineViewportCoarseSampleOrderStateCreateInfoNV);
begin
  vkremitem(a,sizeof(VkPipelineViewportCoarseSampleOrderStateCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceMeshShaderFeaturesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceMeshShaderFeaturesNV));
  a^:=default_VkPhysicalDeviceMeshShaderFeaturesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceMeshShaderFeaturesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceMeshShaderFeaturesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceMeshShaderFeaturesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceMeshShaderFeaturesNV));
  a^:=default_VkPhysicalDeviceMeshShaderFeaturesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceMeshShaderFeaturesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceMeshShaderFeaturesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceMeshShaderPropertiesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceMeshShaderPropertiesNV));
  FillByte(a^,sizeof(VkPhysicalDeviceMeshShaderPropertiesNV),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceMeshShaderPropertiesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceMeshShaderPropertiesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceMeshShaderPropertiesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceMeshShaderPropertiesNV));
  FillByte(a^,sizeof(VkPhysicalDeviceMeshShaderPropertiesNV),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceMeshShaderPropertiesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceMeshShaderPropertiesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkDrawMeshTasksIndirectCommandNV);
begin
  a:=vkgetmem(sizeof(VkDrawMeshTasksIndirectCommandNV));
  a^:=default_VkDrawMeshTasksIndirectCommandNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDrawMeshTasksIndirectCommandNV);
begin
  vkfreemem(a,sizeof(VkDrawMeshTasksIndirectCommandNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDrawMeshTasksIndirectCommandNV);
begin
  vkadditem(a,sizeof(VkDrawMeshTasksIndirectCommandNV));
  a^:=default_VkDrawMeshTasksIndirectCommandNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDrawMeshTasksIndirectCommandNV);
begin
  vkremitem(a,sizeof(VkDrawMeshTasksIndirectCommandNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkRayTracingShaderGroupCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkRayTracingShaderGroupCreateInfoNV));
  a^:=default_VkRayTracingShaderGroupCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRayTracingShaderGroupCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkRayTracingShaderGroupCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRayTracingShaderGroupCreateInfoNV);
begin
  vkadditem(a,sizeof(VkRayTracingShaderGroupCreateInfoNV));
  a^:=default_VkRayTracingShaderGroupCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkRayTracingShaderGroupCreateInfoNV);
begin
  vkremitem(a,sizeof(VkRayTracingShaderGroupCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkRayTracingPipelineCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkRayTracingPipelineCreateInfoNV));
  a^:=default_VkRayTracingPipelineCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRayTracingPipelineCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkRayTracingPipelineCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRayTracingPipelineCreateInfoNV);
begin
  vkadditem(a,sizeof(VkRayTracingPipelineCreateInfoNV));
  a^:=default_VkRayTracingPipelineCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkRayTracingPipelineCreateInfoNV);
begin
  vkremitem(a,sizeof(VkRayTracingPipelineCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkGeometryTrianglesNV);
begin
  a:=vkgetmem(sizeof(VkGeometryTrianglesNV));
  a^:=default_VkGeometryTrianglesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkGeometryTrianglesNV);
begin
  vkfreemem(a,sizeof(VkGeometryTrianglesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkGeometryTrianglesNV);
begin
  vkadditem(a,sizeof(VkGeometryTrianglesNV));
  a^:=default_VkGeometryTrianglesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkGeometryTrianglesNV);
begin
  vkremitem(a,sizeof(VkGeometryTrianglesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkGeometryAABBNV);
begin
  a:=vkgetmem(sizeof(VkGeometryAABBNV));
  a^:=default_VkGeometryAABBNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkGeometryAABBNV);
begin
  vkfreemem(a,sizeof(VkGeometryAABBNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkGeometryAABBNV);
begin
  vkadditem(a,sizeof(VkGeometryAABBNV));
  a^:=default_VkGeometryAABBNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkGeometryAABBNV);
begin
  vkremitem(a,sizeof(VkGeometryAABBNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkGeometryDataNV);
begin
  a:=vkgetmem(sizeof(VkGeometryDataNV));
  a^:=default_VkGeometryDataNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkGeometryDataNV);
begin
  vkfreemem(a,sizeof(VkGeometryDataNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkGeometryDataNV);
begin
  vkadditem(a,sizeof(VkGeometryDataNV));
  a^:=default_VkGeometryDataNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkGeometryDataNV);
begin
  vkremitem(a,sizeof(VkGeometryDataNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkGeometryNV);
begin
  a:=vkgetmem(sizeof(VkGeometryNV));
  a^:=default_VkGeometryNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkGeometryNV);
begin
  vkfreemem(a,sizeof(VkGeometryNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkGeometryNV);
begin
  vkadditem(a,sizeof(VkGeometryNV));
  a^:=default_VkGeometryNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkGeometryNV);
begin
  vkremitem(a,sizeof(VkGeometryNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkAccelerationStructureInfoNV);
begin
  a:=vkgetmem(sizeof(VkAccelerationStructureInfoNV));
  a^:=default_VkAccelerationStructureInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAccelerationStructureInfoNV);
begin
  vkfreemem(a,sizeof(VkAccelerationStructureInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAccelerationStructureInfoNV);
begin
  vkadditem(a,sizeof(VkAccelerationStructureInfoNV));
  a^:=default_VkAccelerationStructureInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAccelerationStructureInfoNV);
begin
  vkremitem(a,sizeof(VkAccelerationStructureInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkAccelerationStructureCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkAccelerationStructureCreateInfoNV));
  a^:=default_VkAccelerationStructureCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAccelerationStructureCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkAccelerationStructureCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAccelerationStructureCreateInfoNV);
begin
  vkadditem(a,sizeof(VkAccelerationStructureCreateInfoNV));
  a^:=default_VkAccelerationStructureCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAccelerationStructureCreateInfoNV);
begin
  vkremitem(a,sizeof(VkAccelerationStructureCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkBindAccelerationStructureMemoryInfoNV);
begin
  a:=vkgetmem(sizeof(VkBindAccelerationStructureMemoryInfoNV));
  a^:=default_VkBindAccelerationStructureMemoryInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBindAccelerationStructureMemoryInfoNV);
begin
  vkfreemem(a,sizeof(VkBindAccelerationStructureMemoryInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBindAccelerationStructureMemoryInfoNV);
begin
  vkadditem(a,sizeof(VkBindAccelerationStructureMemoryInfoNV));
  a^:=default_VkBindAccelerationStructureMemoryInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBindAccelerationStructureMemoryInfoNV);
begin
  vkremitem(a,sizeof(VkBindAccelerationStructureMemoryInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkWriteDescriptorSetAccelerationStructureNV);
begin
  a:=vkgetmem(sizeof(VkWriteDescriptorSetAccelerationStructureNV));
  a^:=default_VkWriteDescriptorSetAccelerationStructureNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkWriteDescriptorSetAccelerationStructureNV);
begin
  vkfreemem(a,sizeof(VkWriteDescriptorSetAccelerationStructureNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkWriteDescriptorSetAccelerationStructureNV);
begin
  vkadditem(a,sizeof(VkWriteDescriptorSetAccelerationStructureNV));
  a^:=default_VkWriteDescriptorSetAccelerationStructureNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkWriteDescriptorSetAccelerationStructureNV);
begin
  vkremitem(a,sizeof(VkWriteDescriptorSetAccelerationStructureNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkAccelerationStructureMemoryRequirementsInfoNV);
begin
  a:=vkgetmem(sizeof(VkAccelerationStructureMemoryRequirementsInfoNV));
  a^:=default_VkAccelerationStructureMemoryRequirementsInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAccelerationStructureMemoryRequirementsInfoNV);
begin
  vkfreemem(a,sizeof(VkAccelerationStructureMemoryRequirementsInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAccelerationStructureMemoryRequirementsInfoNV);
begin
  vkadditem(a,sizeof(VkAccelerationStructureMemoryRequirementsInfoNV));
  a^:=default_VkAccelerationStructureMemoryRequirementsInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAccelerationStructureMemoryRequirementsInfoNV);
begin
  vkremitem(a,sizeof(VkAccelerationStructureMemoryRequirementsInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceRayTracingPropertiesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceRayTracingPropertiesNV));
  FillByte(a^,sizeof(VkPhysicalDeviceRayTracingPropertiesNV),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceRayTracingPropertiesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceRayTracingPropertiesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceRayTracingPropertiesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceRayTracingPropertiesNV));
  FillByte(a^,sizeof(VkPhysicalDeviceRayTracingPropertiesNV),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceRayTracingPropertiesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceRayTracingPropertiesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkDrmFormatModifierPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkDrmFormatModifierPropertiesEXT));
  FillByte(a^,sizeof(VkDrmFormatModifierPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDrmFormatModifierPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkDrmFormatModifierPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDrmFormatModifierPropertiesEXT);
begin
  vkadditem(a,sizeof(VkDrmFormatModifierPropertiesEXT));
  FillByte(a^,sizeof(VkDrmFormatModifierPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDrmFormatModifierPropertiesEXT);
begin
  vkremitem(a,sizeof(VkDrmFormatModifierPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceImageDrmFormatModifierInfoEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceImageDrmFormatModifierInfoEXT));
  a^:=default_VkPhysicalDeviceImageDrmFormatModifierInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceImageDrmFormatModifierInfoEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceImageDrmFormatModifierInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceImageDrmFormatModifierInfoEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceImageDrmFormatModifierInfoEXT));
  a^:=default_VkPhysicalDeviceImageDrmFormatModifierInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceImageDrmFormatModifierInfoEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceImageDrmFormatModifierInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageDrmFormatModifierListCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkImageDrmFormatModifierListCreateInfoEXT));
  a^:=default_VkImageDrmFormatModifierListCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageDrmFormatModifierListCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkImageDrmFormatModifierListCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageDrmFormatModifierListCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkImageDrmFormatModifierListCreateInfoEXT));
  a^:=default_VkImageDrmFormatModifierListCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageDrmFormatModifierListCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkImageDrmFormatModifierListCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageDrmFormatModifierExplicitCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkImageDrmFormatModifierExplicitCreateInfoEXT));
  a^:=default_VkImageDrmFormatModifierExplicitCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageDrmFormatModifierExplicitCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkImageDrmFormatModifierExplicitCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageDrmFormatModifierExplicitCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkImageDrmFormatModifierExplicitCreateInfoEXT));
  a^:=default_VkImageDrmFormatModifierExplicitCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageDrmFormatModifierExplicitCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkImageDrmFormatModifierExplicitCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageDrmFormatModifierPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkImageDrmFormatModifierPropertiesEXT));
  FillByte(a^,sizeof(VkImageDrmFormatModifierPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageDrmFormatModifierPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkImageDrmFormatModifierPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageDrmFormatModifierPropertiesEXT);
begin
  vkadditem(a,sizeof(VkImageDrmFormatModifierPropertiesEXT));
  FillByte(a^,sizeof(VkImageDrmFormatModifierPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageDrmFormatModifierPropertiesEXT);
begin
  vkremitem(a,sizeof(VkImageDrmFormatModifierPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageStencilUsageCreateInfo);
begin
  a:=vkgetmem(sizeof(VkImageStencilUsageCreateInfo));
  a^:=default_VkImageStencilUsageCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageStencilUsageCreateInfo);
begin
  vkfreemem(a,sizeof(VkImageStencilUsageCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageStencilUsageCreateInfo);
begin
  vkadditem(a,sizeof(VkImageStencilUsageCreateInfo));
  a^:=default_VkImageStencilUsageCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageStencilUsageCreateInfo);
begin
  vkremitem(a,sizeof(VkImageStencilUsageCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceMemoryOverallocationCreateInfoAMD);
begin
  a:=vkgetmem(sizeof(VkDeviceMemoryOverallocationCreateInfoAMD));
  a^:=default_VkDeviceMemoryOverallocationCreateInfoAMD;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceMemoryOverallocationCreateInfoAMD);
begin
  vkfreemem(a,sizeof(VkDeviceMemoryOverallocationCreateInfoAMD));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceMemoryOverallocationCreateInfoAMD);
begin
  vkadditem(a,sizeof(VkDeviceMemoryOverallocationCreateInfoAMD));
  a^:=default_VkDeviceMemoryOverallocationCreateInfoAMD;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceMemoryOverallocationCreateInfoAMD);
begin
  vkremitem(a,sizeof(VkDeviceMemoryOverallocationCreateInfoAMD));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceFragmentDensityMapFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceFragmentDensityMapFeaturesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceFragmentDensityMapFeaturesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceFragmentDensityMapFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceFragmentDensityMapFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceFragmentDensityMapFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceFragmentDensityMapFeaturesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceFragmentDensityMapFeaturesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceFragmentDensityMapFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceFragmentDensityMapFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceFragmentDensityMapPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceFragmentDensityMapPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceFragmentDensityMapPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceFragmentDensityMapPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceFragmentDensityMapPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceFragmentDensityMapPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceFragmentDensityMapPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceFragmentDensityMapPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceFragmentDensityMapPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceFragmentDensityMapPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkRenderPassFragmentDensityMapCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkRenderPassFragmentDensityMapCreateInfoEXT));
  a^:=default_VkRenderPassFragmentDensityMapCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRenderPassFragmentDensityMapCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkRenderPassFragmentDensityMapCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRenderPassFragmentDensityMapCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkRenderPassFragmentDensityMapCreateInfoEXT));
  a^:=default_VkRenderPassFragmentDensityMapCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkRenderPassFragmentDensityMapCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkRenderPassFragmentDensityMapCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceScalarBlockLayoutFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceScalarBlockLayoutFeatures));
  a^:=default_VkPhysicalDeviceScalarBlockLayoutFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceScalarBlockLayoutFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceScalarBlockLayoutFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceScalarBlockLayoutFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceScalarBlockLayoutFeatures));
  a^:=default_VkPhysicalDeviceScalarBlockLayoutFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceScalarBlockLayoutFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceScalarBlockLayoutFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkSurfaceProtectedCapabilitiesKHR);
begin
  a:=vkgetmem(sizeof(VkSurfaceProtectedCapabilitiesKHR));
  a^:=default_VkSurfaceProtectedCapabilitiesKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSurfaceProtectedCapabilitiesKHR);
begin
  vkfreemem(a,sizeof(VkSurfaceProtectedCapabilitiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSurfaceProtectedCapabilitiesKHR);
begin
  vkadditem(a,sizeof(VkSurfaceProtectedCapabilitiesKHR));
  a^:=default_VkSurfaceProtectedCapabilitiesKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSurfaceProtectedCapabilitiesKHR);
begin
  vkremitem(a,sizeof(VkSurfaceProtectedCapabilitiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceUniformBufferStandardLayoutFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceUniformBufferStandardLayoutFeatures));
  a^:=default_VkPhysicalDeviceUniformBufferStandardLayoutFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceUniformBufferStandardLayoutFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceUniformBufferStandardLayoutFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceUniformBufferStandardLayoutFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceUniformBufferStandardLayoutFeatures));
  a^:=default_VkPhysicalDeviceUniformBufferStandardLayoutFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceUniformBufferStandardLayoutFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceUniformBufferStandardLayoutFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceDepthClipEnableFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceDepthClipEnableFeaturesEXT));
  a^:=default_VkPhysicalDeviceDepthClipEnableFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceDepthClipEnableFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceDepthClipEnableFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceDepthClipEnableFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceDepthClipEnableFeaturesEXT));
  a^:=default_VkPhysicalDeviceDepthClipEnableFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceDepthClipEnableFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceDepthClipEnableFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineRasterizationDepthClipStateCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkPipelineRasterizationDepthClipStateCreateInfoEXT));
  a^:=default_VkPipelineRasterizationDepthClipStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineRasterizationDepthClipStateCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkPipelineRasterizationDepthClipStateCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineRasterizationDepthClipStateCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkPipelineRasterizationDepthClipStateCreateInfoEXT));
  a^:=default_VkPipelineRasterizationDepthClipStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineRasterizationDepthClipStateCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkPipelineRasterizationDepthClipStateCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceMemoryBudgetPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceMemoryBudgetPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceMemoryBudgetPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceMemoryBudgetPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceMemoryBudgetPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceMemoryBudgetPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceMemoryBudgetPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceMemoryBudgetPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceMemoryBudgetPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceMemoryBudgetPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceMemoryPriorityFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceMemoryPriorityFeaturesEXT));
  a^:=default_VkPhysicalDeviceMemoryPriorityFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceMemoryPriorityFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceMemoryPriorityFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceMemoryPriorityFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceMemoryPriorityFeaturesEXT));
  a^:=default_VkPhysicalDeviceMemoryPriorityFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceMemoryPriorityFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceMemoryPriorityFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryPriorityAllocateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkMemoryPriorityAllocateInfoEXT));
  a^:=default_VkMemoryPriorityAllocateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryPriorityAllocateInfoEXT);
begin
  vkfreemem(a,sizeof(VkMemoryPriorityAllocateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryPriorityAllocateInfoEXT);
begin
  vkadditem(a,sizeof(VkMemoryPriorityAllocateInfoEXT));
  a^:=default_VkMemoryPriorityAllocateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryPriorityAllocateInfoEXT);
begin
  vkremitem(a,sizeof(VkMemoryPriorityAllocateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceBufferDeviceAddressFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceBufferDeviceAddressFeatures));
  a^:=default_VkPhysicalDeviceBufferDeviceAddressFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceBufferDeviceAddressFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceBufferDeviceAddressFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceBufferDeviceAddressFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceBufferDeviceAddressFeatures));
  a^:=default_VkPhysicalDeviceBufferDeviceAddressFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceBufferDeviceAddressFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceBufferDeviceAddressFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceBufferDeviceAddressFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceBufferDeviceAddressFeaturesEXT));
  a^:=default_VkPhysicalDeviceBufferDeviceAddressFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceBufferDeviceAddressFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceBufferDeviceAddressFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceBufferDeviceAddressFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceBufferDeviceAddressFeaturesEXT));
  a^:=default_VkPhysicalDeviceBufferDeviceAddressFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceBufferDeviceAddressFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceBufferDeviceAddressFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkBufferDeviceAddressInfo);
begin
  a:=vkgetmem(sizeof(VkBufferDeviceAddressInfo));
  a^:=default_VkBufferDeviceAddressInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBufferDeviceAddressInfo);
begin
  vkfreemem(a,sizeof(VkBufferDeviceAddressInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBufferDeviceAddressInfo);
begin
  vkadditem(a,sizeof(VkBufferDeviceAddressInfo));
  a^:=default_VkBufferDeviceAddressInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBufferDeviceAddressInfo);
begin
  vkremitem(a,sizeof(VkBufferDeviceAddressInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkBufferOpaqueCaptureAddressCreateInfo);
begin
  a:=vkgetmem(sizeof(VkBufferOpaqueCaptureAddressCreateInfo));
  a^:=default_VkBufferOpaqueCaptureAddressCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBufferOpaqueCaptureAddressCreateInfo);
begin
  vkfreemem(a,sizeof(VkBufferOpaqueCaptureAddressCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBufferOpaqueCaptureAddressCreateInfo);
begin
  vkadditem(a,sizeof(VkBufferOpaqueCaptureAddressCreateInfo));
  a^:=default_VkBufferOpaqueCaptureAddressCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBufferOpaqueCaptureAddressCreateInfo);
begin
  vkremitem(a,sizeof(VkBufferOpaqueCaptureAddressCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkBufferDeviceAddressCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkBufferDeviceAddressCreateInfoEXT));
  a^:=default_VkBufferDeviceAddressCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkBufferDeviceAddressCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkBufferDeviceAddressCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkBufferDeviceAddressCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkBufferDeviceAddressCreateInfoEXT));
  a^:=default_VkBufferDeviceAddressCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkBufferDeviceAddressCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkBufferDeviceAddressCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceImageViewImageFormatInfoEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceImageViewImageFormatInfoEXT));
  a^:=default_VkPhysicalDeviceImageViewImageFormatInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceImageViewImageFormatInfoEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceImageViewImageFormatInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceImageViewImageFormatInfoEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceImageViewImageFormatInfoEXT));
  a^:=default_VkPhysicalDeviceImageViewImageFormatInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceImageViewImageFormatInfoEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceImageViewImageFormatInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkFilterCubicImageViewImageFormatPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkFilterCubicImageViewImageFormatPropertiesEXT));
  FillByte(a^,sizeof(VkFilterCubicImageViewImageFormatPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkFilterCubicImageViewImageFormatPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkFilterCubicImageViewImageFormatPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkFilterCubicImageViewImageFormatPropertiesEXT);
begin
  vkadditem(a,sizeof(VkFilterCubicImageViewImageFormatPropertiesEXT));
  FillByte(a^,sizeof(VkFilterCubicImageViewImageFormatPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkFilterCubicImageViewImageFormatPropertiesEXT);
begin
  vkremitem(a,sizeof(VkFilterCubicImageViewImageFormatPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceImagelessFramebufferFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceImagelessFramebufferFeatures));
  a^:=default_VkPhysicalDeviceImagelessFramebufferFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceImagelessFramebufferFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceImagelessFramebufferFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceImagelessFramebufferFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceImagelessFramebufferFeatures));
  a^:=default_VkPhysicalDeviceImagelessFramebufferFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceImagelessFramebufferFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceImagelessFramebufferFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkFramebufferAttachmentImageInfo);
begin
  a:=vkgetmem(sizeof(VkFramebufferAttachmentImageInfo));
  a^:=default_VkFramebufferAttachmentImageInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkFramebufferAttachmentImageInfo);
begin
  vkfreemem(a,sizeof(VkFramebufferAttachmentImageInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkFramebufferAttachmentImageInfo);
begin
  vkadditem(a,sizeof(VkFramebufferAttachmentImageInfo));
  a^:=default_VkFramebufferAttachmentImageInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkFramebufferAttachmentImageInfo);
begin
  vkremitem(a,sizeof(VkFramebufferAttachmentImageInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkRenderPassAttachmentBeginInfo);
begin
  a:=vkgetmem(sizeof(VkRenderPassAttachmentBeginInfo));
  a^:=default_VkRenderPassAttachmentBeginInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRenderPassAttachmentBeginInfo);
begin
  vkfreemem(a,sizeof(VkRenderPassAttachmentBeginInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRenderPassAttachmentBeginInfo);
begin
  vkadditem(a,sizeof(VkRenderPassAttachmentBeginInfo));
  a^:=default_VkRenderPassAttachmentBeginInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkRenderPassAttachmentBeginInfo);
begin
  vkremitem(a,sizeof(VkRenderPassAttachmentBeginInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT));
  a^:=default_VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT));
  a^:=default_VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceCooperativeMatrixFeaturesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceCooperativeMatrixFeaturesNV));
  a^:=default_VkPhysicalDeviceCooperativeMatrixFeaturesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceCooperativeMatrixFeaturesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceCooperativeMatrixFeaturesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceCooperativeMatrixFeaturesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceCooperativeMatrixFeaturesNV));
  a^:=default_VkPhysicalDeviceCooperativeMatrixFeaturesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceCooperativeMatrixFeaturesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceCooperativeMatrixFeaturesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceCooperativeMatrixPropertiesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceCooperativeMatrixPropertiesNV));
  FillByte(a^,sizeof(VkPhysicalDeviceCooperativeMatrixPropertiesNV),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceCooperativeMatrixPropertiesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceCooperativeMatrixPropertiesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceCooperativeMatrixPropertiesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceCooperativeMatrixPropertiesNV));
  FillByte(a^,sizeof(VkPhysicalDeviceCooperativeMatrixPropertiesNV),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceCooperativeMatrixPropertiesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceCooperativeMatrixPropertiesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkCooperativeMatrixPropertiesNV);
begin
  a:=vkgetmem(sizeof(VkCooperativeMatrixPropertiesNV));
  a^:=default_VkCooperativeMatrixPropertiesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkCooperativeMatrixPropertiesNV);
begin
  vkfreemem(a,sizeof(VkCooperativeMatrixPropertiesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkCooperativeMatrixPropertiesNV);
begin
  vkadditem(a,sizeof(VkCooperativeMatrixPropertiesNV));
  a^:=default_VkCooperativeMatrixPropertiesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkCooperativeMatrixPropertiesNV);
begin
  vkremitem(a,sizeof(VkCooperativeMatrixPropertiesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceYcbcrImageArraysFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceYcbcrImageArraysFeaturesEXT));
  a^:=default_VkPhysicalDeviceYcbcrImageArraysFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceYcbcrImageArraysFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceYcbcrImageArraysFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceYcbcrImageArraysFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceYcbcrImageArraysFeaturesEXT));
  a^:=default_VkPhysicalDeviceYcbcrImageArraysFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceYcbcrImageArraysFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceYcbcrImageArraysFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkImageViewHandleInfoNVX);
begin
  a:=vkgetmem(sizeof(VkImageViewHandleInfoNVX));
  a^:=default_VkImageViewHandleInfoNVX;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkImageViewHandleInfoNVX);
begin
  vkfreemem(a,sizeof(VkImageViewHandleInfoNVX));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkImageViewHandleInfoNVX);
begin
  vkadditem(a,sizeof(VkImageViewHandleInfoNVX));
  a^:=default_VkImageViewHandleInfoNVX;
end;
procedure T_vulkan_auto.vkrem(var a:PVkImageViewHandleInfoNVX);
begin
  vkremitem(a,sizeof(VkImageViewHandleInfoNVX));
end;

procedure T_vulkan_auto.vknew(out a:PVkPresentFrameTokenGGP);
begin
  a:=vkgetmem(sizeof(VkPresentFrameTokenGGP));
  a^:=default_VkPresentFrameTokenGGP;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPresentFrameTokenGGP);
begin
  vkfreemem(a,sizeof(VkPresentFrameTokenGGP));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPresentFrameTokenGGP);
begin
  vkadditem(a,sizeof(VkPresentFrameTokenGGP));
  a^:=default_VkPresentFrameTokenGGP;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPresentFrameTokenGGP);
begin
  vkremitem(a,sizeof(VkPresentFrameTokenGGP));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineCreationFeedbackEXT);
begin
  a:=vkgetmem(sizeof(VkPipelineCreationFeedbackEXT));
  FillByte(a^,sizeof(VkPipelineCreationFeedbackEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineCreationFeedbackEXT);
begin
  vkfreemem(a,sizeof(VkPipelineCreationFeedbackEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineCreationFeedbackEXT);
begin
  vkadditem(a,sizeof(VkPipelineCreationFeedbackEXT));
  FillByte(a^,sizeof(VkPipelineCreationFeedbackEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineCreationFeedbackEXT);
begin
  vkremitem(a,sizeof(VkPipelineCreationFeedbackEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineCreationFeedbackCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkPipelineCreationFeedbackCreateInfoEXT));
  a^:=default_VkPipelineCreationFeedbackCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineCreationFeedbackCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkPipelineCreationFeedbackCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineCreationFeedbackCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkPipelineCreationFeedbackCreateInfoEXT));
  a^:=default_VkPipelineCreationFeedbackCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineCreationFeedbackCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkPipelineCreationFeedbackCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkSurfaceFullScreenExclusiveInfoEXT);
begin
  a:=vkgetmem(sizeof(VkSurfaceFullScreenExclusiveInfoEXT));
  a^:=default_VkSurfaceFullScreenExclusiveInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSurfaceFullScreenExclusiveInfoEXT);
begin
  vkfreemem(a,sizeof(VkSurfaceFullScreenExclusiveInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSurfaceFullScreenExclusiveInfoEXT);
begin
  vkadditem(a,sizeof(VkSurfaceFullScreenExclusiveInfoEXT));
  a^:=default_VkSurfaceFullScreenExclusiveInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSurfaceFullScreenExclusiveInfoEXT);
begin
  vkremitem(a,sizeof(VkSurfaceFullScreenExclusiveInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkSurfaceFullScreenExclusiveWin32InfoEXT);
begin
  a:=vkgetmem(sizeof(VkSurfaceFullScreenExclusiveWin32InfoEXT));
  a^:=default_VkSurfaceFullScreenExclusiveWin32InfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSurfaceFullScreenExclusiveWin32InfoEXT);
begin
  vkfreemem(a,sizeof(VkSurfaceFullScreenExclusiveWin32InfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSurfaceFullScreenExclusiveWin32InfoEXT);
begin
  vkadditem(a,sizeof(VkSurfaceFullScreenExclusiveWin32InfoEXT));
  a^:=default_VkSurfaceFullScreenExclusiveWin32InfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSurfaceFullScreenExclusiveWin32InfoEXT);
begin
  vkremitem(a,sizeof(VkSurfaceFullScreenExclusiveWin32InfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkSurfaceCapabilitiesFullScreenExclusiveEXT);
begin
  a:=vkgetmem(sizeof(VkSurfaceCapabilitiesFullScreenExclusiveEXT));
  a^:=default_VkSurfaceCapabilitiesFullScreenExclusiveEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkSurfaceCapabilitiesFullScreenExclusiveEXT);
begin
  vkfreemem(a,sizeof(VkSurfaceCapabilitiesFullScreenExclusiveEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkSurfaceCapabilitiesFullScreenExclusiveEXT);
begin
  vkadditem(a,sizeof(VkSurfaceCapabilitiesFullScreenExclusiveEXT));
  a^:=default_VkSurfaceCapabilitiesFullScreenExclusiveEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkSurfaceCapabilitiesFullScreenExclusiveEXT);
begin
  vkremitem(a,sizeof(VkSurfaceCapabilitiesFullScreenExclusiveEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDevicePerformanceQueryFeaturesKHR);
begin
  a:=vkgetmem(sizeof(VkPhysicalDevicePerformanceQueryFeaturesKHR));
  a^:=default_VkPhysicalDevicePerformanceQueryFeaturesKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDevicePerformanceQueryFeaturesKHR);
begin
  vkfreemem(a,sizeof(VkPhysicalDevicePerformanceQueryFeaturesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDevicePerformanceQueryFeaturesKHR);
begin
  vkadditem(a,sizeof(VkPhysicalDevicePerformanceQueryFeaturesKHR));
  a^:=default_VkPhysicalDevicePerformanceQueryFeaturesKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDevicePerformanceQueryFeaturesKHR);
begin
  vkremitem(a,sizeof(VkPhysicalDevicePerformanceQueryFeaturesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDevicePerformanceQueryPropertiesKHR);
begin
  a:=vkgetmem(sizeof(VkPhysicalDevicePerformanceQueryPropertiesKHR));
  FillByte(a^,sizeof(VkPhysicalDevicePerformanceQueryPropertiesKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDevicePerformanceQueryPropertiesKHR);
begin
  vkfreemem(a,sizeof(VkPhysicalDevicePerformanceQueryPropertiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDevicePerformanceQueryPropertiesKHR);
begin
  vkadditem(a,sizeof(VkPhysicalDevicePerformanceQueryPropertiesKHR));
  FillByte(a^,sizeof(VkPhysicalDevicePerformanceQueryPropertiesKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDevicePerformanceQueryPropertiesKHR);
begin
  vkremitem(a,sizeof(VkPhysicalDevicePerformanceQueryPropertiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPerformanceCounterKHR);
begin
  a:=vkgetmem(sizeof(VkPerformanceCounterKHR));
  FillByte(a^,sizeof(VkPerformanceCounterKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPerformanceCounterKHR);
begin
  vkfreemem(a,sizeof(VkPerformanceCounterKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPerformanceCounterKHR);
begin
  vkadditem(a,sizeof(VkPerformanceCounterKHR));
  FillByte(a^,sizeof(VkPerformanceCounterKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPerformanceCounterKHR);
begin
  vkremitem(a,sizeof(VkPerformanceCounterKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPerformanceCounterDescriptionKHR);
begin
  a:=vkgetmem(sizeof(VkPerformanceCounterDescriptionKHR));
  FillByte(a^,sizeof(VkPerformanceCounterDescriptionKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPerformanceCounterDescriptionKHR);
begin
  vkfreemem(a,sizeof(VkPerformanceCounterDescriptionKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPerformanceCounterDescriptionKHR);
begin
  vkadditem(a,sizeof(VkPerformanceCounterDescriptionKHR));
  FillByte(a^,sizeof(VkPerformanceCounterDescriptionKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPerformanceCounterDescriptionKHR);
begin
  vkremitem(a,sizeof(VkPerformanceCounterDescriptionKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkQueryPoolPerformanceCreateInfoKHR);
begin
  a:=vkgetmem(sizeof(VkQueryPoolPerformanceCreateInfoKHR));
  a^:=default_VkQueryPoolPerformanceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkQueryPoolPerformanceCreateInfoKHR);
begin
  vkfreemem(a,sizeof(VkQueryPoolPerformanceCreateInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkQueryPoolPerformanceCreateInfoKHR);
begin
  vkadditem(a,sizeof(VkQueryPoolPerformanceCreateInfoKHR));
  a^:=default_VkQueryPoolPerformanceCreateInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkQueryPoolPerformanceCreateInfoKHR);
begin
  vkremitem(a,sizeof(VkQueryPoolPerformanceCreateInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPerformanceCounterResultKHR);
begin
  a:=vkgetmem(sizeof(VkPerformanceCounterResultKHR));
  a^:=default_VkPerformanceCounterResultKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPerformanceCounterResultKHR);
begin
  vkfreemem(a,sizeof(VkPerformanceCounterResultKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPerformanceCounterResultKHR);
begin
  vkadditem(a,sizeof(VkPerformanceCounterResultKHR));
  a^:=default_VkPerformanceCounterResultKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPerformanceCounterResultKHR);
begin
  vkremitem(a,sizeof(VkPerformanceCounterResultKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkAcquireProfilingLockInfoKHR);
begin
  a:=vkgetmem(sizeof(VkAcquireProfilingLockInfoKHR));
  a^:=default_VkAcquireProfilingLockInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAcquireProfilingLockInfoKHR);
begin
  vkfreemem(a,sizeof(VkAcquireProfilingLockInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAcquireProfilingLockInfoKHR);
begin
  vkadditem(a,sizeof(VkAcquireProfilingLockInfoKHR));
  a^:=default_VkAcquireProfilingLockInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAcquireProfilingLockInfoKHR);
begin
  vkremitem(a,sizeof(VkAcquireProfilingLockInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPerformanceQuerySubmitInfoKHR);
begin
  a:=vkgetmem(sizeof(VkPerformanceQuerySubmitInfoKHR));
  a^:=default_VkPerformanceQuerySubmitInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPerformanceQuerySubmitInfoKHR);
begin
  vkfreemem(a,sizeof(VkPerformanceQuerySubmitInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPerformanceQuerySubmitInfoKHR);
begin
  vkadditem(a,sizeof(VkPerformanceQuerySubmitInfoKHR));
  a^:=default_VkPerformanceQuerySubmitInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPerformanceQuerySubmitInfoKHR);
begin
  vkremitem(a,sizeof(VkPerformanceQuerySubmitInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkHeadlessSurfaceCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkHeadlessSurfaceCreateInfoEXT));
  a^:=default_VkHeadlessSurfaceCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkHeadlessSurfaceCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkHeadlessSurfaceCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkHeadlessSurfaceCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkHeadlessSurfaceCreateInfoEXT));
  a^:=default_VkHeadlessSurfaceCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkHeadlessSurfaceCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkHeadlessSurfaceCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceCoverageReductionModeFeaturesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceCoverageReductionModeFeaturesNV));
  a^:=default_VkPhysicalDeviceCoverageReductionModeFeaturesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceCoverageReductionModeFeaturesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceCoverageReductionModeFeaturesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceCoverageReductionModeFeaturesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceCoverageReductionModeFeaturesNV));
  a^:=default_VkPhysicalDeviceCoverageReductionModeFeaturesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceCoverageReductionModeFeaturesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceCoverageReductionModeFeaturesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineCoverageReductionStateCreateInfoNV);
begin
  a:=vkgetmem(sizeof(VkPipelineCoverageReductionStateCreateInfoNV));
  a^:=default_VkPipelineCoverageReductionStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineCoverageReductionStateCreateInfoNV);
begin
  vkfreemem(a,sizeof(VkPipelineCoverageReductionStateCreateInfoNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineCoverageReductionStateCreateInfoNV);
begin
  vkadditem(a,sizeof(VkPipelineCoverageReductionStateCreateInfoNV));
  a^:=default_VkPipelineCoverageReductionStateCreateInfoNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineCoverageReductionStateCreateInfoNV);
begin
  vkremitem(a,sizeof(VkPipelineCoverageReductionStateCreateInfoNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkFramebufferMixedSamplesCombinationNV);
begin
  a:=vkgetmem(sizeof(VkFramebufferMixedSamplesCombinationNV));
  FillByte(a^,sizeof(VkFramebufferMixedSamplesCombinationNV),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkFramebufferMixedSamplesCombinationNV);
begin
  vkfreemem(a,sizeof(VkFramebufferMixedSamplesCombinationNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkFramebufferMixedSamplesCombinationNV);
begin
  vkadditem(a,sizeof(VkFramebufferMixedSamplesCombinationNV));
  FillByte(a^,sizeof(VkFramebufferMixedSamplesCombinationNV),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkFramebufferMixedSamplesCombinationNV);
begin
  vkremitem(a,sizeof(VkFramebufferMixedSamplesCombinationNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL));
  a^:=default_VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL));
  a^:=default_VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL));
end;

procedure T_vulkan_auto.vknew(out a:PVkPerformanceValueDataINTEL);
begin
  a:=vkgetmem(sizeof(VkPerformanceValueDataINTEL));
  a^:=default_VkPerformanceValueDataINTEL;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPerformanceValueDataINTEL);
begin
  vkfreemem(a,sizeof(VkPerformanceValueDataINTEL));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPerformanceValueDataINTEL);
begin
  vkadditem(a,sizeof(VkPerformanceValueDataINTEL));
  a^:=default_VkPerformanceValueDataINTEL;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPerformanceValueDataINTEL);
begin
  vkremitem(a,sizeof(VkPerformanceValueDataINTEL));
end;

procedure T_vulkan_auto.vknew(out a:PVkPerformanceValueINTEL);
begin
  a:=vkgetmem(sizeof(VkPerformanceValueINTEL));
  a^:=default_VkPerformanceValueINTEL;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPerformanceValueINTEL);
begin
  vkfreemem(a,sizeof(VkPerformanceValueINTEL));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPerformanceValueINTEL);
begin
  vkadditem(a,sizeof(VkPerformanceValueINTEL));
  a^:=default_VkPerformanceValueINTEL;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPerformanceValueINTEL);
begin
  vkremitem(a,sizeof(VkPerformanceValueINTEL));
end;

procedure T_vulkan_auto.vknew(out a:PVkInitializePerformanceApiInfoINTEL);
begin
  a:=vkgetmem(sizeof(VkInitializePerformanceApiInfoINTEL));
  a^:=default_VkInitializePerformanceApiInfoINTEL;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkInitializePerformanceApiInfoINTEL);
begin
  vkfreemem(a,sizeof(VkInitializePerformanceApiInfoINTEL));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkInitializePerformanceApiInfoINTEL);
begin
  vkadditem(a,sizeof(VkInitializePerformanceApiInfoINTEL));
  a^:=default_VkInitializePerformanceApiInfoINTEL;
end;
procedure T_vulkan_auto.vkrem(var a:PVkInitializePerformanceApiInfoINTEL);
begin
  vkremitem(a,sizeof(VkInitializePerformanceApiInfoINTEL));
end;

procedure T_vulkan_auto.vknew(out a:PVkQueryPoolCreateInfoINTEL);
begin
  a:=vkgetmem(sizeof(VkQueryPoolCreateInfoINTEL));
  a^:=default_VkQueryPoolCreateInfoINTEL;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkQueryPoolCreateInfoINTEL);
begin
  vkfreemem(a,sizeof(VkQueryPoolCreateInfoINTEL));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkQueryPoolCreateInfoINTEL);
begin
  vkadditem(a,sizeof(VkQueryPoolCreateInfoINTEL));
  a^:=default_VkQueryPoolCreateInfoINTEL;
end;
procedure T_vulkan_auto.vkrem(var a:PVkQueryPoolCreateInfoINTEL);
begin
  vkremitem(a,sizeof(VkQueryPoolCreateInfoINTEL));
end;

procedure T_vulkan_auto.vknew(out a:PVkPerformanceMarkerInfoINTEL);
begin
  a:=vkgetmem(sizeof(VkPerformanceMarkerInfoINTEL));
  a^:=default_VkPerformanceMarkerInfoINTEL;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPerformanceMarkerInfoINTEL);
begin
  vkfreemem(a,sizeof(VkPerformanceMarkerInfoINTEL));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPerformanceMarkerInfoINTEL);
begin
  vkadditem(a,sizeof(VkPerformanceMarkerInfoINTEL));
  a^:=default_VkPerformanceMarkerInfoINTEL;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPerformanceMarkerInfoINTEL);
begin
  vkremitem(a,sizeof(VkPerformanceMarkerInfoINTEL));
end;

procedure T_vulkan_auto.vknew(out a:PVkPerformanceStreamMarkerInfoINTEL);
begin
  a:=vkgetmem(sizeof(VkPerformanceStreamMarkerInfoINTEL));
  a^:=default_VkPerformanceStreamMarkerInfoINTEL;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPerformanceStreamMarkerInfoINTEL);
begin
  vkfreemem(a,sizeof(VkPerformanceStreamMarkerInfoINTEL));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPerformanceStreamMarkerInfoINTEL);
begin
  vkadditem(a,sizeof(VkPerformanceStreamMarkerInfoINTEL));
  a^:=default_VkPerformanceStreamMarkerInfoINTEL;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPerformanceStreamMarkerInfoINTEL);
begin
  vkremitem(a,sizeof(VkPerformanceStreamMarkerInfoINTEL));
end;

procedure T_vulkan_auto.vknew(out a:PVkPerformanceOverrideInfoINTEL);
begin
  a:=vkgetmem(sizeof(VkPerformanceOverrideInfoINTEL));
  a^:=default_VkPerformanceOverrideInfoINTEL;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPerformanceOverrideInfoINTEL);
begin
  vkfreemem(a,sizeof(VkPerformanceOverrideInfoINTEL));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPerformanceOverrideInfoINTEL);
begin
  vkadditem(a,sizeof(VkPerformanceOverrideInfoINTEL));
  a^:=default_VkPerformanceOverrideInfoINTEL;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPerformanceOverrideInfoINTEL);
begin
  vkremitem(a,sizeof(VkPerformanceOverrideInfoINTEL));
end;

procedure T_vulkan_auto.vknew(out a:PVkPerformanceConfigurationAcquireInfoINTEL);
begin
  a:=vkgetmem(sizeof(VkPerformanceConfigurationAcquireInfoINTEL));
  a^:=default_VkPerformanceConfigurationAcquireInfoINTEL;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPerformanceConfigurationAcquireInfoINTEL);
begin
  vkfreemem(a,sizeof(VkPerformanceConfigurationAcquireInfoINTEL));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPerformanceConfigurationAcquireInfoINTEL);
begin
  vkadditem(a,sizeof(VkPerformanceConfigurationAcquireInfoINTEL));
  a^:=default_VkPerformanceConfigurationAcquireInfoINTEL;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPerformanceConfigurationAcquireInfoINTEL);
begin
  vkremitem(a,sizeof(VkPerformanceConfigurationAcquireInfoINTEL));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShaderClockFeaturesKHR);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShaderClockFeaturesKHR));
  a^:=default_VkPhysicalDeviceShaderClockFeaturesKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShaderClockFeaturesKHR);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShaderClockFeaturesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShaderClockFeaturesKHR);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShaderClockFeaturesKHR));
  a^:=default_VkPhysicalDeviceShaderClockFeaturesKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShaderClockFeaturesKHR);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShaderClockFeaturesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceIndexTypeUint8FeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceIndexTypeUint8FeaturesEXT));
  a^:=default_VkPhysicalDeviceIndexTypeUint8FeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceIndexTypeUint8FeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceIndexTypeUint8FeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceIndexTypeUint8FeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceIndexTypeUint8FeaturesEXT));
  a^:=default_VkPhysicalDeviceIndexTypeUint8FeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceIndexTypeUint8FeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceIndexTypeUint8FeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShaderSMBuiltinsPropertiesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShaderSMBuiltinsPropertiesNV));
  FillByte(a^,sizeof(VkPhysicalDeviceShaderSMBuiltinsPropertiesNV),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShaderSMBuiltinsPropertiesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShaderSMBuiltinsPropertiesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShaderSMBuiltinsPropertiesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShaderSMBuiltinsPropertiesNV));
  FillByte(a^,sizeof(VkPhysicalDeviceShaderSMBuiltinsPropertiesNV),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShaderSMBuiltinsPropertiesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShaderSMBuiltinsPropertiesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShaderSMBuiltinsFeaturesNV);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShaderSMBuiltinsFeaturesNV));
  a^:=default_VkPhysicalDeviceShaderSMBuiltinsFeaturesNV;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShaderSMBuiltinsFeaturesNV);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShaderSMBuiltinsFeaturesNV));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShaderSMBuiltinsFeaturesNV);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShaderSMBuiltinsFeaturesNV));
  a^:=default_VkPhysicalDeviceShaderSMBuiltinsFeaturesNV;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShaderSMBuiltinsFeaturesNV);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShaderSMBuiltinsFeaturesNV));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceFragmentShaderInterlockFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT));
  a^:=default_VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceFragmentShaderInterlockFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceFragmentShaderInterlockFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT));
  a^:=default_VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceFragmentShaderInterlockFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceSeparateDepthStencilLayoutsFeatures);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures));
  a^:=default_VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceSeparateDepthStencilLayoutsFeatures);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceSeparateDepthStencilLayoutsFeatures);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures));
  a^:=default_VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceSeparateDepthStencilLayoutsFeatures);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures));
end;

procedure T_vulkan_auto.vknew(out a:PVkAttachmentReferenceStencilLayout);
begin
  a:=vkgetmem(sizeof(VkAttachmentReferenceStencilLayout));
  a^:=default_VkAttachmentReferenceStencilLayout;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAttachmentReferenceStencilLayout);
begin
  vkfreemem(a,sizeof(VkAttachmentReferenceStencilLayout));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAttachmentReferenceStencilLayout);
begin
  vkadditem(a,sizeof(VkAttachmentReferenceStencilLayout));
  a^:=default_VkAttachmentReferenceStencilLayout;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAttachmentReferenceStencilLayout);
begin
  vkremitem(a,sizeof(VkAttachmentReferenceStencilLayout));
end;

procedure T_vulkan_auto.vknew(out a:PVkAttachmentDescriptionStencilLayout);
begin
  a:=vkgetmem(sizeof(VkAttachmentDescriptionStencilLayout));
  a^:=default_VkAttachmentDescriptionStencilLayout;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAttachmentDescriptionStencilLayout);
begin
  vkfreemem(a,sizeof(VkAttachmentDescriptionStencilLayout));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAttachmentDescriptionStencilLayout);
begin
  vkadditem(a,sizeof(VkAttachmentDescriptionStencilLayout));
  a^:=default_VkAttachmentDescriptionStencilLayout;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAttachmentDescriptionStencilLayout);
begin
  vkremitem(a,sizeof(VkAttachmentDescriptionStencilLayout));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR);
begin
  a:=vkgetmem(sizeof(VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR));
  a^:=default_VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR);
begin
  vkfreemem(a,sizeof(VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR);
begin
  vkadditem(a,sizeof(VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR));
  a^:=default_VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR);
begin
  vkremitem(a,sizeof(VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineInfoKHR);
begin
  a:=vkgetmem(sizeof(VkPipelineInfoKHR));
  a^:=default_VkPipelineInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineInfoKHR);
begin
  vkfreemem(a,sizeof(VkPipelineInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineInfoKHR);
begin
  vkadditem(a,sizeof(VkPipelineInfoKHR));
  a^:=default_VkPipelineInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineInfoKHR);
begin
  vkremitem(a,sizeof(VkPipelineInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineExecutablePropertiesKHR);
begin
  a:=vkgetmem(sizeof(VkPipelineExecutablePropertiesKHR));
  FillByte(a^,sizeof(VkPipelineExecutablePropertiesKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineExecutablePropertiesKHR);
begin
  vkfreemem(a,sizeof(VkPipelineExecutablePropertiesKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineExecutablePropertiesKHR);
begin
  vkadditem(a,sizeof(VkPipelineExecutablePropertiesKHR));
  FillByte(a^,sizeof(VkPipelineExecutablePropertiesKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineExecutablePropertiesKHR);
begin
  vkremitem(a,sizeof(VkPipelineExecutablePropertiesKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineExecutableInfoKHR);
begin
  a:=vkgetmem(sizeof(VkPipelineExecutableInfoKHR));
  a^:=default_VkPipelineExecutableInfoKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineExecutableInfoKHR);
begin
  vkfreemem(a,sizeof(VkPipelineExecutableInfoKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineExecutableInfoKHR);
begin
  vkadditem(a,sizeof(VkPipelineExecutableInfoKHR));
  a^:=default_VkPipelineExecutableInfoKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineExecutableInfoKHR);
begin
  vkremitem(a,sizeof(VkPipelineExecutableInfoKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineExecutableStatisticValueKHR);
begin
  a:=vkgetmem(sizeof(VkPipelineExecutableStatisticValueKHR));
  FillByte(a^,sizeof(VkPipelineExecutableStatisticValueKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineExecutableStatisticValueKHR);
begin
  vkfreemem(a,sizeof(VkPipelineExecutableStatisticValueKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineExecutableStatisticValueKHR);
begin
  vkadditem(a,sizeof(VkPipelineExecutableStatisticValueKHR));
  FillByte(a^,sizeof(VkPipelineExecutableStatisticValueKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineExecutableStatisticValueKHR);
begin
  vkremitem(a,sizeof(VkPipelineExecutableStatisticValueKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineExecutableStatisticKHR);
begin
  a:=vkgetmem(sizeof(VkPipelineExecutableStatisticKHR));
  FillByte(a^,sizeof(VkPipelineExecutableStatisticKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineExecutableStatisticKHR);
begin
  vkfreemem(a,sizeof(VkPipelineExecutableStatisticKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineExecutableStatisticKHR);
begin
  vkadditem(a,sizeof(VkPipelineExecutableStatisticKHR));
  FillByte(a^,sizeof(VkPipelineExecutableStatisticKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineExecutableStatisticKHR);
begin
  vkremitem(a,sizeof(VkPipelineExecutableStatisticKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineExecutableInternalRepresentationKHR);
begin
  a:=vkgetmem(sizeof(VkPipelineExecutableInternalRepresentationKHR));
  FillByte(a^,sizeof(VkPipelineExecutableInternalRepresentationKHR),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineExecutableInternalRepresentationKHR);
begin
  vkfreemem(a,sizeof(VkPipelineExecutableInternalRepresentationKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineExecutableInternalRepresentationKHR);
begin
  vkadditem(a,sizeof(VkPipelineExecutableInternalRepresentationKHR));
  FillByte(a^,sizeof(VkPipelineExecutableInternalRepresentationKHR),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineExecutableInternalRepresentationKHR);
begin
  vkremitem(a,sizeof(VkPipelineExecutableInternalRepresentationKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT));
  a^:=default_VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT));
  a^:=default_VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceTexelBufferAlignmentFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT));
  a^:=default_VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceTexelBufferAlignmentFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceTexelBufferAlignmentFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT));
  a^:=default_VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceTexelBufferAlignmentFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceTexelBufferAlignmentPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceTexelBufferAlignmentPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceTexelBufferAlignmentPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceTexelBufferAlignmentPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceSubgroupSizeControlFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceSubgroupSizeControlFeaturesEXT));
  a^:=default_VkPhysicalDeviceSubgroupSizeControlFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceSubgroupSizeControlFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceSubgroupSizeControlFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceSubgroupSizeControlFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceSubgroupSizeControlFeaturesEXT));
  a^:=default_VkPhysicalDeviceSubgroupSizeControlFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceSubgroupSizeControlFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceSubgroupSizeControlFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceSubgroupSizeControlPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceSubgroupSizeControlPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceSubgroupSizeControlPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceSubgroupSizeControlPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceSubgroupSizeControlPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceSubgroupSizeControlPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceSubgroupSizeControlPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceSubgroupSizeControlPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceSubgroupSizeControlPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceSubgroupSizeControlPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT));
  FillByte(a^,sizeof(VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT));
  FillByte(a^,sizeof(VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkMemoryOpaqueCaptureAddressAllocateInfo);
begin
  a:=vkgetmem(sizeof(VkMemoryOpaqueCaptureAddressAllocateInfo));
  a^:=default_VkMemoryOpaqueCaptureAddressAllocateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkMemoryOpaqueCaptureAddressAllocateInfo);
begin
  vkfreemem(a,sizeof(VkMemoryOpaqueCaptureAddressAllocateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkMemoryOpaqueCaptureAddressAllocateInfo);
begin
  vkadditem(a,sizeof(VkMemoryOpaqueCaptureAddressAllocateInfo));
  a^:=default_VkMemoryOpaqueCaptureAddressAllocateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkMemoryOpaqueCaptureAddressAllocateInfo);
begin
  vkremitem(a,sizeof(VkMemoryOpaqueCaptureAddressAllocateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceMemoryOpaqueCaptureAddressInfo);
begin
  a:=vkgetmem(sizeof(VkDeviceMemoryOpaqueCaptureAddressInfo));
  a^:=default_VkDeviceMemoryOpaqueCaptureAddressInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceMemoryOpaqueCaptureAddressInfo);
begin
  vkfreemem(a,sizeof(VkDeviceMemoryOpaqueCaptureAddressInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceMemoryOpaqueCaptureAddressInfo);
begin
  vkadditem(a,sizeof(VkDeviceMemoryOpaqueCaptureAddressInfo));
  a^:=default_VkDeviceMemoryOpaqueCaptureAddressInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceMemoryOpaqueCaptureAddressInfo);
begin
  vkremitem(a,sizeof(VkDeviceMemoryOpaqueCaptureAddressInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceLineRasterizationFeaturesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceLineRasterizationFeaturesEXT));
  a^:=default_VkPhysicalDeviceLineRasterizationFeaturesEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceLineRasterizationFeaturesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceLineRasterizationFeaturesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceLineRasterizationFeaturesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceLineRasterizationFeaturesEXT));
  a^:=default_VkPhysicalDeviceLineRasterizationFeaturesEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceLineRasterizationFeaturesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceLineRasterizationFeaturesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceLineRasterizationPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceLineRasterizationPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceLineRasterizationPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceLineRasterizationPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceLineRasterizationPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceLineRasterizationPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceLineRasterizationPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceLineRasterizationPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceLineRasterizationPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceLineRasterizationPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineRasterizationLineStateCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkPipelineRasterizationLineStateCreateInfoEXT));
  a^:=default_VkPipelineRasterizationLineStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineRasterizationLineStateCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkPipelineRasterizationLineStateCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineRasterizationLineStateCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkPipelineRasterizationLineStateCreateInfoEXT));
  a^:=default_VkPipelineRasterizationLineStateCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineRasterizationLineStateCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkPipelineRasterizationLineStateCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceVulkan11Features);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceVulkan11Features));
  a^:=default_VkPhysicalDeviceVulkan11Features;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceVulkan11Features);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceVulkan11Features));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceVulkan11Features);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceVulkan11Features));
  a^:=default_VkPhysicalDeviceVulkan11Features;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceVulkan11Features);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceVulkan11Features));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceVulkan11Properties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceVulkan11Properties));
  a^:=default_VkPhysicalDeviceVulkan11Properties;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceVulkan11Properties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceVulkan11Properties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceVulkan11Properties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceVulkan11Properties));
  a^:=default_VkPhysicalDeviceVulkan11Properties;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceVulkan11Properties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceVulkan11Properties));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceVulkan12Features);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceVulkan12Features));
  a^:=default_VkPhysicalDeviceVulkan12Features;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceVulkan12Features);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceVulkan12Features));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceVulkan12Features);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceVulkan12Features));
  a^:=default_VkPhysicalDeviceVulkan12Features;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceVulkan12Features);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceVulkan12Features));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceVulkan12Properties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceVulkan12Properties));
  a^:=default_VkPhysicalDeviceVulkan12Properties;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceVulkan12Properties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceVulkan12Properties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceVulkan12Properties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceVulkan12Properties));
  a^:=default_VkPhysicalDeviceVulkan12Properties;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceVulkan12Properties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceVulkan12Properties));
end;

procedure T_vulkan_auto.vknew(out a:PVkPipelineCompilerControlCreateInfoAMD);
begin
  a:=vkgetmem(sizeof(VkPipelineCompilerControlCreateInfoAMD));
  a^:=default_VkPipelineCompilerControlCreateInfoAMD;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPipelineCompilerControlCreateInfoAMD);
begin
  vkfreemem(a,sizeof(VkPipelineCompilerControlCreateInfoAMD));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPipelineCompilerControlCreateInfoAMD);
begin
  vkadditem(a,sizeof(VkPipelineCompilerControlCreateInfoAMD));
  a^:=default_VkPipelineCompilerControlCreateInfoAMD;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPipelineCompilerControlCreateInfoAMD);
begin
  vkremitem(a,sizeof(VkPipelineCompilerControlCreateInfoAMD));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceCoherentMemoryFeaturesAMD);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceCoherentMemoryFeaturesAMD));
  a^:=default_VkPhysicalDeviceCoherentMemoryFeaturesAMD;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceCoherentMemoryFeaturesAMD);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceCoherentMemoryFeaturesAMD));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceCoherentMemoryFeaturesAMD);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceCoherentMemoryFeaturesAMD));
  a^:=default_VkPhysicalDeviceCoherentMemoryFeaturesAMD;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceCoherentMemoryFeaturesAMD);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceCoherentMemoryFeaturesAMD));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceToolPropertiesEXT);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceToolPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceToolPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceToolPropertiesEXT);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceToolPropertiesEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceToolPropertiesEXT);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceToolPropertiesEXT));
  FillByte(a^,sizeof(VkPhysicalDeviceToolPropertiesEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceToolPropertiesEXT);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceToolPropertiesEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkDeviceCreateInfo);
begin
  a:=vkgetmem(sizeof(VkDeviceCreateInfo));
  a^:=default_VkDeviceCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDeviceCreateInfo);
begin
  vkfreemem(a,sizeof(VkDeviceCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDeviceCreateInfo);
begin
  vkadditem(a,sizeof(VkDeviceCreateInfo));
  a^:=default_VkDeviceCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDeviceCreateInfo);
begin
  vkremitem(a,sizeof(VkDeviceCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceMemoryProperties);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceMemoryProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceMemoryProperties),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceMemoryProperties);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceMemoryProperties));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceMemoryProperties);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceMemoryProperties));
  FillByte(a^,sizeof(VkPhysicalDeviceMemoryProperties),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceMemoryProperties);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceMemoryProperties));
end;

procedure T_vulkan_auto.vknew(out a:PVkRenderPassBeginInfo);
begin
  a:=vkgetmem(sizeof(VkRenderPassBeginInfo));
  a^:=default_VkRenderPassBeginInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkRenderPassBeginInfo);
begin
  vkfreemem(a,sizeof(VkRenderPassBeginInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkRenderPassBeginInfo);
begin
  vkadditem(a,sizeof(VkRenderPassBeginInfo));
  a^:=default_VkRenderPassBeginInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkRenderPassBeginInfo);
begin
  vkremitem(a,sizeof(VkRenderPassBeginInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceProperties2);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceProperties2));
  FillByte(a^,sizeof(VkPhysicalDeviceProperties2),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceProperties2);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceProperties2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceProperties2);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceProperties2));
  FillByte(a^,sizeof(VkPhysicalDeviceProperties2),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceProperties2);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceProperties2));
end;

procedure T_vulkan_auto.vknew(out a:PVkPhysicalDeviceMemoryProperties2);
begin
  a:=vkgetmem(sizeof(VkPhysicalDeviceMemoryProperties2));
  FillByte(a^,sizeof(VkPhysicalDeviceMemoryProperties2),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPhysicalDeviceMemoryProperties2);
begin
  vkfreemem(a,sizeof(VkPhysicalDeviceMemoryProperties2));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPhysicalDeviceMemoryProperties2);
begin
  vkadditem(a,sizeof(VkPhysicalDeviceMemoryProperties2));
  FillByte(a^,sizeof(VkPhysicalDeviceMemoryProperties2),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkPhysicalDeviceMemoryProperties2);
begin
  vkremitem(a,sizeof(VkPhysicalDeviceMemoryProperties2));
end;

procedure T_vulkan_auto.vknew(out a:PVkPresentRegionKHR);
begin
  a:=vkgetmem(sizeof(VkPresentRegionKHR));
  a^:=default_VkPresentRegionKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPresentRegionKHR);
begin
  vkfreemem(a,sizeof(VkPresentRegionKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPresentRegionKHR);
begin
  vkadditem(a,sizeof(VkPresentRegionKHR));
  a^:=default_VkPresentRegionKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPresentRegionKHR);
begin
  vkremitem(a,sizeof(VkPresentRegionKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkPresentTimesInfoGOOGLE);
begin
  a:=vkgetmem(sizeof(VkPresentTimesInfoGOOGLE));
  a^:=default_VkPresentTimesInfoGOOGLE;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPresentTimesInfoGOOGLE);
begin
  vkfreemem(a,sizeof(VkPresentTimesInfoGOOGLE));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPresentTimesInfoGOOGLE);
begin
  vkadditem(a,sizeof(VkPresentTimesInfoGOOGLE));
  a^:=default_VkPresentTimesInfoGOOGLE;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPresentTimesInfoGOOGLE);
begin
  vkremitem(a,sizeof(VkPresentTimesInfoGOOGLE));
end;

procedure T_vulkan_auto.vknew(out a:PVkDrmFormatModifierPropertiesListEXT);
begin
  a:=vkgetmem(sizeof(VkDrmFormatModifierPropertiesListEXT));
  FillByte(a^,sizeof(VkDrmFormatModifierPropertiesListEXT),0);
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDrmFormatModifierPropertiesListEXT);
begin
  vkfreemem(a,sizeof(VkDrmFormatModifierPropertiesListEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDrmFormatModifierPropertiesListEXT);
begin
  vkadditem(a,sizeof(VkDrmFormatModifierPropertiesListEXT));
  FillByte(a^,sizeof(VkDrmFormatModifierPropertiesListEXT),0);
end;
procedure T_vulkan_auto.vkrem(var a:PVkDrmFormatModifierPropertiesListEXT);
begin
  vkremitem(a,sizeof(VkDrmFormatModifierPropertiesListEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkFramebufferAttachmentsCreateInfo);
begin
  a:=vkgetmem(sizeof(VkFramebufferAttachmentsCreateInfo));
  a^:=default_VkFramebufferAttachmentsCreateInfo;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkFramebufferAttachmentsCreateInfo);
begin
  vkfreemem(a,sizeof(VkFramebufferAttachmentsCreateInfo));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkFramebufferAttachmentsCreateInfo);
begin
  vkadditem(a,sizeof(VkFramebufferAttachmentsCreateInfo));
  a^:=default_VkFramebufferAttachmentsCreateInfo;
end;
procedure T_vulkan_auto.vkrem(var a:PVkFramebufferAttachmentsCreateInfo);
begin
  vkremitem(a,sizeof(VkFramebufferAttachmentsCreateInfo));
end;

procedure T_vulkan_auto.vknew(out a:PVkPresentRegionsKHR);
begin
  a:=vkgetmem(sizeof(VkPresentRegionsKHR));
  a^:=default_VkPresentRegionsKHR;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkPresentRegionsKHR);
begin
  vkfreemem(a,sizeof(VkPresentRegionsKHR));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkPresentRegionsKHR);
begin
  vkadditem(a,sizeof(VkPresentRegionsKHR));
  a^:=default_VkPresentRegionsKHR;
end;
procedure T_vulkan_auto.vkrem(var a:PVkPresentRegionsKHR);
begin
  vkremitem(a,sizeof(VkPresentRegionsKHR));
end;

procedure T_vulkan_auto.vknew(out a:PVkAllocationCallbacks);
begin
  a:=vkgetmem(sizeof(VkAllocationCallbacks));
  a^:=default_VkAllocationCallbacks;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkAllocationCallbacks);
begin
  vkfreemem(a,sizeof(VkAllocationCallbacks));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkAllocationCallbacks);
begin
  vkadditem(a,sizeof(VkAllocationCallbacks));
  a^:=default_VkAllocationCallbacks;
end;
procedure T_vulkan_auto.vkrem(var a:PVkAllocationCallbacks);
begin
  vkremitem(a,sizeof(VkAllocationCallbacks));
end;

procedure T_vulkan_auto.vknew(out a:PVkDebugReportCallbackCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkDebugReportCallbackCreateInfoEXT));
  a^:=default_VkDebugReportCallbackCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDebugReportCallbackCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkDebugReportCallbackCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDebugReportCallbackCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkDebugReportCallbackCreateInfoEXT));
  a^:=default_VkDebugReportCallbackCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDebugReportCallbackCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkDebugReportCallbackCreateInfoEXT));
end;

procedure T_vulkan_auto.vknew(out a:PVkDebugUtilsMessengerCreateInfoEXT);
begin
  a:=vkgetmem(sizeof(VkDebugUtilsMessengerCreateInfoEXT));
  a^:=default_VkDebugUtilsMessengerCreateInfoEXT;
end;
procedure T_vulkan_auto.vkdispose(var a:PVkDebugUtilsMessengerCreateInfoEXT);
begin
  vkfreemem(a,sizeof(VkDebugUtilsMessengerCreateInfoEXT));
  a:=nil;
end;
procedure T_vulkan_auto.vkadd(out a:PVkDebugUtilsMessengerCreateInfoEXT);
begin
  vkadditem(a,sizeof(VkDebugUtilsMessengerCreateInfoEXT));
  a^:=default_VkDebugUtilsMessengerCreateInfoEXT;
end;
procedure T_vulkan_auto.vkrem(var a:PVkDebugUtilsMessengerCreateInfoEXT);
begin
  vkremitem(a,sizeof(VkDebugUtilsMessengerCreateInfoEXT));
end;

















end.
