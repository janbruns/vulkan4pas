
function remove_curly(s)
{ if (!s) {s="";};
  var q = "";
  for (var i=0; i<s.length; i+=1)
  { var c=s.charAt(i);
    if (c=="{") { c=" <( " };
    if (c=="}") { c=" )> " };
    q+=c;
  }
  return q;
};

function cmp_enumentries(a,b)
{ var av = parseInt(a.value);
  var bv = parseInt(b.value);
  if (av<bv) { return -1 };
  if (av>bv) { return  1 };
  if (av==bv) { return  0 };
  if (isNaN(av)) { return -1};
  if (isNaN(bv)) { return  1};
  return 1;
};

function make_pas_enum(e,o)
{ e.entries.sort(cmp_enumentries);
  var s="";
  var d="";
  var arr=[];
  var lastx=NaN;
  var lastn="";
  for (var i=0; i<e.entries.length; i+=1)
  { var x=parseInt(e.entries[i].value);
    if ((!isNaN(x))&&(x!=(lastx))) 
    { arr.push(e.entries[i]); 
      lastx=x;
      lastn=e.entries[i].name;
    } else {
      if ((!isNaN(x))&&(x==(lastx)))
      { if (lastn!=e.entries[i].name) 
        { s+= "{$WARNING: removed duplicate: "+e.entries[i].name+"}\n";
	};
      } else {
        if (e.entries[i].alias)
	{ //d+="{$define "+e.entries[i].name+":="+e.entries[i].alias+"}\n"
          d+=e.entries[i].name+" = "+e.entries[i].alias+";\n"
	} else {
	  throw("problems with "+e.entries[i].name);
	};
      };
    };
  };

  if (e.cmt)
  { s+= "{"+remove_curly(e.cmt)+"}\n"
  };
  s+=e.name+" = (\n";
  for (var i=0; i<arr.length; i+=1)
  { var x=parseInt(arr[i].value);
    if (!isNaN(x))
    { if (arr[i].comment)
      { s+= "{"+remove_curly(arr[i].comment)+"}\n"
      };
      s+="  "+arr[i].name;
      s+=" := "+x;
      if ((i+1)<arr.length) s+=",";
      if (e.entries[i].reqby)
      { s+= " {"+remove_curly(arr[i].reqby)+"}"
      };
      s+="\n";
    };
  };
  s+=");\n\n";
  return [s,d];
};

function rewrite_pas_constant(s,a)
{ if (a) { return a; };
  if (s=="1000.0f") { return "1000.0"; };
  if (s=="(~0U)")   { return "not(dword(0))"; };
  if (s=="(~0ULL)") { return "not(qword(0))"; };
  if (s=="(~0U-1)") { return "not(qword(0))-1"; };
  if (s=="(~0U-2)") { return "not(qword(0))-2"; };
  return s;  
};

function make_pas_constants(o)
{ var s="\nconst\n\n";
  var c=o.enums["API Constants"].entries;
  for (var i=0; i<c.length; i+=1)
  { s+=c[i].name+" = "+rewrite_pas_constant(c[i].value,c[i].alias)+";\n";
  };
  return s+"\n";
};

function make_pas_bitfields(o)
{ var s="";
  for (var i in o.enums)
  { if (o.enums[i].type=="bitmask")
    { var don={};
      var q=o.enums[i].entries;
      for (var j=0; j<q.length; j+=1)
      { if (!don[q[j].name])
        { var x=parseInt(q[j].bitpos);
          if (!isNaN(x))
	  { if (q[j].comment) { s+= " {"+remove_curly(q[j].comment)+"}\n" };
	    s+= q[j].name+" = 1 shl "+x+";\n"
	    don[q[j].name]=q[j].bitpos+"_"+q[j].value;
	  } else {
	    var x=parseInt(q[j].value);
            if (!isNaN(x))
	    { if (q[j].comment) { s+= " {"+remove_curly(q[j].comment)+"}\n" };
	      s+= q[j].name+" = "+x+";\n"
	      don[q[j].name]=q[j].bitpos+"_"+q[j].value;
	    };
	  };
	} else {
	  if (don[q[j].name]!=(q[j].bitpos+"_"+q[j].value))
	  { s+="{§WARNING: problem 1 with bit "+i+"."+q[j].name+"}\n"
	  };
	};
      };
      for (var j=0; j<q.length; j+=1)
      { if (!don[q[j].name])
	{ if (q[j].alias) 
	  { s+= q[j].name+" = "+q[j].alias+";\n";
	  } else { 
	    s+="{§WARNING: problem 2 with bit "+i+"."+q[j].name+"}\n"
	  };
	};
      };
      s+="\n";
    };
  };
  return s;  
};

function countstars(s)
{ var n=0;
  for (var i=0; i<s.length; i+=1)
  { if (s.charAt(i)=="*") { n+= 1};
  };
  return n;
};

function register_type(t,src,o)
{ //console.log("register_type..."+t);
  o.s+=t+" = "+src+";\n";
  o.s+="P"+t+" = ^"+t+";\n";
  o.s+="PP"+t+" = ^P"+t+";\n";
  var x={};
  x[t]=[];
  x[t].push(""+t);
  x[t].push("P"+t);
  x[t].push("PP"+t);
  add_to_tdb(x,o);
};

function registertype(t,o)
{ //console.log("registertype..."+t);
  var x={};
  x[t]=[];
  x[t].push(""+t);
  x[t].push("P"+t);
  x[t].push("PP"+t);
  add_to_tdb(x,o);
};

function make_pas_vkhandle(t,o)
{ register_type(t,"vulkan_handle_type",o); // class, to get stronger type-checking
};

function make_pas_dvkhandle(t,o)
{ register_type(t,"vulkan_handle_dtype",o); // class, to get stronger type-checking
};

function make_pas_type(t,o)
{ if (t.text)
  { var x=t.text.split(";")[0];
    var unbr=debrake(x);
    x=unbr[0];
    unbr = compile_brackets(unbr);
    var ta=x.split("*");
    var tb=[];
    var stars=ta.length-1;
    var rslvd = null; 
    for (var i=0; i<ta.length; i+=1)
    { tb=tb.concat(ta[i].split(" "));
    };
//console.log("make type "+t.name+" from ("+JSON.stringify(tb)+")");
    for (var i=0; i<tb.length; i+=1)
    { if (tb[i]=="typedef") { tb[i]=""; };
      if (tb[i]=="const")   { tb[i]=""; };
      if (tb[i]==t.name )   { tb[i]=""; };
      if (o.tdb[tb[i].toUpperCase()])     { rslvd=o.tdb[tb[i].toUpperCase()]; tb[i]=""; };
    };
    ta=[];
    for (var i=0; i<tb.length; i+=1)
    { if (tb[i]!="") { ta.push(tb[i]); };
    };
    if (ta.length>0) 
    { throw("probs analysing "+t.name+" = "+JSON.stringify(t));
    } else {
      var x=rslvd[0];
      while (stars>0)
      { x="^"+x;
        stars -= 1;
      };
      register_type(t.name,unbr+x,o)
    };    
  };
};



function register_initializer(t,parr,o)
{ var x = "default_"+t;
  o.init_if += x+ " : "+t+";\n";
  o.init_imp += "  fillbyte("+x+", sizeof("+t+"),0);\n"
  if ((parr.length>1)&&(parr[0][1]=="VkStructureType"))
  { o.init_imp2 += "  "+x+".sType = ;\n"
  };
};

function make_auto(t,have_default,o)
{ 
  o.autoinc_if  += "  procedure vknew(out a:P"+t.name+");\n";
  o.autoinc_if  += "  procedure vkdispose(var a:P"+t.name+");\n";
  o.autoinc_if  += "  procedure vkadd(out a:P"+t.name+");\n";
  o.autoinc_if  += "  procedure vkrem(var a:P"+t.name+");\n";

  o.autoinc_imp += "procedure Tauto.vknew(out a:P"+t.name+");\n";
  o.autoinc_imp += "begin\n";
  o.autoinc_imp += "  a:=vkgetmem(sizeof("+t.name+"));\n";
  if (have_default) { o.autoinc_imp += "  a^:=default_"+t.name+";\n"; }
  else              { o.autoinc_imp += "  FillByte(a^,sizeof("+t.name+"),0);\n"; };
  o.autoinc_imp += "end;\n";

  o.autoinc_imp += "procedure Tauto.vkdispose(var a:P"+t.name+");\n";
  o.autoinc_imp += "begin\n";
  o.autoinc_imp += "  vkfreemem(a,sizeof("+t.name+"));\n";
  o.autoinc_imp += "  a:=nil;\n";
  o.autoinc_imp += "end;\n";

  o.autoinc_imp += "procedure Tauto.vkadd(out a:P"+t.name+");\n";
  o.autoinc_imp += "begin\n";
  o.autoinc_imp += "  vkadditem(a,sizeof("+t.name+"));\n";
  if (have_default) { o.autoinc_imp += "  a^:=default_"+t.name+";\n"; }
  else              { o.autoinc_imp += "  FillByte(a^,sizeof("+t.name+"),0);\n"; };
  o.autoinc_imp += "end;\n";

  o.autoinc_imp += "procedure Tauto.vkrem(var a:P"+t.name+");\n";
  o.autoinc_imp += "begin\n";
  o.autoinc_imp += "  vkremitem(a,sizeof("+t.name+"));\n";
  o.autoinc_imp += "end;\n\n";

};

function debrake(s)
{ var i=s.indexOf("[");
  var j=s.indexOf("]");
  if ((i>=0)&&(j>i))
  { var s2 = s.substr(0,i)+" "+s.substr(j+1);
    var s3 = s.substr(i+1,j-i-1);
    var arr2 = debrake(s2).concat(s3);
    //console.log("bragged "+s+" to "+JSON.stringify(arr2) );
    return arr2;
  };
  return [s];
};

function compile_brackets(arr)
{ var arr2=[];
  for (var i=1; i<arr.length; i+=1)
  { arr2.push("0..("+arr[i]+")-1");
  };
  if (arr2.length>0) {return "ARRAY["+arr2.join(",")+"] of " } else { return ""; };
};


function typerewrite(s,o)
{ if (o.typerewrite[s]) { s=o.typerewrite[s]; };
  return s;
};

function make_pas_param(txt,o)
{ if (!txt) { return false; };
  var x=txt.split(",")[0];
  var unbr=debrake(x);
  x=unbr[0];
  unbr = compile_brackets(unbr);
  var ta=x.split("*");
  var tb=[];
  var stars=ta.length-1;
  var rslvd = null; 
  for (var i=0; i<ta.length; i+=1)
  { tb=tb.concat(ta[i].split(" "));
  };
  for (var i=0; i<tb.length; i+=1)
  { if (tb[i]=="typedef") { tb[i]=""; };
    if (tb[i]=="const")   { tb[i]=""; };
    if (tb[i]=="struct")  { tb[i]=""; };
    if (o.tdb[typerewrite(tb[i],o).toUpperCase()])
    { rslvd=o.tdb[typerewrite(tb[i],o).toUpperCase()]; 
      tb[i]=""; 
    };
  };
  ta=[];
  for (var i=0; i<tb.length; i+=1)
  { if (tb[i]!="") { ta.push(tb[i]); };
  };
  //console.log("finish: "+txt+"\n"+JSON.stringify(ta));
  if ((rslvd)&&(ta.length==1))
  { x="";
    while(stars>2) { x+="^"; stars-=1; };
    return [ta[0],unbr + x+rslvd[stars]];   
  } else { return false; };
};

function make_refvalidator(n,o)
{ o.test_c += '  printf("%i ",sizeof('+n+"));\n";
  o.test_p += "  write(sizeof("+n+"),' ');\n";
};


/* this one is called mutiple times, to reflect type dependencies */
function make_pas_records(o)
{ var cnt=0;
  for (var i in o.types)
  { var t = o.types[i];
    if ((!o.tdb[t.name.toUpperCase()])&&(!t.alias)&&(t.category)&&((t.category=="struct")||(t.category=="union")))
    { var ok=(t.members.length>0);
      var marr = t.members;
      var parr = [];
      for (var j=0; j<marr.length; j+=1)
      { var x=make_pas_param(marr[j].text,o);
        if (!x) 
        { ok=false;
        } else {
	    if ((!x[0])||(!x[1])) 
	    { ok=false; 
	    } else { 
	      parr.push(x); 
	    };
	  };
      };
      if (ok)
      { var s="record\n";
        if (t.category=="struct")
	  { for (var j=0; j<parr.length; j+=1)
          { var lhs = parr[j][0];
	      var rhs = parr[j][1];
	      if (o.paskeyword[lhs.toUpperCase()]) { lhs = "_"+lhs; };
	      s+= "  "+lhs+" : " + rhs + ";\n";
	    };
	  } else {
	    s += "  case byte of\n";
	    for (var j=0; j<parr.length; j+=1)
          { var lhs = parr[j][0];
	      var rhs = parr[j][1];
	      if (o.paskeyword[lhs.toUpperCase()]) { lhs = "_"+lhs; };
	      s+= "    "+j+": ("+lhs+" : " + rhs + ");\n";
	    };
	  };
	  s+="end";
        var have_default=false;
        if ((!t.returnedonly)||((t.returnedonly)&&(t.returnedonly!="true")))
        { register_initializer(t.name,parr,o);
          have_default=true;
        };
        make_auto(t,have_default,o);
        make_refvalidator(t.name,o);
	  register_type(t.name,s,o);
	  o.s+="\n";
	  cnt += 1;
      };
    };
    if ((!o.tdb[t.name.toUpperCase()])&&(t.alias)&&(o.tdb[t.alias.toUpperCase()]))
    { register_type(t.name,t.alias,o);
    };
  };
  return cnt;
};


function make_pas_funcpointers(o)
{ var fp={};
  for (var f in o.types)
  { var typ = o.types[f];
    var q={};
    if (typ.category=="funcpointer")
    { var a=typ.text.split("(");
      if (a.length!=3) { throw("problems interpreting funcptr-type "+typ.name); };
      for (var i=0; i<a.length; i+=1)
      { a[i]=a[i].split(")").join("");
        a[i]=a[i].split("\n").join("");
      };
      a[0]=a[0].split("*");
      var stars=a[0].length-1;
      a[0]=a[0].join("").split(" ");     
      for (var i=0; i<a[0].length; i+=1)
      { if (a[0][i]=="typedef") { a[0][i]=""; };    
        if (a[0][i]=="const")   { a[0][i]=""; };
      };
      a[0]=a[0].join("").toUpperCase();
      q.rettype=o.tdb[a[0]][stars];
      a[2]=a[2].split(";").join("");
      q.pl=a[2].split(",");
      for (var i=0; i<q.pl.length; i+=1)
      { q.pl[i]=make_pas_param(q.pl[i],o);
      };
      fp[typ.name]=q;
    };
  };
  if (fp["PFN_vkVoidFunction"])
  { register_type("PFN_vkVoidFunction","pointer",o);
    delete fp.PFN_vkVoidFunction;
  };
  o.s+="\n{ callback funtions }\n\n";
  put_pas_proctype_list(fp,o,"vulkan_cb_callingconv");
  o.s+="\n\n\n";
};

function put_pas_proctype_list(fp,o,cs)
{ for (var f in fp)
  { var pl=fp[f].pl;
    var s="(";
    for (var i=0; i<pl.length; i+=1)
    { s+="_"+pl[i][0]+" : "+pl[i][1];
      if ((i+1)<pl.length) { s+= "; "; };
    };
    s+=")";
    if (fp[f].rettype) 
    { s = "FUNCTION" + s + " : "+fp[f].rettype;
    } else {
      s = "PROCEDURE" + s;
    };
    s+="; "+cs;
    register_type(f,s,o);
    o.s += "\n";
  };
};

function make_temptype(s,o)
{ o.temptypenum += 1;
  var x="Ttemptype"+o.temptypenum;
  o.s += x+"="+s+";\n";
  return x;
};

function make_pas_commands(o)
{ var fp={};
  var fs={};
  o.staticlink="";
  for (var n in o.cmd)
  { var cmd=o.cmd[n];
    if (!cmd.alias)
    { var q={};
      q.pl=[];
      for (var i=0; i<cmd.pl.length; i+=1)
      { var x = make_pas_param(cmd.pl[i].text,o);
        if (!x) 
        { var s=""; 
          for (var j in o.tdb) { s+= " "+j;};
          //console.log("tdb: "+s);

          var s=""; 
          for (var j in o.types) { s+= " "+j;};          
          //console.log("types:"+s);

          throw("Problems with param "+(i+1)+" of cmd "+n+": \n"+cmd.pl[i].text); 
	    /* Pascal doesn't allow this kind parameter-type def */
        };
	  if (x[1].indexOf("ARRAY")>=0)
	  { x[1] = make_temptype(x[1],o);
	  };
        q.pl.push(x);
      };
      q.rettype=make_pas_param(cmd.pr.text,o);
      if (!q.rettype) { throw("Problems with prettype of cmd "+n); };
      q.rettype = q.rettype[1];
      if (q.rettype=="null") { q.rettype=false; };
      if (cmd.reqby) { q.reqby=cmd.reqby };
      fp["T_"+n]=q;
      fs[n]=q;
    };
  };
  put_pas_proctype_list(fp,o,"vulkan_callingconv");
  put_pas_procstatic(fs,o);
  var fsa={};
  var fsb={};
  for (var n in o.cmd)
  { var cmd=o.cmd[n];
    var alias=cmd.alias;
    if (alias)
    { while (!fs[alias])
      { alias = o.cmd[alias].alias;
      };
      fsa[n]=fs[alias];
      fsa[n].reqby=cmd.reqby;
      fsa[n].acmt=alias;
    };
  };
  put_pas_procstatic(fsa,o);
  var procvarsects={};
  var namestringid=0;
  o.pvs_name = "";
  for (var n in o.cmd)
  { var cmd=o.cmd[n];
    var alias=cmd.alias;
    if (alias)
    { while (!fs[alias])
      { alias = o.cmd[alias].alias;
      };
    } else { alias=n; };
    if (!procvarsects[cmd.reqby]) 
    { procvarsects[cmd.reqby]=[]; 
      //console.log(cmd.reqby); 
    };
    
    procvarsects[cmd.reqby].push([n,alias]);
    cmd.namepos=namestringid;
    o.pvs_name += "  '"+n+"',\n";
    namestringid += 1;
  };
  var API_cls = "T_vulkan_API_commands_class";

  o.pvs_name += "  '');\n";
  o.pvs_name = "const known_cmd_names : array[0.."+namestringid+"] of Pchar = (\n"+o.pvs_name;

  o.pvs="";
  o.pvs_if="{$mode objfpc}\nunit vulkan_funcs;";
  o.pvs_cls_if="{$mode objfpc}\nunit vulkan_class;";
  var x="\n\ninterface\nuses vulkan_h;\n\n\n\n{all known command procvars}\n\n\n";
  o.pvs_if+=x+"var\n\n";
  o.pvs_cls_if+=x;
  var cls="";
  
  for (var n in procvarsects)
  { o.pvs_if+="{ from "+n+" }\n";
    var p=procvarsects[n];
    for (var i=0; i<p.length; i+=1)
    { var x =p[i][0]+" : T_"+p[i][1]+";\n";
      o.pvs_if += x;
      cls += "  "+x;
    };
    o.pvs_if += "\n";
  };
  o.pvs_if += "var vulkan_available : boolean;\nprocedure load_all_vk(i:VkInstance; d:VkDevice);\n\n\n";
  o.pvs_cls_if += "\n\ntype\n"+API_cls+"=class\n  constructor create();\npublic\n  vulkan_available : boolean;\n"+cls;
  var x="implementation\nuses vulkan_lib;\n\n";
  o.pvs_cls_imp = x;
  o.pvs_imp = x;
    
  x ="load_vk(i:VkInstance; d:VkDevice; idx:longint; var p:pointer);";
  o.pvs_cls_if+="private\n  procedure "+x+"\n";
  x+="\nvar p2 : pointer;\n";
  x+="begin\n";
  x+="  p2 := nil;\n";
  x+="  if (d=VK_NIL_HANDLE) then begin\n";
  x+="    if ((i<>VK_NIL_HANDLE)and(nil<>pointer(vkGetInstanceProcAddr)))\n";
  x+="    then begin\n";
  x+="      p2 := vkGetInstanceProcAddr(i,known_cmd_names[idx]);\n";
  x+="      if not(p2=nil) then p:=p2;\n";
  x+="    end else p:=nil;\n";
  x+="  end else begin\n";
  x+="    p2 := vkGetDeviceProcAddr(d,known_cmd_names[idx]);\n";
  x+="    if not(p2=nil) then p:=p2;\n";
  x+="  end;\n";
  x+="end;\n\n";
  o.pvs_imp+="procedure "+x;
  o.pvs_cls_imp+="procedure "+API_cls+"."+x;
 
 



  var la="";
  for (var n in procvarsects)
  { var x="load_"+n;
    la += "  "+x+"(i,d);\n";
    x +="(i:VkInstance; d:VkDevice);";
    o.pvs_cls_if  +="  procedure "+x+"\n";
    o.pvs_imp     +=  "procedure "+x+"\n";
    o.pvs_cls_imp +=  "procedure "+API_cls+"."+x+"\n";
    x="begin\n";
    o.pvs_imp+=x;
    o.pvs_cls_imp+=x;
    var p=procvarsects[n];
    for (var i=0; i<p.length; i+=1)
    { var x= "  load_vk(i,d,"+o.cmd[p[i][0]].namepos+",pointer("+p[i][0]+"));\n";
      o.pvs_imp += x;
      o.pvs_cls_imp+=x;
    };
    o.pvs_imp+="end;\n\n";
    o.pvs_cls_imp+="end;\n\n";
  };
  la+="end;\n";
  var x="load_all_vk(i:VkInstance; d:VkDevice);\n";
  o.pvs_cls_if+="public\n  procedure "+x;
  x+="begin\n";
  o.pvs_cls_imp+="procedure "+API_cls+"."+x+la;
  o.pvs_imp+="procedure "+x+la;
  
  o.pvs_cls_imp += "\nconstructor "+API_cls+".create();\n";
  o.pvs_imp += "\nprocedure init_vk();\n";  
  x ="begin\n";
  x+="  vulkan_available := false;\n";
  x+="  load_all_vk(VK_NIL_HANDLE,VK_NIL_HANDLE);\n";
  x+="  pointer(vkGetInstanceProcAddr) := nil;\n";
  x+="  pointer(vkGetInstanceProcAddr) := vulkan_getGIPA();\n";
  x+="  if (nil<>pointer(vkGetInstanceProcAddr)) then begin\n";
  x+="    pointer(vkEnumerateInstanceExtensionProperties) := vkGetInstanceProcAddr(VK_NIL_HANDLE,'vkEnumerateInstanceExtensionProperties');\n";
  x+="    pointer(vkEnumerateInstanceLayerProperties)     := vkGetInstanceProcAddr(VK_NIL_HANDLE,'vkEnumerateInstanceLayerProperties');\n";
  x+="    pointer(vkEnumerateInstanceVersion)             := vkGetInstanceProcAddr(VK_NIL_HANDLE,'vkEnumerateInstanceVersion');\n";
  x+="    pointer(vkCreateInstance)                       := vkGetInstanceProcAddr(VK_NIL_HANDLE,'vkCreateInstance');\n";
  x+="    vulkan_available := (nil<>pointer(vkCreateInstance));\n";
  x+="  end;\n";
  x+="end;\n\n";
  o.pvs_cls_imp += x;
  o.pvs_imp += x;  

  o.pvs_imp += "\n\nbegin\n  init_vk();\nend.\n";
  o.pvs_cls_imp += "\n\nbegin\nend.\n";

  o.pvs_cls_if+="end;\n\n\n\n";

  o.pvs = o.pvs_if+o.pvs_imp;
  o.pvs_cls = o.pvs_cls_if+o.pvs_cls_imp;

};


function reqby_expr(r)
{ if (!r) { return "{$if TRUE}\n"; };
  var s = "{$if ";
  for (var i=0; i<r.length; i+= 1)
  { if (i>0) { s+= " OR "};
    s += "DEFINED("+r[i]+")";
  };
  return s+"}\n";
};

function put_pas_procstatic(fp,o)
{ for (var f in fp)
  { var pl=fp[f].pl;
    var pre=reqby_expr(fp[f].reqby);
    if (fp[f].acmt) { pre+="{ alias "+fp[f].acmt+" }\n"};
    var s="(";
    for (var i=0; i<pl.length; i+=1)
    { s+="_"+pl[i][0]+" : "+pl[i][1];
      if ((i+1)<pl.length) { s+= "; "; };
    };
    s+=")";
    if (fp[f].rettype) 
    { s = "FUNCTION " +f+ s + " : "+fp[f].rettype;
    } else {
      s = "PROCEDURE "+f + s;
    };
    s+="; ext_cb_callspec;";
    s+=" external vklib name '"+f+"';\n";
    o.staticlink += pre+s+"{$endif}\n\n";
  };
};

function add_to_tdb(e,o)
{ for (var i in e)
  { //console.log("add_db: "+i);
    var j=i.toUpperCase();
    if (o.tdb[j]) { throw("Type already in tdb: "+i); };
    o.tdb[j]=e[i];
    o.knowntypes[i]=true;
  };
};


function register_pfs(pfs,tg,o)
{ for (var pf in pfs)
  { o.s += "{$ifdef HAVE_VK_PF_"+pf+"}\n";  
    for (t in pfs[pf])
    { o.s+="P"+pfs[pf][t]+"=^"+pfs[pf][t]+";\n";
      o.s+="PP"+pfs[pf][t]+"=^P"+pfs[pf][t]+";\n";
    };  
    o.s+="{$else}\n";
    for (var t in pfs[pf])
    { o.knowntypes[t]=true;
      o.s+="// "+t+"\n";
      o.typerewrite[t]=pfs[pf][t];
      register_type(pfs[pf][t],tg[pfs[pf][t]],o)
    };
    o.s+="{$endif}\n\n"; 
  };
};


function make_pas_enums(o)
{ 
  o.init_if  = "unit vulkan_typeinit;\n\ninterface\n\n\n";
  o.init_imp = "\n\nimplementation\n\nbegin\n";
  o.init_imp2 = "\n\n";
  o.test_c = "";
  o.test_p = "";
  o.autoinc_if="";
  o.autoinc_imp="";
  o.knowntypes={};

  var tdb = { "int64_t"  : ["int64"  ,"Pint64"  ,"PPint64"  ],
              "int32_t"  : ["int32"  ,"Pint32"  ,"PPint32"  ],
	        "int16_t"  : ["int16"  ,"Pint16"  ,"PPint16"  ],
	        "int8_t"   : ["int8"   ,"Pint8"   ,"PPint8"   ],
              "uint64_t" : ["uint64" ,"Puint64" ,"PPuint64" ],
              "uint32_t" : ["uint32" ,"Puint32" ,"PPuint32" ],
	        "uint16_t" : ["uint16" ,"Puint16" ,"PPuint16" ],
	        "uint8_t"  : ["uint8"  ,"Puint8"  ,"PPuint8"  ],
	        "void"     : [ null    ,"pointer" ,"Ppointer" ],
	        "char"     : ["char"   ,"Pchar"   ,"PPchar"   ],
	        "int"      : ["SYSC_int","PSYSC_int","PPSYSC_int"],
	        "uint"     : ["SYSC_uint","PSYSC_uint","PPSYSC_uint"],
	        "size_t"   : ["SYSC_size_t","PSYSC_size_t","PPSYSC_size_t"],
	        "double"   : ["double" ,"Pdouble" ,"PPdouble" ],
	        "float"    : ["single" ,"Psingle" ,"PPsingle" ] };


  o.tdb={};
  add_to_tdb(tdb,o);
  o.temptypenum = 0;
  o.s = "unit vulkan_h;\n\n";
  o.s+= "interface\n\n";
  o.s+= "{$macro on}\n"
  o.s+= "{$PACKRECORDS C}\n";
  o.s+= "{$PACKENUM 4} \n\n";
  o.s+= "{ 32bit targets: VK_HANDLES32_USE_CLASS strengthens type-checks, but VK_NIL_HANDLE!=VK_NULL_HANDLE}\n";
  o.s+= "{_$define VK_HANDLES32_USE_CLASS}\n";
  o.s+= "\n{$include vulkan_calling.inc}\n"
  o.s+= "\n{_$include vk_h_pf.inc} // override to get specific platforms\n";

  o.s+= "\n\nconst\n";
  o.s+= "{$ifdef VK_USING_CLASS_HANDLES}\n";
  o.s+= "invalid_vulkan_handle = nil;\n";
  o.s+= "{$else}\n";
  o.s+= "invalid_vulkan_handle = 0;\n";
  o.s+= "{$endif}\n";
  o.s+= "VK_NULL_HANDLE=invalid_vulkan_handle;\n";
  o.s+= "{$ifdef VK_HANDLES32_USING_DWORD}\n";
  o.s+= "VK_NIL_HANDLE=0;\n";
  o.s+= "{$else}\n";
  o.s+= "VK_NIL_HANDLE=nil;\n";
  o.s+= "{$endif}\n";
  o.s+= "\n\ntype\n\n";


  var tra = {
    "C":
    {"int":"SYSC_int",
     "uint":"SYSC_uint",
     "size_t":"SYSC_size_t",
    },
    "xlib": {
      "Display":"T_XLIB_Display",
      "VisualID":"T_XLIB_VisualID",
      "Window":"T_XLIB_Window"
    },
    "XRANDR":{
      "RROutput":"T_XRANDR_RROutput"
    },
    "wayland":{
      "wl_display":"T_WAYL_display",
      "wl_surface":"T_WAYL_surface"
    },
    "win":{
      "HINSTANCE":"T_WIN_HINSTANCE",
      "HWND":"T_WIN_hwnd",
      "HMONITOR":"T_WIN_HMONITOR",
      "HANDLE":"T_WIN_HANDLE",
      "SECURITY_ATTRIBUTES":"T_WIN_SECURITY_ATTRIBUTES",
      "DWORD":"T_WIN_DWORD",
      "LPCWSTR":"T_WIN_LPCWSTR"
    },
    "directfb":{
      "IDirectFB":"T_DFB_IDirectFB",
      "IDirectFBSurface":"T_DFB_IDirectFBSurface"
    },
    "xcb":{
      "xcb_connection_t":"T_XCB_connection",
      "xcb_visualid_t":"T_XCB_visualid",
      "xcb_window_t":"T_XCB_window"
    },
    "zirkon":{
      "zx_handle_t":"T_ZIRK_handle"
    },
    "googlegames":{
      "GgpStreamDescriptor":"T_GGP_StreamDescriptor",
      "GgpFrameToken":"T_GGP_FrameToken"
    },
    "metal":{
      "CAMetalLayer":"T_metal_CAMetalLayer",
      "MTLDevice_id":"T_metal_MTLDevice",
      "MTLCommandQueue_id":"T_metal_MTLCommandQueue",
      "MTLBuffer_id":"T_metal_MTLBuffer",
      "MTLTexture_id":"T_metal_MTLTexture",
      "MTLSharedEvent_id":"T_metal_MTLSharedEvent",
      "IOSurfaceRef":"T_metal_IOSurfaceRef"
    },
    "android":{
      "ANativeWindow":"T_android_ANativeWindow",
      "AHardwareBuffer":"T_android_AHardwareBuffer"
    },
    "screen":{
      "_screen_context":"T_screen_screen_context",
      "_screen_window":"T_screen_screen_window"
    }    
  };
  var tg = {
      "SYSC_int":"longint",
      "SYSC_uint":"dword",
      "SYSC_size_t":"SizeInt",
      "T_XLIB_Display":"record end",
      "T_XLIB_VisualID":"dword",
      "T_XLIB_Window":"dword",
      "T_XRANDR_RROutput":"record end",
      "T_WAYL_display":"record end",
      "T_WAYL_surface":"record end",
      "T_WIN_HINSTANCE":"record end",
      "T_WIN_hwnd":"record end",
      "T_WIN_HMONITOR":"record end",
      "T_WIN_HANDLE":"record end",
      "T_WIN_SECURITY_ATTRIBUTES":"record end",
      "T_WIN_DWORD":"dword",
      "T_WIN_LPCWSTR":"record end",
      "T_XCB_connection":"record end",
      "T_XCB_visualid":"record end",
      "T_XCB_window":"record end",
      "T_ZIRK_handle":"record end",
      "T_GGP_StreamDescriptor":"record end",
      "T_GGP_FrameToken":"record end",
      "T_metal_CAMetalLayer":"record end",
      "T_android_ANativeWindow":"record end",
      "T_android_AHardwareBuffer":"record end" 
  };
  o.typerewrite = {};
  register_pfs(tra,tg,o);
  
  o.paskeyword = { "TYPE":true, "OBJECT":true, "SET":true, "UNIT":true, "WINDOW":true };
  
  
  o.s+= make_pas_constants(o) +"\n";
  
  o.s+= make_pas_bitfields(o)+ "\n\ntype\n"; 
  
  o.d="";

  for (var i in o.enums)
  { if (o.enums[i].type=="enum")
    { var x=make_pas_enum(o.enums[i],o);
      o.s += x[0]+"\n";
      o.d += x[1];
    };
  };
  o.s+="\n\n";

  o.s+="PPint64=^Pint64;\nPPint32=^Pint32;\nPPint16=^Pint16;\nPPint8=^Pint8;\n";
  o.s+="PPuint64=^Pint64;\nPPuint32=^Puint32;\nPPuint16=^Puint16;\nPPuint8=^Puint8;\n";


//console.log("knowntype upto here: "+JSON.stringify(o.knowntypes));

  for (var i in o.types)
  { if ((!o.knowntypes[i])&&(o.types[i].category)&&(o.types[i].category=="basetype"))
    { make_pas_type(o.types[i],o);
      o.s += "\n";
    };
  };
  for (var i in o.types)
  { if ((o.types[i].category)&&(o.types[i].category=="handle"))
    { if (o.types[i].alias)
      { register_type(i,o.types[i].alias,o); 
      } else
      { if (o.types[i].args[0]=="VK_DEFINE_NON_DISPATCHABLE_HANDLE")
        { make_pas_vkhandle(i,o);
        } else {
          make_pas_dvkhandle(i,o);
        };
        make_auto(o.types[i],false,o)
      };
      o.s += "\n";
    };
  };  
  
  for (var i in o.types)
  { if ((o.types[i].category)&&(o.types[i].category=="bitmask"))
    { var rhs = o.types[i].args[0];
      if (!rhs) { rhs=o.types[i].alias; };
      if (!rhs) { rhs="VkFlags"; };
      registertype(i,o);
      o.s += i+"="+rhs+";\n"
      o.s += "P"+i+"=^"+i+";\n";
      o.s += "PP"+i+"=^P"+i+";\n\n";
    };
  };
    
  for (var i in o.enums)
  { if ((o.enums[i].type)&&(o.enums[i].type=="enum"))
    { registertype(i,o);
      o.s += "P"+i+"=^"+i+";\n";
      o.s += "PP"+i+"=^P"+i+";\n\n";
    };

    // some bitmasks aren't explicitly typed in vk.xml!!
    if ((o.enums[i].type)&&(o.enums[i].type=="bitmask"))
    { if (!o.tdb[i.toUpperCase()])
      { var rhs="VkFlags";
        registertype(i,o);
        o.s += i+"="+rhs+";\n"
        o.s += "P"+i+"=^"+i+";\n";
        o.s += "PP"+i+"=^P"+i+";\n\n";
      };
    };

  };  
  
  while (make_pas_records(o)>0) {};
  
  make_pas_funcpointers(o);
  
  while (make_pas_records(o)>0) {};
 


  make_pas_commands(o);

  o.d = "\n\nconst\n"+o.d;
  o.s += o.pvs_name;
  o.s += o.d;
  
  o.s+= "\n\nimplementation\n\n";

  o.s+="\nbegin\nend.\n";

  o.s = "{$mode objfpc}\n"+o.s;

  o.init = o.init_if + o.init_imp+o.init_imp2+"end.\n\n";
  o.test = o.test_c + o.test_p;
  o.auto = o.autoinc_if+"\n\n\n"+o.autoinc_imp;
  return o;
};

