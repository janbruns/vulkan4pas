

function getAttributesObj(n)
{ var o={};
  var m=n.getAttributeNames();
  for (var i=0; i<m.length; i+=1)
  { o[m[i]]=n.getAttribute(m[i]);
  };
  return o;
};

function getobjtxt(n)
{ var o={};
  o.text=n.textContent;
  n=n.firstChild;
  while (n)
  { if (n.nodeType==1)
    { o[n.nodeName] = n.textContent; //collect_node_text(n);
    };
    n=n.nextSibling;
  };
  return o;
};

function collect_node_text(n)
{ var s="";
  while (n)
  { if (n.nedeType==3)
    { s += n.textContent;
    };
    if (n.firstChild) { s+= collect_node_text(n.firstChild); };
    n=n.nextSibling;
  };
  return s;
};





function read_single_vk_xml_enum(n)
{ var o={};
  o.type=n.getAttribute("type");
  o.name=n.getAttribute("name");
  o.cmt =n.getAttribute("comment");
  o.entries=[];
  n=n.firstChild;
  while(n)
  { if ((n.nodeType==1)&&(n.nodeName=="enum"))
    { var v=getAttributesObj(n);
      if (v) { o.entries.push(v); }
      else { console.log("enum "+o.name+" was empty."); };
    };
    n=n.nextSibling;
  };
  return o;
};

function read_vk_xml_enums(reg)
{ var n=reg.firstChild;
  var enums={ "undefined":{"entries":[]} };
  var readenums=0;
  while (n)
  { if ((n.nodeType==1)&&(n.nodeName=="enums"))
    { var o=read_single_vk_xml_enum(n);
      if (o)
      { if (enums[o.name]) { console.log("redef of enum "+o.name); };
        enums[o.name]=o;
        readenums+=1;
      };
    };   
    n=n.nextSibling;
  };
  return enums;
};






function read_single_vk_xml_cmd(n)
{ var o=getAttributesObj(n);
  o.pl=[];
  n=n.firstChild;
  while (n)
  { if (n.nodeName=="proto")
    { o.pr=getobjtxt(n);
    };
    if (n.nodeName=="param")
    { var x=getobjtxt(n);
      o.pl.push(x);
    };
    n=n.nextSibling;
  };
  if ((!o.name)&&(o.pr)) { o.name=o.pr.name; };
  if (!o.name) { console.log("cmd proto without name"); };
  return o;
};

function read_vk_xml_cmds(reg)
{ var n=reg.firstChild;
  var cmds={};
  var readcmds=0;
  while ((n) && (n.nodeName!="commands"))
  { n=n.nextSibling;
  };
  if (n)
  { n=n.firstChild;
    while (n)
    { if ((n.nodeType==1)&&(n.nodeName=="command"))
      { var o=read_single_vk_xml_cmd(n);
        if (o)
        { if (cmds[o.name]) { console.log("redef of proto cmd "+o.name); };
          cmds[o.name]=o;
          readcmds+=1;
        };
      };   
      n=n.nextSibling;
    };
  } else { console.log("missing commands section"); };
  return cmds;
};

function read_member(n)
{ var o={"text":""};
  while(n)
  { if (n.nodeType==1)
    { if (!(n.nodeName=="comment"))
      { var s=n.textContent;
        o[n.nodeName]=s;
	o.text += s+" ";
      } else {
        o[n.nodeName]=n.textContent;
      };
    } else { o.text+= n.textContent; };
    n=n.nextSibling;
  };
  return o;
};

function read_single_vk_xml_type(n)
{ var o=getAttributesObj(n);

  if ((o.category=="struct")||(o.category=="union"))
  { o.members=[];
    n=n.firstChild;
    while (n)
    { if (n.nodeName=="member")
      { var x=getobjtxt(n);
        o.members.push(read_member(n.firstChild)); 
      };
      n=n.nextSibling;
    };
  }  
  else // if ((o.category=="funcpointer")||(o.category=="handle")||(o.category=="bitmask"))
  { o.args=[];
    o.text=n.textContent;
    n=n.firstChild;
    while (n)
    { if (n.nodeName=="name")
      { o.name=n.textContent;
      };
      if (n.nodeName=="type")
      { o.args.push(n.textContent); 
      };
      n=n.nextSibling;
    };
  };  
  
  if (!o.name) { console.log("type without name:"+n.textContent); };
  return o;
};

function read_vk_xml_types(reg)
{ var n=reg.firstChild;
  var types={};
  var readtypes=0;
  while ((n) && (n.nodeName!="types"))
  { n=n.nextSibling;
  };
  if (n)
  { n=n.firstChild;
    while (n)
    { if ((n.nodeType==1)&&(n.nodeName=="type"))
      { var o=read_single_vk_xml_type(n);
        if (o)
        { if (types[o.name]) { console.log("redef of type "+o.name); };
	  o.typeidx=readtypes;
          types[o.name]=o;
        };
        readtypes+=1;
      };   
      n=n.nextSibling;
    };
  } else { console.log("missing types section"); };
  return types;
};


function read_vk_xml_platforms(reg)
{ var n=reg.firstChild;
  var pf={};
  var readpf=0;
  while ((n) && (n.nodeName!="platforms"))
  { n=n.nextSibling;
  };
  if (n)
  { n=n.firstChild;
    while (n)
    { if ((n.nodeType==1)&&(n.nodeName=="platform"))
      { var o=getAttributesObj(n);
        pf[o.name]=o;
        readpf+=1;
      };   
      n=n.nextSibling;
    };
  } else { console.log("missing platforms section"); };
  return pf;
};


function do_vkn_require_type(n,fext,reqh,dat)
{ var a=getAttributesObj(n);
  if (!dat.exp_types[a.name]) { dat.exp_types[a.name] = {}; };
  dat.exp_types[a.name][fext.name] = true;
};

function do_vkn_require_enum(n,fext,reqh,dat)
{ var a=getAttributesObj(n);
  var exten = a["extends"];
  var ofs = false;
  if (a.offset) { ofs=parseInt(a.offset); };
  var extnumber = false;
  if (a.extnumber) { extnumber=parseInt(a.extnumber); } else {
    if (fext.number) { extnumber=parseInt(fext.number); };
  };
  //if (!exten) { console.log("exten undefed:"+JSON.stringify(a)); };

  if (a.bitpos)
  { dat.enums[exten].entries.push({name:a.name, bitpos:parseInt(a.bitpos), reqby:fext.name, alias:a.alias});
  } else {
    if (a.alias)
    { //console.log(exten+"#"+a.alias);
      dat.enums[exten].entries.push({name:a.name, reqby:fext.name, alias:a.alias});
    } else {
      if ((exten)&&(dat.enums[exten]))
      { var x=1000000000 +(extnumber-1)*1000 +ofs;
        if (a["value"]) { x=a["value"]; };
	if (a["dir"]=="-") { x=-x; };
        dat.enums[exten].entries.push({name:a.name, value:x, reqby:fext.name});
      };
    };
  };
};

function do_vkn_require_cmd(n,fext,reqh,dat)
{ var a=getAttributesObj(n);
//console.log("req cmd="+a.name+"@"+fext.name);
  if (!dat.exp_cmds[a.name]) { dat.exp_cmds[a.name] = {}; };
  dat.exp_cmds[a.name][fext.name] = true;
  if (dat.cmd[a.name]) 
  { if (!dat.cmd[a.name].reqby) { dat.cmd[a.name].reqby=[]; };
    if (dat.cmd[a.name].reqby.length>0) 
    { //console.log(a.name+" also required by "+fext.name);
    } else {dat.cmd[a.name].reqby.push(fext.name);};
  } else {
    throw("feature "+fext.name+" requires unknown command "+a.name);
  };
};


function do_vkn_require(n,fext,reqh,dat)
{ //console.log("req="+n)
  while (n)
  { //if (n.nodeType==1) { console.log("reqn:"+n.nodeName); };
    if ((n.nodeType==1)&&(n.nodeName=="type"))
    { do_vkn_require_type(n,fext,reqh,dat);
    };   
    if ((n.nodeType==1)&&(n.nodeName=="enum"))
    { do_vkn_require_enum(n,fext,reqh,dat);
    };
    if ((n.nodeType==1)&&(n.nodeName=="command"))
    { do_vkn_require_cmd(n,fext,reqh,dat);
    };    
    n=n.nextSibling;
  };
};

function read_vk_xml_feature(n,sekt)
{ var f=getAttributesObj(n);
  //console.log("readfeat:"+f.name);
  n=n.firstChild;
  while (n)
  { if ((n.nodeType==1)&&(n.nodeName=="require"))
    { var o=getAttributesObj(n);
      do_vkn_require(n.firstChild,f,o,sekt);
    };   
    n=n.nextSibling;
  };
};

function read_vk_xml_features(reg,sekt)
{ sekt.curmode_features=true;
  var n=reg.firstChild;
  while (n)
  { if ((n.nodeType==1)&&(n.nodeName=="feature"))
    { read_vk_xml_feature(n,sekt);
    };   
    n=n.nextSibling;
  };
  delete sekt.curmode_features;
};



function read_vk_xml_extensions(reg,sekt)
{ var n=reg.firstChild;
  while (n)
  { if ((n.nodeType==1)&&(n.nodeName=="extensions"))
    { var n2=n.firstChild;
      while (n2)
      { if ((n2.nodeType==1)&&(n2.nodeName=="extension"))
        {  read_vk_xml_feature(n2,sekt);
	  };
        n2=n2.nextSibling;
      };
    };   
    n=n.nextSibling;
  };
};

function read_vk_xml(doc)
{ var registry=doc.firstChild;
  if (registry.nodeName=="registry")
  { var o={};
    o.enums=read_vk_xml_enums(registry);
    o.cmd=read_vk_xml_cmds(registry);
    o.types=read_vk_xml_types(registry);
    o.platforms=read_vk_xml_platforms(registry);

    o.exp_enums = {};
    o.exp_types = {};
    o.exp_cmds  = {};

    read_vk_xml_features(registry,o);
    read_vk_xml_extensions(registry,o);
    return o;
    
  } else { console.log("maybe not vk.xml selected.");
  };
};

