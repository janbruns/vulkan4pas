# vulkan4pas

Vulkan headers for pascal and html-based header generator

## Getting started

- [ ] [documentation](./vulkan1.2/doc/index.html) 

## Generating fresh headers

The generator html can do most of the job of converting 

- [ ] [vk.xml](https://github.com/KhronosGroup/Vulkan-Docs/tree/main/xml) 

but there some manual work to do left over. This includes applying this little patch before (to remove a circular refernce, doing that in the parser is relatively complicated, because it already attempts to sort by reference), and it makes no sense to implement something for this pedantic demo/placeholder struct that noone really ever uses:
~~~
jan@rechner2a:~/Downloads$ diff -Naur vk_orig.xml vk.xml
--- vk_orig.xml 2022-07-05 14:13:53.264416791 +0200
+++ vk.xml      2022-07-05 15:01:59.464961052 +0200
@@ -865,11 +865,11 @@
             <comment>Struct types</comment>
         <type category="struct" name="VkBaseOutStructure">
             <member><type>VkStructureType</type> <name>sType</name></member>
-            <member optional="true">struct <type>VkBaseOutStructure</type>* <name>pNext</name></member>
+            <member optional="true">const <type>void</type>*     <name>pNext</name></member>
         </type>
         <type category="struct" name="VkBaseInStructure">
             <member><type>VkStructureType</type> <name>sType</name></member>
-            <member optional="true">const struct <type>VkBaseInStructure</type>* <name>pNext</name></member>
+            <member optional="true">const <type>void</type>*     <name>pNext</name></member>
         </type>
         <type category="struct" name="VkOffset2D">
             <member><type>int32_t</type>        <name>x</name></member>
~~~
